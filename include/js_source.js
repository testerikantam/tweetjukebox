// JavaScript Document
// http://www.jsobfuscate.com/index.php
// important - any changes here - you need to recompile - refer to js_min.js   BEST to put this in place for testing then recompile to js_min

function tweetFromJukebox(tweet_text, rowID) {
	//hideDivs();	
	var msg = decodeURIComponent((tweet_text+'').replace(/\+/g, '%20'));	
	$.ajax({
  		type: "post",
  		url: "/members/sendTweet.php",
		data: {'message': msg, 'rowID': rowID, 'opt': '1'} ,
		dataType: "html"
	})
  	.done(function( msg ) { 
		$('#msg').val(''); 
		$('#messageCount').html(140);			
		var tweetDate = '#tweetDate_' + String(rowID);
		$(tweetDate).text('Just Tweeted') ;
  	});	
}

function tweetScheduledTweetNow(tweet_text, rowID) {
	//hideDivs();	
	var msg = decodeURIComponent((tweet_text+'').replace(/\+/g, '%20'));	
	$.ajax({
  		type: "post",
  		url: "/members/sendScheduledTweet.php",
		data: {'message': msg, 'rowID': rowID} ,
		dataType: "html"
	})
  	.done(function( msg ) { 
		$('#msg').val(''); 
		$('#messageCount').html(140);			
		response = msg.split(",");				
		$('#statuses_count').html(response[0]);
		$('#followers_count').html(response[1]);
		$('#friends_count').html(response[2]);
		var tweetDate = '#tweetDate_' + String(rowID);
		$(tweetDate).text('Just Tweeted') ;
  	});	
}

function scheduledList() {
	hideDivs();
	$('#masterDiv').css('display', 'block');	
	viewScheduledList();
}

function viewScheduledList() {		
	$('#masterDiv').html('<div align="center">Please wait while we get that for you....<p><img src="../images/wait.GIF" border="0"></p></div>');	
	$.ajax({
  		type: "get",
  		url: "/members/getScheduledTweetsBoot.php",
		//data: {'message': msg} ,
		dataType: "html"
	})
  	.done(function( msg ) { 
		$('#masterDiv').html(msg);
  	});	
}

function masterList(x) {  
	hideDivs();
	if(x==0) x=$('#upload_list_number').val();
	$('#masterDiv').css('display', 'block');	
	viewMasterList(x);
}

function masterListDEBUG(x) {  
	hideDivs();
	$('#masterDiv').css('display', 'block');	
	viewMasterListDEBUG(x);
}

function setupSchedule(x) {  
	hideDivs();	
	$('#masterDiv').html('');
	$.ajax({  		
		type: "get",
  		url: "/members/getScheduleBoot.php?list_number="+x,
		dataType: "html"
	})
  	.done(function( msg ) { 			   
		for(row=1; row<8; row++) {
			$('#start_1'+ String(row)).prop('disabled', true);
			$('#end_1'+ String(row)).prop('disabled', true);
			$('#frequency_1'+ String(row)).prop('disabled', true);	
			$('#start_1'+ String(row)).removeClass('start_1_on');
			$('#end_1'+ String(row)).removeClass('end_1_on');
			$('#frequency_1'+ String(row)).removeClass('frequency_1_on');
			$('#frequency_hours_1'+ String(row)).removeClass('frequency_hours_1_on');
			$('#frequency_minutes_1'+ String(row)).removeClass('frequency_minutes_1_on');	
		}
		$('#monDay').css('background-color', '#dda100');
		$('#tueDay').css('background-color', '#dda100');
		$('#wedDay').css('background-color', '#dda100');
		$('#thurDay').css('background-color', '#dda100');
		$('#friDay').css('background-color', '#dda100');
		$('#satDay').css('background-color', '#dda100');
		$('#sunDay').css('background-color', '#dda100');				   
//		$('#JukeboxNumber').html(x);						
		$('#masterDiv').html(msg);
		$('#list_name').val( $('#current_list_name').val() );
		$('#masterDiv').css('display', 'block');	
  	});
}

function addJukeBox() {
	$.ajax({
  		type: "get",
  		url: "/members/addJukeBox.php",
		dataType: "html"
	})
  	.done(function( msg ) { 
		location.assign("/members/");
  	});	
}

function makeJBCopy(list) {
	$.ajax({
  		type: "post",
  		url: "/members/addJukeBox.php?list="+list,
		dataType: "html"
	})
  	.done(function( msg ) { 
		location.assign("/members/");
  	});	
}



function deleteJukeBox(jb) {
	$.ajax({
  		type: "get",
  		url: "/members/deleteJukeBox.php?jb="+jb,
		dataType: "html"
	})
  	.done(function( msg ) { 
		//alert(msg);		   
		location.reload();
  	});	
}

function sayThanks() {
	hideDivs();	
	$('#masterDiv').html('');		
	$.ajax({
  		type: "get",
  		url: "/members/thanks.php",
		dataType: "html"
	})
  	.done(function( msg ) { 
		$('#masterDiv').html('<script>$(function() {$( "#thanks_date_start" ).datepicker();});$(function() {$( "#thanks_date_end" ).datepicker();});</script>' + msg);
		$('#masterDiv').css('display', 'block');
  	});
}

function thanksSetup() {
	hideDivs();	
	$('#masterDiv').html('');		
	$.ajax({
  		type: "get",
  		url: "/members/thanksSetupBoot.php",
		dataType: "html"
	})
  	.done(function( msg ) { 
		$('#masterDiv').html(msg);
		$('#masterDiv').css('display', 'block');
  	});												
}						

function recentMentions() {
	hideDivs();
	$('#recentMentionsDiv').css('display', 'block');
	$('#recentMentionsDiv').html('<div align="center">Please wait while we get that for you....<p><img src="../images/wait.GIF" border="0"></p></div>');
	$.ajax({
  		type: "post",
  		url: "/members/recentMentionsBoot.php",
		dataType: "html"
	})
  	.done(function( msg ) { 
		$('#recentMentionsDiv').html(msg);
		$('#recentMentionsDiv').css('display', 'block');
  	});
	//$('#recentMentionsDiv').css('display', 'block');
}

function recentMentionsCharted(range) {
	hideDivs();
	$('#recentMentionsDiv').css('display', 'block');
	$('#recentMentionsDiv').html('<div align="center">Please wait while we get that for you....<p><img src="../images/wait.GIF" border="0"></p></div>');
	$.ajax({
  		type: "post",
  		url: "/members/recentMentionsChartedBoot.php",
		data: {'range': range} ,
		dataType: "html"
	})
  	.done(function( msg ) { 
		$('#recentMentionsDiv').html(msg);
		//$('#recentMentionsDiv').css('display', 'block');
  	});
	//$('#recentMentionsDiv').css('display', 'block');
}

function recentMentionsUniqueUsers(range) {
	hideDivs();
	$('#recentMentionsDiv').css('display', 'block');
	$('#recentMentionsDiv').html('<div align="center">Please wait while we get that for you....<p><img src="../images/wait.GIF" border="0"></p></div>');
	$.ajax({
  		type: "post",
  		url: "/members/recentMentionsChartedBoot.php",
		data: {'range': range, 'type': 'users'} ,
		dataType: "html"
	})
  	.done(function( msg ) { 
		$('#recentMentionsDiv').html(msg);
		//$('#recentMentionsDiv').css('display', 'block');
  	});
	$('#recentMentionsDiv').css('display', 'block');
}

function recentRetweets() {
	hideDivs();
	$('#recentMentionsDiv').css('display', 'block');
	$('#recentMentionsDiv').html('<div align="center">Please wait while we get that for you....<p><img src="../images/wait.GIF" border="0"></p></div>');
	$.ajax({
  		type: "post",
  		url: "/members/recentRetweetsBoot.php",
		dataType: "html"
	})
  	.done(function( msg ) { 
		$('#recentMentionsDiv').html(msg);
		$('#recentMentionsDiv').css('display', 'block');
  	});
	$('#recentMentionsDiv').css('display', 'block');
}

function followersList() {
	hideDivs();
	$('#recentMentionsDiv').css('display', 'block');
	$('#recentMentionsDiv').html('<div align="center">Please wait while we get that for you....<p><img src="../images/wait.GIF" border="0"></p></div>');
	$.ajax({
  		type: "post",
  		url: "/members/followersListBoot.php",
		dataType: "html"
	})
  	.done(function( msg ) { 
		$('#recentMentionsDiv').html(msg);
		$('#recentMentionsDiv').css('display', 'block');
  	});
	$('#recentMentionsDiv').css('display', 'block');
}

function viewAll(id) {
	hideDivs();
	$('#recentMentionsDiv').css('display', 'block');
	$('#recentMentionsDiv').html('<div align="center">Please wait while we get that for you....<p><img src="../images/wait.GIF" border="0"></p></div>');
	$.ajax({
  		type: "get",
  		url: "/members/viewAllBoot.php",
		data: {'id': id} ,
		dataType: "html"
	})
  	.done(function( msg ) { 
		$('#recentMentionsDiv').html(msg);
		$('#recentMentionsDiv').css('display', 'block');
  	});
	$('#recentMentionsDiv').css('display', 'block');
}

function uploadTweets(x) {
	hideDivs(); 
	$('#upload_list_number').val(x);
	$('#uploadH2').html('Upload Tweets');
	$('#uploadDiv').css('display', 'block');	
}

function searchNow() {
	hideDivs();
	$('#searchDiv').css('display', 'block');
}

function hideDivs() {
	$('#masterDiv').css('display', 'none');
	$('#recentMentionsDiv').css('display', 'none');
	$('#uploadDiv').css('display', 'none');	
	$('#searchDiv').css('display', 'none');
	$('#scheduleDiv').css('display', 'none');
	window.scrollTo(0,0);
}

function tweetTest(msg) {
	alert("No tests are set up right now.");
}

function editMasterRow(rowID) {
	// get the jquerry references	
	//$('#tweetRowType').html('Add New Tweet');
	if(rowID == '0') {
		$('#tweet_text').val( '' );
		$('#tweet_category').val( '' );
		$('#tweet_author').val( '' );
		$('#tweet_tags').val( '' );
		$('#editTweetRowID').val( '' );
		$('#editMessageCount').html('140');
	} else {
		var txt = '#text_' + String(rowID);		
		var category = '#category_' + String(rowID);		
		var author = '#author_' + String(rowID);		
		var tags = '#tags_' + String(rowID);
		var tweet_photo = '#currentTweetPhoto_' + String(rowID);
		$('#tweet_text').val( $(txt).text() );
		$('#tweet_category').val( $(category).text() );
		$('#tweet_author').val( $(author).text() );
		$('#tweet_tags').val( $(tags).text() );
		$('#tweetPhoto').attr('src', $(tweet_photo).attr('src') );
		$('#editTweetRowID').val( rowID );
		var c = 140 - $(txt).text().length;
		if($('#tweetPhoto').attr('src') ) {
			c = c -23;
			$('#removePhotoLabel').html("Click to remove this photo: ");
			$('#removePhoto').css('visibility', 'visible');
			$('#removePhoto').prop('checked', false);
		}
		$('#editMessageCount').html(c);
	}
}

function confirmDelete(rowArray, list) {		
	if (confirm("Are you sure you want to delete these?") == true) {
	$.ajax({
  		type: "post",
  		url: "/members/getMasterListBoot.php",
		data: {'delete': rowArray} ,
		dataType: "html"
	})
  	.done(function( msg ) { 
		viewMasterList(list);
		//alert(msg);
		//var rid = '#' + String(rowID);
		//$(rid).css('display', 'none');
		//rid = '#hr_' + String(rowID);
		//$(rid).css('display', 'none');		
  	});		
	}
}

function confirmMultiCopy(rowArray, list, toList) {
	if (confirm("Are you sure you want to copy these?") == true) {
	$.ajax({
  		type: "post",
  		url: "/members/getMasterListBoot.php",
		data: {'copy': rowArray, 'toList': toList} ,
		dataType: "html"
	})
  	.done(function( msg ) { 
		viewMasterList(list);
		//alert(msg);
		//var rid = '#' + String(rowID);
		//$(rid).css('display', 'none');
		//rid = '#hr_' + String(rowID);
		//$(rid).css('display', 'none');		
  	});		
	}			
}

function confirmMultiMove(rowArray, list, toList) {
	if (confirm("Are you sure you want to move these?") == true) {
	$.ajax({
  		type: "post",
  		url: "/members/getMasterListBoot.php",
		data: {'move': rowArray, 'toList': toList} ,
		dataType: "html"
	})
  	.done(function( msg ) { 
		viewMasterList(list);
		//alert(msg);
		//var rid = '#' + String(rowID);
		//$(rid).css('display', 'none');
		//rid = '#hr_' + String(rowID);
		//$(rid).css('display', 'none');		
  	});		
	}			
}


function confirmScheduledTweetDelete(rowID) {
	if (confirm("Are you sure you want to delete this scheduled tweet?") == true) {
	$.ajax({
  		type: "post",
  		url: "/members/getScheduledTweetsBoot.php",
		data: {'delete': rowID} ,
		dataType: "html"
	})
  	.done(function( msg ) { 
		scheduledList();
		//var rid = '#' + String(rowID);
		//$(rid).css('display', 'none');
  	});		
	}
}

function viewMasterList(x) {		
	$('#masterDiv').html('<div align="center">Please wait while we get that for you....<p><img src="../images/wait.GIF" border="0"></p></div>');	
	$.ajax({
  		type: "get",
  		url: "/members/getMasterListBoot.php?s=tweet_text&list_number="+x,
		//data: {'message': msg} ,
		dataType: "html"
	})
  	.done(function( msg ) { 
		$('#masterDiv').html(msg);
  	});	
}

function viewMasterListDEBUG(x) {		
	$('#masterDiv').html('<div align="center">Please wait while we get that for you....<p><img src="../images/wait.GIF" border="0"></p></div>');	
	$.ajax({
  		type: "get",
  		url: "/members/getMasterListDEBUG.php?s=tweet_text&list_number="+x,
		//data: {'message': msg} ,
		dataType: "html"
	})
  	.done(function( msg ) { 
		$('#masterDiv').html(msg);
  	});	
}

function getNextMaster(vars) {
	$('#masterDiv').html('<div align="center">Please wait while we get that for you....<p><img src="../images/wait.GIF" border="0"></p></div>');
	$.ajax({
  		type: "get",
  		url: "/members/getMasterListBoot.php" + vars,
		//data: {'message': msg} ,
		dataType: "html"
	})
  	.done(function( msg ) { 
		$('#masterDiv').html(msg);
  	});	
}

function getNextScheduled(vars) {
	$('#masterDiv').html('<div align="center">Please wait while we get that for you....<p><img src="../images/wait.GIF" border="0"></p></div>');
	$.ajax({
  		type: "get",
  		url: "/members/getScheduledTweetsBoot.php" + vars,
		//data: {'message': msg} ,
		dataType: "html"
	})
  	.done(function( msg ) { 
		$('#masterDiv').html(msg);
  	});	
}

function switchSchedule(state, x) {
	$.ajax({
  		type: "get",
  		url: "/members/switchSchedule.php",
		data: {'state': state, 'list_number': x} ,
		dataType: "html"
	})	
}

function switchScheduleTweets(state) {
	$.ajax({
  		type: "get",
  		url: "/members/switchSchedule.php",
		data: {'state': state, 'list_number': '0'} ,
		dataType: "html"
	})	
	.done(function( msg ) { 		
  	});			   
}

function switchAllTweets(state) {
	$.ajax({
  		type: "get",
  		url: "/members/switchSchedule.php",
		data: {'state': state, 'list_number': 'x'} ,
		dataType: "html"
	})	
	.done(function( msg ) { 		
  	});			   
}

function searchTwitter() {
	var searchTerm = $('#searchTerm').val();
	$.ajax({
  		type: "post",
  		url: "/members/twitterSearch.php",
		data: {'searchTerm': searchTerm} ,
		dataType: "html"
	})
  	.done(function( msg ) { 
		$('#searchTerm').val(''); 				
		$('#searchResultsDiv').html(msg);
  	});	
}

function showCharCount(msg) {
	var c = 140 - msg.length;
	if(c < 1 ) { 
		c = 0;
		var m = msg.substring(0,140);
		$('#msg').val(m);
		$('#messageCount').html(c);			
	} else {
		$('#messageCount').html(c);
	}	
}

function showEditCharCount(msg, photo) {
	var c = 140 - msg.length;
	if(photo > '') c = 140 - 23 - msg.length;
	if(c < 1 ) { 
		c = 0;
		var m = msg.substring(0,140);
		$('#msg').val(m);
		$('#editMessageCount').html(c);
		$('#tweet_text').val(m);
	} else {
		$('#editMessageCount').html(c);
	}	
}

function showEditCharCount2(msg) {
	var c = 140 - msg.length;
	if(c < 1 ) { 
		c = 0;
		var m = msg.substring(0,140);
		$('#msg').val(m);
		$('#editMessageCount2').html(c);	
		$('#scheduled_tweet_text').val(m);
	} else {
		$('#editMessageCount2').html(c);
	}	
}

function dayClicked(d, row) {
	var rgb = $(d).css('background-color');
	bgc = (rgb2hex(rgb)) ;
	if(bgc == '#dda100') {
		$(d).css('background-color', '#00CC33');
		$('#start_1'+ String(row)).prop('disabled', false);
		$('#end_1'+ String(row)).prop('disabled', false);
		$('#frequency_1'+ String(row)).prop('disabled', false);
		$('#frequency_hours_1'+ String(row)).prop('disabled', false);
		$('#frequency_minutes_1'+ String(row)).prop('disabled', false);
		// class stuff here
		$('#start_1'+ String(row)).addClass('start_1_on');
		$('#end_1'+ String(row)).addClass('end_1_on');
		$('#frequency_1'+ String(row)).addClass('frequency_1_on');
		$('#frequency_hours_1'+ String(row)).addClass('frequency_hours_1_on');
		$('#frequency_minutes_1'+ String(row)).addClass('frequency_minutes_1_on');
	} else {
		$(d).css('background-color', '#dda100');
		// save this rows stuff 
		formData = $( "#scheduleForm" ).serializeArray();
//		alert($('#list_number').val());
		$.ajax({
			type: "post",
			url: "/members/saveSchedule.php",
			data: {'data': formData },
			dataType: "html"
		})
		.done(function( msg ) { 					
			$('#start_1'+ String(row)).prop('disabled', true);
			$('#end_1'+ String(row)).prop('disabled', true);

			$('#frequency_1'+ String(row)).prop('disabled', true); 
			$('#frequency_hours_1'+ String(row)).prop('disabled', true); 
			$('#frequency_minutes_1'+ String(row)).prop('disabled', true); 
			// class stuff here
			$('#start_1'+ String(row)).removeClass('start_1_on');
			$('#end_1'+ String(row)).removeClass('end_1_on');
			$('#frequency_1'+ String(row)).removeClass('frequency_1_on');
			$('#frequency_hours_1'+ String(row)).removeClass('frequency_hours_1_on');
			$('#frequency_minutes_1'+ String(row)).removeClass('frequency_minutes_1_on');								
	   });
	}
}

function setRetweetDays(d, l) {
	$.ajax({
		type: "post",
		url: "/members/setRetweetDays.php",
		data: {'days': d, 'listID': l } ,
		dataType: "html"
	})	
}

function myAccount() {
	hideDivs();
	$.ajax({
 		type: "get",
  		url: "/members/myAccountBoot.php",
		dataType: "html"
	})
  	.done(function( msg ) {
		$('#masterDiv').html(msg);
		$('#masterDiv').css('display', 'block');
  	});	
}

function jbOptions(x) {
	hideDivs();
	$.ajax({
  		type: "get",
  		url: "/members/jbOptions.php?list_number="+x,
		dataType: "html"
	})
  	.done(function( msg ) {
		$('#masterDiv').html(msg);
		$('#masterDiv').css('display', 'block');
		$('#list_name').focus();
		$('#stopDate').datepicker();
  	});	
}

function disableAllDays() {			
	for(row=1; row<8; row++) {
		$('#frequency_hours_1'+String(row)).prop('disabled', false);
		$('#frequency_minutes_1'+String(row)).prop('disabled', false);
		$('#start_1'+ String(row)).prop('disabled', false);
		$('#end_1'+ String(row)).prop('disabled', false);
		$('#frequency_1'+ String(row)).prop('disabled', false);			
	}
	formData = $( "#scheduleForm" ).serializeArray();
	var x = $('#list_number').val() ;		
	$.ajax({
		type: "post",
		url: "/members/saveSchedule.php",
		data: {'data': formData },
		dataType: "html"
	})
	.done(function( msg ) { 			   
		for(row=1; row<8; row++) {
			$('#start_1'+ String(row)).prop('disabled', true);
			$('#end_1'+ String(row)).prop('disabled', true);
			$('#frequency_1'+ String(row)).prop('disabled', true);	
			$('#start_1'+ String(row)).removeClass('start_1_on');
			$('#end_1'+ String(row)).removeClass('end_1_on');
			$('#frequency_1'+ String(row)).removeClass('frequency_1_on');
			$('#frequency_hours_1'+ String(row)).removeClass('frequency_hours_1_on');
			$('#frequency_minutes_1'+ String(row)).removeClass('frequency_minutes_1_on');	
		}
		$('#monDay').css('background-color', '#dda100');
		$('#tueDay').css('background-color', '#dda100');
		$('#wedDay').css('background-color', '#dda100');
		$('#thurDay').css('background-color', '#dda100');
		$('#friDay').css('background-color', '#dda100');
		$('#satDay').css('background-color', '#dda100');
		$('#sunDay').css('background-color', '#dda100');
	});					
	$('#masterDiv').css('display', 'block');
	$('#masterDiv').html('<div align="center" style="padding-top:300px;">Your schedule is being saved....<p><img src="/images/wait.GIF" class="img-responsive" border="0"></p></div>');	
	setTimeout(function (){					
		setupSchedule(x);
	}, 2000);
}

function rgb2hex(rgb) {
     if (  rgb.search("rgb") == -1 ) {
          return rgb;
     } else {
          rgb = rgb.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+))?\)$/);
          function hex(x) {
               return ("0" + parseInt(x).toString(16)).slice(-2);
          }
          return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]); 
     }
}

function adjustFrequency(row) {
	var hours = parseInt($('#frequency_hours_1'+row).val());
	//alert(hours);
	var minutes = parseInt($('#frequency_minutes_1'+row).val());
	//alert(minutes);
	var totalMinutes = parseInt(hours) * 60 + parseInt(minutes);
	//alert(totalMinutes);
	//$('#frequency_1'+row).val(totalMinutes);
	$('.frequency_1_on').val(totalMinutes);
	$('.frequency_minutes_1_on').val(minutes);
	$('.frequency_hours_1_on').val(hours);
}

function savedRepeatDays() {		
	$("#savedRepeatDaysMsg").fadeTo(0, 3000);
	$("#savedRepeatDaysMsg").html("Setting Saved");
	$("#savedRepeatDaysMsg").fadeTo(3000, 0);	
}

function saveJBOptions() {	
	formData = $( "#jbOptionsForm" ).serializeArray();				
	$.ajax({
		type: "post",
		url: "/members/saveJBOptions.php",
		data: {'data': formData },
		dataType: "html"
	})
	.done(function( msg ) { 
		jbOptions(msg);
		setTimeout(function () {
		   $('#jbOptionsMsg').fadeTo(0, 2000);
		   $('#jbOptionsMsg').html('Settings Were Saved');
		   $('#jbOptionsMsg').fadeTo(2000, 0);		   
		}, 200); 				   
	});	
}

function bonusJB(x) {		
	if(x == '1') {	
		$('#masterDiv').html('<div align="center">Please wait while we get your bonus content....<p><img src="../images/wait.GIF" border="0"></p></div>');	
	}
	$.ajax({
  		type: "get",
  		url: "/members/buildBonusJB.php?x="+x,
		//data: {'message': msg} ,
		dataType: "html"
	})
  	.done(function( list_id ) { 
		if(list_id) {
			//masterList(list_id)
			location.reload();
		} else {
			$('#masterDiv').html('<center><img src="/images/tweetjukebox3.png" class="img-responsive" style="padding-top:50px;" ></center>');		
		}
  	});	
}

function settingsForm() {
	$.ajax({
  		type: "get",
  		url: "/members/settingsForm.php",
		dataType: "html"
	})
  	.done(function( msg ) { 
		$('#masterDiv').html(msg);		
  	});		
}

function jbDailyContent () {
	$.ajax({
  		type: "get",
  		url: "/members/jbDailyContent.php",
		dataType: "html"
	})
  	.done(function( msg ) { 
		$('#masterDiv').html(msg);		
  	});			
}