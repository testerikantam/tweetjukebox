<? 
$expireTime = 60*60*24*2; // 2 days
session_set_cookie_params($expireTime);
ini_set('session.gc_maxlifetime', $expireTime);
session_start();

header('Content-Type: text/html; charset=utf-8');

$serverName = explode(".", $_SERVER['HTTP_HOST']);
if(strtolower($serverName[0]) != "www") header ("Location: http://tj.local");


@define(TRIAL_PERIOD, "45");

@define(FREE_VERSION_MONTHLY_FEE, "Free");
@define(BASIC_VERSION_MONTHLY_FEE, "12.99");
@define(PRO_VERSION_MONTHLY_FEE, "24.99");
@define(BUSINESS_VERSION_MONTHLY_FEE, "49.99");

@define(FREE_VERSION_ANNUAL_FEE, "0.00");
@define(BASIC_VERSION_ANNUAL_FEE, "119.88");
@define(PRO_VERSION_ANNUAL_FEE, "239.88");
@define(BUSINESS_VERSION_ANNUAL_FEE, "479.88");

@define(FREE_VERSION_JB_BOX_LIMIT, "2");
@define(BASIC_VERSION_JB_BOX_LIMIT, "10");
@define(PRO_VERSION_JB_BOX_LIMIT, "50");
@define(BUSINESS_VERSION_JB_BOX_LIMIT, "100");

@define(FREE_VERSION_JB_TWEET_LIMIT, "300");
@define(BASIC_VERSION_JB_TWEET_LIMIT, "5000");
@define(PRO_VERSION_JB_TWEET_LIMIT, "10000");
@define(BUSINESS_VERSION_JB_TWEET_LIMIT, "100000");

@define(FREE_VERSION_SCHEDULED_TWEET_LIMIT, "1");
@define(BASIC_VERSION_SCHEDULED_TWEET_LIMIT, "10");
@define(PRO_VERSION_SCHEDULED_TWEET_LIMIT, "100");
@define(BUSINESS_VERSION_SCHEDULED_TWEET_LIMIT, "500");

@define(FREE_VERSION_ACCOUNTS_LIMIT, "1");
@define(BASIC_VERSION_ACCOUNTS_LIMIT, "3");
@define(PRO_VERSION_ACCOUNTS_LIMIT, "10");
@define(BUSINESS_VERSION_ACCOUNTS_LIMIT, "100");

@define(FREE_VERSION_THANK_YOU_TWEETS, "50");
@define(BASIC_VERSION_THANK_YOU_TWEETS, "100");  	// WEEKLY ON FRIDAYS 
@define(PRO_VERSION_THANK_YOU_TWEETS, "100");			// DAYS OF THE WEEK USER CHOOSES IE MONDAY AND THURSDAY OR EVERY DAY , ETC.
@define(BUSINESS_VERSION_THANK_YOU_TWEETS, "200");		// DAYS OF THE WEEK USER CHOOSES IE MONDAY AND THURSDAY OR EVERY DAY , ETC.

/*

@define(BASIC_PLAN_BOX_LIMIT, "100");
@define(SILVER_PLAN_BOX_LIMIT, "999999999");
@define(GOLD_PLAN_BOX_LIMIT, "999999999");

@define(BASIC_PLAN_SCHEDULE_LIMIT, "50");
@define(SILVER_PLAN_SCHEDULE_LIMIT, "100");
@define(GOLD_PLAN_SCHEDULE_LIMIT, "999999999");
*/


@define(SITE_EMAIL, "info@TweetJukebox.com");
@define(SITE_NAME, "TweetJukebox");


// The following should not be edited
global $conn;
$conn = mysqli_connect ("jukebox072015.cwnmecnpl5sq.us-east-1.rds.amazonaws.com", "jukeuser", "E073xKA0&El4", "tweetoca_twitter") or die ("Could Not Connect To DB");
mysqli_set_charset($conn, "utf8"); 
if (!$conn)
  {
  die('Could not connect: ' . mysqli_error());
  }


@define(SITE_URL, "tj.local");
@define(DOMAIN, "TweetJukebox");



$doc_name = $_SERVER['PHP_SELF']; 
if (strpos($doc_name, "admin/") > 0 && $_SESSION['isAdmin'] != 1  ) { // get out of here
	//echo $_SESSION['userID'];
	header("Location: /");
}
?>
<?
function getPagingQuery($sql, $itemPerPage)
{
	if (isset($_GET['pageNum']) && (int)$_GET['pageNum'] > 0) {
		$pageNum = (int)$_GET['pageNum'];
	} else {
		$pageNum = 1;
	}
	
	// start fetching from this row number
	$offset = ($pageNum - 1) * $itemPerPage;
	
	return $sql . " LIMIT $offset, $itemPerPage ";
}

function getPagingLink($sql, $itemPerPage = 10, $queryString, $ajax = false, $sortBy = '', $source='')
{
	$conn = mysqli_connect ("jukebox072015.cwnmecnpl5sq.us-east-1.rds.amazonaws.com", "jukeuser", "E073xKA0&El4", "tweetoca_twitter") or die ("Could Not Connect To DB");
	mysqli_set_charset($conn, "utf8"); 
	$result        = mysqli_query($conn, $sql);
	$pagingLink    = '';
	$totalResults  = mysqli_num_rows($result);
	$totalPages    = ceil($totalResults / $itemPerPage);
	
	if($totalPages <= 1) return;
	
	$queryParts = explode("&", $queryString);
	$queryString = '';
	
	foreach($queryParts as $variable) {
		if(!strpos($variable, "ageNum=")) {
			$queryString .= $variable."&";
		}		
	}
	$queryString = rtrim( $queryString, "&" );
	
	//$queryString = str_replace("&pageNum=", "&prevNum=", $queryString);
	
	// how many link pages to show
	$numLinks      = 10;
	
		
	// create the paging links only if we have more than one page of results
	if ($totalPages > 1 && $ajax == false) {
	
		$self = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'] ;
		

		if (isset($_GET['pageNum']) && (int)$_GET['pageNum'] > 0) {
			$pageNumber = (int)$_GET['pageNum'];
		} else {
			$pageNumber = 1;
		}		
		// print 'previous' link only if we're not
		// on page one
		if ($pageNumber > 1) {
			$pageNum = $pageNumber - 1;
			//if ($pageNum == 1) {
				$prev = " <a href=\"$self?".$queryString."&pageNum=$pageNum\">[Prev]</a> ";
			//}	
				
			$first = " <a  href=\"$self?".$queryString."\">[First]</a> ";
		} else {
			$prev  = ''; // we're on page one, don't show 'previous' link
			$first = ''; // nor 'first page' link
		}
	
		// print 'next' link only if we're not
		// on the last page
		if ($pageNumber < $totalPages) {
			$page = $pageNumber + 1;
			$next = " <a href=\"$self?".$queryString."&pageNum=$page\">[Next]</a> ";
			$last = " <a href=\"$self?".$queryString."&pageNum=$totalPages\">[Last]</a> ";
		} else {
			$next = ''; // we're on the last page, don't show 'next' link
			$last = ''; // nor 'last page' link
		}

		$start = $pageNumber - ($pageNumber % $numLinks) + 1;				
		
		if($start >= $totalPages) $start = $pageNumber -9;
						
		$end   = $start + $numLinks - 1;		
		
		$end   = min($totalPages, $end);
				
		
		$pagingLink = array();
	
		for($page = $start; $page <= $end; $page++)	{
			if ($page == $pageNumber) {
				$pagingLink[] = " <a style='color:red;'>$page</a> ";   // no need to create a link to current page
			} else {
				if ($page == 1) {
					$pagingLink[] = " <a href=\"$self?".$queryString."\">$page</a> ";
				} else {	
					$pagingLink[] = " <a href=\"$self?pageNum=$page&".$queryString."\">$page</a> ";
				}	
			}
	
		}
				
		$pagingLink = implode(' | ', $pagingLink);
		
		// return the page navigation link
		$pagingLink = $first . $prev . $pagingLink . $next . $last;
	} else {
		//                                    A J A X   AND  J
		if (isset($_GET['pageNum']) && (int)$_GET['pageNum'] > 0) {
			$pageNumber = (int)$_GET['pageNum'];
		} else {
			$pageNumber = 1;
		}		
		// print 'previous' link only if we're not
		// on page one
		if ($pageNumber > 1) {
			$pageNum = $pageNumber - 1;
			
				if($source == 'master') {
					$prev = " <a href=\"#\" onclick=\"getNextMaster('?pageNum=$pageNum&s=$sortBy')\">[Prev]</a> ";
					$first = " <a href=\"#\" onclick=\"getNextMaster('?s=$sortBy')\">[First]</a> ";
				} else {
					if($source == 'pic_master') {
						$prev = " <a href=\"#\" onclick=\"getNextPicMaster('?pageNum=$pageNum&s=$sortBy')\">[Prev]</a> ";
						$first = " <a href=\"#\" onclick=\"getNextPicMaster('?s=$sortBy')\">[First]</a> ";
					
					} else {					
						$prev = " <a href=\"#\" onclick=\"getNextScheduled('?pageNum=$pageNum&s=$sortBy')\">[Prev]</a> ";
						$first = " <a href=\"#\" onclick=\"getNextScheduled('?s=$sortBy')\">[First]</a> ";
					}	
				}	
			
				
			
		} else {
			$prev  = ''; // we're on page one, don't show 'previous' link
			$first = ''; // nor 'first page' link
		}
	
		// print 'next' link only if we're not
		// on the last page
		if ($pageNumber < $totalPages) {
			$page = $pageNumber + 1;
			if($source == 'master') {
				$next = " <a href=\"#\" onclick=\"getNextMaster('?pageNum=$page&s=$sortBy')\">[Next]</a> ";
				$last = " <a href=\"#\" onclick=\"getNextMaster('?pageNum=$totalPages&s=$sortBy')\">[Last]</a> ";
			} else {
				if($source == 'pic_master') {
					$next = " <a href=\"#\" onclick=\"getNextPicMaster('?pageNum=$page&s=$sortBy')\">[Next]</a> ";
					$last = " <a href=\"#\" onclick=\"getNextPicMaster('?pageNum=$totalPages&s=$sortBy')\">[Last]</a> ";
				} else {
					$next = " <a href=\"#\" onclick=\"getNextScheduled('?pageNum=$page&s=$sortBy')\">[Next]</a> ";
					$last = " <a href=\"#\" onclick=\"getNextScheduled('?pageNum=$totalPages&s=$sortBy')\">[Last]</a> ";
				}	
			}	
		} else {
			$next = ''; // we're on the last page, don't show 'next' link
			$last = ''; // nor 'last page' link
		}

		$start = $pageNumber - ($pageNumber % $numLinks) + 1;				
		
		if($start >= $totalPages) $start = $pageNumber -9;
						
		$end   = $start + $numLinks - 1;		
		
		$end   = min($totalPages, $end);
				
		
		$pagingLink = array();
	
		for($page = $start; $page <= $end; $page++)	{
			if ($page == $pageNumber) {
				$pagingLink[] = " <a style='color:red;'>$page</a> ";   // no need to create a link to current page
			} else {
				if ($page == 1) {
					if($source == 'master') {
						$pagingLink[] = " <a href=\"#\" onclick=\"getNextMaster('?pageNum=$page&s=$sortBy')\">$page</a> ";
					} else {
						if($source == 'pic_master') {
							$pagingLink[] = " <a href=\"#\" onclick=\"getNextPicMaster('?pageNum=$page&s=$sortBy')\">$page</a> ";
						} else {								
							$pagingLink[] = " <a href=\"#\" onclick=\"getNextScheduled('?pageNum=$page&s=$sortBy')\">$page</a> ";
						}	
					}	
				} else {	
					if($source == 'master') {
						$pagingLink[] = " <a href=\"#\" onclick=\"getNextMaster('?pageNum=$page&s=$sortBy')\">$page</a> ";
					} else {
						if($source == 'pic_master') {
							$pagingLink[] = " <a href=\"#\" onclick=\"getNextPicMaster('?pageNum=$page&s=$sortBy')\">$page</a> ";
						} else {
							$pagingLink[] = " <a href=\"#\" onclick=\"getNextScheduled('?pageNum=$page&s=$sortBy')\">$page</a> ";
						}	
					}	
				}	
			}
	
		}
				
		$pagingLink = implode(' | ', $pagingLink);
		
		// return the page navigation link
		$pagingLink = $first . $prev . $pagingLink . $next . $last;
	
	
	}
	
	return $pagingLink;
}
function object_to_array($data)
{
    if (is_array($data) || is_object($data))
    {
        $result = array();
        foreach ($data as $key => $value)
        {
            $result[$key] = object_to_array($value);
        }
        return $result;
    }
    return $data;
}

?>