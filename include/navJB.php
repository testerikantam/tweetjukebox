<?
$SQL_S = "select id, list_name, isMyStoreItem, list_id, storeItemID from user_schedule where user_id = '$user_id' and list_id = '$list_number'  ";
$result_s = mysqli_query($conn, $SQL_S) or die(mysqli_error());
$row_s = mysqli_fetch_assoc($result_s);
$isMyStoreItem = $row_s['isMyStoreItem'];
$list_name = stripslashes($row_s['list_name']);
$list_id = stripslashes($row_s['list_id']);
$storeItemID = $row_s['storeItemID'];


$SQL2 = "select count(id) as tweetTotal from tweets where user_id = '$user_id' and list_id = '$list_number' ";	
$result2 = mysqli_query($conn, $SQL2) or die(mysqli_error());
$row2 = mysqli_fetch_assoc($result2);
$tweetTotal = $row2['tweetTotal'];

?>
<div class="row" align="center">
<div class="col-md-12">
	<ul class="nav navbar-nav" style="cursor:pointer;">
		<li id="tweetLI" class=""><a onclick="masterList(<? echo $list_number; ?>);"><i class="fa fa-twitter"></i> Tweets </a></li>
		<li id="scheduleLI" class=""><a onclick="setupSchedule(<? echo $list_number; ?>);"><i class="fa fa-calendar"></i> Schedule </a></li>
		<li id="optionLI" class=""><a onclick="jbOptions(<? echo $list_number; ?>);"><i class="fa fa-cog"></i> Options </a></li>
		<? 
		if($_SESSION['isAdmin'] == '1' || $_SESSION['isSpecial'] == '1') { 
			if($isMyStoreItem) {
		?>	
				<li id="storeLI" class=""><a onclick="removeStoreItem('<? echo $list_number; ?>');"><i class="fa fa-trash-o"></i> Remove From Store </a></li>
		<?	} else { 
				if($storeItemID == '0' && $tweetTotal > 49 ) {		
		?>								
					<li id="storeLI" class=""><a onclick="addStoreItem('<? echo $list_number; ?>', '<? echo addslashes($list_name); ?>');"><i class="fa fa-plus-circle"></i> Add To Library </a></li>
		<?		}
			}		
		} 
		?>						
	</ul>
</div>	
</div>
<script>
function addStoreItem(l, n) {
	if(confirm("This action will immediatly add this JB to our store where it will be available to other members to download.\n\nIs this what you want to do?")) {
		$.ajax({
			type: "post",
			url: "/members/addStoreItem.php",
			data: {'list_id': l, 'item_name': n} ,
			dataType: "html"
		})
		.done(function( msg ) { 
			//alert(msg);
			myStoreItems(msg);
		});	
	} 		
}
function removeStoreItem(l) {
	if(confirm("This action will immediatly remove this JB from our store.\n\nIt will not remove it from any users that have previously downloaded it.\n\nIs this what you want to do?")) {
		$.ajax({
			type: "post",
			url: "/members/deleteStoreItem.php",
			data: {'list_number': l} ,
			dataType: "html"
		})
		.done(function( msg ) { 
			//alert(msg);
			//myStoreItems(msg);
			location.assign("/members/");
		});
	}
}
</script>
