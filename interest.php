<? include ($_SERVER['DOCUMENT_ROOT']."/include/config.php") ; ?>
<?
if($_POST['submit']) {
	include($_SERVER['DOCUMENT_ROOT']."/include/mail/class.phpmailer.php");
	
	if($_POST['phone'] == '') {
		$email = mysqli_real_escape_string($conn, $_POST['email']);
		$name = mysqli_real_escape_string($conn, $_POST['name']);
		$msg = mysqli_real_escape_string($conn, $_POST['msg']);
		
		$mail = new PHPMailer();
		$mail->IsSendmail();									// use sendmail
		$mail->From = SITE_EMAIL;
		$mail->FromName = SITE_EMAIL;
		$mail->AddAddress(SITE_EMAIL, SITE_EMAIL);
		//$mail->AddAddress("ellen@example.com");                  // name is optional
		$mail->AddReplyTo($email, $email);
		$mail->WordWrap = 50;                                 // set word wrap to 50 characters
		//$mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
		//$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
		$mail->IsHTML(true);                                  // set email format to HTML
		$mail->Subject = SITE_NAME." Registration Interest";
		$mail->Body = "Name: ".$name."<br>Email: ".$email."<br>Message: ".$msg;
		if(!$mail->Send())
		{
		   echo "Message could not be sent. <p>";
		   echo "Mailer Error: " . $mail->ErrorInfo;
		   exit;
		}
	}	
	$thanks = "Thanks for your interest in Tweet Jukebox. We'll  be in touch shortly.";
}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Tweet Jukebox</title>
<!-- BOOTSTRAP STYLES-->
<link href="../assets/css/bootstrap_v3.3.4.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="../assets/css/font-awesome.css" rel="stylesheet" />
<!-- MORRIS CHART STYLES-->
<link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="../assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<!-- JQUERY SCRIPTS -->
<script src="../assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../assets/js/bootstrap_v3.3.4.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="../assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="/include/js_boot.js"></script>


<script>
function validateEmail(sEmail) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,5}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {
		$('#submit').prop( "disabled", false );
		return true;
	}
	else {
		$('#submit').prop( "disabled", true ); 
		return false;
	}
}
</script>
</head>
<body>
<div id="wrapper">
  <nav class="navbar navbar-default navbar-cls-top navbar-collapse" role="navigation" style="margin-bottom: 0">
  <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;">
    <ul class="nav navbar-nav navbar-cls-top" style="cursor:pointer;">
      <li><a href="/"><i class="fa fa-home"></i> Home </a></li>
      <li><a href="/about"> About </a></li>
      <li><a href="/contact"> Contact us </a></li>
      <li><a href="/faq"><i class="fa fa-info-circle"></i> FAQ </a></li>
    </ul>
  </div>
</div>


<div class="container">
   <? if($thanks) { ?>
   <div class="row">
		 <div class="col-lg-12"><h2 align="center"><? echo $thanks; ?></h2></div>
   </div>
   <? } else { ?>
   <div class="row" align="center">
   		<h2 align="center">We are currently performing server maintenance and will be back shortly!</h2>	
		 <form method="post" enctype="application/x-www-form-urlencoded" accept-charset="utf-8" >
			 <p><b>Members</b>, please try again in a few minutes as we're currently performing system maintenance.
			 	<br>No worries, we're still sending out your Jukebox and Scheduled Tweets!
			 </p>
			 <p><b>New members</b>, you can try us again in a few minutes OR feel free to leave us your email and we'll contact you as soon as we're back online for new registrations!</p>
			 <label for="name">Name</label>
			 <input type="text" id="name" name="name" value="" />
			 <br>
			 <label for="email">Email</label>
			 <input type="email" id="email" name="email" onChange="validateEmail(this.value)" value="" />
			 <br>
			 <label for="msg">Message</label><br>
			 <textarea name="msg" cols="60" rows="10" id="msg"></textarea>
			 <input type="hidden" id="phone" class="smokeLS" value="">
			 <br>
			 <input type="submit" id="submit" name="submit" value="Send" class="button" disabled="disabled" />				 
		 </form>
   </div>
   <? } ?>
</div>
<div class="container" >
  <div class="row" style="padding-top:50px;">
    <div class="col-md-3 col-sm-3 col-xs-3"></div>
    <div class="col-md-6 col-sm-6 col-xs-6">
      <? if($_SESSION['ENTRY_DOMAIN'] == 'sjb') { ?>
	  	<p align="center"> <span class="left">www.SocialJukebox.com &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> |
	  <? } else { ?>
	  	<p align="center"> <span class="left">tj.local &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> |
	  <? } ?>			
        <!--a href="/plans.php">PLANS</a> | -->
        <a href="/contact">CONTACT US</a> | <a href="/privacy">PRIVACY POLICY</a> | <a href="/terms">TERMS AND CONDITIONS</a> </p>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3"></div>
  </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
<script src="assets/js/script.js"></script>
</body>
</html>
