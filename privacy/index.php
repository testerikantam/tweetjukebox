<?php  include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Tweet Jukebox</title>
<meta name="description" content="Tweet Jukebox - Thinking Inside the Box">
<!-- BOOTSTRAP STYLES-->
<link href="../assets/css/bootstrap_v3.3.4.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="../assets/css/font-awesome.css" rel="stylesheet" />
<!-- MORRIS CHART STYLES-->
<link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="../assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="jquery.timepicker.css" />
<link href="/members/chart.css" rel="stylesheet" type="text/css">
<!-- JQUERY SCRIPTS -->
<script src="../assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../assets/js/bootstrap_v3.3.4.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="../assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../assets/js/custom.js"></script>
<script src="/include/js_boot.js"></script>
<script type="text/javascript" src="jquery.timepicker.js"></script>
<link rel="stylesheet" href="/highslide-4.1.13/highslide/highslide.css" type="text/css" />
<!-- 9d83e18a764256552baf6956b777f190 -->
<script type="text/javascript" src="/highslide-4.1.13/highslide/highslide-full.packed.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/highslide-4.1.13/highslide/graphics/';
	hs.showCredits = false;
    hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = .45;
	hs.zIndexCounter = 2000;
//	hs.objectLoadTime = 'after';
//	hs.forceAjaxReload = 'true';
</script>
<script src="../assets/js/dropdown.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64707958-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="wrapper">	    
	<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="/">Tweet Jukebox</a> 		
	</div>
    <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
		<ul class="nav navbar-nav navbar-cls-top" style="cursor:pointer;">
            <? require_once($_SERVER['DOCUMENT_ROOT']."/include/mainNav.php") ; ?>	
		</ul>
	</div>
</div>	
<div class="container" >
	<div class="row">
	<div class="col-md-1 col-sm-1 col-xs-1"></div>
	<div class="col-md-10 col-sm-10 col-xs-10">

   <section role="banner">
       <hgroup>
         <h1>Privacy Policy</h1>
       </hgroup>
	   
       <article role="main" class="clearfix contact">
<div class="post" style="width:auto;">
  <P>This Privacy Policy is to inform you how and what data about you is collected and how the data is used and maintained, as well as to inform you about your obligations regarding privacy of those that might be involved.</P>
<P>Your use of this Site and or the services offered on this site signifies acknowledgement of and agreement to this Privacy Policy. From time to time we may make changes to our privacy policy without notification.</P>
<P><STRONG>WHAT INFORMATION IS COLLECTED</STRONG><BR>We collect only data which is essential to create and maintain your account and the photos you submit to us and data that you provide to us via any electronic means, including e-mail, account registration, uploads, and any other method you initiate. The following information is collected:</P>
<OL>
<LI>Registration data - the information you provide when you register on our site. 
<LI>Any information that you provide to us by any means. 
<LI>The information we collect is used to improve the content of our Web Site. It is not shared with other organizations for commercial purposes. 
<LI>We use cookies to record user-specific information on what pages users may access or visit. 
<LI>When you request a page from our site we automatically track the IP address where the requested information is sent. </LI></OL>
<P><STRONG>HOW THE INFORMATION IS USED </STRONG><BR>
Registration data - will be used only to create and maintain your account. Your personal information submitted to us will not be passed to third parties. We may occasionally contact you via provided e-mail address to ensure proper maintenance and security of your account. We may also occasionally send you a newsletter with information about latest changes or additions to our Site. An option for not receiving our newsletter will be provided to you. Under no circumstances will we use spam or send you unsolicited e-mail. Cookies. A cookie contains no personal information, but is used to help making an access to your account more secure. Cookies are also used to help maintain access counters (including counting visitors to your photos). Internet tracking information. This information contains no personal data, although it does contain your IP address and the type of browser you used to access our Site. The information is used for statistics and is helpful to make the Site better for our users needs.</P>
<P><STRONG>HOW THE INFORMATION IS PROTECTED </STRONG><BR>
Credit Card Information - We only collect credit card information over an encrypted connection and only when you conduct a financial transaction with us. We do not store any credit card information in our data base. Therefore, every transaction you do on our site, you must enter your credit card information again. Access to your account data you provided is restricted to you and only to those internal administrators of our Web Site that have been given permission to access the data for administrative and or diagnostic purposes. Every effort will be made to assure security of the information you provide. </P>
<P><STRONG>YOUR OBLIGATIONS </STRONG><BR>If you visit our site, you agree to respect the privacy of other visitors as well as the privacy of all others using and/or providing the services for the Site. If you submit photos to us, you agree to respect and protect the privacy of any recognizable person(s) shown in your photographs or otherwise mentioned in the submission of your photos. If your photo shows any recognizable person(s), your submission acts as a statment that you have written approval of the person(s) involved to have the photo displayed on our web site.</P>
<P>Under no circumstances will TweetJukebox.com be responsible or liable for any loss or damages of any kind arising from claims against infringement of the right of privacy by the photos or other content that you uploaded or displayed on our site.</P>
</div>
     </article>
	   </section>
	   </div>
	   <div class="col-md-1 col-sm-1 col-xs-1"></div>
	</div>
</div>   
<div class="container" >
	<div class="row" style="padding-top:50px;">
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<p align="center">
   			<span class="left">tj.local &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> | <!--a href="/plans.php">PLANS</a> | --><a href="/contact">CONTACT US</a> | <a href="/privacy">PRIVACY POLICY</a> | <a href="/terms">TERMS AND CONDITIONS</a>
			</p>
		</div>	
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
	</div>
</div>	   
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
  <script src="assets/js/script.js"></script>
</body>
</html>