<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<? 
require_once $_SERVER['DOCUMENT_ROOT'].'/braintree-php-3.1.0/lib/Braintree.php';
//  sandbox 
Braintree_Configuration::environment('sandbox');
Braintree_Configuration::merchantId('zsbs7swsdwdzvmwg');
Braintree_Configuration::publicKey('yb7qkdry9qfvzbn9');
Braintree_Configuration::privateKey('36ae489f8f321abd38f0971f5f1ff2cf');
// prodcution
//Braintree_Configuration::environment('production');
//Braintree_Configuration::merchantId('dxpnw5b448vwd9rp');
//Braintree_Configuration::publicKey('mg2pmbnsn3g4cqnq');
//Braintree_Configuration::privateKey('cf12d53af0f632d8fb98c91af4b86741');
?>
<?
$clientToken = Braintree_ClientToken::generate([ ]);
?>
<?														//   for now this is only good for current members
if(!isset($_SESSION['access_token'])) header('Location: /');
//if($_SESSION['validated'] != 'validated') header('Location: /');
//if($_SESSION['status'] != 'verified') header('Location: /');

$pid = $_REQUEST['pid'];

if($pid == 'Free') {
	$userID = $_SESSION['access_token']['user_id'];
	
	$access_token = $_SESSION['access_token'];
	$oauth_token = $access_token['oauth_token'];
	$oauth_token_secret = $access_token['oauth_token_secret'];
	
	$_SESSION['loggedInAs'] = $userID;
	$_SESSION['validated'] = 'validated';
	$_SESSION['screen_name'] = $screen_name;	
	$_SESSION['expireDate'] = date('m/d/Y', strtotime($row['expireDate']));
	$SQL = "update users set planName = 'Free', oauth_token = '$oauth_token', oauth_token_secret = '$oauth_token_secret', status = '1', last_login = '$last_login', ip_address = '$ip_address'  where user_id = '$userID' ";				
	$result = mysqli_query($conn, $SQL);
	header("Location: /members");
	exit();
}

if (isset($_SESSION['access_token']) && isset($_SESSION['access_token']['user_id'])) {
    $userID = $_SESSION['access_token']['user_id'];
    $SQL = "select * from users where user_id = '$userID'";
    $result = mysqli_query($conn, $SQL);
    $row = mysqli_fetch_assoc($result);
}


?>
<!doctype html>
<html class="no-js" lang="en">
<head>
<link rel="shortcut icon" href="/favicon.ico?v=2">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Tweet Jukebox</title>
<meta name="description" content="Tweet Jukebox - Thinking Inside the Box">
<!-- BOOTSTRAP STYLES-->
<link href="/assets/css/bootstrap_v3.3.4.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="/assets/css/font-awesome.css" rel="stylesheet" />
<!-- MORRIS CHART STYLES-->
<link href="/assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="/assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<link href="/members/chart.css" rel="stylesheet" type="text/css">
<!-- JQUERY SCRIPTS -->
<script src="/assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="/assets/js/bootstrap_v3.3.4.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="/assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="/assets/js/custom.js"></script>
<script src="/include/js_boot.js"></script>
<link rel="stylesheet" href="/highslide-4.1.13/highslide/highslide.css" type="text/css" />
<!-- 9d83e18a764256552baf6956b777f190 -->
<script type="text/javascript" src="/highslide-4.1.13/highslide/highslide-full.packed.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/highslide-4.1.13/highslide/graphics/';
	hs.showCredits = false;
    hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = .45;
	hs.zIndexCounter = 2000;
//	hs.objectLoadTime = 'after';
//	hs.forceAjaxReload = 'true';
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64707958-1', 'auto');
  ga('send', 'pageview');

</script>

</head>
<body>
<div id="wrapper">	    
	<nav class="navbar navbar-default navbar-cls-top navbar-collapse" role="navigation" style="margin-bottom: 0">
    <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
		<ul class="nav navbar-nav navbar-cls-top" style="cursor:pointer;">
            <? require_once($_SERVER['DOCUMENT_ROOT']."/include/mainNav.php") ; ?>
		</ul>
	</div>
</div>
<div class="container">
<div class="row">
<div class="col-lg-3"></div>
<div class="col-lg-6">	
	<h2 align="center">Secure Payment Form</h2>
	<p class="text-info"><strong>Please be advised, you must be the owner of the Twitter account you use   when paying for your service, as it will be your master account. If you   do not own the Twitter account, it is possible for it to be cancelled by   the owner without your consent.</strong></p>
	<form class="form-group" action="transaction.php" method="post" id="paymentForm" accept-charset="utf-8">
	<div style="padding:5px; background-color:#ffffff; border-radius:6px; margin-bottom:5px; border:solid #000000 1px;">
	<p><strong>You are subscribing to a recurring payment agreement. Once you submit your payment, your access will remain valid on an ongoing basis until your membership is canceled. By checking this box you agree to these terms. </strong>    
		<input type="checkbox" id="agreedTo" name="agreedTo" value="1" /></p>
		<input type="hidden" value="" name="pid" id="pid"  />
		<input type="hidden" value="" name="payment_method_nonce" id="payment_method_nonce" />
		<input type="hidden" value="" name="saved_payment_method_token"/>
		<input type="hidden" name="currDate" id="currDate" value="<? echo date("Ym"); ?>" />
	</p>
	</div>	
		<table class="table-responsive" style="border:0; width:100%;">
		<tr>
		  <th valign="middle"><? echo $pid. " Membership"; ?> Payment Plan</th>	
		  <td valign="middle">
          	<select class="form-control"  id="paymentPlan" name="paymentPlan" onChange="document.getElementById('pid').value = this.value;">
				<option value="0">Select Billing Cycle</option>
			<?
				if($pid == 'Advanced') {		
			?>
				<? if($_SESSION['billingCycle'] == 'Monthly' || $_SESSION['billingCycle'] == '') { ?>
				<option value="Advanced-Monthly">$<? echo ADVANCED_VERSION_MONTHLY_FEE; ?> / month</option>
				<? } ?>
				<? if($_SESSION['billingCycle'] == 'Annual' || $_SESSION['billingCycle'] == '') { ?>
				<option value="Advanced-Annual">$<? echo ADVANCED_VERSION_ANNUAL_FEE; ?> / year save $<? $fullYear = (float)ADVANCED_VERSION_MONTHLY_FEE *12 ; $annual = (float)ADVANCED_VERSION_ANNUAL_FEE; echo number_format($fullYear - $annual, 2);   ?></option>				
				<? } ?>
			<? } ?>
			<?
				if($pid == 'Pro') {		
			?>
				<? if($_SESSION['billingCycle'] == 'Monthly' || $_SESSION['billingCycle'] == '') { ?>
				<option value="Pro-Monthly">$<? echo PRO_VERSION_MONTHLY_FEE; ?> / month</option>
				<? } ?>
				<? if($_SESSION['billingCycle'] == 'Annual' || $_SESSION['billingCycle'] == '') { ?>
				<option value="Pro-Annual">$<? echo PRO_VERSION_ANNUAL_FEE; ?> / year save $<? $fullYear = (float)PRO_VERSION_MONTHLY_FEE *12 ; $annual = (float)PRO_VERSION_ANNUAL_FEE; echo number_format($fullYear - $annual, 2);   ?></option>
				<? } ?>
			<? } ?>	
			<?
				if($pid == 'Business') {		
			?>
				<? if($_SESSION['billingCycle'] == 'Monthly' || $_SESSION['billingCycle'] == '') { ?>
				<option value="Business-Monthly">$<? echo BUSINESS_VERSION_MONTHLY_FEE; ?> / month</option>
				<? } ?>
				<? if($_SESSION['billingCycle'] == 'Annual' || $_SESSION['billingCycle'] == '') { ?>
				<option value="Business-Annual">$<? echo BUSINESS_VERSION_ANNUAL_FEE; ?> / year save $<? $fullYear = (float)BUSINESS_VERSION_MONTHLY_FEE *12 ; $annual = (float)BUSINESS_VERSION_ANNUAL_FEE; echo number_format($fullYear - $annual, 2);   ?></option>			
				<? } ?>
			<? } ?>	
			</select>	
		  </td>
        </tr>
		<tr>
          <th valign="middle">First Name</th>
          <td valign="middle"><input class="form-control" id="user_first_name" name="user_first_name" type="text" value="<? echo $_SESSION['user_first_name'];?>" /></td>
        </tr>
		<tr>
          <th valign="middle">Last Name</th>
          <td valign="middle"><input class="form-control" id="user_last_name" name="user_last_name" type="text" value="<? echo $_SESSION['user_last_name'];?>" /></td>
        </tr>
		<tr>
          <th valign="middle">Email</th>
          <td valign="middle"><input class="form-control" id="email" name="email" type="text" value="<? echo $_SESSION['email'];?>" /></td>
        </tr>		
		<tr>
			<td colspan="2" align="center" height="40px;"><b>Pay with PayPal Or Credit Card</b></td>
		</tr>
		<tr>
			<td colspan="2" id="payment-form"></td>
		</tr>
  		</table>			
		<center><input id="submitPaymentButton" type="submit" class="button" value="Submit Payment" ></center>
        <?php
            $userID = $_SESSION['access_token']['user_id'];
            try {
                /** @var \Braintree_Customer $customer */
                $customer = Braintree_Customer::find($userID);
                $paymentsMethods = $customer->paymentMethods;

                $activePaymentMethodToken = null;
                foreach ($customer->paymentMethods as $paymentMethod) {
                    if (count($paymentMethod->subscriptions)) {
                        foreach ($paymentMethod->subscriptions as $subscription) {
                            if ($subscription->id == $row['bt_plan_id']) {
                                $activePaymentMethodToken = $paymentMethod->token;
                            }
                        }
                    }
                }

                ?>
                <?php if (count($paymentsMethods)) : ?>
                    <h4 class="text-center">or use the existing payment method</h4>
                <?php endif; ?>
                <?php foreach ($paymentsMethods as $paymentMethod) : ?>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <?php if ($paymentMethod instanceof Braintree_CreditCard) : ?>
                                <div class="col-md-6">
                                    <div>Card number: <?php echo $paymentMethod->maskedNumber; ?></div>
                                    <div>Expiration date: <?php echo $paymentMethod->expirationDate; ?></div>
                                </div>
                                <div class="col-md-2">
                                    <img src="<?php echo $paymentMethod->imageUrl; ?>" alt="">
                                </div>
                                <div class="col-md-4">
                                    <?php if ($activePaymentMethodToken == $paymentMethod->token) : ?>
                                        <span class="btn btn-info col-md-5">Active</span>
                                        <button type="button" class="btn btn-success pay-by-saved-payment-method col-md-offset-2 col-md-5" value="<?php echo $paymentMethod->token; ?>">Pay</button>
                                    <?php else : ?>
                                        <button type="button" class="btn btn-success pay-by-saved-payment-method col-md-12" value="<?php echo $paymentMethod->token; ?>">Use this card</button>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                            <?php if ($paymentMethod instanceof Braintree_PayPalAccount) : ?>
                                <div class="col-md-6">
                                    <div>Email: <?php echo $paymentMethod->email; ?></div>
                                </div>
                                <div class="col-md-2">
                                    <img src="<?php echo $paymentMethod->imageUrl; ?>" alt="">
                                </div>
                                <div class="col-md-4">
                                    <?php if ($activePaymentMethodToken == $paymentMethod->token) : ?>
                                        <span class="btn btn-info col-md-5">Active</span>
                                        <button type="button" class="btn btn-success pay-by-saved-payment-method col-md-offset-2 col-md-5" value="<?php echo $paymentMethod->token; ?>">Pay</button>
                                    <?php else : ?>
                                        <button type="button" class="btn btn-success pay-by-saved-payment-method col-md-12" value="<?php echo $paymentMethod->token; ?>">Use this account</button>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php } catch (Exception $e) {} ?>
	</form>
</div>
<div class="col-lg-3"></div>	
</div>
<div id="errorDiv" style="visibility:hidden; background-color:#FFE8E8; color:#FF0000; font-weight:bold; text-align:center; border:thin #750000 1px;" align="center">OOPS! An error occured.<br><? echo $_SESSION['GatewayError'];?><br>Check your information and try again.</div>
</div>
<?	
if($_REQUEST['err'] ) {
?>
<script>
document.getElementById('errorDiv').style.visibility = 'visible';
</script>
<?
}
?>
<script src="https://js.braintreegateway.com/v2/braintree.js"></script>
<script>
var formErrors = '';

function submitForm() {
    formErrors = '';
    if($('#paymentPlan').val() == '0') {
        formErrors+="Please select a valid payment cycle.\n"
    }
    if(document.getElementById("agreedTo").checked == false) {
        formErrors+="You must acknowledge the subscription terms.\n";
    }
    if(formErrors) {
        $('#submitPaymentButton').prop('disabled', false);
        alert(formErrors);
        //return false;
    } else {
        //alert('here');
        $("#paymentForm").submit();
    }
}

braintree.setup("<? echo $clientToken; ?>", "dropin", {
  container: "payment-form",
  onPaymentMethodReceived: function (obj) { 	
	$('#submitPaymentButton').prop('disabled', true);
	$('#payment_method_nonce').val((obj.nonce));
      $('[name="saved_payment_method_token"]').val(null);
      submitForm();
  }	
});

$('.pay-by-saved-payment-method').on('click', function () {
    var $payButton = $(this);
    var token = $payButton.val();
    $('[name="saved_payment_method_token"]').val(token);
    submitForm();
});

</script>
<script>
	// sandbox
	var braintree = Braintree.create("MIIBCgKCAQEAnww8b1E5KYGIsDYHbyW06G7CsAf+gkUp3/x8go+EaqBiw9ZLjTqCwgze7Is+m1Wau8wn5QjA8NouOW9tik9IskoCYDF8IgHsp1IlGbKRo0gWSnBYcUoNU/PLA6O7Mqjqtep+i5wcrAxoyt89av65ZytYMeLwZCVG+5rxpUFLov6lXbLiJgtS+YdF5xkS+UYKcM/iD40Oefjo/U0SuUzxipTI1ryrqds3p8hYRPJ1IvP97taGIQf3mAnm3yB7m4HtlJS61tC3xYJFTge9DkNtRS9Pr4ta96A04g/x4n1dSG86qfd2TJOJpfj40vcYHBfgVtE1QdCYbl7fTZ+K7fKxKQIDAQAB");   
	// production 
	//var braintree = Braintree.create('MIIBCgKCAQEArO6mDKhEP6AD4IvYCbD3dXP4Bu0EzGjODyYhVvORCQRP139imr3pFhRUWhHrky6ba7/jR0Oa4LYp5TnifAhPz7bzMrK+QWr3/YhoNYhkzR2vYY+kB7bKYUQyaGCCbJNRFIJ2eiiLsH5tguQFGv9DoU1/M//5uP92IzedHocZtaOQqVi0X47SK+4xAPQCJilcLfArvd5FCxBTG1oJdNROTKmVzPZitJOqYhr9ObSDTE8ruL1CGDvxaG85IE+eRu8ZyTjR/WYDMBxDTMkM7CsWL2sGZEMEgnl+1e4IAdQdR5/ZDwCqBLQn/goHB9ZF12d4vxrTQvUktYVXy25fvQvrGQIDAQAB');	
	//braintree.onSubmitEncryptForm('braintree-payment-form');
</script>
<div class="container" >
	<div class="row" style="padding-top:50px;">
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<p align="center">
   			<span class="left">tj.local &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> | <!--a href="/plans.php">PLANS</a> | --><a href="/contact">CONTACT US</a> | <a href="/privacy">PRIVACY POLICY</a> | <a href="/terms">TERMS AND CONDITIONS</a>
			</p>
		</div>	
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
	</div>
</div>
</body>	  
</html>