<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?														//   for now this is only good for current members
//var_dump($_SESSION); die();
if(!isset($_SESSION['access_token'])) header('Location: /');
//if($_SESSION['status'] != 'verified') header('Location: /');
if($_SESSION['master_account_id'] != '' && $_SESSION['master_account_id'] != $_SESSION['access_token']['user_id']) header('Location: /');

$access_token = $_SESSION['access_token'];
$screen_name = $_SESSION['access_token']['screen_name'];
$user_id = $_SESSION['access_token']['user_id'];

$SQL = "select * from users where user_id = '$user_id'  ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$row = mysqli_fetch_assoc($result);	
$_SESSION['OFFER_SEEN'] = $row['offer_seen'];
$_SESSION['thanksLimit'] = $row['thanks_limit'];

$currentStatus = $row['status'];
// if the current status is 2 this is a special case where the account was cancelled either by us, the user, or a master 
// we want them to be able to signup but not be able to turn the master switch on until they fit into a plan


$_SESSION['master_account_id'] = $row['master_account_id'];

$_SESSION['user_first_name'] = stripslashes($row['user_first_name']);
$_SESSION['user_last_name'] = stripslashes($row['user_last_name']);
$_SESSION['email'] = stripslashes($row['email']);

if($row['status'] == 0 ) {
	header("Location: /oops.php?err=expired");
	exit();
}	

$userStatus = $row['status'];

//$_SESSION['planName'] = $row['planName'];
$planName = explode("-", $row['planName']);	
$_SESSION['planName'] = $planName[0];

$_SESSION['isAdmin'] = $row['isAdmin'];
$_SESSION['isSpecial'] = $row['isSpecial'];


$all_tweet_status = $row['all_tweet_status'];
$user_id = $row['user_id'];
$send_thanks = $row['send_thanks'];

if($row['planName'] == 'Gold') { 
	$_SESSION['isGrandfathered'] = '1'; 
} else {
	$_SESSION['isGrandfathered'] = '0';
	if($_SESSION['planName'] != 'Free') {
		$plan = explode("-", $_SESSION['planName']);
		$_SESSION['planName'] = $plan[0];
		$_SESSION['billingCycle'] = $plan[1];
	} 	
}

if( $_SESSION['planName'] == 'Free' || $row['planName'] == '' || $row['planName'] == 'Gold') {
	$_SESSION['planName'] = 'Free';
	$_SESSION['boxLimit'] = FREE_VERSION_JB_BOX_LIMIT;
	$_SESSION['tweetLimit'] = FREE_VERSION_JB_TWEET_LIMIT;
	$_SESSION['scheduledLimit'] = FREE_VERSION_SCHEDULED_TWEET_LIMIT;
	$_SESSION['accountsLimit'] = FREE_VERSION_ACCOUNTS_LIMIT;
	$_SESSION['thankLimit'] = FREE_VERSION_THANK_YOU_TWEETS;
}	
if($_SESSION['planName'] == 'Advanced') {
	$_SESSION['boxLimit'] = ADVANCED_VERSION_JB_BOX_LIMIT;
	$_SESSION['tweetLimit'] = ADVANCED_VERSION_JB_TWEET_LIMIT;
	$_SESSION['scheduledLimit'] = ADVANCED_VERSION_SCHEDULED_TWEET_LIMIT;
	$_SESSION['accountsLimit'] = ADVANCED_VERSION_ACCOUNTS_LIMIT;
	$_SESSION['thankLimit'] = ADVANCED_VERSION_THANK_YOU_TWEETS;
}	
if( $_SESSION['planName'] == 'Pro') {
	$_SESSION['boxLimit'] = PRO_VERSION_JB_BOX_LIMIT;
	$_SESSION['tweetLimit'] = PRO_VERSION_JB_TWEET_LIMIT;
	$_SESSION['scheduledLimit'] = PRO_VERSION_SCHEDULED_TWEET_LIMIT;
	$_SESSION['accountsLimit'] = PRO_VERSION_ACCOUNTS_LIMIT;
	$_SESSION['thankLimit'] = PRO_VERSION_THANK_YOU_TWEETS;
}	
if( $_SESSION['planName'] == 'Business') {
	$_SESSION['boxLimit'] = BUSINESS_VERSION_JB_BOX_LIMIT;
	$_SESSION['tweetLimit'] = BUSINESS_VERSION_JB_TWEET_LIMIT;
	$_SESSION['scheduledLimit'] = BUSINESS_VERSION_SCHEDULED_TWEET_LIMIT;
	$_SESSION['accountsLimit'] = BUSINESS_VERSION_ACCOUNTS_LIMIT;
	$_SESSION['thankLimit'] = BUSINESS_VERSION_THANK_YOU_TWEETS;
}	


$SQL = "select id from user_schedule where user_id = '$user_id' ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$_SESSION['boxCount'] = mysqli_num_rows($result);


$SQL = "select id from tweets where user_id = '$user_id' ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$_SESSION['tweetCount'] = mysqli_num_rows($result);

$SQL = "select id from scheduled_tweets where user_id = '$user_id' ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$_SESSION['scheduledCount'] = mysqli_num_rows($result);


$SQL = "select id from users where master_account_id = '".$_SESSION['master_account_id']."' and master_account_id != ''"; 
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$_SESSION['linkedAccounts'] = mysqli_num_rows($result);

if($_SESSION['linkedAccounts'] == 0 ) $_SESSION['linkedAccounts'] = 1 ; 
$planFit = ''; // this is what plan the user should be in based on their useage
if(	
$_SESSION['boxCount'] <= FREE_VERSION_JB_BOX_LIMIT && 
$_SESSION['tweetCount'] <= FREE_VERSION_JB_TWEET_LIMIT && 
$_SESSION['scheduledCount'] <= FREE_VERSION_SCHEDULED_TWEET_LIMIT && 
$_SESSION['thanksLimit'] <= FREE_VERSION_THANK_YOU_TWEETS
// note need to check for linked accounts - somehow
) $planFit = 'Free';
if(	$planFit == '' && 
$_SESSION['boxCount'] <= ADVANCED_VERSION_JB_BOX_LIMIT && 
$_SESSION['tweetCount'] <= ADVANCED_VERSION_JB_TWEET_LIMIT && 
$_SESSION['scheduledCount'] <= ADVANCED_VERSION_SCHEDULED_TWEET_LIMIT && 
$_SESSION['thanksLimit'] <= ADVANCED_VERSION_THANK_YOU_TWEETS
// note need to check for linked accounts - somehow
) $planFit = 'Advanced';
if(	$planFit == '' && 
$_SESSION['boxCount'] <= PRO_VERSION_JB_BOX_LIMIT && 
$_SESSION['tweetCount'] <= PRO_VERSION_JB_TWEET_LIMIT && 
$_SESSION['scheduledCount'] <= PRO_VERSION_SCHEDULED_TWEET_LIMIT && 
$_SESSION['thanksLimit'] <= PRO_VERSION_THANK_YOU_TWEETS
// note need to check for linked accounts - somehow
) $planFit = 'Pro';
if($planFit == '') $planFit = 'Business'; 
$_SESSION['planFit'] = $planFit;
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
<link rel="shortcut icon" href="/favicon.ico?v=2">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Tweet Jukebox</title>
<meta name="description" content="Tweet Jukebox - Thinking Inside the Box">
<!-- BOOTSTRAP STYLES-->
<link href="/assets/css/bootstrap_v3.3.4.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="/assets/css/font-awesome.css" rel="stylesheet" />
<!-- MORRIS CHART STYLES-->
<link href="/assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="/assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<link href="/members/chart.css" rel="stylesheet" type="text/css">
<!-- JQUERY SCRIPTS -->
<script src="/assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="/assets/js/bootstrap_v3.3.4.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="/assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="/assets/js/custom.js"></script>
<script src="/include/js_boot.js"></script>
<link rel="stylesheet" href="/highslide-4.1.13/highslide/highslide.css" type="text/css" />
<!-- 9d83e18a764256552baf6956b777f190 -->
<script type="text/javascript" src="/highslide-4.1.13/highslide/highslide-full.packed.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/highslide-4.1.13/highslide/graphics/';
	hs.showCredits = false;
    hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = .45;
	hs.zIndexCounter = 2000;
//	hs.objectLoadTime = 'after';
//	hs.forceAjaxReload = 'true';
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64707958-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="wrapper">	    
	<nav class="navbar navbar-default navbar-cls-top navbar-collapse" role="navigation" style="margin-bottom: 0">
    <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
		<ul class="nav navbar-nav navbar-cls-top" style="cursor:pointer;">
            <? require_once($_SERVER['DOCUMENT_ROOT']."/include/mainNav.php") ; ?>
		</ul>
	</div>
</div>

<div class="container">
	<div class="text-center row">
   		<div class="col-md-12"><h1><img class="img-responsive" style="display:inline;" src="/images/tjbIcon_64.png"> Membership Plans </h1></div>
	</div>
	<div class="text-center row">
   		<div class="col-md-3"></div>
		<div class="col-md-6" style="padding:10px;">
		  <button class="btn" style="color:#000000; background-color:#FFFFCC; border:solid #000000 2px;">My current usage: 
			<? echo $_SESSION['boxCount'];?> Jukeboxes&nbsp;&nbsp;&nbsp;&nbsp;
			<? echo $_SESSION['tweetCount'];?> Jukebox Tweets&nbsp;&nbsp;&nbsp;&nbsp;
			<? echo $_SESSION['scheduledCount'];?> Scheduled Tweets **
			</button>
		</div>
		<div class="col-md-3"></div>
	</div>
	<div class="row">		   
		<div class="col-md-3">
			<? 
			if($_SESSION['planFit'] == 'Free' ) { 
				echo '<div class="jumbotron" style="font-size:small; background-color:#FFFFCC; border:solid #000000 2px;">';
			} else {					
				echo '<div class="jumbotron" style="font-size:small; background-color:#F5F5F5; border:solid #000000 2px;">';
			}	
			?> 
			<h3 style="margin-top:-20px; color:#0099FF;">Free</h3>
			<ul class="list-group">
				<li class="list-group-item"><strong><? echo FREE_VERSION_JB_BOX_LIMIT; ?></strong> Jukeboxes</li>
				<li class="list-group-item"><strong><? echo FREE_VERSION_JB_TWEET_LIMIT; ?></strong> Tweets</li>
				<li class="list-group-item"><strong><? echo FREE_VERSION_SCHEDULED_TWEET_LIMIT; ?></strong> Scheduled Tweets </li>
				<li class="list-group-item"><strong><? echo FREE_VERSION_ACCOUNTS_LIMIT; ?></strong> Linked Twitter&#8482; Accounts </li>
				<li class="list-group-item"><strong><? echo FREE_VERSION_THANK_YOU_TWEETS; ?></strong> Thankyou Tweets </li>
			</ul>			
			<h4><? // echo FREE_VERSION_MONTHLY_FEE; ?></h4>
			<? if($_SESSION['planName'] == 'Free'  && $userStatus == '1' || $userStatus = '2'  ) { 	?>
				<button class="btn-danger btn-block" onClick="confirmSubscription('Free');">Subscribe Now</button>
			<? } ?>	
			<? 
			if($_SESSION['planFit'] != 'Free' ) { 
				echo '<div class="text-center"><b>Your current useage exceeds this plan\'s limits! **</b><br><strong>** You can sign up for this plan, but all automatic tweets will be disabled.  Once you reduce your useage to meet the plan limits you will be able to turn your automatic tweets on (under options).</strong></div>';
			} 
			if($_SESSION['planName'] == 'Free'  && $userStatus == '1'  ) { 
				echo '<div class="text-primary text-center"><b>Your Current Plan</b></div>';
			} 	
			echo '</div>';
			?>
		</div>
		
		<div class="col-md-3">
			<? 
			if($_SESSION['planFit'] == 'Advanced' ) { 
				echo '<div class="jumbotron" style="font-size:small; background-color:#FFFFCC; border:solid #000000 2px;">';
			} else {							 
				echo '<div class="jumbotron" style="font-size:small; background-color:#F5F5F5; border:solid #000000 2px;">';
			} 
			?>	
			<h3 style="margin-top:-20px; color:#0099FF;">Advanced</h3>
			<ul class="list-group">
				<li class="list-group-item"><strong><? echo ADVANCED_VERSION_JB_BOX_LIMIT; ?></strong> Jukeboxes</li>
				<li class="list-group-item"><strong><? echo ADVANCED_VERSION_JB_TWEET_LIMIT; ?></strong> Tweets</li>
				<li class="list-group-item"><strong><? echo ADVANCED_VERSION_SCHEDULED_TWEET_LIMIT; ?></strong> Scheduled Tweets </li>
				<li class="list-group-item"><strong><? echo ADVANCED_VERSION_ACCOUNTS_LIMIT; ?></strong> Linked Twitter&#8482; Accounts </li>
				<li class="list-group-item"><strong><? echo ADVANCED_VERSION_THANK_YOU_TWEETS; ?></strong> Thankyou Tweets </li>
			</ul>
			<h4 align="center">
				<? if($_SESSION['billingCycle'] == 'Monthly' && $userStatus == '1') { ?>
					$<? echo ADVANCED_VERSION_MONTHLY_FEE; ?> / month
				<? } elseif ($_SESSION['billingCycle'] == 'Annual' && $userStatus == '1')	{ ?>
					$<? echo ADVANCED_VERSION_ANNUAL_FEE; ?> / year
				<? } else { ?>
					$<? echo ADVANCED_VERSION_MONTHLY_FEE; ?> / month
					<br>or<br>$<? echo ADVANCED_VERSION_ANNUAL_FEE; ?> / year*<br><br>*save $<? $fullYear = (float)ADVANCED_VERSION_MONTHLY_FEE *12 ; $annual = (float)ADVANCED_VERSION_ANNUAL_FEE; echo number_format($fullYear - $annual, 2);   ?>					
				<? } ?>	
			</h4>
			<? if( $userStatus != '1' || $_SESSION['isGrandfathered'] == '1'  ) { ?>
				<button class="btn-danger btn-block" onClick="confirmSubscription('Advanced');">Subscribe Now</button>
			<? } ?>	
			<? if( $_SESSION['planFit'] == 'Advanced' ) { ?>	
					<div class="text-center" style="margin-top:10px;"><h4>** Perfect Match For My Use</h4></div>
			<? } ?>			
			<? if( ($_SESSION['planFit'] != 'Free' && $_SESSION['planFit'] != 'Advanced' ) ) { ?>	
					<div class="text-center"><b>Your current useage exceeds this plan's limits! **</b><br><strong>** You can sign up for this plan, but all automatic tweets will be disabled.  Once you reduce your useage to meet the plan limits you will be able to turn your automatic tweets on (under options).</strong></div>
			<? } ?>
			<? if( $_SESSION['planName'] == 'Advanced'   ) { ?>	
					<div class="text-primary text-center"><b>Your Current Plan</b></div>
			<? } ?>
			<? if( $_SESSION['planName'] == 'Free'  && $userStatus == '1' && $_SESSION['isGrandfathered'] == '0' ) { ?>	
				<button class="btn-primary btn-block" onClick="confirmSubscription('Advanced');">Upgrade Now</button>		
			<? } 		
			echo '</div>'; 
			?>
		</div> 

		<div class="col-md-3">
			<? 
			if($_SESSION['planFit'] == 'Pro' ) { 
				echo '<div class="jumbotron" style="font-size:small; background-color:#FFFFCC; border:solid #000000 2px;">';
			} else {			
				echo '<div class="jumbotron" style="font-size:small; background-color:#F5F5F5; border:solid #000000 2px;">';
			} ?>	
			<h3 style="margin-top:-20px; color:#0099FF;">Pro</h3>
			<ul class="list-group">
				<li class="list-group-item"><strong><? echo PRO_VERSION_JB_BOX_LIMIT; ?></strong> Jukeboxes</li>
				<li class="list-group-item"><strong><? echo PRO_VERSION_JB_TWEET_LIMIT; ?></strong> Tweets</li>
				<li class="list-group-item"><strong><? echo PRO_VERSION_SCHEDULED_TWEET_LIMIT; ?></strong> Scheduled Tweets </li>
				<li class="list-group-item"><strong><? echo PRO_VERSION_ACCOUNTS_LIMIT; ?></strong> Linked Twitter&#8482; Accounts </li>
				<li class="list-group-item"><strong><? echo PRO_VERSION_THANK_YOU_TWEETS; ?></strong> Thankyou Tweets </li>
			</ul>
			<h4 align="center">
				<? if($_SESSION['billingCycle'] == 'Monthly' && $userStatus == '1') { ?>
					$<? echo PRO_VERSION_MONTHLY_FEE; ?> / month
				<? } elseif ($_SESSION['billingCycle'] == 'Annual' && $userStatus == '1')	{ ?>
					$<? echo PRO_VERSION_ANNUAL_FEE; ?> / year
				<? } else { ?>
					$<? echo PRO_VERSION_MONTHLY_FEE; ?> / month
					<br>or<br>$<? echo PRO_VERSION_ANNUAL_FEE; ?> / year*<br><br>*save $<? $fullYear = (float)PRO_VERSION_MONTHLY_FEE *12 ; $annual = (float)PRO_VERSION_ANNUAL_FEE; echo number_format($fullYear - $annual, 2);   ?>				
				<? } ?>	
			</h4>
			<? if( $userStatus != '1' || $_SESSION['isGrandfathered'] == '1'  ) { ?>
				<button class="btn-danger btn-block" onClick="confirmSubscription('Pro');">Subscribe Now</button>
			<? } ?>
			<? if( $_SESSION['planFit'] == 'Pro' ) { ?>	
					<div class="text-center" style="margin-top:10px;"><h4>** Perfect Match For My Use</h4></div>
			<? } ?>
			<? if( $_SESSION['planFit'] != 'Free' && $_SESSION['planFit'] != 'Advanced' && $_SESSION['planFit'] != 'Pro' ) { ?>
					<div class="text-center"><b>Your current useage exceeds this plan's limits! **</b><br><strong>** You can sign up for this plan, but all automatic tweets will be disabled.  Once you reduce your useage to meet the plan limits you will be able to turn your automatic tweets on (under options).</strong></div>
			<? } ?>
			<? if($_SESSION['planName'] == 'Pro'  ) { ?>	
					<div class="text-primary text-center"><b>Your Current Plan</b></div>
			<? } ?>
			<? if( $userStatus == '1' &&  ($_SESSION['planName'] == 'Free' || $_SESSION['planName'] == 'Advanced' ) && $_SESSION['isGrandfathered'] == '0'  ) { ?>	
				<button class="btn-primary btn-block" onClick="confirmSubscription('Pro');">Upgrade Now</button>		
			<? } 	
			echo '</div>';
			?>
		</div> 
		
		<div class="col-md-3">
			<? 
			if($_SESSION['planFit'] == 'Business' ) { 
				echo '<div class="jumbotron" style="font-size:small; background-color:#FFFFCC; border:solid #000000 2px;">';
			} else {			
				echo '<div class="jumbotron" style="font-size:small; background-color:#F5F5F5; border:solid #000000 2px;">';				
			} ?>	
			<h3 style="margin-top:-20px; color:#0099FF;">Business</h3>
			<ul class="list-group">
				<li class="list-group-item"><strong><? echo BUSINESS_VERSION_JB_BOX_LIMIT; ?></strong> Jukeboxes</li>
				<li class="list-group-item"><strong><? echo BUSINESS_VERSION_JB_TWEET_LIMIT; ?></strong> Tweets</li>
				<li class="list-group-item"><strong><? echo BUSINESS_VERSION_SCHEDULED_TWEET_LIMIT; ?></strong> Scheduled Tweets </li>
				<li class="list-group-item"><strong><? echo BUSINESS_VERSION_ACCOUNTS_LIMIT; ?></strong> Linked Twitter&#8482; Accounts </li>
				<li class="list-group-item"><strong><? echo BUSINESS_VERSION_THANK_YOU_TWEETS; ?></strong> Thankyou Tweets </li>
			</ul>
			<h4 align="center">
				<? if($_SESSION['billingCycle'] == 'Monthly' && $userStatus == '1') { ?>
					$<? echo BUSINESS_VERSION_MONTHLY_FEE; ?> / month
				<? } elseif ($_SESSION['billingCycle'] == 'Annual' && $userStatus == '1')	{ ?>
					$<? echo BUSINESS_VERSION_ANNUAL_FEE; ?> / year
				<? } else { ?>
					$<? echo BUSINESS_VERSION_MONTHLY_FEE; ?> / month
					<br>or<br>$<? echo BUSINESS_VERSION_ANNUAL_FEE; ?> / year*<br><br>*save $<? $fullYear = (float)BUSINESS_VERSION_MONTHLY_FEE *12 ; $annual = (float)BUSINESS_VERSION_ANNUAL_FEE; echo number_format($fullYear - $annual, 2);   ?>
				<? } ?>	
			</h4>
			<? if( $userStatus != '1' || $_SESSION['isGrandfathered'] == '1'  ) { ?>
				<button class="btn-danger btn-block" onClick="confirmSubscription('Business');">Subscribe Now</button>
			<? } ?>
			<? if( $_SESSION['planFit'] == 'Business' ) { ?>						
					<div class="text-center" style="margin-top:10px;"><h4>** Perfect Match For My Use</h4></div>
			<? } ?>
			<? if( $_SESSION['planName'] == 'Business'  ) { ?>	
					<div class="text-primary text-center"><b>Your Current Plan</b></div>
			<? } ?>
			<? if(  $userStatus == '1' &&  ($_SESSION['planName'] == 'Free' || $_SESSION['planName'] == 'Advanced' || $_SESSION['planName'] == 'Pro') && $_SESSION['isGrandfathered'] == '0'   ) { ?>	
				<button class="btn-primary btn-block" onClick="confirmSubscription('Business');">Upgrade Now</button>		
			<? }
			echo '</div>';
			?>
		</div>
	</div>	
</div> 

<div class="container" >
	<div class="row" style="padding-top:50px;">
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<p align="center">
   			<span class="left">tj.local &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> | <!--a href="/plans.php">PLANS</a> | --><a href="/contact">CONTACT US</a> | <a href="/privacy">PRIVACY POLICY</a> | <a href="/terms">TERMS AND CONDITIONS</a>
			</p>
		</div>	
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
	</div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
<script src="/assets/js/script.js"></script>
<script>
function confirmSubscription(pid) {
	location.assign("subscribe.php?pid="+pid);
	//return hs.htmlExpand(null, {src:'subscribe.php?pid='+pid, objectType: 'iframe', width: 600 , height: 700, preserveContent: false, align: 'center' } )
}
</script>
<pre>
<? //print $_SESSION['planName'] ;//print_r ($_SESSION); ?>
</body>
</html>