<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?														//   for now this is only good for current members
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');

$pid = $_REQUEST['pid'];
?>
<link rel="stylesheet" href="/assets/fonts/raphaelicons.css">
<link href="/assets/css/bootstrap_v3.3.4.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="/assets/css/font-awesome.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="/assets/css/custom.css" rel="stylesheet" />
<style>
td { padding-bottom:5px; }
</style>
<html>
<body>
<span class="member-icon" style="font-size:24px; color:#00FF00; float:right;">W</span>
<img src="/images/tjbIcon_64.png" border="0" style="float:left" />	
<h2 align="center">Secure Payment Form</h2>
<p>Once you submit your payment, your access will remain valid on an ongoing basis until your membership is canceled.</p>
<p>If you wish to cancel your membership you may do so by clicking on the "Cancel My Membersip" button on your control panel, or by emailing us with a notice to cancel.</p>
<p align="center" class="fullLink"><? echo $pid. " membership"; ?></p>
<form class="form-group" action="transaction.php" method="POST" id="paymentForm" name="paymentForm" onSubmit="return validateForm();" accept-charset="utf-8">        	
		<table class="table-responsive" style="border:0;">
		<tr>
		  <td valign="middle">Payment Plan</td>	
		  <td valign="middle">
          	<select class="form-control"  id="paymentPlan" name="paymentPlan" onChange="document.getElementById('pid').value = this.value">
				<option value="0">Select</option>
			<?
				if($pid == 'Basic') {		
			?>
				<? if($_SESSION['billingCycle'] == 'Monthly') { ?>
				<option value="Basic-Monthly">$<? echo BASIC_VERSION_MONTHLY_FEE; ?> / month</option>
				<? } ?>
				<? if($_SESSION['billingCycle'] == 'Annual') { ?>
				<option value="Basic-Annual">$<? echo BASIC_VERSION_ANNUAL_FEE; ?> / year save $<? $fullYear = (float)BASIC_VERSION_MONTHLY_FEE *12 ; $annual = (float)BASIC_VERSION_ANNUAL_FEE; echo number_format($fullYear - $annual, 2);   ?>
				<? } ?>
			<? } ?>
			<?
				if($pid == 'Pro') {		
			?>
				<? if($_SESSION['billingCycle'] == 'Monthly') { ?>
				<option value="Pro-Monthly">$<? echo PRO_VERSION_MONTHLY_FEE; ?> / month</option>
				<? } ?>
				<? if($_SESSION['billingCycle'] == 'Annual') { ?>
				<option value="Pro-Annual">$<? echo PRO_VERSION_ANNUAL_FEE; ?> / year save $<? $fullYear = (float)PRO_VERSION_MONTHLY_FEE *12 ; $annual = (float)PRO_VERSION_ANNUAL_FEE; echo number_format($fullYear - $annual, 2);   ?>
				<? } ?>
			<? } ?>	
			<?
				if($pid == 'Business') {		
			?>
				<? if($_SESSION['billingCycle'] == 'Monthly') { ?>
				<option value="Business-Monthly">$<? echo BUSINESS_VERSION_MONTHLY_FEE; ?> / month</option>
				<? } ?>
				<? if($_SESSION['billingCycle'] == 'Annual') { ?>
				<option value="Business-Annual">$<? echo BUSINESS_VERSION_ANNUAL_FEE; ?> / year save $<? $fullYear = (float)BUSINESS_VERSION_MONTHLY_FEE *12 ; $annual = (float)BUSINESS_VERSION_ANNUAL_FEE; echo number_format($fullYear - $annual, 2);   ?>
				<? } ?>
			<? } ?>	
			</select>	
		  </td>
        </tr>
		<tr>
          <td valign="middle">First Name</td>
          <td valign="middle"><input class="form-control" id="user_first_name" name="user_first_name" type="text" value="<? echo $_SESSION['user_first_name'];?>" /></td>
        </tr>
		<tr>
          <td valign="middle">Last Name</td>
          <td valign="middle"><input class="form-control" id="user_last_name" name="user_last_name" type="text" value="<? echo $_SESSION['user_last_name'];?>" /></td>
        </tr>
		<tr>
          <td valign="middle">Last Name</td>
          <td valign="middle"><input class="form-control" id="email" name="email" type="text" value="<? echo $_SESSION['email'];?>" /></td>
        </tr>		
		<tr>
          <td valign="middle">Card Number <img title="All digits - no spaces or dashes" src="/images/question_blue.png"></td>
          <td valign="middle"><input class="form-control" id="number" name="number" type="text" size="20" autocomplete="off" data-encrypted-name="number" onKeyUp="numbersOnly(this)" /></td>
        </tr>
        <tr>
          <td valign="middle">CVV <img title="Three or four digit security code for your card" src="/images/question_blue.png"></td>
          <td valign="middle"><input class="form-control" id="cc_cvv" name="cc_ccv" type="text" size="4" autocomplete="off" data-encrypted-name="cvv"  onKeyUp="numbersOnly(this)" /></td>
        </tr>
        <tr>
          <td valign="middle">Expiration Month</td>
          <td valign="middle">
		  <select class="form-control" name="month" id="cc_expires_month" >
		    <option value="01" selected="selected">January</option>
			<option value="02">February</option>
			<option value="03">March</option>
			<option value="04">April</option>
			<option value="05">May</option>
			<option value="06">June</option>
			<option value="07">July</option>
			<option value="08">August</option>
			<option value="09">September</option>
			<option value="10">October</option>
			<option value="11">November</option>
			<option value="12">December</option>
		  </select>		  
		  </td>
		</tr>
		<tr>
		  <td valign="middle">Exipration Year</td>
		  <td valign="middle">
		  <select class="form-control" name="year" id="cc_expires_year">
			<? $year = date("Y"); ?>
			  <? do { ?>
					<option value="<? echo $year; ?>" <? if($year == date("Y")) echo ' selected="selected"'; ?>><? echo $year; ?></option>
			  <? 
					$year ++;
				} while ($year < date("Y") + 9);
			  ?>	
		  </select>
		  
		  </td>
        </tr>
  </table>
		<p>You are subscribing to a recurring payment agreement. By checking this box you agree to these terms. <input type="checkbox" id="agreedTo" name="agreedTo" value="1" /></p></td>
		<input type="hidden" value="" name="pid" id="pid"  />
		<input type="hidden" name="currDate" id="currDate" value="<? echo date("Ym"); ?>" />	
		<input type="submit" id="submit" value="Submit Payment" />

</form>	  
<div id="errorDiv" style="visibility:hidden; background-color:#FFE8E8; color:#FF0000; font-weight:bold; text-align:center; border:thin #750000 1px;" align="center">OOPS! An error occured.<br>Check your information and try again.</div>
<script src="https://js.braintreegateway.com/v1/braintree.js"></script>
<script>
	// sandbox
	var braintree = Braintree.create("MIIBCgKCAQEAnww8b1E5KYGIsDYHbyW06G7CsAf+gkUp3/x8go+EaqBiw9ZLjTqCwgze7Is+m1Wau8wn5QjA8NouOW9tik9IskoCYDF8IgHsp1IlGbKRo0gWSnBYcUoNU/PLA6O7Mqjqtep+i5wcrAxoyt89av65ZytYMeLwZCVG+5rxpUFLov6lXbLiJgtS+YdF5xkS+UYKcM/iD40Oefjo/U0SuUzxipTI1ryrqds3p8hYRPJ1IvP97taGIQf3mAnm3yB7m4HtlJS61tC3xYJFTge9DkNtRS9Pr4ta96A04g/x4n1dSG86qfd2TJOJpfj40vcYHBfgVtE1QdCYbl7fTZ+K7fKxKQIDAQAB");
    
	// production 
	//var braintree = Braintree.create('MIIBCgKCAQEArO6mDKhEP6AD4IvYCbD3dXP4Bu0EzGjODyYhVvORCQRP139imr3pFhRUWhHrky6ba7/jR0Oa4LYp5TnifAhPz7bzMrK+QWr3/YhoNYhkzR2vYY+kB7bKYUQyaGCCbJNRFIJ2eiiLsH5tguQFGv9DoU1/M//5uP92IzedHocZtaOQqVi0X47SK+4xAPQCJilcLfArvd5FCxBTG1oJdNROTKmVzPZitJOqYhr9ObSDTE8ruL1CGDvxaG85IE+eRu8ZyTjR/WYDMBxDTMkM7CsWL2sGZEMEgnl+1e4IAdQdR5/ZDwCqBLQn/goHB9ZF12d4vxrTQvUktYVXy25fvQvrGQIDAQAB');
	
	
	braintree.onSubmitEncryptForm('braintree-payment-form');
</script>
<script>
function numbersOnly(obj) {
    if (obj.value != obj.value.replace(/[^0-9\.]/g, '')) {
       obj.value = obj.value.replace(/[^0-9\.]/g, '');
    }
};

function validateForm() {
	var errors = '';
	x = document.getElementById('paymentPlan').value;
	if(x == 0) {
		errors+="Please select a payment plan.\n";
	}
	
	
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	var x = '';
	x = document.getElementById("number").value;
	if(x.length < 15 || x.length > 19 ) {
		errors+="CC Number length must be betwen 15 and 19 digits without any spaces or dashes.\n";
	}
	x = document.getElementById("cc_cvv").value;
	if(x.length != 3 && x.length != 4 ) {
		errors+="Please enter the CVV Number without any spaces or dashes.\n";
	}		
	var year = document.getElementById("cc_expires_year").value	
	var month = document.getElementById("cc_expires_month").value 
	var currDate = document.getElementById("currDate").value 
	if(currDate > year+month) {
		errors+="Credit card is expired.\n";
	}
	if(document.getElementById("agreedTo").checked == false) {
		errors+="You must acknowledge the subscription terms.\n";
	}		
	if(errors) {
		alert(errors);
		return false;
	} else {
		return true;
	}	
}
</script>
<?	
if($_REQUEST['err'] ) {
?>
<script>
document.getElementById('errorDiv').style.visibility = 'visible';
</script>
<?
}
?>
</body>	  
</html>