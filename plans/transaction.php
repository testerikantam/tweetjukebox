<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<? 
require_once $_SERVER['DOCUMENT_ROOT'].'/braintree-php-3.1.0/lib/Braintree.php';
//  sandbox
Braintree_Configuration::environment('sandbox');
Braintree_Configuration::merchantId('zsbs7swsdwdzvmwg');
Braintree_Configuration::publicKey('yb7qkdry9qfvzbn9');
Braintree_Configuration::privateKey('36ae489f8f321abd38f0971f5f1ff2cf');
//// prodcution
//Braintree_Configuration::environment('production');
//Braintree_Configuration::merchantId('dxpnw5b448vwd9rp');
//Braintree_Configuration::publicKey('mg2pmbnsn3g4cqnq');
//Braintree_Configuration::privateKey('cf12d53af0f632d8fb98c91af4b86741');
?>
<?
$_SESSION['GatewayError'] = '';
if($_REQUEST['pid'] == 'Advanced-Monthly') $amount = ADVANCED_VERSION_MONTHLY_FEE;
if($_REQUEST['pid'] == 'Advanced-Annual') $amount = ADVANCED_VERSION_ANNUAL_FEE;

if($_REQUEST['pid'] == 'Pro-Monthly') $amount = PRO_VERSION_MONTHLY_FEE;
if($_REQUEST['pid'] == 'Pro-Annual') $amount = PRO_VERSION_ANNUAL_FEE;

if($_REQUEST['pid'] == 'Business-Monthly') $amount = BUSINESS_VERSION_MONTHLY_FEE;
if($_REQUEST['pid'] == 'Business-Annual') $amount = BUSINESS_VERSION_ANNUAL_FEE;

$planID = $_REQUEST['pid'] ;
$userID = $_SESSION['access_token']['user_id'];
$basePlan = substr($planID,0, strpos($planID, "-"));

$user_first_name = mysqli_real_escape_string($conn, $_POST['user_first_name']);
$user_last_name = mysqli_real_escape_string($conn, $_POST['user_last_name']);
$email = mysqli_real_escape_string($conn, $_POST['email']);

$datePaid = date("Y-m-d");


$SQL = "select * from users where user_id = '$userID' ";
$result = mysqli_query($conn, $SQL);
if(mysqli_num_rows($result) != 1 ) {
	$_SESSION['GatewayError'] = "User Validation Issue - ".$userID." Please logout and try again";
	header("Location: subscribe.php?err=1&pid=".$basePlan);	
	exit();
}	

$result = Braintree_Customer::create(array(
	'id' => $userID,
	'firstName' => $_POST['user_first_name'],
	'lastName' => $_POST['user_last_name'],
	'email' => $_POST['email'],
	'paymentMethodNonce' => $_POST['payment_method_nonce']
	)   
);

if($result->success ) { // we created a new customer and verified the cc
	$paymentMethodToken = $result->customer->paymentMethods[0]->token;
	$result = Braintree_Subscription::create(array(
	  'paymentMethodToken' => $paymentMethodToken,
	  'planId' => $planID
	));
			
	if ($result->success) {
		$plan = explode("-", $planID);
		$userPlan = $plan[0];
		$bt_plan_id = $result->subscription->id;
		
		$SQL = "update users set status = '1', email = '$email', user_first_name = '$user_first_name', user_last_name = '$user_last_name', bt_plan_id = '$bt_plan_id', planName = '$planID', master_account_id = '$userID', firstPaidDate = '$datePaid' where user_id = '$userID' ";
		$result = mysqli_query($conn, $SQL) or die(mysqli_error());
		$SQL = "update users set status = '1' where master_account_id = '$userID' ";
		$result = mysqli_query($conn, $SQL) or die(mysqli_error());
		$_SESSION['planName'] = $userPlan;
		$_SESSION['validated'] = 'validated';
		$_SESSION['loggedInAs'] = $userID;
	} else {  // something went wrong creating the subscription - this shouldn't happen 	
		foreach($result->errors->deepAll() AS $error) {
			echo($error->code . ": " . $error->message . "\n");
		}
		$_SESSION['GatewayError'] = "Unknown Error";
		header("Location: subscribe.php?err=1&pid=".$basePlan);	
		exit();
	}			
} else {	// something went wrong creating customer / see if the customer already exists - if not then the payment method did not verify		
	try { 
		$customer = Braintree_Customer::find($userID);
	} catch (Exception $e) {
		$_SESSION['GatewayError'] = "Processor Declined";
		header("Location: subscribe.php?err=1&pid=".$basePlan);
		exit();
	}
	$x = 0;
	$subscriptionID = '';
	foreach ($customer->paymentMethods as $joking) {
		$y = 0;
		foreach($customer->paymentMethods[$x]->subscriptions as $stillJoking) {
			if($customer->paymentMethods[$x]->subscriptions[$y]) {
				if($customer->paymentMethods[$x]->subscriptions[$y]->status != 'Canceled' ) {
					$subscriptionID = $customer->paymentMethods[$x]->subscriptions[$y]->id;
				}
			}
			$y++;
		}	
		$x ++;				
	}

$paymentMethod = null;
if (isset($_POST['saved_payment_method_token']) && !empty($_POST['saved_payment_method_token'])) {
    $paymentMethod = Braintree_PaymentMethod::find($_POST['saved_payment_method_token']);
} else {
    $result = Braintree_PaymentMethod::create([
        'customerId' => $userID,
        'paymentMethodNonce' => $_POST['payment_method_nonce'],
        'options' => [
            'makeDefault' => true
        ]
    ]);

    foreach ($customer->paymentMethods as $paymentMethod) {
        if (((
                $paymentMethod instanceof Braintree_CreditCard
                && $result->paymentMethod instanceof Braintree_CreditCard
                && $paymentMethod->uniqueNumberIdentifier == $result->paymentMethod->uniqueNumberIdentifier
            ) ||(
                $paymentMethod instanceof Braintree_PayPalAccount
                && $result->paymentMethod instanceof Braintree_PayPalAccount
                && $paymentMethod->email == $result->paymentMethod->email)
            )
            && $paymentMethod->token != $result->paymentMethod->token
        ) {
            $deleteResult = Braintree_PaymentMethod::delete($result->paymentMethod->token);
            $_SESSION['GatewayError'] = "You already have this card attached to your account.";
            header("Location: subscribe.php?err=1&pid=".$basePlan);
            exit();
        }
    }

    $paymentMethod = $result->paymentMethod;
}
	if($paymentMethod) {
		$paymentMethodToken = $paymentMethod->_attributes['token'];
		if($subscriptionID == '') {
			$result = Braintree_Subscription::create(array(
			'paymentMethodToken' => $paymentMethodToken,
			'planId' => $planID
			));												
		} else {
			$result = Braintree_Subscription::update($subscriptionID, array(
				'paymentMethodToken' => $paymentMethodToken,
				'planId' => $planID,
				'price' => $amount
			));
		}

		if($result->success) { // we be good 			
			$plan = explode("-", $planID);
			$userPlan = $plan[0];
			$bt_plan_id = $result->subscription->id;
			$SQL = "update users set status = '1', email = '$email', user_first_name = '$user_first_name', user_last_name = '$user_last_name', bt_plan_id = '$bt_plan_id', planName = '$planID', master_account_id = '$userID', firstPaidDate = '$datePaid' where user_id = '$userID'  ";
			$result = mysqli_query($conn, $SQL) or die(mysqli_error());
			$SQL = "update users set status = '1' where master_account_id = '$userID' ";
			$result = mysqli_query($conn, $SQL) or die(mysqli_error());
			$_SESSION['planName'] = $userPlan;
			$_SESSION['validated'] = 'validated';
			$_SESSION['loggedInAs'] = $userID;
		} else {  // something went wrong creating the subscription - this shouldn't happen 	
			echo "subscription error<br>";
			foreach($result->errors->deepAll() AS $error) {						
				echo($error->code . ": " . $error->message . "\n");
				//exit();
			}
			header("Location: subscribe.php?err=1&pid=".$basePlan);
			exit();			
		}
	} else {
		$_SESSION['GatewayError'] = "Payment Method Error";
		header("Location: subscribe.php?err=1&pid=".$basePlan);
		exit();
	}	
}		
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
<link rel="shortcut icon" href="/favicon.ico?v=2">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Tweet Jukebox</title>
<meta name="description" content="Tweet Jukebox - Thinking Inside the Box">
<!-- BOOTSTRAP STYLES-->
<link href="/assets/css/bootstrap_v3.3.4.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="/assets/css/font-awesome.css" rel="stylesheet" />
<!-- MORRIS CHART STYLES-->
<link href="/assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="/assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<link href="/members/chart.css" rel="stylesheet" type="text/css">
<!-- JQUERY SCRIPTS -->
<script src="/assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="/assets/js/bootstrap_v3.3.4.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="/assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="/assets/js/custom.js"></script>
<script src="/include/js_boot.js"></script>
<link rel="stylesheet" href="/highslide-4.1.13/highslide/highslide.css" type="text/css" />
<!-- 9d83e18a764256552baf6956b777f190 -->
<script type="text/javascript" src="/highslide-4.1.13/highslide/highslide-full.packed.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/highslide-4.1.13/highslide/graphics/';
	hs.showCredits = false;
    hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = .45;
	hs.zIndexCounter = 2000;
//	hs.objectLoadTime = 'after';
//	hs.forceAjaxReload = 'true';
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64707958-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="wrapper">	    
	<nav class="navbar navbar-default navbar-cls-top navbar-collapse" role="navigation" style="margin-bottom: 0">
    <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
		<ul class="nav navbar-nav navbar-cls-top" style="cursor:pointer;">
            <? require_once($_SERVER['DOCUMENT_ROOT']."/include/mainNav.php") ; ?>
		</ul>
	</div>
</div>
<div class="container">
<div class="row">
<div class="col-lg-2"></div>
<div class="col-lg-8">
	<h2 align="center"> Payment Receipt </h2>
	<p>&nbsp;</p>
	<p align="center">Thank you for subscribing in the <? echo $planID; ?> plan!</p>
	<p>We're glad you joined us and we're excited to help you make the most of your Twitter&#8482; account.  </p>
	<p>Your payment has been successfully processed and you are now enrolled in a subscription plan that includes all the benefits as described on our website.</p>
	<p>&nbsp; </p>
	<p>&nbsp;</p>
</div>
<div class="col-lg-2"></div>
</div>
</div>
<div class="container" >
	<div class="row" style="padding-top:50px;">
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<p align="center">
   			<span class="left">tj.local &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> | <!--a href="/plans.php">PLANS</a> | --><a href="/contact">CONTACT US</a> | <a href="/privacy">PRIVACY POLICY</a> | <a href="/terms">TERMS AND CONDITIONS</a>
			</p>
		</div>	
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
	</div>
</div>
</body>
</html>