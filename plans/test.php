<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<? 
require_once $_SERVER['DOCUMENT_ROOT'].'/braintree-php-3.1.0/lib/Braintree.php';
//  sandbox 
Braintree_Configuration::environment('sandbox');
Braintree_Configuration::merchantId('zsbs7swsdwdzvmwg');
Braintree_Configuration::publicKey('yb7qkdry9qfvzbn9');
Braintree_Configuration::privateKey('36ae489f8f321abd38f0971f5f1ff2cf');
// prodcution
//Braintree_Configuration::environment('production');
//Braintree_Configuration::merchantId('ry95gz2bg2g4xd9s');
//Braintree_Configuration::publicKey('3xmty3qwmphwjfs9');
//Braintree_Configuration::privateKey('06502b4f7b1d9893d8bd88e7e6f02a2c');
?>
<?
$clientToken = Braintree_ClientToken::generate([ ]);
?>
<?														//   for now this is only good for current members
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');

$pid = $_REQUEST['pid'];
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
<script src="../assets/js/jquery-1.10.2.js"></script>
<link rel="stylesheet" href="/assets/fonts/raphaelicons.css">
<link href="/assets/css/bootstrap_v3.3.4.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="/assets/css/font-awesome.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="/assets/css/custom.css" rel="stylesheet" />
<style>
td { padding-bottom:5px; }
</style>
</head>
<html>
<body>
<span class="member-icon" style="font-size:24px; color:#00FF00; float:right;">W</span>
<img src="/images/tjbIcon_64.png" border="0" style="float:left" />	
<div class="container">
<div class="row">
<div class="col-lg-3"></div>
<div class="col-lg-6">
	<h2 align="center">Secure Payment Form</h2>
	<p>Once you submit your payment, your access will remain valid on an ongoing basis until your membership is canceled.</p>
	<p>If you wish to cancel your membership you may do so by clicking on the "Cancel My Membersip" button on your control panel, or by emailing us with a notice to cancel.</p>
	<form class="form-group" action="transaction.php" method="post" id="paymentForm" accept-charset="utf-8">
	<p>You are subscribing to a recurring payment agreement. By checking this box you agree to these terms. <input type="checkbox" id="agreedTo" name="agreedTo" value="1" /></p>
		<input type="hidden" value="" name="pid" id="pid"  />
		<input type="hidden" value="" name="paymentType" id="paymentType" />
		<input type="hidden" name="currDate" id="currDate" value="<? echo date("Ym"); ?>" />
		<table class="table-responsive" style="border:0; width:100%;">
		<tr>
		  <th valign="middle"><? echo $pid. " membership"; ?> Payment Plan</th>	
		  <td valign="middle">
          	<select class="form-control"  id="paymentPlan" name="paymentPlan" onChange="document.getElementById('pid').value = this.value;">
				<option value="0">Select Billing Cycle</option>
			<?
				if($pid == 'Basic') {		
			?>
				<? if($_SESSION['billingCycle'] == 'Monthly') { ?>
				<option value="Basic-Monthly">$<? echo BASIC_VERSION_MONTHLY_FEE; ?> / month</option>
				<? } ?>
				<? if($_SESSION['billingCycle'] == 'Annual') { ?>
				<option value="Basic-Annual">$<? echo BASIC_VERSION_ANNUAL_FEE; ?> / year save $<? $fullYear = (float)BASIC_VERSION_MONTHLY_FEE *12 ; $annual = (float)BASIC_VERSION_ANNUAL_FEE; echo number_format($fullYear - $annual, 2);   ?></option>
				<? } ?>
			<? } ?>
			<?
				if($pid == 'Pro') {		
			?>
				<? if($_SESSION['billingCycle'] == 'Monthly') { ?>
				<option value="Pro-Monthly">$<? echo PRO_VERSION_MONTHLY_FEE; ?> / month</option>
				<? } ?>
				<? if($_SESSION['billingCycle'] == 'Annual') { ?>
				<option value="Pro-Annual">$<? echo PRO_VERSION_ANNUAL_FEE; ?> / year save $<? $fullYear = (float)PRO_VERSION_MONTHLY_FEE *12 ; $annual = (float)PRO_VERSION_ANNUAL_FEE; echo number_format($fullYear - $annual, 2);   ?></option>
				<? } ?>
			<? } ?>	
			<?
				if($pid == 'Business') {		
			?>
				<? if($_SESSION['billingCycle'] == 'Monthly') { ?>
				<option value="Business-Monthly">$<? echo BUSINESS_VERSION_MONTHLY_FEE; ?> / month</option>
				<? } ?>
				<? if($_SESSION['billingCycle'] == 'Annual') { ?>
				<option value="Business-Annual">$<? echo BUSINESS_VERSION_ANNUAL_FEE; ?> / year save $<? $fullYear = (float)BUSINESS_VERSION_MONTHLY_FEE *12 ; $annual = (float)BUSINESS_VERSION_ANNUAL_FEE; echo number_format($fullYear - $annual, 2);   ?></option>
				<? } ?>
			<? } ?>	
			</select>	
		  </td>
        </tr>
		<tr>
          <th valign="middle">First Name</th>
          <td valign="middle"><input class="form-control" id="user_first_name" name="user_first_name" type="text" value="<? echo $_SESSION['user_first_name'];?>" /></td>
        </tr>
		<tr>
          <th valign="middle">Last Name</th>
          <td valign="middle"><input class="form-control" id="user_last_name" name="user_last_name" type="text" value="<? echo $_SESSION['user_last_name'];?>" /></td>
        </tr>
		<tr>
          <th valign="middle">Last Name</th>
          <td valign="middle"><input class="form-control" id="email" name="email" type="text" value="<? echo $_SESSION['email'];?>" /></td>
        </tr>		
		<tr>
			<td colspan="2" align="center" height="40px;"><b>Pay with PayPal Or Credit Cart</b></td>
		</tr>
		<tr>
			<td colspan="2" id="payment-form"></td>
		</tr>
  		</table>			
		<center><button type="submit" class="button" value="Submit Payment" >Submit Payment</button></center>
	</form>
</div>
<div class="col-lg-3"></div>	
</div>
<div id="errorDiv" style="visibility:hidden; background-color:#FFE8E8; color:#FF0000; font-weight:bold; text-align:center; border:thin #750000 1px;" align="center">OOPS! An error occured.<br>Check your information and try again.</div>
</div>
<script>
function numbersOnly(obj) {
    if (obj.value != obj.value.replace(/[^0-9\.]/g, '')) {
       obj.value = obj.value.replace(/[^0-9\.]/g, '');
    }
};

function validateForm() {	
	if($('#paymentPlan').val() == '0') {
		errors+="Please select a valid payment cycle.\n"
	}
	if(document.getElementById("agreedTo").checked == false) {
		errors+="You must acknowledge the subscription terms.\n";
	}
	if(errors) {
		alert(errors);
		return false;
	} else {
		alert('here');
		$("#paymentForm").submit();
	}
}

function showPaymentType(t){
	if(t == '0') {
		$("#paypalRow").css('visibility', 'hidden');
		$("#ccPaymentDiv").css('display', 'none');
	}
	if(t == 'PayPal') {
		$("#paypalRow").css('visibility', 'visible');
		$("#ccPaymentDiv").css('display', 'none');
	}
	if(t == 'CC') {
		$("#paypalRow").css('visibility', 'hidden');
		$("#ccPaymentDiv").css('display', 'block');
	}
	$('#paymentType').val(t);
}
</script>
<?	
if($_REQUEST['err'] ) {
?>
<script>
document.getElementById('errorDiv').style.visibility = 'visible';
</script>
<?
}
?>
<script src="https://js.braintreegateway.com/v2/braintree.js"></script>
<script>
var formErrors = '';
braintree.setup("<? echo $clientToken; ?>", "dropin", {
  container: "payment-form",
  form: "paymentForm",
  onPaymentMethodReceived: function (obj) {
	formErrors = '';
	if($('#paymentPlan').val() == '0') {
	formErrors+="Please select a valid payment cycle.\n"
	}
	if(document.getElementById("agreedTo").checked == false) {
		formErrors+="You must acknowledge the subscription terms.\n";
	}
	if(formErrors) {
		alert(formErrors);
		//return false;
	} else {
		//alert('here');
		$("#paymentForm").submit();
	}		
  }	
});
</script>
<script>
	// sandbox
	var braintree = Braintree.create("MIIBCgKCAQEAnww8b1E5KYGIsDYHbyW06G7CsAf+gkUp3/x8go+EaqBiw9ZLjTqCwgze7Is+m1Wau8wn5QjA8NouOW9tik9IskoCYDF8IgHsp1IlGbKRo0gWSnBYcUoNU/PLA6O7Mqjqtep+i5wcrAxoyt89av65ZytYMeLwZCVG+5rxpUFLov6lXbLiJgtS+YdF5xkS+UYKcM/iD40Oefjo/U0SuUzxipTI1ryrqds3p8hYRPJ1IvP97taGIQf3mAnm3yB7m4HtlJS61tC3xYJFTge9DkNtRS9Pr4ta96A04g/x4n1dSG86qfd2TJOJpfj40vcYHBfgVtE1QdCYbl7fTZ+K7fKxKQIDAQAB");   
	// production 
	//var braintree = Braintree.create('MIIBCgKCAQEArO6mDKhEP6AD4IvYCbD3dXP4Bu0EzGjODyYhVvORCQRP139imr3pFhRUWhHrky6ba7/jR0Oa4LYp5TnifAhPz7bzMrK+QWr3/YhoNYhkzR2vYY+kB7bKYUQyaGCCbJNRFIJ2eiiLsH5tguQFGv9DoU1/M//5uP92IzedHocZtaOQqVi0X47SK+4xAPQCJilcLfArvd5FCxBTG1oJdNROTKmVzPZitJOqYhr9ObSDTE8ruL1CGDvxaG85IE+eRu8ZyTjR/WYDMBxDTMkM7CsWL2sGZEMEgnl+1e4IAdQdR5/ZDwCqBLQn/goHB9ZF12d4vxrTQvUktYVXy25fvQvrGQIDAQAB');	
	//braintree.onSubmitEncryptForm('braintree-payment-form');
</script>
</body>	  
</html>