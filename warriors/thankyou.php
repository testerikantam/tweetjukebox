<?  
include ($_SERVER['DOCUMENT_ROOT']."/mc/Mailchimp.php"); 
include($_SERVER['DOCUMENT_ROOT']."/include/mail/class.phpmailer.php");		
$connection = new Mailchimp();
$list = new Mailchimp_Lists($connection);	
$id = '7d7eba6afe';
$email = array('email'=>$_POST['EMAIL']);	
$memberInfo =  array("NAME" => $_POST['NAME']);		
	
$result = $list->memberInfo($id, array($email));	

if($result['success_count'] == '1') {
	$status = "Duplicate Email";	
} else {
	$email = array("email" => $_POST['EMAIL']);	
	$result = $list->subscribe($id, $email, $memberInfo , 'html', 'false', 'false', 'true', 'false'); 
}	
// send it anyway
$status = "Book Sent";			
$mail = new PHPMailer();
$mail->IsSendmail();									// use sendmail
$mail->From = "no_reply@tweetjukebox.com";
$mail->FromName = "no_reply@tweetjukebox.com";
$mail->AddAddress($_POST['EMAIL'], $_POST['EMAIL']);
//$mail->AddBCC(SITE_EMAIL, SITE_EMAIL);
//$mail->AddAddress("ellen@example.com");                  // name is optional
$mail->AddReplyTo("no_reply@tweetjukebox.com");
$mail->WordWrap = 50;                                 // set word wrap to 50 characters
$mail->AddAttachment($_SERVER['DOCUMENT_ROOT']."/warriors/j8s70s/AlphabetSuccess.pdf");         // add attachments
//$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
$mail->IsHTML(true);                                  // set email format to HTML
$mail->Subject = "Alphabet Success";
$mail->Body = "Hi there,<p>Thanks for downloading Alphabet Success - Keeping it Simple. I appreciate you taking the time to read my book.</p>
<p>As you read the book, you're likely to say, 'I know this'. The question is, do you do it?</p>
<p>If not, figure out a way to start!</p>
<p>In addition, I would love to get your feedback. You can reach me at tim@tweetjukebox.com with any questions or comments you may have.</p>
<p>My very best wishes to you.</p>
<br><br>
Tim Fargo<br>
President<br>
tweetjukebox.com";

if(!$mail->Send())
{
   echo "Message could not be sent. <p>";
   echo "Mailer Error: " . $mail->ErrorInfo;
   $status = "Mail error";
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Tweetjukebox Warriors</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="responsive/bootstrap.min.css" >
  <script src="/responsive/jquery.min.js"></script>
  <script src="/responsive/bootstrap.min.js"></script>
</head>
<body>

<div class="container" style="padding-top:20px; padding-bottom:20px;">
  <div  class="img-rounded text-center">
	<h1>Alphabet Success</h1> 
	<p>For a limited time, get your free copy of Alphabet Success.</p> 
  </div>
</div>


<div id="header">
	<div class="container">
		<div class="row">
			<div class="col-lg-1"></div>
			<div class="col-lg-5">
			<? if($status == 'Book Sent') { ?>
				<h1>Thank you!<br>Your free copy is on the way. </h1>
				<h2>I hope you enjoy it! </h2>	
			<? } ?>				
			<? if($status == 'Mail error') { ?>
				<h1>OOPS, Something went wrong trying to email your copy!</h1>
				<h2>Please go back and try again!</h2>	
			<? } ?>
				<h2>Want to double down?</h2>
				<h3>Get a FREE trial for Tweet Jukebox</h3>
				<p><a href="/TOC/redirect.php"><img src="/images/sign-in-with-twitter-gray.png" alt="Sign in with Twitter" class="img-responsive" /></a></p>
			</div>
			<div class="col-lg-5">
				<img src="images/abc.jpg" class="img-responsive" />
			</div>
			<div class="col-lg-1"></div>				
		</div>
	</div>
</div>
<?
//print "<hr>";
//print "<pre>";
//print_r($result);
?>	
</body>
</html>
