<? include ($_SERVER['DOCUMENT_ROOT']."/include/config.php") ; ?>
<?
if($_SESSION['isAdmin'] != 1 ) {
	header("Location: /oops.php");
	exit();	
}
?>
<?
if($_SESSION['isAdmin'] == 1  && $_POST['loginUser']) {
	$loginUser = $_POST['loginUser'];
	$SQL = "select * from users where user_id = '$loginUser' ";  
	$result = mysqli_query($conn, $SQL) or die(mysqli_error());
	$row = mysqli_fetch_assoc($result);	
	session_destroy();
	session_start();
	$_SESSION['access_token']['oauth_token'] = $row['oauth_token'];
	$_SESSION['access_token']['oauth_token_secret'] = $row['oauth_token_secret'];
	$_SESSION['access_token']['user_id'] = $row['user_id'];
	$_SESSION['access_token']['screen_name'] = $row['screen_name'];						
	$_SESSION['screen_name'] = $row['screen_name'];	
	$_SESSION['email'] = $row['email'];								
	$_SESSION['validated'] = 'validated';
	
	$_SESSION['user_first_name'] = stripslashes($row['user_first_name']);
	$_SESSION['user_last_name'] = stripslashes($row['user_last_name']);
	
	
	$_SESSION['expireDate'] = date('m/d/Y', strtotime($row['expireDate']));			
	
	header("location: /members/");
	exit();
}
?>
<?
$maxRows = 10;
$pageNum = 0;
$currentPage = $_SERVER['QUERY_STRING'];

if (isset($_GET['pageNum'])) {
  $pageNum = $_GET['pageNum'];
}
$startRow = $pageNum * $maxRows;


$SQL = "select * from users where status = 86 " ;					

if($_POST['searchScreenName']!='') $SQL .= " where screen_name like '%".$_POST['searchScreenName']."%' ";
if($_POST['searchEmail']!='') $SQL .= " where email like '%".$_POST['searchEmail']."%' ";

$sortBy = " group by users.user_id order by signupDate DESC, last_login DESC  ";
if($_REQUEST['s']) $sortBy = "order by ".$_REQUEST['s'];

$SQL .= $sortBy;					
$SQL2 = getPagingQuery($SQL, $maxRows ); 

$pagingLink = getPagingLink($SQL, $maxRows, $_SERVER['QUERY_STRING']);

$result = mysqli_query($conn, $SQL2);
//$row = mysqli_fetch_assoc($result);
?>
<!DOCTYPE html>
<html lang="en"><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title>Tweet Jukebox</title>
<!-- InstanceEndEditable -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link href="/css/font-awesome.min.css" rel="stylesheet">
  <link  href="http://fonts.googleapis.com/css?family=Oswald:regular" rel="stylesheet" type="text/css" >
  <link href='http://fonts.googleapis.com/css?family=Junge' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="/assets/fonts/raphaelicons.css">
  <link rel="stylesheet" href="/assets/css/main.css">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
      <script src="/js/respond.min.js"></script>
    <![endif]-->
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
</head>
<body>
<div class="container-fluid" style="padding-bottom:50px;">
<div class="navbar navbar-default navbar-fixed-top ">
  <div class="container">
    <div class="navbar-header"> <a href="/" class="navbar-brand TJBlogo">Tweet Jukebox</a>
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
    </div>
    <div class="navbar-collapse collapse" id="navbar-main">
      <ul class="nav navbar-nav">
        <li> <a href="jbAssignments.php">Jukebox Assignments</a></li>
      </ul>      
    </div>
  </div>
</div>
</div>
<!-- InstanceBeginEditable name="pageHeaderArea" -->
<div class="container">
  <div class="page-header" id="banner">
    <h1>Suspended Accounts</h1>	
	<a href="index.php"><button>Back To Admin</button></a>
  </div>
</div>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="pageContentArea" -->
<?
if(mysqli_num_rows($result)) {
	$row = mysqli_fetch_assoc($result); 
	do {
		foreach( $row AS $key => $val ) {
			$$key = stripslashes( $val );					
		}
		//if($status == '1') $status = "Active"; else $status = "Inactive";
		$status = "Suspended";
		if($send_thanks == '1') $thanks = "TYT on ".$thanks_limit; else $thanks = "TYT off";
		
		$SQL2 = "select count(scheduled_tweets.id) as scht from scheduled_tweets where user_id = '$user_id' ";
		$result2 = mysqli_query($conn, $SQL2);
		$row2 = mysqli_fetch_assoc($result2); 
		$scht = $row2['scht'];

?>
		<div class="container">
			<div class="row text-left">
				<div class="col-lg-2 col-md-2 col-sm-2">
					<? echo $screen_name;?><br />&nbsp;<? echo date("m/d/Y H:i:s", strtotime($signupDate)); ?><br><? echo $user_id;?>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3">
					
					<? if($status == 'Inactive' ) { echo $email;?><br><a href="#" onClick="verifyAction('activate', '<? echo $user_id; ?>');" >Activate This User</a> <? } ?>
					<? if($status == 'Suspended' ) { echo $email;?><br><a href="#" onClick="verifyAction('activate', '<? echo $user_id; ?>');" >Activate This User</a> <? } ?>
					<? if($status == 'Active' ) { echo $email;?><br><a href="#" onClick="verifyAction('deactivate', '<? echo $user_id; ?>');" >Deactivate This User</a> <? } ?>
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2"><? echo $user_first_name;?>	
				<? if($_SESSION['isAdmin'] =='1') {?>
						<form id="loginForm" action="index.php" method="post" enctype="application/x-www-form-urlencoded">	
						<input type="hidden" name="loginUser" id="loginUser" value="<? echo $user_id;?>"> 
						<input type="submit" value="login as user" >
						</form>
					<? } ?>									
				</div>
				<div class="col-lg-2 col-md-2 col-sm-2"><? echo $user_last_name;?></div>
				<div class="col-lg-3 col-md-3 col-sm-3">
					<font style="color:red;">
					<? echo $status;?></font>&nbsp;<? echo $jbs;?>/<? echo $jbson;?> JB's | <? echo $thanks; ?> | Sch: <? echo $scht; ?>
					<br />&nbsp;<? echo date("m/d/Y H:i:s", strtotime($last_login)); ?>
				</div>
			</div>
			<hr />			
		</div>
<? 
	}while($row = mysqli_fetch_assoc($result));
?>
	<div class="container">
		<div class="row text-center">
			<div class="col-lg-2 col-md-2 col-sm-2"></div>
			<div class="col-lg-8 col-md-8 col-sm-8"><? echo $pagingLink?></div>
			<div class="col-lg-2 col-md-2 col-sm-2"></div>
		</div>
	</div>
<?	
}		
?>	
<script>
function verifyAction(a, u) {
	if(confirm("Are you sure you want to "+a+" this user?")) {
		$.ajax({
			type: "get",
			url: "/members/updateUser.php?status="+a+"&u="+u,			
			dataType: "html"
		})
		.done(function( msg ) { 
			//alert(msg);
			$('#searchScreenName').val('');
			$('#searchEmail').val('');	
			window.location.reload(true);		
		});
	}
}

</script>
<!-- InstanceEndEditable -->
<div class="container" style="padding-top:50px;">
  <footer>
    <div class="row well">
      <div class="col-lg-4"> tj.local &copy; - <? echo date("Y"); ?> </div>
      <div class="col-lg-8 text-right"> <a href="/">HOME</a> | <a href="/about.php">ABOUT</a> | <a href="/contact.php">CONTACT US</a> | <a href="/privacy.php">PRIVACY POLICY</a> | <a href="/terms.php">TERMS AND CONDITIONS</a> | <a href="#">Goto Top</a> </div>
    </div>
  </footer>
</div>
<!-- InstanceBeginEditable name="pageModalArea" -->
<!--Login Modal-->

<!-- /.modal -->
<!-- InstanceEndEditable -->
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="../assets/js/bootstrap_v3.3.4.min.js"></script>
<script src="/include/js_min.js"></script>
</body>
<!-- InstanceEnd --></html>
