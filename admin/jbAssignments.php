<? include ($_SERVER['DOCUMENT_ROOT']."/include/config.php") ; ?>
<?
if($_SESSION['isAdmin'] != 1 ) {
	header("Location: /oops.php");
	exit();	
}
?>
<!DOCTYPE html>
<html lang="en"><!-- InstanceBegin template="/Templates/main.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="utf-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title>Tweet Jukebox</title>
<!-- InstanceEndEditable -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link href="/css/font-awesome.min.css" rel="stylesheet">
  <link  href="http://fonts.googleapis.com/css?family=Oswald:regular" rel="stylesheet" type="text/css" >
  <link href='http://fonts.googleapis.com/css?family=Junge' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="/assets/fonts/raphaelicons.css">
  <link rel="stylesheet" href="/assets/css/main.css">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
      <script src="/js/respond.min.js"></script>
    <![endif]-->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
</head>
<body>
<div class="container-fluid" style="padding-bottom:50px;">
<div class="navbar navbar-default navbar-fixed-top ">
  <div class="container">
    <div class="navbar-header"> <a href="/" class="navbar-brand TJBlogo">Tweet Jukebox</a>
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
    </div>
    <div class="navbar-collapse collapse" id="navbar-main">
      <ul class="nav navbar-nav">
        <li> <a href="jbAssignments.php">Jukebox Assignments</a></li>
      </ul>      
    </div>
  </div>
</div>
</div>
<!-- InstanceBeginEditable name="pageHeaderArea" -->
<div class="container">
  <div class="page-header" id="banner">
    <h1>Jukebox Assignments</h1>	
	<a href="index.php"><button>Back To Admin</button></a>
  </div>
</div>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="pageContentArea" -->
<div class="container">
	<div class="row">
		<div class="col-lg-6">
			<label for="fromUserName"> Enter From User </label>
			<input class="form-control" type="text" id="fromUserName" name="fromUserName" value="" onChange="findUserByName('from', this.value);" />
			<input type="hidden" id="fromUserID" name="fromUserID" value="0">
		</div>
		<div class="col-lg-6"></div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<label for="jbList">Jukeboxes</label>
			<div id="jbList">
				<select id="jbToMove" name="jbToMove"  class="form-control">
					<option value="0">Select Jukebox</option>
				</select>	
			</div>
		</div>
		<div class="col-lg-6"></div>		
	</div>
	<div class="row">	
		<div class="col-lg-6">
			<label for="toUserName"> Enter To User </label>
			<input class="form-control" type="text" id="toUserName" name="toUserName" value="" onChange="findUserByName('to', this.value);" />
			<input type="hidden" id="toUserID" name="toUserID" value="0">
		</div>
		<div class="col-lg-6"></div>
	</div>
	<hr>
	<div class="row">
		<div class="col-lg-6">			
			<a href="#" onClick="assignJB();"><button>Assign</button></a>
		</div>
		<div class="col-lg-6"></div>
	</div>
</div>
<script>
function findUserByName(t, n) {
	$.ajax({
  		type: "post",
  		url: "/admin/findUserByName.php",
		data: {'userType': t, 'userName': n} ,
		dataType: "html"
	})
  	.done(function( msg ) { 
		if(msg == 'NOT FOUND') {
			alert('That user is inactive or not found');
			if(t == 'to')   $('#toUserID').val('0');
			if(t == 'from') $('#fromUserID').val('0');			
			return;
		}	
		
		if(t=='from') { // this is the from account so we have a list of JB's and name
			stuff = msg.split('~~~');
			$('#fromUserID').val(stuff[0]);
			$('#jbList').html(stuff[1]);	
		}
		
		if(t=='to') {
			$('#toUserID').val(msg);	
		}				
  	});	
}

function assignJB() {
	if($('#jbToMove').val() == 0) {
		alert("You must select a JB to assign");
		return;
	}					
	if($('#toUserID').val() == '0') {
		alert("You must select a TO user");
		return;
	}
	
	
	$.ajax({
		type: "post",
		url: "/admin/assignJB.php",
		data: {
			'jbID': $('#jbToMove').val(),
			'userID': $('#toUserID').val(),
			'assignee': $('#fromUserID').val()
		} ,
		dataType: "html"
	})
	.done(function( msg ) { 								
		alert("Assigment Completed");
		location.reload();	
	});			
	
}
</script>


<!-- InstanceEndEditable -->
<div class="container" style="padding-top:50px;">
  <footer>
    <div class="row well">
      <div class="col-lg-4"> tj.local &copy; - <? echo date("Y"); ?> </div>
      <div class="col-lg-8 text-right"> <a href="/">HOME</a> | <a href="/about.php">ABOUT</a> | <a href="/contact.php">CONTACT US</a> | <a href="/privacy.php">PRIVACY POLICY</a> | <a href="/terms.php">TERMS AND CONDITIONS</a> | <a href="#">Goto Top</a> </div>
    </div>
  </footer>
</div>
<!-- InstanceBeginEditable name="pageModalArea" -->
<!-- InstanceEndEditable -->
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="../assets/js/bootstrap_v3.3.4.min.js"></script>
<script src="/include/js_min.js"></script>
</body>
<!-- InstanceEnd --></html>