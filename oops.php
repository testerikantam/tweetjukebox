<?php  include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Tweet Jukebox</title>
<!-- BOOTSTRAP STYLES-->
<link href="../assets/css/bootstrap_v3.3.4.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="../assets/css/font-awesome.css" rel="stylesheet" />
<!-- MORRIS CHART STYLES-->
<link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="../assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="jquery.timepicker.css" />
<link href="/members/chart.css" rel="stylesheet" type="text/css">
<!-- JQUERY SCRIPTS -->
<script src="../assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../assets/js/bootstrap_v3.3.4.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="../assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../assets/js/custom.js"></script>
<script src="/include/js_boot.js"></script>
<script type="text/javascript" src="jquery.timepicker.js"></script>
<link rel="stylesheet" href="/highslide-4.1.13/highslide/highslide.css" type="text/css" />
<!-- 9d83e18a764256552baf6956b777f190 -->
<script type="text/javascript" src="/highslide-4.1.13/highslide/highslide-full.packed.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/highslide-4.1.13/highslide/graphics/';
	hs.showCredits = false;
    hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = .45;
	hs.zIndexCounter = 2000;
//	hs.objectLoadTime = 'after';
//	hs.forceAjaxReload = 'true';
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64707958-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="wrapper">
  <nav class="navbar navbar-default navbar-cls-top navbar-collapse" role="navigation" style="margin-bottom: 0">
  <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;">
    <ul class="nav navbar-nav navbar-cls-top" style="cursor:pointer;">
      <li><a href="/"><i class="fa fa-home"></i> Home </a></li>
      <li><a href="/about"> About </a></li>
      <li><a href="/contact"> Contact us </a></li>
      <li><a href="/faq"><i class="fa fa-info-circle"></i> FAQ </a></li>
	<li><a href="/members/logout.php"><i class="fa fa-sign-out"></i> Log Out</a></li>
    </ul>
  </div>
</div>
<div class="container" >
  <div class="row">
		<div class="col-md-4 col-sm-4 col-xs-4"></div>
		<div class="col-md-4 col-sm-4 col-xs-4">			
			<p align="center">
			<img src="images/tweetjukebox3.png" class="img-responsive" >
			</p>			
		</div>
		<div class="col-md-4 col-sm-4 col-xs-4"></div>
	</div>
  <div class="row">
    <div class="col-md-1 col-sm-1 col-xs-1"></div>
    <div class="col-md-10 col-sm-10 col-xs-10 text-center">
      <section role="banner">
        <hgroup>
          <h1>OOPS - Something went wrong!</h1>
        </hgroup>
        <article role="main" class="clearfix contact"> </article>
        <article role="main" class="clearfix contact">
          <div class="post" style="width:auto;">
            <div align="center"> </div>
            <? if($_REQUEST['err'] == 'cancelled') { ?>
            <h2 align="center">Your membership is currently inactive&nbsp; :( </h2>
            <p>You are seeing this because your plan was cancelled due to lack of payment or because you requested to cancel it.</p>            
            <?	
				$user_id = $_SESSION['access_token']['user_id'];
				$SQL = "select * from users where user_id = '$user_id' ";
				$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
				$row = mysqli_fetch_assoc($result);
				//if($row['master_account_id'] == $user_id || $row['master_account_id'] == '') {
			?>
				<h3>If you would like to renew your subscription you may <a href="/plans/">review our plans here---> Plans</a></h3>			
			<p style="padding-top:20px;">If you think this is not correct, please contact us!</p>
            <p><a href="mailto:tim@tweetjukebox.com"><i class="fa fa-envelope-o fa-2x"></i>&nbsp;tim[at]TweetJukebox[dot]com</a>&nbsp; </p>
            <p>&nbsp;</p>	
			<?
			} 
			?> 
			<? 
			if($_REQUEST['err'] == 'expired') { 
				session_start();
				session_destroy();
			?>
            <h2 align="center">Your account expired&nbsp; :( </h2>
            <p>If you are seeing this it is because your session may have expired.&nbsp; </p>
            <p align="center">Just login again and everything should be back to normal!</p>
            <p>If you think to get this page and didn't do anything wrong, please contact us!</p>
            <p><a href="mailto:tim@tweetjukebox.com"><i class="fa fa-envelope-o fa-2x"></i>&nbsp;tim[at]TweetJukebox[dot]com</a>&nbsp; </p>
            <p>&nbsp;</p>
            <?	} ?>                        
			<? 
			if($_REQUEST['err'] == 'expired') {
				echo '<h1>It looks like your account expired or was cancelled!</h1>'; 
				//session_start();
				session_destroy(); 
			}	
			?>
            <? 
			if($_REQUEST['err'] == 'maxJ') {
				echo '<h1>You have reached your total Jukebox tweet limit!</h1>'; 
				echo '<p align="center">We added as many as we could up to your Plan Limit!</p>';
				//session_start();
				//session_destroy();
			}
			?>
            <?
			if($_REQUEST['err'] =='') { 
				//session_start();
				session_destroy();
			?>
            <!--h2>Something went wrong!&nbsp; </h2-->
            <p>If you are seeing this something went wrong.&nbsp; Perhaps your session timed out or you requested a page that doesn't exist, or we did something wrong.&nbsp; We're not above making mistakes you know.</p>
            <!--p>If you think you got this page and didn't do anything wrong, please contact us and tell us why you think so.</p-->
            <p>As a safety precaution, we have logged you out. </p>
            <p><a href="mailto:info@tweetjukebox.com">info[at]TweetJukebox[dot]com</a>&nbsp; </p>
            <p>&nbsp;</p>
            <? 					
			} 										
			?>
			<? 	
			if($_REQUEST['err'] =='pending') { 
				//session_start();
				session_destroy();
			?>
			<h2>ATTENTION</h2>
			<p>Your account is currently suspended. If you feel this message is in error, please contact us at info@tweetjukebox.com.</p>
			<? } ?>
			
			<? 	
			if($_REQUEST['err'] =='sjb') { 
				//session_start();
				session_destroy();
			?>
			<h2>ATTENTION</h2>
			<h3>You've authorized Social Jukebox and are trying to login to Tweet Jukebox.<br>Please login to Social Jukebox <a href="/TOC/redirect.php">Here</a></h3>
			<? } ?>	
						
          </div>
        </article>
      </section>
    </div>
    <div class="col-md-1 col-sm-1 col-xs-1"></div>
  </div>
</div>
<div class="container" >
  <div class="row" style="padding-top:50px;">
    <div class="col-md-3 col-sm-3 col-xs-3"></div>
    <div class="col-md-6 col-sm-6 col-xs-6">
      <? if($_SESSION['ENTRY_DOMAIN'] == 'sjb') { ?>
	  	<p align="center"> <span class="left">www.SocialJukebox.com &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> |
	  <? } else { ?>
	  	<p align="center"> <span class="left">tj.local &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> |
	  <? } ?>
        <!--a href="/plans.php">PLANS</a> | -->
        <a href="/contact">CONTACT US</a> | <a href="/privacy">PRIVACY POLICY</a> | <a href="/terms">TERMS AND CONDITIONS</a> </p>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-3"></div>
  </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
<script src="assets/js/script.js"></script>
</body>
</html>