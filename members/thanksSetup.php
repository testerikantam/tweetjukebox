<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/twitteroauth/twitteroauth.php");
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/config.php");
?>
<?
if(!isset($_SESSION['access_token'])) die();

// get users last params
$user_id = $_SESSION['access_token']['user_id'];

$screen_name = $_SESSION['screen_name'];
//$access_token = $_SESSION['access_token'];
//$user_id = $_SESSION['access_token']['user_id'];

$SQL = "select * from users where user_id = '$user_id'  ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$row = mysqli_fetch_assoc($result);	
$send_thanks = $row['send_thanks'];
$thanks_limit = $row['thanks_limit'];

if($thanks_limit == 0 ) $thanks_limit = '1';

?>
<div id="sayThanksHelp">
	<div align="center">
	  <h3>You have the power to  send out thank you tweets every Friday!</h3>
	</div>
	<p>
	Friday we will grab all unique users that mentioned you during the prior week and construct a thank you tweet for the number of users you want to thank.  </p>
	<p>
	You don't need to do anything except to opt in and tell us how many unique users you want to thank (up to 50). </p>
	<p align="center" id="saveThanksSettingMessage" class="ui-state-highlight" style="visibility:hidden;">&nbsp;</p>
</div>
	<h2 align="center">Say thanks to unique users that have mentioned you.</h2>
	<form id="thanksSetupForm" name="thanksSetupForm" method="post" enctype="multipart/form-data" accept-charset="utf-8">	
    <table align="center" style="background:#FFFFFF;">
		<tr valign="middle" style="background:#FFFFFF;">
			<td align="right" style="background:#FFFFFF;">Send Thank You Tweets? </td>
			<td style="background:#FFFFFF;"><select name="send_thanks" id="send_thanks">
			  <option value="1" <? if($send_thanks == '1') echo 'selected="selected"' ; ?>>Yes</option>
			  <option value="0" <? if($send_thanks == '0' || $send_thanks == '') echo 'selected="selected"' ; ?>>No</option>
			  </select>
			</td>
		</tr>
		<tr valign="middle" style="background:#FFFFFF;">
		  <td id="thanksLimitLabel" align="right" style="background:#FFFFFF;">Max Number Of Unique Users <br />
	      <strong>*  (max of 50)</strong>: </td>
		  <td style="background:#FFFFFF;"><input type="number" class="inputText" id="thanks_limit" name="thanks_limit" min="1" max="50" value="<? echo $thanks_limit; ?>"></td>
      </tr>
		<? if($queueTotal == 0 ) { ?>
		<tr>
			<td colspan="2" align="center"><p>
				<input type="button" value="Save Settings" id="thanks_button" name="thanks_button" onclick="submitThanksSetupForm()"  />
				</p>			</td>
		</tr>	
		<? } ?>
	</table>
	</form>	

<script>
function submitThanksSetupForm() {
	var send_thanks = $('#send_thanks').val();
	var thanks_limit = $('#thanks_limit').val();
	if(parseInt(thanks_limit) > 50 || !parseInt(thanks_limit) ) {
		$('#thanksLimitLabel').css('color', '#FF0000');
		return;
	}
	$.ajax({
  		type: "get",
  		url: "/members/saveThanksSetup.php",
		data: {'thanks_limit': thanks_limit, 'send_thanks': send_thanks} ,
		dataType: "html"
	})
  	.done(function( msg ) { 		
		$('#saveThanksSettingMessage').html(msg);
		$('#saveThanksSettingMessage').css('visibility', 'visible');
  	});		
}
</script>		