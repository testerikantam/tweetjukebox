<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');
$user_id = $_SESSION['access_token']['user_id'];
?>
<?
if(isset($_POST['data'])) {
	foreach($_POST['data'] as $data) {
		$$data['name'] = $data['value']; 
	}	
	// first lets save the data
	$thanks_text = mysqli_real_escape_string($conn, $thanks_text) ;
	$thanks_date_start = date("Y-m-d", strtotime(mysqli_real_escape_string($conn, $thanks_date_start))) ;
	$thanks_date_end = date("Y-m-d", strtotime(mysqli_real_escape_string($conn, $thanks_date_end))) ;
	$thanks_delay_min = mysqli_real_escape_string($conn, $thanks_delay_min) ;
	$thanks_delay_max = mysqli_real_escape_string($conn, $thanks_delay_max) ;
	$thanks_random_tweets = mysqli_real_escape_string($conn, $thanks_random_tweets) ;
	
	$thanks_group = mysqli_real_escape_string($conn, $thanks_group) ;
	$thanks_tweet_limit = mysqli_real_escape_string($conn, $thanks_tweet_limit) ;
	$thanks_min_mentions = mysqli_real_escape_string($conn, $thanks_min_mentions) ;
	
	$hash_tag_0 = mysqli_real_escape_string($conn, $hash_tag_0) ;
	$hash_tag_1 = mysqli_real_escape_string($conn, $hash_tag_1) ;
	$hash_tag_2 = mysqli_real_escape_string($conn, $hash_tag_2) ;
	$hash_tag_3 = mysqli_real_escape_string($conn, $hash_tag_3) ;
	$hashTags = array();
	if($hash_tag_0) array_push($hashTags, $hash_tag_0);
	if($hash_tag_1) array_push($hashTags, $hash_tag_1);
	if($hash_tag_2) array_push($hashTags, $hash_tag_2);
	if($hash_tag_3) array_push($hashTags, $hash_tag_3);
	$thanks_hash_tags = implode(",", $hashTags);
	
	$SQL = "select id from user_settings where user_id = '$user_id' ";
	$SQL = "select * from user_settings where user_id = '$user_id'";
	
	$result = mysqli_query($conn, $SQL);
	if(mysqli_num_rows($result)) {
		$row = mysqli_fetch_assoc($result);
		$id = $row['id'];
		$SQL = "update user_settings set thanks_text = '$thanks_text', thanks_date_start = '$thanks_date_start', thanks_date_end = '$thanks_date_end', thanks_delay_min = '$thanks_delay_min', thanks_delay_max = '$thanks_delay_max', thanks_hash_tags = '$thanks_hash_tags', thanks_random_tweets = '$thanks_random_tweets', thanks_group = '$thanks_group', thanks_tweet_limit = '$thanks_tweet_limit', thanks_min_mentions = '$thanks_min_mentions' where id = '$id' ";
	} else {
		$SQL = "insert into user_settings set user_id = '$user_id',  thanks_text = '$thanks_text', thanks_date_start = '$thanks_date_start', thanks_date_end = '$thanks_date_end', thanks_delay_min = '$thanks_delay_min', thanks_delay_max = '$thanks_delay_max', thanks_hash_tags = '$thanks_hash_tags', thanks_random_tweets = '$thanks_random_tweets', thanks_group = '$thanks_group', thanks_tweet_limit = '$thanks_tweet_limit', thanks_min_mentions = '$thanks_min_mentions' ";
	}
		
	$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn). $SQL);
	
	$tempTable = 'tweet_works_'.$user_id;
	
	$SQL = "CREATE TEMPORARY TABLE `".$tempTable."` (	  
	  `user_id` varchar(200) NOT NULL,
	  `mention_screen_name` varchar(200),
	  `total_mentions` int(11) NOT NULL,
	  `total_reach` int(11) NOT NULL,
	  
	  PRIMARY KEY (`mention_screen_name`)
	)";
	
	$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn). $SQL);
	
				
	$thanks_date_end .= " 25";
	
	// thanks_group 1 = everybody, 2 = most mentions, 3 = total reach
	// see if we're doing total reach group - if we are we need to get each mention users followers count - this will take some time to dig out
	if($thanks_group == '3') {
		//$SQL = "select mention_screen_name, mention_data from user_mentions where user_id = '$user_id' and mention_date >= '$thanks_date_start' and mention_date <= '$thanks_date_end' ";		
		$SQL = "select mention_screen_name, followers_count from user_mentions where user_id = '$user_id' and mention_date >= '$thanks_date_start' and mention_date <= '$thanks_date_end' ";		
	} else {			
		$SQL = "select mention_screen_name from user_mentions where user_id = '$user_id' and mention_date >= '$thanks_date_start' and mention_date <= '$thanks_date_end' ";		
	}	
	//die($SQL);
	
	$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn).$SQL);
	$row = mysqli_fetch_assoc($result);	
	if($thanks_group == '3') {
		//$stuff = json_decode($row['mention_data']);
		//$followers_count = $stuff->user->followers_count;
		$followers_count = $row['followers_count'];
	} else {
		$followers_count = 0; 
	}
	
	do {			
		$SQL1 = "select * from ".$tempTable." where mention_screen_name = '" . $row['mention_screen_name'] . "' ";
		$result1 = mysqli_query($conn, $SQL1) or die(mysqli_error($conn).$SQL1);
		if(mysqli_num_rows($result1)) {
			$row1 = mysqli_fetch_assoc($result1);
			$total_mentions = $row1['total_mentions'] + 1;	
			$total_reach = $followers_count * $total_mentions;
			$SQL1 = "update ".$tempTable." set total_mentions = $total_mentions, total_reach = '$total_reach' where mention_screen_name = '" . $row['mention_screen_name'] . "' ";
			$result1 = mysqli_query($conn, $SQL1) or die(mysqli_error($conn).$SQL1);
		} else {
			$SQL1 = "insert into ".$tempTable." set total_mentions = '1', total_reach = '$followers_count', mention_screen_name = '" . $row['mention_screen_name'] . "' ";
			$result1 = mysqli_query($conn, $SQL1) or die(mysqli_error($conn).$SQL1);
		}						
	}while(	$row = mysqli_fetch_assoc($result));
	
	// ok now purge all users that have less than the minimum required
	$SQL1 = "delete from ".$tempTable." where total_mentions < $thanks_min_mentions ";
	$result1 = mysqli_query($conn, $SQL1) or die(mysqli_error($conn).$SQL1);
	
	// sort now depending on option selected 
	// thanks_group 1 = everybody, 2 = most mentions, 3 = total reach
	if($thanks_group == '1' ) {
		$SQL1 = "select * from ".$tempTable;
	}
	if($thanks_group == '2' ) {
		$SQL1 = "select * from ".$tempTable." order by total_mentions DESC ";
	}
	
	if($thanks_group == '3' ) {
		$SQL1 = "select * from ".$tempTable." order by total_reach DESC ";
	}
	$result1 = mysqli_query($conn, $SQL1) or die(mysqli_error($conn).$SQL1);
	$row1 = mysqli_fetch_assoc($result1);	
	
	// now create thanks downloadable file	
	$f = @fopen($_SERVER['DOCUMENT_ROOT']."/uploads/thanks".$user_id.".csv", 'w');
	$ty_tweet_text = '';
	$this_hash = $hashTags[rand(0,3)];
	
	$tweetCount = 0;
	$userCount = 0;
	
	$postDate = date("m/d/Y H:00") ;
	$postDate = strtotime($postDate) + 60 * 60;  // start an hour from now
	
	do {		
		$userCount ++;
		if(strlen($ty_tweet_text . $thanks_text. ' ' . $this_hash . $row1['mention_screen_name']) > 125) {
			$ty_tweet_text .= ' '.$thanks_text. ' ' . $this_hash;
			$fields = array(date('m/d/Y H:i', $postDate), $ty_tweet_text, '');
			//send_this_tweet($ty_tweet_text);			
			@fputcsv($f, $fields);
			$postDate = $postDate + 300;			
			//@fputs($f, $ty_tweet_text."\n");						
			$tweetCount ++;
			$ty_tweet_text = '';
			$this_hash = $hashTags[rand(0,3)];
			if($tweetCount == $thanks_tweet_limit && $thanks_tweet_limit != 0) { break; }
		}
		$ty_tweet_text.= '@'.$row1['mention_screen_name']." ";			
	}while($row1 = mysqli_fetch_assoc($result1));	
?>
	<h2 align="center">Your thank!<br>Your file is ready for downloading.</h2>
	<p align="center">There are <? echo $tweetCount; ?> tweets for <? echo $userCount; ?> unique users.</p>
	<p align="center">Click here to get your file -> <a href="downloadThanks.php">Download</a></p>

<?			
} else {
	die("Something went wrong!");
}
?>