<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');
$user_id = $_SESSION['access_token']['user_id'];
$itemID = mysqli_real_escape_string($conn, $_REQUEST['itemID'] ); 


// aws code
require_once("/home/yauhen/dev/tweetjunkebox/tweetjunkebox/aws/aws-autoloader.php");

use Aws\Common\Aws;
// Create the AWS service builder, providing the path to the config file
$aws = Aws::factory('/home/yauhen/dev/tweetjunkebox/tweetjunkebox/aws/config.php');
$s3Client = $aws->get('s3');



?>
<?
if($_POST['editItemID'] != '' ) {
	$itemID = $_POST['editItemID'];
	$item_name = mysqli_real_escape_string($conn, $_POST['item_name'] );
	$description = mysqli_real_escape_string($conn, $_POST['description'] ); 	
	$keyword_1 = mysqli_real_escape_string($conn, $_POST['keyword_1'] ); 
	$keyword_2 = mysqli_real_escape_string($conn, $_POST['keyword_2'] ); 
	$keyword_3 = mysqli_real_escape_string($conn, $_POST['keyword_3'] ); 
	
	$categoryID = mysqli_real_escape_string($conn, $_POST['categoryID'] ); 
	
	if($_POST['status']) $status = '1'; else $status = '0';	
	
	if($_FILES['myPhoto']['name']>'') {  // we are trying to upload an image<strong></strong>
		
		$fileName = $_FILES['myPhoto']['name'];
		$tmpName  = $_FILES['myPhoto']['tmp_name'];
		$fileSize = $_FILES['myPhoto']['size'];	
		$fileType = $_FILES['myPhoto']['type'];
	
		$mime = image_type_to_mime_type(exif_imagetype($_FILES['myPhoto']['tmp_name']));
	
		if($fileSize > 3000000) {
?>
			<script type='text/javascript'>
				
				alert("Your file is too large.  Please reduce the size to under 3Mb and try again");
				setTimeout(function () {
				   try {
					  parent.window.hs.getExpander().close();
					  parent.window.viewMasterList(<? echo $list_number; ?>) 
				   } catch (e) {}
				}, 200);      
			</script>
	
<?	
			die();
		} else {			
			$xFileParts = explode(".", $fileName);
			$numberOfParts = count ($xFileParts) - 1;			
			$fileName = $itemID . "_" . rand(1,1000) . "." . $xFileParts[$numberOfParts];							
			//move_uploaded_file($_FILES['myPhoto']['tmp_name'], $_SERVER['DOCUMENT_ROOT'].'/members/storeImages/'.stripslashes($fileName));
			$photo = stripslashes($fileName); 			
			
			$result = $s3Client->putObject(array(
				'Bucket'     => 'jukebox-store-images',
				'Key'        => $fileName,
				'ContentType' => $mime,
				'SourceFile' => $_FILES['myPhoto']['tmp_name']				
			));
			
			$SQL = "update  store_items set status = '$status', categoryID = '$categoryID', item_name = '$item_name', description = '$description', album_cover = '$photo', keyword_1 = '$keyword_1', keyword_2 = '$keyword_2', keyword_3 = '$keyword_3' where user_id = '$user_id' and id = '$itemID' ";				
		}
	} else {
		$SQL = "update  store_items set status = '$status', categoryID = '$categoryID', item_name = '$item_name', description = '$description',  keyword_1 = '$keyword_1', keyword_2 = '$keyword_2', keyword_3 = '$keyword_3' where user_id = '$user_id' and id = '$itemID' ";				
	} 
	$result = mysqli_query($conn, $SQL) or die($SQL);	
?>	
	<script type='text/javascript'>
		setTimeout(function () {
		   try {
			  parent.window.hs.getExpander().close();
			  
			  <? if($photo) { ?>
			  parent.window.albumCover<? echo $itemID; ?>.src = 'https://s3.amazonaws.com/jukebox-store-images/<? echo $photo; ?>'; 
			  <? } ?>
		   } catch (e) {}
		}, 200);      
	</script>
	
<?	
}
?>
<?
$SQL = "select * from store_items where user_id = '$user_id' and id = '$itemID' ";
$result = mysqli_query($conn, $SQL);
if(mysqli_num_rows($result)) {
	$row = mysqli_fetch_assoc($result);
	foreach( $row AS $key => $val ) {
		$$key = stripslashes( $val );					
	}
} else {
	header('Location: /oops.php');
	exit();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-Equiv="Cache-Control" Content="no-cache">
<meta http-Equiv="Pragma" Content="no-cache">
<meta http-Equiv="Expires" Content="0">
<title>Untitled Document</title>
<link rel="stylesheet" href="/assets/css/style.css">
<link rel="stylesheet" href="/assets/fonts/raphaelicons.css">
<link rel="stylesheet" href="/assets/css/main.css">

<script src="//code.jquery.com/jquery-1.11.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<!--link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" /-->
<link rel="stylesheet" href="/assets/css/jquery-ui.css">
<script>window.jQuery || document.write('<script src="/assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
<script src="/include/js_boot.js"></script>
</head>

<body>
  <form class="c-form" accept-charset="utf-8" id="storeItemForm" method="post" enctype="multipart/form-data" action="editMyStoreItem.php" >
  <div style="float:right; min-height:150px; vertical-align:text-top;">  		
	<input type="hidden" name="MAX_FILE_SIZE" value="0" />				
	<img id="album_cover" src="<? if($album_cover) echo "https://s3.amazonaws.com/jukebox-store-images/".$album_cover; else echo "/images/tjbIcon_128.png"; ?>" alt="" width="128"  />  
	<label>
		Jukebox Cover<br />
		<input type="file" name="myPhoto" id="myPhoto"  onChange="readImage(this); " />
		<p>Ideal image dimensions are 100 X 100.<br />Images must be under 12K in size.<br />Images must be in jpg or png formats.</p>
	</label>
  </div>
	<input type="hidden" name="itemID" id="itemID" value="<? echo $itemID; ?>" />
	<input type="hidden" id="editItemID" name="editItemID" value="0" >
	<!--input type="hidden" name="currentMasterPage" id="currentMasterPage" value="<? echo $currentMasterPage;?>"  /-->
    <label>Jukebox Name
	<input name="item_name" type="text" id="item_name" value="<? echo $item_name;?>" style="width:150px;" >		
	</label>
		
		<label>Category
			<select name="categoryID" id="categoryID">
			<?
			$SQL = "select * from store_categories order by category ";
			$result = mysqli_query($conn, $SQL);
			$row = mysqli_fetch_assoc($result);
			do {			
			?>
				<option value="<? echo $row['id'];?>" <? if($categoryID == $row['id']) echo 'selected="selected"';?>><? echo $row['category']; ?></option>
			<?	
			}while ($row=mysqli_fetch_assoc($result));
			?>
			</select>
		</label>
		
	<label>Description
	<textarea name="description" rows="3" id="description" style="width:150px;"><? echo $description;?></textarea>		
	</label>
	<label>Keyword 1
	<input name="keyword_1" type="text" id="keyword_1" value="<? echo $keyword_1;?>" style="width:100px;" >
	</label>
	
	<label>Keyword 2
	<input name="keyword_2" type="text" id="keyword_2" value="<? echo $keyword_2;?>" style="width:100px;" >
	</label>
	
	<label>Keyword 3
	<input name="keyword_3" type="text" id="keyword_3" value="<? echo $keyword_3;?>" style="width:100px;" >		  
	</label>   
	<label>
	By checking the box below, I understand that this Jukebox will be made available to other members.  I also herby certify that the contents of this Jukebox are in accordance with Twitter's guidelines for acceptable content.
	<input id="status" name="status" <? if($status == '1') echo 'checked="checked"'; ?> type="checkbox" value="1"  />
	</label>   
	
  <p align="center"><input type="button" name="submitButton" id="submitButton" value="Submit" onClick="checkItemEdit();" /></p>
  </form>

<script>
function checkItemEdit() {
	$('#editItemID').val( $('#itemID').val() );
	$('#storeItemForm').submit();
}

function readImage(input) {  		
	var imageExts = ["jpg","jpeg","png"];
	var filePathName = $('#myPhoto').val();
	var fileParts = filePathName.split(".");
	var ext = fileParts[fileParts.length -1];
	ext = ext.toLowerCase();	
		
	if( input.files[0].size > 20*1024 ) {
		alert("File is too large. Must be under 20K");
		$('#myPhoto').val('');
		return false;
	
	}	
			
	if(imageExts.indexOf(ext) != -1) {   // only render if image is ok to display					
		if (input.files && input.files[0])  {
			var reader = new FileReader();
			reader.onload = function (e) {									  
				$('#album_cover')
				.attr('src',e.target.result)
				.height(100);				
			};
			reader.readAsDataURL(input.files[0]);			
	
	   }
	} else {
		alert("Invalid format. Must be .png or .jpg types only");
		$('#myPhoto').val('');
	}	
}
</script>
</body>
</html>
