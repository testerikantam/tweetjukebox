<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
$user_id = $_SESSION['access_token']['user_id'];
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');
?>
<table class="table table-striped">
	<tr class="warning">
		<th>Jukebox Name</th>
		<th>Monday</th>
		<th>Tuesday</th>
		<th>Wednesday</th>
		<th>Thursday</th>
		<th>Friday</th>
		<th>Saturday</th>
		<th>Sunday</th>
	</tr>	
<?
$SQL = "select * from user_schedule where user_id = '$user_id' order by list_name ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$row = mysqli_fetch_assoc($result);	
if(mysqli_num_rows($result)) {
	do{
		foreach( $row AS $key => $val ) {
			$$key = stripslashes( $val );					
		}
?>   	
  	<tr>
    	<td><? echo htmlentities($list_name); ?></td>
		<td> 
		<? 
		if ($schedule_status == '0' || $mon_frequency_1 == 'off') {
			echo 'None'; 
		} else {
			$startTime = strtotime($mon_start_1);
			$endTime = strtotime($mon_end_1);
			$dailyTweets = floor(($endTime - $startTime) / ($mon_frequency_1 * 60) );
			$dailyTweets ++;
			if($dailyTweets == 1) {
				echo $dailyTweets." Tweet<br>";
			} else {
				echo $dailyTweets." Tweets<br>";
			}
			$nextTweet = strtotime($mon_start_1);			
			do {
				echo date("h:i A", $nextTweet)."<br>";				
				$nextTweet += $mon_frequency_1 * 60;														
			} while( $nextTweet <= strtotime($mon_end_1) );
			
		}	
		?>	
		</td>
		<td>
		<? 
		if ($schedule_status == '0' || $tue_frequency_1 == 'off') {
			echo 'None'; 
		} else {
			$startTime = strtotime($tue_start_1);
			$endTime = strtotime($tue_end_1);
			$dailyTweets = floor(($endTime - $startTime) / ($tue_frequency_1 * 60) );
			$dailyTweets ++;
			if($dailyTweets == 1) {
				echo $dailyTweets." Tweet<br>";
			} else {
				echo $dailyTweets." Tweets<br>";
			}
			$nextTweet = strtotime($tue_start_1);			
			do {
				echo date("h:i A", $nextTweet)."<br>";				
				$nextTweet += $tue_frequency_1 * 60;														
			} while( $nextTweet <= strtotime($tue_end_1) );
		}	
		?>
		</td>
		<td>
		<? 
		if ($schedule_status == '0' || $wed_frequency_1 == 'off') {
			echo 'None'; 
		} else {
			$startTime = strtotime($wed_start_1);
			$endTime = strtotime($wed_end_1);
			$dailyTweets = floor(($endTime - $startTime) / ($wed_frequency_1 * 60) );
			$dailyTweets ++;
			if($dailyTweets == 1) {
				echo $dailyTweets." Tweet<br>";
			} else {
				echo $dailyTweets." Tweets<br>";
			}
			$nextTweet = strtotime($wed_start_1);			
			do {
				echo date("h:i A", $nextTweet)."<br>";				
				$nextTweet += $wed_frequency_1 * 60;														
			} while( $nextTweet <= strtotime($wed_end_1) );
		}	
		?>
		</td>
		<td>
		<? 
		if ($schedule_status == '0' || $thur_frequency_1 == 'off') {
			echo 'None'; 
		} else {
			$startTime = strtotime($thur_start_1);
			$endTime = strtotime($thur_end_1);
			$dailyTweets = floor(($endTime - $startTime) / ($thur_frequency_1 * 60) );
			$dailyTweets ++;
			if($dailyTweets == 1) {
				echo $dailyTweets." Tweet<br>";
			} else {
				echo $dailyTweets." Tweets<br>";
			}
			$nextTweet = strtotime($thur_start_1);			
			do {
				echo date("h:i A", $nextTweet)."<br>";				
				$nextTweet += $thur_frequency_1 * 60;														
			} while( $nextTweet <= strtotime($thur_end_1) );
		}	
		?>
		</td>
		<td>
		<? 
		if ($schedule_status == '0' || $fri_frequency_1 == 'off') {
			echo 'None'; 
		} else {
			$startTime = strtotime($fri_start_1);
			$endTime = strtotime($fri_end_1);
			$dailyTweets = floor(($endTime - $startTime) / ($fri_frequency_1 * 60) );
			$dailyTweets ++;
			if($dailyTweets == 1) {
				echo $dailyTweets." Tweet<br>";
			} else {
				echo $dailyTweets." Tweets<br>";
			}
			$nextTweet = strtotime($fri_start_1);			
			do {
				echo date("h:i A", $nextTweet)."<br>";				
				$nextTweet += $fri_frequency_1 * 60;														
			} while( $nextTweet <= strtotime($fri_end_1) );
		}	
		?>
		</td>
		<td>
		<? 
		if ($schedule_status == '0' || $sat_frequency_1 == 'off') {
			echo 'None'; 
		} else {
			$startTime = strtotime($sat_start_1);
			$endTime = strtotime($sat_end_1);
			$dailyTweets = floor(($endTime - $startTime) / ($sat_frequency_1 * 60) );
			$dailyTweets ++;
			if($dailyTweets == 1) {
				echo $dailyTweets." Tweet<br>";
			} else {
				echo $dailyTweets." Tweets<br>";
			}
			$nextTweet = strtotime($sat_start_1);			
			do {
				echo date("h:i A", $nextTweet)."<br>";				
				$nextTweet += $sat_frequency_1 * 60;														
			} while( $nextTweet <= strtotime($sat_end_1) );
		}	
		?>
		</td>
		<td>
		<? 
		if ($schedule_status == '0' || $sun_frequency_1 == 'off') {
			echo 'None'; 
		} else {
			$startTime = strtotime($sun_start_1);
			$endTime = strtotime($sun_end_1);
			$dailyTweets = floor(($endTime - $startTime) / ($sun_frequency_1 * 60) );
			$dailyTweets ++;
			if($dailyTweets == 1) {
				echo $dailyTweets." Tweet<br>";
			} else {
				echo $dailyTweets." Tweets<br>";
			}
			$nextTweet = strtotime($sun_start_1);			
			do {
				echo date("h:i A", $nextTweet)."<br>";				
				$nextTweet += $sun_frequency_1 * 60;														
			} while( $nextTweet <= strtotime($sun_end_1) );
		}	
		?>
		</td>
	</tr>			
<?
	} while ($row = mysqli_fetch_assoc($result)); 
}
?>
</table>