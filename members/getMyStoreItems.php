<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');
$user_id = $_SESSION['access_token']['user_id'];

$SQL = "select store_items.*, users.screen_name as from_name from store_items join users on users.user_id = store_items.user_id where store_items.user_id = '$user_id' order by item_name ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn)) ;
$row = mysqli_fetch_assoc($result);	
?>
<h2 align="center">My Library Items</h2>

<div class="container" style="padding:20px;">
  <div class="row">
<?				
	do {
		foreach( $row AS $key => $val ) {
			$$key = stripslashes( $val );					
		}
		$score = $oneStars * 1 + $twoStars * 2 + $threeStars * 3 + $fourStars * 4 + $fiveStars * 5 ;
		$count = $oneStars + $twoStars + $threeStars + $fourStars + $fiveStars ;
		if($count != 0) $rating = number_format($score / $count, 2) . " stars" ; else $rating = "Unrated";			
?>
	<div class="col-lg-3 text-center" style="padding-bottom:20px;">
		<? if($album_cover == '') $album_cover = '/images/tjbIcon_128.png'; ?>
		<img id="albumCover<? echo $id; ?>" name="albumCover<? echo $id; ?>" src="https://s3.amazonaws.com/jukebox-store-images/<? echo $album_cover;?>" height="100" >
		<div><? echo $item_name; ?>
			<br />By: <? echo $from_name; ?>
		</div>
		<div class="small">
			<span>
				<a id="storeItem<? echo $id; ?>" name="storeItem<? echo $id; ?>" href="/members/editMyStoreItem.php?itemID=<? echo $id; ?>" onclick="return hs.htmlExpand(this,  { objectType: 'iframe', width: '750' , height: '650', preserveContent: 'false', align: 'center' } )" tabindex="0" ><i class="fa fa-pencil-square-o fa-2x"></i></a>
				<img src="../images/spacer.gif" width="60px;" />
				<a href="#" onclick="removeThisStoreItem(<? echo $id; ?>)" ><i class="fa fa-trash-o fa-2x"></i></a>
			</span>
			<br />Downloaded: <? echo $downloads;?>
			<br />Rating: <? echo $rating; ?>
			<?// if($_SESSION['isAdmin'] || $_SESSION['screen_name'] == 'SEETEK_AU' || $_SESSION['screen_name'] == 'themoodcards' || $_SESSION['screen_name'] == 'moodcards') { ?>
				<br /><a href="#" id="jbStoreItem<? echo $id;?>" onClick="storeList(<? echo $id; ?>)">Live Edit</a>
			<?// } ?>	
		</div>
	</div>	
<? } while($row = mysqli_fetch_assoc($result));	?>	
</div>
<script>
function removeThisStoreItem(l) {
	if(confirm("This action will immediatly remove this JB from our library.\n\nIt will not remove it from any users that have previously downloaded it.\n\nIs this what you want to do?")) {
		$.ajax({
			type: "post",
			url: "/members/deleteStoreItem.php",
			data: {'itemID': l} ,
			dataType: "html"
		})
		.done(function( msg ) { 
			//alert(msg);
			//myStoreItems(msg);
			location.assign("/members/");
		});
	}
}

function refreshAlbumCover(i, p) {	
	$(i).src(p);	
}
</script>