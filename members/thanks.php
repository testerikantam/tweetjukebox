<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/twitteroauth/twitteroauth.php");
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/config.php");
?>
<?
if(!isset($_SESSION['access_token'])) die();

// get users last params
$user_id = $_SESSION['access_token']['user_id'];
$SQL = "select * from user_settings where user_id = '$user_id'";


$result = mysqli_query($conn, $SQL);
if(mysqli_num_rows($result)) {
	$row = mysqli_fetch_assoc($result);
	foreach($row as $key=>$value) {
		$$key = stripslashes($value);
	}
	$hash_tags = explode(",", $thanks_hash_tags);
}	

$SQL = "select count(id) as queueTotal from cron_thanks_tweets where user_id = '$user_id'";
$result = mysqli_query($conn, $SQL);
$row = mysqli_fetch_assoc($result);
$queueTotal = $row['queueTotal'];

?>
<div id="sayThanksHelp" style="display:none; padding:5px; border:solid 1px #666666; width:650px; text-align:left; margin:auto;">
	<h3>Say Thanks Help<span style="cursor:pointer; float:right; padding-right:5px; color:#FF0000; font-weight:bold;" onclick="$('#sayThanksHelp').css('display', 'none');$('#sayThanksHelpButton').css('display', 'block');">X</span></h3>
	Enter your thank you text, a date range, a delay minimum and maximum duration, and up to four hash tags.			
	<p>
	We will grab all unique users that mentioned you between the dates you select and construct a thank you tweet using the thank you text.  Each tweet will include your thank you text, a random hash tag, and as many users that we can fit into the TY tweet.  Each tweet will pause for a random amount of seconds between your delay min and max values to simulate a "manual" effort until all users have been thanked!
	</p>
	<p>
	Thank you tweets are automatically sent.  Twenty five (25) thank you tweets are immediatly sent, followed by batches of 75 tweets every three hours. You will not be able to add more to your queue, until your queue is finished.  Depending on your date range, and unique user count, this may take days to finish.
	</p>
	<p>The CSV option will include all unique users within the date range you select.  Regardless if they have been actualy thanked or not.</p>
	<p>SEND THANKS, will gather unique users within the date range and automatically send them a thank you tweet. It will omit <em><strong>mentions</strong></em> that have previously thanked (if you accidently overlap dates).</p>
</div>

	<div style="margin:auto;" id="sayThanksHelpButton" class="button" onclick="$('#sayThanksHelpButton').css('display', 'none');$('#sayThanksHelp').css('display', 'block');">Help Me With This Form</div>
	<h2 align="center">Say thanks to unique users that have mentioned you.</h2>
	<? if($queueTotal) { ?>
		<h3 align="center">You have <? echo $queueTotal; ?> thank you tweets pending.</h3>
	<? } ?>	
	<form id="thanksForm" name="thanksForm" method="post" enctype="multipart/form-data" accept-charset="utf-8">	
    <table style="background:#FFFFFF;">
		<tr valign="middle" style="background:#FFFFFF;">
			<td align="right" style="background:#FFFFFF;">Thank You Text</td>
			<td style="background:#FFFFFF;"><input class="inputText" type="text" name="thanks_text" id="thanks_text" maxlength="20" value="<? echo $thanks_text; ?>" /></td>
	    </tr>	
		<tr valign="middle" style="background:#FFFFFF;">
			<td align="right" style="background:#FFFFFF;">Date Range<br /></td>
		    <td style="background:#FFFFFF;">
				<input type="text" class="inputText" name="thanks_date_start" id="thanks_date_start" size="11" value="<? if($thanks_date_start) echo date("m/d/Y", strtotime($thanks_date_start)); ?>"> 	
				<input type="text" class="inputText" name="thanks_date_end" id="thanks_date_end" size="11" value="<? if($thanks_date_end) echo date("m/d/Y", strtotime($thanks_date_end)); ?>"></td>
	    </tr>
		<tr valign="middle" style="background:#FFFFFF;">
		  <td align="right" style="background:#FFFFFF;">User Group To Thanks<br />
	      <strong>* Reach = (Mentions * User Followers) </strong></td>
		  <td style="background:#FFFFFF;"><select name="thanks_group" id="thanks_group">
		    <option value="1" <? if($thanks_group == '1' || $thanks_group == '') echo 'selected="selected"'; ?>>Everybody</option>
		    <option value="2" <? if($thanks_group == '2') echo 'selected="selected"'; ?>>Most Mentions</option>
		    <option value="3" <? if($thanks_group == '3') echo 'selected="selected"'; ?>>Largest Reach</option>
		    </select>
		  &nbsp; <input name="thanks_ignore" type="checkbox" id="thanks_ignore" value="1" />
		  Include Prior Thanks </td>
      </tr>
		<tr valign="middle" style="background:#FFFFFF;">
		  <td align="right" style="background:#FFFFFF;">Max Number Of Tweets<br />
	      <strong>*  (zero = no max)</strong>: </td>
		  <td style="background:#FFFFFF;"><input type="number" class="inputText" id="thanks_tweet_limit" name="thanks_tweet_limit" min="0" max="5000" value="<? echo $thanks_tweet_limit; ?>"></td>
      </tr>
		<tr valign="middle" style="background:#FFFFFF;">
		  <td align="right" style="background:#FFFFFF;">Minimum Mentions<br />
		  <strong>* Exclude users with less than this number of mentions</strong>: </td>
		  <td style="background:#FFFFFF;"><input type="number" class="inputText" id="thanks_min_mentions" name="thanks_min_mentions" min="0" max="100" value="<? echo $thanks_min_mentions; ?>"></td>
	  </tr>	
		<tr valign="middle" style="background:#FFFFFF;">
			<td align="right" style="background:#FFFFFF;">Random Delay (in seconds) Between Tweets<br />
		    <strong>* 1-600 seconds max allowed</strong></td>
		  <td style="background:#FFFFFF;">
				<input type="number" class="inputText" id="thanks_delay_min" name="thanks_delay_min" min="1" max="600" value="<? echo $thanks_delay_min; ?>">			&nbsp;to&nbsp;&nbsp;
            <input type="number" class="inputText" id="thanks_delay_max" name="thanks_delay_max" min="1" max="600" value="<? echo $thanks_delay_max; ?>" /></td>
	    </tr>
		<tr valign="middle" style="background:#FFFFFF;">
			<td align="right" style="background:#FFFFFF;"> (optional) Send<br />
		    <strong>* random tweets from your Jukebox after each batch</strong></td>
			<td style="background:#FFFFFF;">
				<input type="number" class="inputText" id="thanks_random_tweets" name="thanks_random_tweets" min="0" max="3" value="<? echo $thanks_random_tweets; ?>"></td>
	    </tr>
		<tr valign="middle" style="background:#FFFFFF;">
			<td align="right" style="background:#FFFFFF;">Hash Tags<br />
			  <strong>* random tags to use on each tweet </strong></td>
			<td style="background:#FFFFFF;" nowrap="nowrap"><input type="text" class="inputText" id="hash_tag_0" name="hash_tag_0" size="11" value="<? if($hash_tags[0]) echo $hash_tags[0]; ?>"> 
				<input type="text" class="inputText" id="hash_tag_1" name="hash_tag_1" size="11" value="<? if($hash_tags[1]) echo $hash_tags[1]; ?>"> 
				<input type="text" class="inputText" id="hash_tag_2" name="hash_tag_2" size="11" value="<? if($hash_tags[2]) echo $hash_tags[2]; ?>"> 
				<input type="text" class="inputText" id="hash_tag_3" name="hash_tag_3" size="11" value="<? if($hash_tags[3]) echo $hash_tags[3]; ?>"></td>
		</tr>
		<? // if($queueTotal == 0 ) { ?>
		<tr>
			<td colspan="2" align="center">
				<p><span style="cursor:pointer; background:#CCCCCC; border:solid 1px #000;" class="results" onclick="validate_thanksForm('0');" >Download as CSV file.</span></p>
				<p>
				<input class="button" type="button" value="Send Thanks" id="thanks_button" name="thanks_button" onclick="xxxvalidate_thanksForm('1');" />
				</p>			</td>
		</tr>	
		<? // } ?>
	</table>
	<input type="hidden" name="post_thanks" id="post_thanks" value="0"  />
	<input type="hidden" name="user_id" id="user_id" value="<? echo $user_id; ?>"  />
	</form>	
<script>
function validate_thanksForm(w) {
	var errors = '';	 
	formData = $( "#thanksForm" ).serializeArray();
	if($('#thanks_text').val().length < 2 ) {
		errors = errors + "Thank you text must be at least two charactlers\n";
	}

	var start= new Date($('#thanks_date_start').val());
 	var end= new Date($('#thanks_date_end').val()); 
	
	//alert(end - start); 
	/*
	if (start > end) {
		errors = errors + "Dates are out of range\n";    	
	} else {
		var diff = (end - start) / 1000 ; // number of seconds   there are 86400 seconds per day we allow only 10 days or 864000 seconds
		if(diff > 86400 * 9) errors = errors + "Date range may not exceed 10 days\n";
	}
	*/
	if (start > end) {
		errors = errors + "Dates are out of range\n";    	
	}
	
	
	
	str = $('#thanks_delay_min').val();
	x = /^\+?(0|[1-9]\d*)$/.test(str); 
	if(x == false) {
		errors = errors + "Minimum delay is invalid\n";
	}
	
	if($('#thanks_delay_min').val() >600 || $('#thanks_delay_min').val() < 1) {
		errors = errors + "Minimum delay is out of range (1 - 15)\n";
	}
	
	str = $('#thanks_delay_max').val();
	y = /^\+?(0|[1-9]\d*)$/.test(str); 
	if(x == false) {
		errors = errors + "Maximum deleay is invalid\n";
	}
	
	if($('#thanks_delay_max').val() >600 || $('#thanks_delay_max').val() < 1) {
		errors = errors + "Maximum delay is out of range (1 - 15)\n";
	}
	
	if($('#thanks_delay_min').val() > $('#thanks_delay_max').val() ) {
		errors = errors + "Delay range is invalid\n";
	}
	if(w == '0') {
		if(errors == '') {
			$('#post_thanks').val('1');
			$('#masterDiv').html('<div align="center">Please wait while we build your file....<p><img src="../images/wait.GIF" border="0"></p></div>');				
			$.ajax({
				type: "post",
				url: "/members/tweetThanksText.php",
				//data: {'message': msg} ,
				data: {'data': formData },
				dataType: "html"
			})
			.done(function( msg ) { 
				$('#masterDiv').html(msg);
			});						
		} else {
			alert(errors);
			return false;	
		}	
	}
	if(w == '1') {
		if(errors == '') {			
			hideDivs();
			$('#post_thanks').val('1');
			$("#thanksForm").attr("action", "updateThanks.php" );
			$('#thanksForm').submit();
		} else {
			alert(errors);
			return false;	
		}	
	}
}
</script>