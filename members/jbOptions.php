<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?php require_once(MODEL_PATH.'/User.php');?>
<?php require_once(MODEL_PATH.'/SocialSchedule.php');?>
<?
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');
$user_id = $_SESSION['access_token']['user_id'];
$user = new User();
$user->getByUserId($user_id);
$list_number = $_REQUEST['list_number'];
?>
<?
$SQL = "select * from user_schedule where user_id = '$user_id' and list_id = '$list_number' " ;
$result = mysqli_query($conn, $SQL);
$schRow = mysqli_fetch_assoc($result);
$twitterHashtagOption = $schRow['twitterHashtagOption'];
$stopDate = $schRow['stopDate'];
$startDate = $schRow['startDate'];
$isOnceAndDone = $schRow['isOnceAndDone'];


$list_name = stripslashes($schRow['list_name']);
$retweet_days = $schRow['retweet_days'];
$schedule_status = $schRow['schedule_status'];
if($list_name == '') $list_name = 'Jukebox #'.$list_number;
?>
<? include($_SERVER['DOCUMENT_ROOT']."/include/navJB.php"); ?>
<div class="row">
	<div class="col-md-12">
		<h2>
		<? 
			if($list_name) {
				echo $list_name; 
			} else {	
				echo 'Jukebox # '.$list_number; 
			}	
		?> Options
		</h2>		
	</div>
</div>

<div class="row" style="font-size:smaller;">
	<div class="col-md-1">
	<div class="form-group" style="padding-top:10px;">
		
	</div>
	</div>
	<div class="col-md-2">	
		<div class="form-group">
		</div>
	</div>
	<div class="col-md-2">
	<div class="form-group">

	</div>		
	</div>
	<div class="col-md-2">
	<div class="form-group">

	</div>	
	</div>	
	<div class="col-md-5"></div>
</div>
<form class="form-inline" id="jbOptionsForm" name="jbOptionsForm" accept-charset="utf-8"  >      
  <div style="padding-bottom:20px;">
    <label for="jbName">Jukebox Name</label>
    <input type="hidden" name="list_number" id="list_number" value="<? echo $list_number; ?>" />
	<input type="text" class="form-control" id="list_name" name="list_name" placeholder="Juke Box Name" style="width:300px;" value="<? echo htmlentities($list_name);?>">	
  </div>
  		<div style="padding-bottom:20px;">   
		<label for="select_JB_type">Jukebox Type&nbsp;</label>
	 	<select class="form-control" id="select_JB_type" name="select_JB_type" onchange="showSelectType(this.value)">
			<option <? if($isOnceAndDone == '0' and $stopDate == 0) echo 'selected="selected"'; ?> value="1">Regular</option>			
			<? if($_SESSION['planName'] != 'Free') { ?>
				<option <? if($isOnceAndDone == '1' ) echo 'selected="selected"'; ?> value="2">Once And Done</option>
				<option <? if($stopDate != '0' ) echo 'selected="selected"'; ?> value="3">Date Range</option>
			<? } else { ?>
				<option disabled="disabled" <? if($isOnceAndDone == '1' ) echo 'selected="selected"'; ?> value="2">Once And Done (Only With Paid Subscription)</option>	
				<option disabled="disabled" <? if($stopDate != '0' ) echo 'selected="selected"'; ?> value="3">Date Range (Only With Paid Subscription)</option>
			<? } ?>	
		</select>	
  		</div>
  <div style="padding-bottom:20px;">
  	<label for="schedule_status">This Jukebox Is:
		<select id="schedule_status" name="schedule_status" class="form-control">
			<option value="1"  <? if($schedule_status == 1)  echo 'selected="selected"';  ?>> Jukebox Is ON</option>
			<option value="0"  <? if($schedule_status == 0)  echo 'selected="selected"';  ?>> Jukebox Is OFF </option>
		</select>&nbsp;&nbsp;<? if($schedule_status == 1) echo '<i class="fa fa-thumbs-o-up fa-2x text-success"></i>'; else echo '<i class="fa fa-thumbs-o-down fa-2x text-danger"></i>'; ?> </label>
  </div>

    <?php if ($user->hasPlan()):?>
        <?php
            $jukeboxId = $schRow['id'];
            $schedule = new SocialSchedule();
            //$schedulesArray = $schedule->getSocialSchedulesArrayByJukeboxId($jukeboxId);
        ?>
        <div style="padding-bottom:20px;">
            <p><label>Social platforms:</label></p>
            <?php if ($user->hasSocialAccount('facebook')):?>
                <div class="form-group">

                    <input type="checkbox" id="in-facebook" value="1" name="in_facebook" class="form-control"<?php //if (array_key_exists('facebook', $schedulesArray)):?><?php// endif;?>><label for="in-facebook">Facebook</label>
                </div>

            <?php endif;?>
        </div>
    <?php endif;?>

    <div class="form-group" id="juboxSettings" style="padding-bottom:20px;">
		<p><label>Twitter Hashtag Options:</label></p>
		<p><strong>Example: I'm going on #Vacation this month. #DE #Europe</strong></p>
		<div class="form-group">			
		<select name="twitterHashtagOption" id="twitterHashtagOption" >
			<option value="0" <? if($twitterHashtagOption == 0) echo 'selected="selected"';?> >No Action (I'm going on #Vacation this month. #DE #Europe)</option>
			<option value="1" <? if($twitterHashtagOption == 1) echo 'selected="selected"';?>>Remove all '#' (I'm going on Vacation this month. DE Europe)</option>
			<option value="2" <? if($twitterHashtagOption == 2) echo 'selected="selected"';?>>Remove all '#' and all ending Hashtags (I'm going on Vacation this month.)</option>
			<option value="3" <? if($twitterHashtagOption == 3) echo 'selected="selected"';?>>Remove all Hashtags (I'm going on this month.)</option>				
		</select>
		</div>					
	</div>	  
    
  
  <!----                                             added hidden value here    be sure to remove it if you reinstate retweet days -->
  <input type="hidden" name="retweet_days" id="retweet_days" value="<? echo $retweet_days; ?>"  />
  <div style="padding-bottom:20px;">
    <label for="exampleInputPassword1">Do not repeat a tweet for at least 
    <input type="number" id="retweet_days" name="retweet_days" min="0" class="form-control" max="30" maxlength="2" style="width:100px;" value="<? echo $retweet_days; ?>"  > days.  </label>
	<br><strong>WARNING</strong>, setting this value too high may stop your tweets from playing depending on your inventory and frequency of tweeting.
  </div>       
  
  <div id="onceAndDoneDiv" style="padding:15px; border: 1px solid #000; background-color:#EEEEEE; <? if($isOnceAndDone == '1' ) echo 'display:block'; else echo 'display:none'; ?>" class="text-info" >
    <label for="isOnceAndDone">Once and Done! </label>
    <input type="checkbox" style="visibility:hidden;" id="isOnceAndDone" name="isOnceAndDone" readonly="" class="form-control" value="1" <? if ($isOnceAndDone) echo 'checked="checked"' ; ?>   >	
	<br><strong><font color="#FF0000">WARNING!!</font></strong> This as a "Once and Done" Jukebox!  Once all of these tweets are have been tweeted, the Jukebox will be turned off automatically.<br />Comes in handy for current events you may want to gather using our extension tool!
	<p><label for="verifyOnceAndDone"><input type="checkbox" id="verifyOnceAndDone" name="verifyOnceAndDone" value="1" <? if ($isOnceAndDone) echo 'checked="checked"' ; ?> />&nbsp;&nbsp;<font color="#FF0000">Check this box to verify that you understand these settings!&nbsp;&nbsp;</font></label></p>	
  </div>
  <div id="goodUntilDiv" style="padding:15px; border: 1px solid #000; background-color:#EEEEEE; <? if($stopDate != '0' ) echo 'display:block'; else echo 'display:none'; ?>" class="text-info" >
    <? if($_SESSION['planName'] != 'Free' || $_SESSION['isAdmin']) { ?>
		<label for="startDate">Start Date </label>
		<input id="startDate" name="startDate" class="datepicker" data-provide="datepicker-inline" data-date-format="mm/dd/yyyy" value="<? if($startDate) echo date("m/d/Y", $startDate);?>">
		<label for="stopDate">End Date </label>
		<input id="stopDate" name="stopDate" class="datepicker" data-provide="datepicker-inline" data-date-format="mm/dd/yyyy" value="<? if($stopDate) echo date("m/d/Y", $stopDate);?>">
		<label id="dateRangeError" style="color:#FF0000; visibility:hidden;"> Date Range Is Invalid!</label>
		<p>A "Date Range" Jukebox - is just like a regular Jukebox. BUT the Jukebox will start and then stop tweeting <strong>on</strong> the dates you specifiy.<br />Comes in handy for seasonal or promotional periods!</p>	
	<? } ?>	
  </div>  

  <div style="padding-bottom:20px; padding-left:75px; padding-top:20px; padding-right:100px;">  
	<button type="button" class="btn btn-primary" onclick="if( $('#schedule_status').val() == '1' ) { $('#jbLinkList<? echo $list_number;?>').text( $('#list_name').val() ) ; } else { $('#jbLinkList<? echo $list_number;?>').text( $('#list_name').val() + ' - off');  }  validateData();">Save Changes</button>    
	<a style="margin-right:50px;" type="button" class="btn btn-danger btn-xs pull-right" onclick="if(confirm('All tweets and the schedule for this JB will be removed.\n\nThis action is not reversible.\n\nAre you sure you want to delete this Juke Box?') == true) { deleteJukeBox(<? echo $list_number; ?>); }" ><i class="fa fa-trash-o"></i> Delete This Jukebox</a>  	
  </div>
  <div style="padding-bottom:20px; padding-left:75px; padding-top:30px; padding-right:100px;">  
  <? if( ($_SESSION['planName'] != 'Free' && $_SESSION['loggedInAs'] == $_SESSION['master_account_id'] )  || $_SESSION['isAdmin']) { ?>
		<button type="button" class="btn btn-primary" onclick="$('#linkedAccounts').css('display', 'block');">Copy Jukebox</button>
		<div id="linkedAccounts" style="display:none;">		
			<?
			$SQL = "select screen_name, user_id from users where master_account_id = '". $_SESSION['master_account_id'] ."' and master_account_id != '' order by screen_name ";
			$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
			$row = mysqli_fetch_assoc($result);				
			?>
			<?	
				do {
					$multiScreenName = stripslashes($row['screen_name']);
					//if($row['user_id'] != $_SESSION['master_account_id'] ) {
					if($row['user_id'] != $user_id ) {
			?>	
					<input class="form-control listedAccount" type="checkbox" value="<? echo $row['user_id'];?>"  />&nbsp;<? echo $multiScreenName; ?><br />
			<?
					}
				} while($row = mysqli_fetch_assoc($result));	
			?>
			<button onclick="copyThisJukebox('<? echo $list_number; ?>');" value="Copy" >Copy To Selected Accounts</button>
		</div>
  <? } else { ?>
  		<button type="button" disabled="disabled" class="btn btn-primary">Copy Jukebox<br />
		<? // if($_SESSION['planName'] == 'Free') echo '(Only With Paid Subscription)'; else echo '(Only From Master Account)'; ?>
		<? if($_SESSION['planName'] == 'Free') echo '(Only With Paid Subscription)'; ?>
		</button>  
  <? } ?>
  </div>	
  <p class="text-success">
	<span id="jbOptionsMsg">&nbsp;</span>
  </p>
</form>
<script>	
	$('#scheduleLI').removeClass('jbc');
	$('#tweetLI').removeClass('jbc');
	$('#optionLI').addClass('jbc');
	//$('.datepicker').datepicker();
	$('#stopDate').datepicker(
		{
			format: 'mm/dd/yyyy',
			startDate: '+2d',
			autoclose: true			
		}
	);
	$('#startDate').datepicker(
		{
			format: 'mm/dd/yyyy',
			startDate: '+1d',
			autoclose: true			
		}
	);
</script>
<script>
function copyThisJukebox(list) {			
	var accountList = new Array();
	$('.listedAccount').each(function() {
		if($(this).prop('checked') == true)   {
			accountList.push( $(this).val() );
		}	
	});
	
	if( confirm("Are you sure you want to make a copy of this jukebox?") ) {
		makeJBCopy(list, accountList);
	}
}

function showSelectType(type) {
	if(type == '1') {
		$('#onceAndDoneDiv').css('display', 'none');
		$('#goodUntilDiv').css('display', 'none');
		$('#isOnceAndDone').prop('checked', false);	
		$('#verifyOnceAndDone').prop('checked', false);	
		$('#stopDate').val('0');
		$('#startDate').val('0');
		$('#dateRangeError').css('visibility', 'hidden');	
	}
	if(type == '2') {
		$('#onceAndDoneDiv').css('display', 'block');
		$('#goodUntilDiv').css('display', 'none');
		$('#isOnceAndDone').prop('checked', true);	
		$('#stopDate').val('0');
		$('#startDate').val('0');
		$('#dateRangeError').css('visibility', 'hidden');	
	}
	if(type == '3') {
		$('#onceAndDoneDiv').css('display', 'none');
		$('#goodUntilDiv').css('display', 'block');
		$('#isOnceAndDone').prop('checked', false);	
		$('#verifyOnceAndDone').prop('checked', false);	
		$('#stopDate').val('<? echo date("m/d/Y", strtotime("+2 days")); ?>');
		$('#startDate').val('<? echo date("m/d/Y", strtotime("tomorrow")); ?>');
	}

}
function validateData() {
	if($('#stopDate').val() != 0 ) {
		var startDt = $('#startDate').val();
		var endDt = $('#stopDate').val();
		
		if( new Date(startDt).getTime() >= new Date(endDt).getTime() ) 	{		
			$('#dateRangeError').css('visibility', 'visible');						
		} else {		
			saveJBOptions();
		}	
	} else {  
		saveJBOptions();
	}	
}
</script>