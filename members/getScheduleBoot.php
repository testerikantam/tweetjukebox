<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');
$user_id = $_SESSION['access_token']['user_id'];
$list_number = $_REQUEST['list_number'];

$SQL = "select * from user_schedule where user_id = '$user_id' and list_id = '$list_number' " ;
$result = mysqli_query($conn, $SQL);
$schRow = mysqli_fetch_assoc($result);

$list_name = stripslashes($schRow['list_name']);

if($list_name == '') $list_name = 'Jukebox #'.$list_number;

$row = 1;
$days = array('mon', 'tue', 'wed', 'thur', 'fri', 'sat', 'sun');

echo '<form class="l-form" id="scheduleForm" name="scheduleForm" method="post" enctype="multipart/form-data" accept-charset="utf-8">';


echo '<input type="hidden" id="list_number" name="list_number" value="'.$list_number.'"  />';
echo '<input type="hidden" id="current_list_name" name="current_list_name" value="'.$list_name.'"  />';
echo '<input type="hidden" id="list_name" name="list_name" value="'.$list_name.'"  />';
?>
<? include($_SERVER['DOCUMENT_ROOT']."/include/navJB.php"); ?>
<div class="row" style="padding-bottom:20px;">
	<div class="col-md-6 text-center">
		<h2>
		<? 
			$SQL2 = "select list_name from user_schedule where user_id = '$user_id' and list_id = '$list_number'  ";
			$result2 = mysqli_query($conn, $SQL2) or die(mysqli_error());
			$row2 = mysqli_fetch_assoc($result2);	
			$list_name = stripslashes($row2['list_name']);
			if($list_name) {
				echo $list_name; 
			} else {	
				echo 'Jukebox # '.$list_number; 
			}	
		?>
		&nbsp;Schedule
		</h2>
		<p><strong>Click</strong> the days to set your schedule for. Changes will be applied to all days selected.
		<br />Click again to lock in and save your settings for that day.
		</p>					
	</div>
	<div class="col-md-2">	
		<button type="button" class="btn btn-success" onClick="validateSchedule(<? echo $user_id. ", ". $list_number; ?>);" >adjust my schedule</button>
	</div>		
	<div class="col-md-2">	
		<button type="button" class="btn btn-success" onClick="disableAllDays();" >save and exit</button>	
	</div>
	<div class="col-md-2">
		<button type="button" class="btn btn-default" onClick="setupSchedule(<? echo $list_number; ?>);" >exit without saving</button>
	</div>		
	</div>
</div>
<?
do {
	$day = $days[$row - 1];	
?>
<div class="row">
	<div class="col-md-1">
	<div class="form-group" style="padding-top:10px;">
		<button type="button" class="lead dayOfWeek form-control" id="<? echo $day;?>Day" style="width:65px; background-color:#dda100;" onClick="checkEditedSchedule('<? echo $row;?>'); dayClicked('#<? echo $day;?>Day', '<? echo $row;?>');"><? echo strtoupper($day);?></button></li>
		
	</div>
	</div>
	<div class="col-md-1">
	<div class="form-group" style="padding-top:10px;">
		<?
		if($schRow[$day."_frequency_1"] != 'off') {
		?>	
				<span id="dayStatusOn<? echo $day;?>"  style="display:block; padding-bottom:10px; padding-top:5px; color:#009900;" onClick="$('#dayStatusOn<? echo $day;?>').css('display', 'none'); $('#dayStatusOff<? echo $day;?>').css('display', 'block'); setDay('off', '<? echo $row; ?>');"  ><i class="fa fa-thumbs-o-up fa-2x"></i> On</span>
				<span id="dayStatusOff<? echo $day;?>" style="display:none; padding-bottom:10px; padding-top:5px; color:#FF0000;" onclick="$('#dayStatusOn<? echo $day;?>').css('display', 'block'); $('#dayStatusOff<? echo $day;?>').css('display', 'none'); setDay('on', '<? echo $row; ?>');" ><i class="fa fa-thumbs-o-down fa-2x"></i> Off</span>
		<?	
			} else { 
		?>
				<span id="dayStatusOn<? echo $day;?>"  style="display:none; padding-bottom:10px; padding-top:5px; color:#009900;" onClick="$('#dayStatusOn<? echo $day;?>').css('display', 'none');   $('#dayStatusOff<? echo $day;?>').css('display', 'block'); setDay('off', '<? echo $row; ?>'); "  ><i class="fa fa-thumbs-o-up fa-2x"></i> On</span>
				<span id="dayStatusOff<? echo $day;?>" style="display:block; padding-bottom:10px; padding-top:5px; color:#FF0000;" onclick="$('#dayStatusOn<? echo $day;?>').css('display', 'block'); $('#dayStatusOff<? echo $day;?>').css('display', 'none'); setDay('on', '<? echo $row; ?>');" ><i class="fa fa-thumbs-o-down fa-2x"></i> Off</span>
		<?	
			} 
		?>				
	</div>
	</div>
	<div class="col-md-2">	
		<?
		if($schRow[$day."_frequency_1"] != 'off') {	
			$frequencyHours = floor($schRow[$day."_frequency_1"] / 60);
			$frequencyMinutes = $schRow[$day."_frequency_1"] % 60;
		} else {
			$frequencyHours = 0;
			$frequencyMinutes = 0;	
		}
		?>		
		<input type="hidden" id="frequency_1<? echo $row; ?>" name="frequency_1<? echo $row; ?>"  class=""  value="<? echo $schRow[$day."_frequency_1"];?>"  />
		<div class="form-group">
			<label>Tweet Every:</label>
			<br />
			<input type="number" class="inputText form-inline" style="width:40px;" disabled="disabled"  id="frequency_hours_1<? echo $row; ?>" name="frequency_hours_1<? echo $row; ?>" min="0" max="24" value="<? echo $frequencyHours;?>" onchange="adjustFrequency(<? echo $row; ?>)">
			&nbsp;/&nbsp;
			<input type="number" class="inputText form-inline" style="width:40px;" disabled="disabled"  id="frequency_minutes_1<? echo $row; ?>" name="frequency_minutes_1<? echo $row; ?>" min="0" max="59" value="<? echo $frequencyMinutes;?>" onchange="adjustFrequency(<? echo $row; ?>)">
			<br />
			Hours / Minutes
		</div>
	</div>
	
	<div class="col-md-2">
	<div class="form-group">
		<label for="start_1<? echo $row; ?>">Start:</label>	
		<input id="start_1<? echo $row; ?>" name="start_1<? echo $row; ?>" type="text" class="form-control time" onChange="$('.start_1_on').val(this.value);" style="width:100px;"  />
	</div>		
	</div>
	<div class="col-md-2">
	<div class="form-group">
		<label for="end_1<? echo $row; ?>">End:</label>	
		<input id="end_1<? echo $row; ?>" name="end_1<? echo $row; ?>" type="text" class="form-control time" onChange="$('.end_1_on').val(this.value);" style="width:100px;" />		
	</div>	
	</div>
	<script>
		//$('#jb_start_time'+i).val(''); 
		$('#start_1<? echo $row; ?>').val('<? echo date("h:i A", strtotime($schRow[$day."_start_1"]));?>');	
		$('#start_1<? echo $row; ?>').prop('disabled', true); 
		$('#start_1<? echo $row; ?>').timepicker({ 'disableTextInput': true, 'scrollDefaultNow': true, 'step': 1, 'timeFormat': 'h:i A' , 'minTime': '00:00 AM', 'maxTime': '11:59 PM'  }); 
		
		//$('#jb_end_time'+i).val(''); 
		$('#end_1<? echo $row; ?>').val('<? echo date("h:i A", strtotime($schRow[$day."_end_1"]));?>');
		$('#end_1<? echo $row; ?>').prop('disabled', true); 
		$('#end_1<? echo $row; ?>').timepicker({ 'disableTextInput': true, 'scrollDefaultNow': true, 'step': 1, 'timeFormat': 'h:i A' , 'minTime': '00:00 AM', 'maxTime': '11:59 PM'  }); 
	</script>								
	<div class="col-md-4" style="padding-top:30px;">
	<? 
	if( strtotime($schRow[$day."_start_1"]) > strtotime($schRow[$day."_end_1"]) ) { 
	?>	
		<span id="schErrorRow<? echo $row; ?>" class="bg-danger">Start & end times conflict - setting will be ignored</span>
	<? } ?>
	</div>	
</div>
<? 
   $row++;
 } while($row < 8);
?>
</form>
<script>
	$('#tweetLI').removeClass('jbc');
	$('#optionLI').removeClass('jbc');	
	$('#scheduleLI').addClass('jbc');


function setDay(status, r)  { 		
	if(status == 'off') {		
		$('#frequency_hours_1'+r).val('0');
		$('#frequency_minutes_1'+r).val('0');
		$('#frequency_1'+r).val('0');
	} else {
		$('#frequency_hours_1'+r).val('1');
		$('#frequency_minutes_1'+r).val('0');
		$('#frequency_1'+r).val('60');
	}	
}	

function checkEditedSchedule(r) {		
	var s =  $('#start_1' + String(r)).val() ;
	var e =  $('#end_1' + String(r)).val();
	
	if(s.indexOf('AM') > 0 ) {
		parts = s.split(":");
		if(parts[0] == '12' ) {
			parts[0] = '00';
		}
		s = String(parts[0]) + ":" + String(parts[1]);
	}
	
	if(s.indexOf('PM') > 0 ) {
		parts = s.split(":");
		if(parts[0] != '12' ) {
			parts[0] = parseInt(parts[0]) + 12;
		}
		s = String(parts[0]) + ":" + String(parts[1]);
	}
	
	if(e.indexOf('AM') > 0 ) {
		parts = e.split(":");
		if(parts[0] == '12' ) {
			parts[0] = '00';
		}
		e = String(parts[0]) + ":" + String(parts[1]);
	}
	
	if(e.indexOf('PM') > 0 ) {
		parts = e.split(":");
		if(parts[0] != '12' ) {
			parts[0] = parseInt(parts[0]) + 12;
		}
		e = String(parts[0]) + ":" + String(parts[1]);
	}
	
		
	if( s.substr(0,2) + s.substr(3.2) > e.substr(0,2) + e.substr(3.2)  ) {
		$('#schErrorRow' + String(r)).html("Start & end times conflict - setting is ignored");
	} else {
		$('#schErrorRow' + String(r)).html("");		
	}

}


</script>