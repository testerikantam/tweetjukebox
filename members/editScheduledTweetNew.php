<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');
$user_id = $_SESSION['access_token']['user_id'];
$currentMasterPage = mysqli_real_escape_string($conn, $_REQUEST['currentMasterPage'] ); 
$rowID = $_REQUEST['rowID'];

$screen_name = $_SESSION['screen_name'];
//$access_token = $_SESSION['access_token'];
//$user_id = $_SESSION['access_token']['user_id'];

$SQL = "select * from users where screen_name = '$screen_name'  ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$row = mysqli_fetch_assoc($result);	
$userTimeZone = $row['user_timezone'];
date_default_timezone_set($userTimeZone);


// aws code
require_once("/home/yauhen/dev/tweetjunkebox/tweetjunkebox/aws/aws-autoloader.php");

use Aws\Common\Aws;
// Create the AWS service builder, providing the path to the config file
$aws = Aws::factory('/home/yauhen/dev/tweetjunkebox/tweetjunkebox/aws/config.php');
$s3Client = $aws->get('s3');


?>
<?
if($_POST['scheduledRowID'] != '' ) {				
	$key = 'AIzaSyDWTkJNT9grybmoRDpvy73yNcM3xkfcHM8';
	$googer = new GoogleURLAPI($key);

	//print "<pre>";
	//print_r($_REQUEST);
	//die();
	
	
	
	if($_REQUEST['tweet_status']) {
		$tweet_status = 'draft'; 
	} else {
		$tweet_status = 'active';
	}
	
	$start_date = mysqli_real_escape_string($conn, $_REQUEST['tweet_start_date'] );

	if($_REQUEST['stop_date']	 != '') {
		$stop_date = mysqli_real_escape_string($conn, $_REQUEST['stop_date']);
		$stop_date = date("Y-m-d", strtotime($stop_date));
	} else {
		$stop_date = '0000-00-00';
	}	
	
	if($_POST['reset_schedule'] == '1' && date("Y-m-d", strtotime($start_date)) < date("Y-m-d")  ) {
		$start_date = date("Y-m-d");	
		$stop_date = '0000-00-00';		
	}
	
	$start_time = mysqli_real_escape_string($conn, $_REQUEST['tweet_start_time'] );
	
	$start_date_time = date("Y-m-d H:i:00", strtotime($start_date . " " . $start_time)); 


	if($start_date_time < date("Y-m-d H:i:00") ) {
		$start_date_time = date("Y-m-d H:i:00", strtotime("tomorrow". " " . $start_time)); 
	}
		
	$rowID = $_POST['scheduledRowID'];
	$tweet_text = mysqli_real_escape_string($conn, $_POST['scheduled_tweet_text'] );	
	
	if($_SESSION['isAdmin'])  {
		$tweet_text_parts = explode(" ", $tweet_text);
		$newText = '';
		foreach($tweet_text_parts as $word) {		
			if(substr( strtoupper($word), 0, 4) == 'WWW.' ) {
				$response = $googer->shorten($word);
				if($response) $word = $response;
			}
			if(substr( strtoupper($word), 0, 7) == 'HTTP://' ) {
				$response = $googer->shorten($word);
				if($response) $word = $response;
			}
			if(substr( strtoupper($word), 0, 7) == 'HTTPS://' ) {
				$response = $googer->shorten($word);
				if($response) $word = $response;
			}
			$newText .= $word. " ";
		}
		$tweet_text = rtrim($newText);
	}
	
	
	$frequency = mysqli_real_escape_string($conn, $_POST['tweet_frequency'] );	
	if($rowID == 0 ) {
		$SQL = "insert into scheduled_tweets set  user_id = '$user_id', tweet_text = '$tweet_text',  start_date_time = '$start_date_time', frequency = '$frequency', tweet_status = '$tweet_status', tweet_sent = '0', exceptions = '', stop_date = '$stop_date' ";	
		$result = mysqli_query($conn, $SQL) or die(mysqli_error());	
		
		$rowID = $conn->insert_id ;
		
	
	}
	
	if($_FILES['myPhoto']['name']>'') {  // we are trying to upload an image<strong></strong>
	
		$fileName = $_FILES['myPhoto']['name'];
		$tmpName  = $_FILES['myPhoto']['tmp_name'];
		$fileSize = $_FILES['myPhoto']['size'];	
		$fileType = $_FILES['myPhoto']['type'];
	
		if($fileSize > 3145728) {
?>
			<script type='text/javascript'>				
				alert("Your file is too large.  Please reduce the size to under 3Mb and try again");
				//var object = parent.jQuery("#containment_wrapper");
				//var currentDesign = parent.jQuery("#containment_wrapper").html();
				//parent.jQuery("#containment_wrapper").html(currentDesign);		
				setTimeout(function () {
				   try {
					  parent.window.hs.getExpander().close();
					  parent.window.scheduledList();
				   } catch (e) {}
				}, 200);      
			</script>
	
<?	
			die();
		} else {
			
			
			if($rowID != '') {								
				
				$objects = $s3Client->getIterator('ListObjects', array(
					'Bucket' => 'jukebox-member-images',
					'Prefix' => $rowID . "_S_" . $user_id
				));
				
				
				foreach ($objects as $object) {															
					$deleteResult = $s3Client->deleteObject(array(
						'Bucket' => 'jukebox-member-images',
						'Key'    => $object['Key'] 
					));	
				}								
			}	
			
			
			$xFileParts = explode(".", $fileName);
			$numberOfParts = count ($xFileParts) - 1;
			
			$mime = image_type_to_mime_type(exif_imagetype($_FILES['myPhoto']['tmp_name']));
									
			$fileName = $rowID . "_" . $user_id. "_S_" . rand(1,1000) . "." . $xFileParts[$numberOfParts];									
			// Upload an object by streaming the contents of a file
			// $pathToFile should be absolute path to a file on disk
			$result = $s3Client->putObject(array(
				'Bucket'     => 'jukebox-member-images',
				'Key'        => $fileName,
				'ContentType' => $mime,
				'SourceFile' => $_FILES['myPhoto']['tmp_name']				
			));

			
			
			$SQL = "update  scheduled_tweets set photo = '$fileName', media_id = '' where user_id = '$user_id' and id = '$rowID' ";
			$result = mysqli_query($conn, $SQL);				
		}
	} else {		
		if($_POST['removePhoto']) {
			//$SQL = "update  scheduled_tweets set photo = '' where user_id = '$user_id' and id = '$rowID' ";
			if($_POST['reset_schedule']) {
				$SQL = "update  scheduled_tweets set tweet_last_post_date = '0000-00-00 00:00:00', tweet_text = '$tweet_text', start_date_time = '$start_date_time', frequency = '$frequency', tweet_status = '$tweet_status', photo = '', media_id = '', stop_date = '0000-00-00' where user_id = '$user_id' and id = '$rowID' ";
			} else {
				$SQL = "update  scheduled_tweets set tweet_text = '$tweet_text', start_date_time = '$start_date_time', frequency = '$frequency', tweet_status = '$tweet_status', photo = '', media_id = '', stop_date = '$stop_date' where user_id = '$user_id' and id = '$rowID' ";
			}		
		} else {
			if($_POST['reset_schedule']) {
				$SQL = "update  scheduled_tweets set tweet_last_post_date = '0000-00-00 00:00:00', tweet_text = '$tweet_text', start_date_time = '$start_date_time', frequency = '$frequency', tweet_status = '$tweet_status', stop_date = '$stop_date' where user_id = '$user_id' and id = '$rowID' ";
			} else {
				$SQL = "update  scheduled_tweets set tweet_text = '$tweet_text', start_date_time = '$start_date_time', frequency = '$frequency', tweet_status = '$tweet_status', stop_date = '$stop_date' where user_id = '$user_id' and id = '$rowID' ";
			}		
		}	
	}
	$result = mysqli_query($conn, $SQL);
	//print $stop_date;
	//die($SQL);	
?>	
	<script type='text/javascript'>
		
		setTimeout(function () {
		   try {
			  parent.window.hs.getExpander().close();
			  parent.window.scheduledList();		  
		   } catch (e) {}
		}, 200);      
	</script>	
<?	
}
?>
<?
if($rowID > 0 ) {
	$SQL = "select * from scheduled_tweets where user_id = '$user_id' and id = '$rowID' ";
	$result = mysqli_query($conn, $SQL);
	if(mysqli_num_rows($result)) {
		$row = mysqli_fetch_assoc($result);
		foreach( $row AS $key => $val ) {
			$$key = stripslashes( $val );					
		}
	} else {
		header('Location: /oops.php');
		exit();
	}
} else {
	$tweet_text = '';
	$photo = '';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-Equiv="Cache-Control" Content="no-cache">
<meta http-Equiv="Pragma" Content="no-cache">
<meta http-Equiv="Expires" Content="0">
<title>Untitled Document</title>
<link rel="stylesheet" href="/assets/css/style.css">
<link rel="stylesheet" href="/assets/fonts/raphaelicons.css">
<link rel="stylesheet" href="/assets/css/main.css">

<script src="//code.jquery.com/jquery-1.11.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<!--link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" /-->
<link rel="stylesheet" href="/assets/css/jquery-ui.css">
<script>window.jQuery || document.write('<script src="/assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
<script src="/include/js_boot.js"></script>
<script type="text/javascript" src="jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="jquery.timepicker.css" />
<script>
function showRemainingScheduledCharacters(msg, photo) {
	var currentPhoto = '';
	<? if($photo > '') { ?>
	currentPhoto = 'exists';
	<? } ?>	
	if(photo.length > 0 || currentPhoto == 'exists' ) {	
		var c = 140 - 25 - twttr.txt.getTweetLength(msg); 	
	} else {
		var c = 140 - twttr.txt.getTweetLength(msg);
	}
	$('#editScheduledMessageCount').val(c);
	if(parseInt(c) >= 0) {
		$('#scheduleLengthError').html('');	
	} else {
		$('#scheduleLengthError').html('Tweet is too large');	
	}	
	$('#editScheduledMessageCount').val(c);
	

}
</script>
</head>

<body>
<script src="/include/twitter-text.js" ></script>
   <div class="validateTips">&nbsp;</div>
  <form class="c-form" id="scheduledTweetForm" name="scheduledTweetForm" accept-charset="utf-8"  method="post" enctype="multipart/form-data" action="editScheduledTweet.php" >
	<div style="min-height:125px; vertical-align:text-top; float:right; padding-top:200px; " >
	  <? if($photo) { ?>Remove this photo <input name="removePhoto" id="removePhoto" type="checkbox" value="1"  />&nbsp;<? } ?>	  
	  <img id="tweetPhoto" src="https://s3.amazonaws.com/jukebox-member-images/<? if($photo) echo $photo; else echo "spacer.gif"; ?>" alt="" height="100" />
	</div>  
		<fieldset>
		<input id="editScheduledMessageCount" type="text" readonly="" value="" style="display:inline-block; width:50px;"><span id="scheduleLengthError" style="display:inline-block; color:#FF0000;"></span>
		<script>
			var remainingCharacters = 140 - twttr.txt.getTweetLength('<? echo mysqli_real_escape_string($conn, $tweet_text); ?>'); 	
			<? if($photo > '') { ?>
				remainingCharacters = remainingCharacters - 25;
			<? } ?>	
			$('#editScheduledMessageCount').val(remainingCharacters);
		</script>	
		<textarea name="scheduled_tweet_text" cols="30" rows="10" id="scheduled_tweet_text" onKeyUp="showRemainingScheduledCharacters(this.value, $('#myPhoto').val());"><? echo $tweet_text;?></textarea>
		<img id="wait_screen" src="/images/wait.GIF" style="float:right; display:none; "  />

<!--		
		<label for="tweet_start_date" >Start Date</label>
		<input style="width:auto;" type="text" id="tweet_start_date"  name="tweet_start_date" value="<? echo date("m/d/Y", strtotime($row['start_date_time']));?>" readonly="" onChange="adjustTimePicker(this.value, '<? echo date('m/d/Y'); ?>', '<? echo date("h:00 A", strtotime("+15 minutes")); ?>')"  />		
		<label for="stop_date" >Stop Date</label>
		<input style="width:auto;" type="text" id="stop_date"  name="stop_date" value="<? echo date("m/d/Y", strtotime($row['stop_date']));?>" readonly=""   />		
-->
		<label for="tweet_start_date">Start Date </label>
		<input id="tweet_start_date" name="tweet_start_date" class="datepicker" data-provide="datepicker-inline" data-date-format="mm/dd/yyyy" value="<? echo date("m/d/Y", $row['start_date_time']);?>"  onChange="adjustTimePicker(this.value, '<? echo date('m/d/Y'); ?>', '<? echo date("h:00 A", strtotime("+15 minutes")); ?>')" />

		<label for="tweet_start_time">Time</label>
        <input style="width:auto;" id="tweet_start_time" name="tweet_start_time" type="text" class="time"  />
		<label for="tweet_frequency" >Frequency</label>
		<select id="tweet_frequency" name="tweet_frequency" style="width:auto;">
			<option value="0">Select a frequency...</option>
			<option value="86" <? if($frequency == 86) echo 'selected="selected"'; ?>>Once and Done</option>
			<!--option value="3600" <? if($frequency == 3600) echo 'selected="selected"'; ?>>Once every hour</option>
			<option value="7200" <? if($frequency == 7200) echo 'selected="selected"'; ?>>Once every two hours</option>
			<option value="10800" <? if($frequency == 10800) echo 'selected="selected"'; ?>>Once every three hours</option>
			<option value="14400" <? if($frequency == 14400) echo 'selected="selected"'; ?>>Once every four hours</option-->
			<option value="18000" <? if($frequency == 18000) echo 'selected="selected"'; ?>>Once every five hours</option>
			<option value="21600" <? if($frequency == 21600) echo 'selected="selected"'; ?>>Once every six hours</option>
			<option value="25200" <? if($frequency == 25200) echo 'selected="selected"'; ?>>Once every seven hours</option>
			<option value="28800" <? if($frequency == 28800) echo 'selected="selected"'; ?>>Once every eight hours</option>
			<option value="32400" <? if($frequency == 32400) echo 'selected="selected"'; ?>>Once every nine hours</option>
			<option value="36000" <? if($frequency == 36000) echo 'selected="selected"'; ?>>Once every ten hours</option>
			<option value="43200" <? if($frequency == 43200) echo 'selected="selected"'; ?>>Once every twelve hours</option>
			<option value="54000" <? if($frequency == 54000) echo 'selected="selected"'; ?>>Once every fifteen hours</option>
			<option value="64800" <? if($frequency == 64800) echo 'selected="selected"'; ?>>Once every eighteen hours</option>
			<option value="75600" <? if($frequency == 75600) echo 'selected="selected"'; ?>>Once every twenty-one hours</option>
			<option value="86400" <? if($frequency == 86400) echo 'selected="selected"'; ?>>Once every day</option>
			<option value="108000" <? if($frequency == 108000) echo 'selected="selected"'; ?>>Once every thirty hours</option>
			<option value="129600" <? if($frequency == 129600) echo 'selected="selected"'; ?>>Once every thirty-six hours</option>
			<option value="151200" <? if($frequency == 151200) echo 'selected="selected"'; ?>>Once every fourty-two hours</option>
			<option value="172800" <? if($frequency == 172800) echo 'selected="selected"'; ?>>Once every two days</option>
			<option value="259200" <? if($frequency == 259200) echo 'selected="selected"'; ?>>Once every three days</option>
			<option value="345600" <? if($frequency == 345600) echo 'selected="selected"'; ?>>Once every four days</option>
			<option value="432000" <? if($frequency == 432000) echo 'selected="selected"'; ?>>Once every five days</option>
			<option value="518400" <? if($frequency == 518400) echo 'selected="selected"'; ?>>Once every six days</option>
			<option value="604800" <? if($frequency == 604800) echo 'selected="selected"'; ?>>Once every week</option>
			<option value="691200" <? if($frequency == 691200) echo 'selected="selected"'; ?>>Once every eighth day</option>
			<option value="777600" <? if($frequency == 777600) echo 'selected="selected"'; ?>>Once every nineth day</option>
			<option value="864000" <? if($frequency == 864000) echo 'selected="selected"'; ?>>Once every tenth day</option>
			<option value="950400" <? if($frequency == 950400) echo 'selected="selected"'; ?>>Once every eleventh day</option>
			<option value="1036800" <? if($frequency == 1036800) echo 'selected="selected"'; ?>>Once every twelfth day</option>
			<option value="1123200" <? if($frequency == 1123200) echo 'selected="selected"'; ?>>Once every thirteenth day</option>
			<option value="1209600" <? if($frequency == 1209600) echo 'selected="selected"'; ?>>Once every two weeks</option>
			<option value="1814400" <? if($frequency == 1814400) echo 'selected="selected"'; ?>>Once every three weeks</option>
			<option value="2419200" <? if($frequency == 2419200) echo 'selected="selected"'; ?>>Once every four weeks</option>
			<option value="3024000" <? if($frequency == 3024000) echo 'selected="selected"'; ?>>Once every five weeks</option>
			<option value="3628800" <? if($frequency == 3628800) echo 'selected="selected"'; ?>>Once every six weeks</option>
			<option value="4838400" <? if($frequency == 4838400) echo 'selected="selected"'; ?>>Once every eight weeks</option>
			<option value="7257600" <? if($frequency == 7257600) echo 'selected="selected"'; ?>>Once every 12 weeks</option>
			<option value="14515200" <? if($frequency == 14515200) echo 'selected="selected"'; ?>>Once every 24 weeks</option>
			<option value="31536000" <? if($frequency == 31536000) echo 'selected="selected"'; ?>>Once every year</option>
		</select>
		
		<label for="stop_date">End Date </label>
		<input id="stop_date" name="stop_date" class="datepicker" data-provide="datepicker-inline" data-date-format="mm/dd/yyyy" value="<? if($row['stop_date'] != '0000-00-00') echo date("m/d/Y", strtotime($row['stop_date']));?>">
		<label id="dateRangeError" style="color:#FF0000; visibility:hidden;"> Date Range Is Invalid!</label>
		
		
		<label for="myPhoto" ><b>Add Photo</b></label>
		<input type="file" name="myPhoto" id="myPhoto"  onChange="readImage(this);" />
		<input type="hidden" name="scheduledRowID" id="scheduledRowID" value="<? echo $rowID; ?>" />
		<p style="padding-top:10px;">
		<input type="checkbox" value="1" id="tweet_status" name="tweet_status" <? if($tweet_status == 'draft') echo 'checked="checked"'; ?>  /> <b>Hold As Draft</b> - Do Not Tweet
		</p>
		<? if($rowID) { ?>
		<p style="padding-top:10px;">
		<input type="checkbox" name="reset_schedule" id="reset_schedule" value="1"   />	<b>Reset Schedule</b> will reset the tweet as NOT POSTED and adjust next post date.
		</p>	
		<? } ?>		
		</fieldset>		
		<hr />
		<p align="center">
			<input type="button" name="submitButton" id="submitButton" value="Submit" onClick="checkForm();" />				
		</p>		
	</form>		
<script>
$('#tweet_start_date').val(''); 
$('#tweet_start_time').val(''); 
$('#tweet_frequency').val('0'); 
$( '#tweet_start_date' ).prop('disabled', false);  
$( '#tweet_start_time' ).prop('disabled', false); 
$('#tweet_start_time').timepicker({ 'scrollDefaultNow': true, 'step': 15, 'timeFormat': 'h:i A' , 'minTime': '<? echo date("H:00 A", strtotime("+15 minutes")); ?>', 'maxTime': '11:45 PM'  }); 
$( '#tweet_start_date' ).datepicker({ minDate: new Date( ) }); 
<? if($tweet_status == 'draft' ) {?> 
	$('#tweet_status').prop('checked', true); <? } 
else { ?> 
	$('#tweet_status').prop('checked', false); 
<? } ?> 
<? if($rowID > 0) { ?>
	$('#tweet_start_time').timepicker({ 'scrollDefaultNow': true, 'step': 15 });  
	//$( '#tweet_start_date' ).prop('disabled', true);  
	//$( '#tweet_start_time' ).prop('disabled', true); 
	$( '#tweet_start_date' ).datepicker(); 			
	$('#tweet_frequency').val('<? echo ($row['frequency']);?>'); 
	$('#tweet_start_date').val('<? echo date("m/d/Y", strtotime($row['start_date_time']));?>'); 
	$('#tweet_start_time').val('<? echo date("h:i A", strtotime($row['start_date_time']));?>');
<? } ?>

var 
  scheduled_tweet_text = $( "#scheduled_tweet_text" ),
  tweet_start_date = $( "#tweet_start_date" ),
  tweet_start_time = $( "#tweet_start_time" ),
  tweet_frequency = $( "#tweet_frequency" ),
  allFields = $( [] ).add( scheduled_tweet_text ).add( tweet_start_date ).add( tweet_start_time ).add ( tweet_frequency),
  tips = $( ".validateTips" );
function checkForm() {
	var bValid = true;	
	$('#dateRangeError').css('visibility', 'hidden');
	if($('#stop_date').val() != '0000-00-00' ) {
		var startDt = $('#tweet_start_date').val();
		var endDt = $('#stop_date').val();
		if( new Date(startDt).getTime() >= new Date(endDt).getTime() ) 	{		
			$('#dateRangeError').css('visibility', 'visible');
			bValid = false;					
		}
	}
						
	//bValid = bValid && checkLength( tweet_text, "Tweet Text", 3, 140 );
	//bValid = bValid && checkLength( tweet_category, "Category", 3, 200 );
	//bValid = bValid && checkLength( tweet_author, "Author", 3, 200 );
	//bValid = bValid && checkLength( tweet_tags, "Tags", 3, 200 );
	// bValid = bValid && checkRegexp( name, /^[a-z]([0-9a-z_])+$/i, "Username may consist of a-z, 0-9, underscores, begin with a letter." );
	// bValid = bValid && checkRegexp( email, /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "eg. ui@jquery.com" );
	// bValid = bValid && checkRegexp( password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );
	//bValid = bValid && checkLength( scheduled_tweet_text, "Tweet Text", 3, 140 );
	bValid = bValid && checkDate( tweet_start_date );
	bValid = bValid && checkTime( tweet_start_time );
	bValid = bValid && checkFrequency( tweet_frequency );
	
	if( parseInt($('#editScheduledMessageCount').val()) <0 ) {
		$('#scheduleLengthError').html('Tweet is too large');
		bValid = false;
		return false;
	}	
		
	if(bValid) {
		$('#wait_screen').css('display', 'block'); document.getElementById('scheduledTweetForm').submit();
	}
	
}
function checkDate(o) { 
	if ( o.val().length == 0  ) {
		o.addClass( "ui-state-error" );
		updateTips( "You must enter a start date." );
		return false;
	} else {
		o.removeClass( "ui-state-error" );
		return true;
	}
}
	
function checkTime(o) {
	if ( o.val().length == 0  ) {
		o.addClass( "ui-state-error" );
		updateTips( "You must enter a start time." );
		return false;
	} else {
		o.removeClass( "ui-state-error" );
		return true;
	}
}
 	
function checkFrequency(o) {
	if ( o.val() == '0'  ) {
		o.addClass( "ui-state-error" );
		updateTips( "You must enter a frequency." );
		return false;
	} else {
		o.removeClass( "ui-state-error" );
		return true;
	}
}
 
function checkLength( o, n, min, max ) { 
  if ( o.val().length > max || o.val().length < min ) {
	o.addClass( "ui-state-error" );
	updateTips( "Length of " + n + " must be between " +
	  min + " and " + max + "." );
	return false;
  } else {
	o.removeClass( "ui-state-error" );
	return true;
  }
}

function checkRegexp( o, regexp, n ) {
  if ( !( regexp.test( o.val() ) ) ) {
	o.addClass( "ui-state-error" );
	updateTips( n );
	return false;
  } else {
	o.removeClass( "ui-state-error" );
	return true;
  }
}
function updateTips( t ) {
  tips
	.text( t )
	.addClass( "ui-state-highlight" );
  setTimeout(function() {
	tips.removeClass( "ui-state-highlight", 1500 );
  }, 500 );
}
function adjustTimePicker(sd, td, tn) {
	$('#tweet_start_time').timepicker('remove');
	if(sd == td) {
		$('#tweet_start_time').timepicker({ 'scrollDefaultNow': true, 'step': 15, 'timeFormat': 'h:i A' , 'minTime': tn, 'maxTime': '11:45 PM'  });
		//$('#tweet_start_time').timepicker({ 'minTime': tn });
	} else {
		$('#tweet_start_time').timepicker({ 'scrollDefaultNow': true, 'step': 15, 'timeFormat': 'h:i A' , 'minTime': '12:00 AM' , 'maxTime': '11:45 PM'  });
		//$('#tweet_start_time').timepicker({ 'minTime': '12:00 AM' });		
	}
}
</script>	
<?

function timetostr($frequency) {  // frequency as seconds int
	if($frequency =="3600") return "Once every hour";
	if($frequency =="7200") return "Once every two hours";
	if($frequency =="10800") return "Once every three hours";
	if($frequency =="14400") return "Once every four hours";
	if($frequency =="18000") return "Once every five hours";
	if($frequency =="21600") return "Once every six hours";
	if($frequency =="25200") return "Once every seven hours";
	if($frequency =="28800") return "Once every eight hours";
	if($frequency =="32400") return "Once every nine hours";
	if($frequency =="36000") return "Once every ten hours";
	if($frequency =="43200") return "Once every twelve hours";
	if($frequency =="54000") return "Once every fifteen hours";
	if($frequency =="64800") return "Once every eighteen hours";
	if($frequency =="75600") return "Once every twenty-one hours";
	if($frequency =="86400") return "Once every day";
    if($frequency =="108000") return "Once every thirty hours";
	if($frequency =="129600") return "Once every thirty-six hours";
	if($frequency =="151200") return "Once every fourty-two hours";
	if($frequency =="172800") return "Once every two days";
	if($frequency =="259200") return "Once every three days";
	if($frequency =="345600") return "Once every four days";
	if($frequency =="432000") return "Once every five days";
	if($frequency =="518400") return "Once every six days";
	if($frequency =="604800") return "Once every week";
	if($frequency =="691200") return "Once every eighth day";
	if($frequency =="777600") return "Once every nineth day";
	if($frequency =="864000") return "Once every tenth day";
	if($frequency =="950400") return "Once every eleventh day";
	if($frequency =="1036800") return "Once every twelfth day";
	if($frequency =="1123200") return "Once every thirteenth day";
	if($frequency =="1209600") return "Once every two weeks";
	if($frequency =="1814400") return "Once every three weeks";
	if($frequency =="2419200") return "Once every four weeks";
	if($frequency =="3024000") return "Once every five weeks";
	if($frequency =="3628800") return "Once every six weeks";
	if($frequency =="4838400") return "Once every eight weeks";
	if($frequency =="7257600") return "Once every 12 weeks";
	if($frequency =="14515200") return "Once every 24 weeks";
	if($frequency =="31536000") return "Once every year";	    	
}
?>
<script>
$('#tweet_start_date').datepicker(
	{
		format: 'mm/dd/yyyy',
//		startDate: '+2d',
		autoclose: true			
	}
);
$('#stop_date').datepicker(
	{
		format: 'mm/dd/yyyy',	
		autoclose: true			
	}
);

function readImage(input) {  	
	var imageExts = ["jpg","jpeg","png", "gif"];
	var filePathName = $('#myPhoto').val();
	var fileParts = filePathName.split(".");
	var ext = fileParts[fileParts.length -1];
	ext = ext.toLowerCase();	
	
	
	if(imageExts.indexOf(ext) != -1) {   // only render if image is ok to display			
		if (input.files && input.files[0])  {
						
			var reader = new FileReader();
			reader.onload = function (e) {									  
				$('#tweetPhoto')
				.attr('src',e.target.result)
				.height(100);				
			};
			reader.readAsDataURL(input.files[0]);
			var remains = 140 - 25 - twttr.txt.getTweetLength($('#scheduled_tweet_text').val()); 
			$('#editScheduledMessageCount').val(remains);
	   }
	} else {
		alert("Invalid format. Must be .png, .gif, or .jpg types only");
		$('#myPhoto').val('');
	}	
}
</script>
<?
// Declare the class
class GoogleUrlApi {
	
	// Constructor
	function GoogleURLAPI($key,$apiURL = 'https://www.googleapis.com/urlshortener/v1/url') {
		// Keep the API Url
		$this->apiURL = $apiURL.'?key='.$key;
	}
	
	// Shorten a URL
	function shorten($url) {
		// Send information along
		$response = $this->send($url); 
		// Return the result
		return isset($response['id']) ? $response['id'] : false;
	}
	
	// Expand a URL
	function expand($url) {
		// Send information along
		$response = $this->send($url,false);
		// Return the result
		return isset($response['longUrl']) ? $response['longUrl'] : false;
	}
	
	// Send information to Google
	function send($url,$shorten = true) {
		// Create cURL
		$ch = curl_init();
		// If we're shortening a URL...
		if($shorten) {
			curl_setopt($ch,CURLOPT_URL,$this->apiURL);
			curl_setopt($ch,CURLOPT_POST,1);
			curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode(array("longUrl"=>$url)));
			curl_setopt($ch,CURLOPT_HTTPHEADER,array("Content-Type: application/json"));
		}
		else {
			curl_setopt($ch,CURLOPT_URL,$this->apiURL.'&shortUrl='.$url);
		}
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		// Execute the post
		$result = curl_exec($ch);
		// Close the connection
		curl_close($ch);
		// Return the result
		return json_decode($result,true);
	}		
}

?>
</body>
</html>
