<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');
$user_id = $_SESSION['access_token']['user_id'];

if($_SESSION['planName'] == 'Free' ) {
	$SQL = "update users set master_account_id = '', bt_plan_id = '', planName = 'Cancelled', status = '2', all_tweet_status = '0', scheduled_tweets_status = '0', send_thanks = '0'  where user_id = '$user_id' " ;
	$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
	$SQL = "update user_schedule set schedule_status = '0' where user_id = '$user_id' ";
	$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
	$SQL = "update scheduled_tweets set tweet_status = 'draft' where user_id = '$user_id' ";
	$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
	exit();
}


require_once $_SERVER['DOCUMENT_ROOT'].'/braintree-php-3.1.0/lib/Braintree.php';
// sandbox 
/*
Braintree_Configuration::environment('sandbox');
Braintree_Configuration::merchantId('zsbs7swsdwdzvmwg');
Braintree_Configuration::publicKey('yb7qkdry9qfvzbn9');
Braintree_Configuration::privateKey('36ae489f8f321abd38f0971f5f1ff2cf');
*/

Braintree_Configuration::environment('production');
Braintree_Configuration::merchantId('dxpnw5b448vwd9rp');
Braintree_Configuration::publicKey('mg2pmbnsn3g4cqnq');
Braintree_Configuration::privateKey('cf12d53af0f632d8fb98c91af4b86741');


$SQL = "select bt_plan_id from users where user_id = '$user_id' ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
$row = mysqli_fetch_assoc($result);
$bt_plan_id = $row['bt_plan_id'];
if($bt_plan_id) $cannedIt = Braintree_Subscription::cancel($bt_plan_id);


$SQL = "select master_account_id from users where user_id = '$user_id' ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
if(mysqli_num_rows($result) == 1 ) {
	$row = mysqli_fetch_assoc($result);
	$master_account_id = $row['master_account_id'];
	if( $master_account_id && $master_account_id == $_SESSION['loggedInAs'] ) { // ok we have a master account id and it is the master who is logged in - we need to cancel them all
		$SQL2 = "select * from users where master_account_id = '$master_account_id' "; 
		$result2 = mysqli_query($conn, $SQL2) or die(mysqli_error($conn));
		$row2 = mysqli_fetch_assoc($result2);
		do {
			$user_id = $row2['user_id'];		
			//$SQL = "update users set planName = 'Gold', master_account_id = '', status = '2', all_tweet_status = '0', scheduled_tweets_status = '0', send_thanks = '0'  where user_id = '$user_id' " ;
			$SQL = "update users set planName = 'Cancelled', status = '2', all_tweet_status = '0', bt_plan_id = '', scheduled_tweets_status = '0', send_thanks = '0'  where user_id = '$user_id' " ;
			$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
			$SQL = "update user_schedule set schedule_status = '0' where user_id = '$user_id' ";
			$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
			$SQL = "update scheduled_tweets set tweet_status = 'draft' where user_id = '$user_id' ";
			$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));									
		} while($row2 = mysqli_fetch_assoc($result2));	
	} else {  // we have no linked accounts so this is easy just nix the one account	
		//$SQL = "update users set planName = 'Gold', master_account_id = '', status = '2', all_tweet_status = '0', scheduled_tweets_status = '0', send_thanks = '0'  where user_id = '$user_id' " ;
		$SQL = "update users set planName = 'Cancelled', status = '2', all_tweet_status = '0', bt_plan_id = '', master_account_id = '', scheduled_tweets_status = '0', send_thanks = '0'  where user_id = '$user_id' " ;
		$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
		$SQL = "update user_schedule set schedule_status = '0' where user_id = '$user_id' ";
		$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
		$SQL = "update scheduled_tweets set tweet_status = 'draft' where user_id = '$user_id' ";
		$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));			
 	}
} else {  // means we have no user record for the current user - should never happen
	exit();
}	
exit();
?>