<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/twitteroauth/twitteroauth.php");
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/config.php");

if(!isset($_SESSION['access_token'])) die();
?>
<?	
	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);		
	//$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);		
	$content = $connection->get('search/tweets', array('q' => '@'.$_SESSION['screen_name'], 'count' => 100));
	
	
	if (is_array($content) || is_object($content)) {
			$thisContent = object_to_array($content);	
	}
	
	/*
	print $_SESSION['screen_name'];	
	print "<pre>";
	print_r($thisContent['statuses'][2]);
	print "<hr>";
	print_r($thisContent['statuses'][3]);
	print "<hr>";
	print_r($thisContent['statuses'][4]);
	
	print_r($thisContent);
	
	die();
	*/
	if( count($thisContent['statuses'])  ) {	
		$mentions = 0;				
	?>
	<h2 align="center">Who is mentioning or retweeting me?</h2>
		<span style="float:right; cursor:pointer;">
			<img src="../images/users.png" title="Unique Users"  />
			<select style="display:inline-table; cue:url(../images/bar-chart.png)" onchange="if(this.value != '0' ) recentMentionsUniqueUsers(this.value)">
				<option value="0">Select Time</option>
				<option value="Today">Today</option>
				<option value="Last 7 Days">Last 7 Days</option>
				<option value="Last 30 Days">Last 30 Days</option>
				<option value="This Month To Date">This Month</option>
				<option value="Last Month">Last Month</option>		
			</select>
			<img src="../images/spacer.gif" width="10px" />
			<img src="../images/bar-chart.png"  />
			<select style="display:inline-table; cue:url(../images/bar-chart.png)" onchange="if(this.value != '0' ) recentMentionsCharted(this.value)">
				<option value="0">Select Time</option>
				<option value="Today">Today</option>
				<option value="Last 7 Days">Last 7 Days</option>
				<option value="Last 30 Days">Last 30 Days</option>
				<option value="This Month To Date">This Month</option>
				<option value="Last Month">Last Month</option>
			</select>
		</span>
	<table id="mentionsTable" border="" width="100%" >				  
	  <tbody>
	<?
		foreach($thisContent['statuses'] as $status) {
			if($status['user']['screen_name'] != $_SESSION['screen_name'] ) {
	?>
			  <tr>						
				<td nowrap="nowrap">								
					<img src="<? echo $status['user']['profile_image_url'];?>" alt="image description" width="44" height="44" />
					<p><a href="http://twitter.com/<? echo $status['user']['screen_name']; ?>" target="_blank" ><? echo $status['user']['screen_name'] ; ?></a></p>
				</td>
				<td>
					<? echo $status['text'] ; ?>
					<p><a href="http://twitter.com/<? echo $status['user']['screen_name'];?>/status/<? echo $status['id'];?>" target="_blank"><? echo $status['created_at'];?></a></p>
					<p>Has <? echo $status['user']['followers_count']. ' Followers'; ?>&nbsp;&nbsp;And Follows: <? echo $status['user']['friends_count']; ?> others.</p>
				</td>
			  </tr>	
	<?
				$mentions ++;
			}
		}
	?>	
	  </tbody>
	  </table>	
	<?
		if($mentions == 0 ) {
	?>	
			<h2 align="center">Sorry, <? echo $_SESSION['screen_name'];?>, you don't seem to have any mentions</h2>
	<? 
		} 
	?>
	<?					
	} else {
	?>
	<h2 align="center">Sorry, <? echo $_SESSION['screen_name'];?>, you don't seem to have any mentions</h2>
	<?	
	}
?>