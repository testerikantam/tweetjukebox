<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/twitteroauth/twitteroauth.php");
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/config.php");
?>
<?
if(!isset($_SESSION['access_token'])) die();

$text = $_REQUEST['message'];
str_replace("#", '%23', $text);
str_replace("'", '%27', $text);

if($_REQUEST['rowID']) {
	$rowID = $_REQUEST['rowID'];
	$tweetSent = date("Y-m-d H:i:s");
	$SQL = "update scheduled_tweets set tweet_last_post_date = '$tweetSent', tweet_sent = '1', exceptions = '' where id = '$rowID' ";
	$result = mysqli_query($conn, $SQL);	
}

$SQL2 = "select * from scheduled_tweets where id = '$rowID' ";
$result2 = mysqli_query($conn, $SQL2) ;	
$row2 = mysqli_fetch_assoc($result2);	

$fileName = stripslashes($row2['photo']);

$access_token = $_SESSION['access_token'];
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
// here is the tweet
//$text = htmlentities($text);
$text = str_replace('�', "'", $text);
//$text = mb_convert_encoding($text,'HTML-ENTITIES','utf-8');

$text = stripslashes($text);

if($fileName == '' ) { // nope if this is not an image
	$content = $connection->post('statuses/update', array('status' => $text));
	if($content->errors) {			
		$errorCodes = '';
		foreach($content->errors as $error) {
			$errorCodes .= "(".$error->code.") ".$error->message. "   ";
		}														
		$tweetSent = date("Y-m-d H:i:s", $currentDateTime);					
		$SQL = "update scheduled_tweets set tweet_last_post_date = '$tweetSent', tweet_sent = '0', exceptions = '$errorCodes' where id = '$rowID' ";
		$result = mysqli_query($conn, $SQL);	
	}
} else { 												
	//$fileName = "@/home/yauhen/dev/tweetjunkebox/tweetjunkebox".$filename;
	$filePath = "https://s3.amazonaws.com/jukebox-member-images/".$fileName;
	//$content = $connection->upload('statuses/update_with_media', array('status' => $text, 'media[]' => $fileName  )); 
	
	$url = 'https://upload.twitter.com/1.1/media/upload.json';
	$method = 'POST';
	$parameters = array(
		'media' => base64_encode(file_get_contents($filePath)),
	);
	$request = OAuthRequest::from_consumer_and_token($connection->consumer, $connection->token, $method, $url, $parameters);
	$request->sign_request($connection->sha1_method, $connection->consumer, $connection->token);
	
	$response = $connection->http($request->get_normalized_http_url(), $method, $request->to_postdata());
	if ($connection->format === 'json' && $connection->decode_json) {
		$response = json_decode($response);
		//$response = object_to_array($response);
	}
	
	if($response->media_id_string ) {
		$media_id = $response->media_id_string;
		$content = $connection->post('statuses/update', array('status' => $text, 'media_ids' => $media_id));	
		//$content = json_decode($content);
	}
	if($content->errors) {			
		$errorCodes = '';
		foreach($content->errors as $error) {
			$errorCodes .= "(".$error->code.") ".$error->message. "   ";
		}														
		$tweetSent = date("Y-m-d H:i:s", $currentDateTime);					
		$SQL = "update scheduled_tweets set tweet_sent = '0', exceptions = '$errorCodes' where id = '$rowID' ";
		$result = mysqli_query($conn, $SQL);	
	}
	
	
}



//$content = $connection->post('statuses/update', array('status' => $text));
if (is_array($content) || is_object($content)) {
	$content = object_to_array($content);
}
if($content['errors']) {
	//print "<pre>";	
	print $content['errors'][0]['code']. "(" . $content['errors'][0]['message']. ")";  // 186 is status over 140 characters
	//print_r($content); 
	exit();
}
	
	
/* use for testing images ??????????????????????????????
$image = "./dickvandyke.jpg";
array(
   'media[]'  => "@{$image}",
   'status'   => "Don't slip up" // Don't give up..
),
*/
/*
$screen_name = $_SESSION['access_token']['screen_name'];
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
$content = $connection->get('users/show', array('screen_name' => $screen_name));

if (is_array($content) || is_object($content)) {
	$content = object_to_array($content);
}

$response = ($content['statuses_count'] .",". $content[followers_count] .",". $content['friends_count']);

echo $response;
*/
exit();
?>