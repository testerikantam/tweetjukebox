<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/twitteroauth/twitteroauth.php");
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/config.php");

if(!isset($_SESSION['access_token'])) die();
?>
<?	
$access_token = $_SESSION['access_token'];
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);		
$content = $connection->get('followers/list', array('count' => 200));
//$content = $connection->get('search/tweets', array('q' => 'alphabetsuccess', 'count' => 200));				
if (is_array($content) || is_object($content)) {
		$thisContent = object_to_array($content);	
}


if( count($thisContent && $thisContent['users'])  ) {	
?>
<h2 align="center">Who is following me?</h2>
<table id="mentionsTable" border="" width="100%" >				  
  <tbody>
<?
	foreach($thisContent['users'] as $status) {		
?>
		  <tr>						
			<td nowrap="nowrap">								
				<span>
					<img src="<? echo $status['profile_image_url'];?>" alt="image description" width="44" height="44" />
					<a href="http://twitter.com/<? echo $status['screen_name']; ?>" target="_blank" ><? echo $status['screen_name'] ; ?></a>
				</span>
			</td>
			<td>Has <? echo $status['followers_count']. ' Followers'; ?>&nbsp;&nbsp;And Follows: <? echo $status['friends_count']; ?> others.</td>
		  </tr>	
<?
		}
?>	
  </tbody>
  </table>	
<?					
} else {
?>
<h2 align="center">Sorry, <? echo $_SESSION['screen_name'];?>, you don't seem to have anyone following you</h2>
<?	
}
?>	