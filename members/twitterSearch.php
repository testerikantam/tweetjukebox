<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/twitteroauth/twitteroauth.php");
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/config.php");
?>
<?
if(!isset($_SESSION['access_token'])) die();

$searchTerm = $_REQUEST['searchTerm'];

$searchTerm = urlencode($searchTerm);


$access_token = $_SESSION['access_token'];
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
			
$content = $connection->get('search/tweets', array('q' => $searchTerm, 'count' => 5));
//$content = $connection->get('search/tweets', array('q' => 'alphabetsuccess', 'count' => 200));				
if (is_array($content) || is_object($content)) {
		$thisContent = object_to_array($content);	
}
if( count($thisContent['statuses'])  ) {	
	$mentions = 0;				
?>

<table id="mentionsTable" border="" width="100%" >
  <thead>
    <tr>
      <th>Time</th>
      <th>Screen Name</th>
      <th>Text</th>
    </tr>
  </thead>
  <tbody>
    <?
					foreach($thisContent['statuses'] as $status) {
						if($status['user']['screen_name'] != $_SESSION['screen_name'] ) {
						//if($status['user']['screen_name'] != 'alphabetsuccess' ) {
				?>
    <tr>
      <td nowrap="nowrap"><? echo date("m/d/Y g:i A", strtotime($status['created_at']) ); ?></td>
      <td><? echo $status['user']['screen_name'] ; ?></td>
      <td><? echo $status['text'] ; ?></td>
    </tr>
    <?
							$mentions ++;
						}
					}
				?>
  </tbody>
</table>
<?
					if($mentions == 0 ) {
				?>
<h3 align="center">Sorry, your search for <b><? echo $_REQUEST['searchTerm']; ?></b> did not return any results.</h3>
<? 
					} 
				?>
<?					
				} else {
				?>
<h3 align="center">Sorry, your search for <b><? echo $_REQUEST['searchTerm']; ?></b> did not return any results.</h3>
<?	
				}
				?>
