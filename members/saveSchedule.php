<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');
?>
<?
$user_id = $_SESSION['access_token']['user_id'];

$days = array('mon', 'tue', 'wed', 'thur', 'fri', 'sat', 'sun');


$sqlSet = 'set ';

if (!is_array($_POST['data'])) {

	exit();
}	



foreach($_POST['data'] as $data) {
	$key = $data['name']; 
	if($key != 'list_number' && $key != 'list_name' && $key != 'current_list_name' && substr($key, 10, 5) != 'hours' && substr($key, 10, 7) != 'minutes' ) {  // these are special
		$value = $data['value'];	
		$row = substr($key, strlen($key) -1 , 1) ;
			
		$day = $days[$row -1 ];  // gives us a piece of the field in the DB    mon_start_1, mon_end_1, mon_frequency_1, ... etc.
		
		$name = substr($key, 0, strlen($key) - 1) ; // second piece
		
		$fieldName = $day."_".$name;

		if($fieldName == 'mon_frequency_1' && $value < 1 ) $value = 'off';
		if($fieldName == 'tue_frequency_1' && $value < 1 ) $value = 'off';
		if($fieldName == 'wed_frequency_1' && $value < 1 ) $value = 'off';
		if($fieldName == 'thur_frequency_1' && $value < 1 ) $value = 'off';
		if($fieldName == 'fri_frequency_1' && $value < 1 ) $value = 'off';
		if($fieldName == 'sat_frequency_1' && $value < 1 ) $value = 'off';
		if($fieldName == 'sun_frequency_1' && $value < 1 ) $value = 'off';
		
		if($fieldName == 'mon_frequency_1' && $value < 5 && $value != 'off' ) $value = '5';
		if($fieldName == 'tue_frequency_1' && $value < 5 && $value != 'off' ) $value = '5';
		if($fieldName == 'wed_frequency_1' && $value < 5 && $value != 'off' ) $value = '5';
		if($fieldName == 'thur_frequency_1' && $value < 5 && $value != 'off' ) $value = '5';
		if($fieldName == 'fri_frequency_1' && $value < 5 && $value != 'off' ) $value = '5';
		if($fieldName == 'sat_frequency_1' && $value < 5 && $value != 'off' ) $value = '5';
		if($fieldName == 'sun_frequency_1' && $value < 5 && $value != 'off' ) $value = '5';
		
		
		//if($value == "12:00 AM") $value = "00:00";
		
		if(strpos($value, " AM") ) {
			$value = str_replace(" AM", "", $value);
			$timeParts = explode(":", $value);
			if($timeParts[0] == '12') {
				$timeParts[0] = '00';
			}	
			$value = $timeParts[0] . ":" . $timeParts[1];
		}
		
		
		
		
		
		//$value = str_replace(" AM", "", $value);
		
		if(strpos($value, " PM") ) {
			$value = str_replace(" PM", "", $value);
			$timeParts = explode(":", $value);
			if($timeParts[0] != '12') {
				$timeParts[0] = $timeParts[0] + 12;
			}	
			$value = $timeParts[0] . ":" . $timeParts[1];
		}
								
		
		$sqlSet .= $fieldName." = '".$value."', ";
	} else {
		if($key == 'list_number') $list_number = $data['value'];
		if($key == 'list_name') $list_name = mysqli_real_escape_string($conn, $data['value']);	
	} 	
	
	
}
//$list_number = $_REQUEST['list_number'];
$sqlSet .= "user_id = '$user_id', list_id = '$list_number', list_name = '$list_name' ";
$SQL = "select id from user_schedule where user_id = '$user_id' and list_id = '$list_number' ";
$result=mysqli_query($conn, $SQL);
if(mysqli_num_rows($result)) {
	$SQL = "update user_schedule ".$sqlSet." where user_id = '$user_id' and list_id = '$list_number' ";
} else {
	$SQL = "insert into user_schedule ".$sqlSet;
} 
$result = mysqli_query($conn, $SQL) or die(mysqli_error() . $SQL);	

sleep(1); // delay on purpose to let server catch up with JS

?>