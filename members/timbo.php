<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/twitteroauth/twitteroauth.php");
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/config.php");
?>
<?
//if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') {
	header('Location: /');
	exit();
}	
$screen_name = $_SESSION['screen_name'];
//$access_token = $_SESSION['access_token'];
//$user_id = $_SESSION['access_token']['user_id'];

$SQL = "select * from users where screen_name = '$screen_name'  ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$row = mysqli_fetch_assoc($result);	
$_SESSION['OFFER_SEEN'] = $row['offer_seen'];

if($row['status'] == 0 ) {
	header("Location: /oops.php?err=expired");
	exit();
}	





$_SESSION['planName'] = $row['planName'];
$_SESSION['isAdmin'] = $row['isAdmin'];
$list_count = $row['list_count'];
$all_tweet_status = $row['all_tweet_status'];
$user_id = $row['user_id'];
$send_thanks = $row['send_thanks'];




$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
$content = $connection->get('users/show', array('screen_name' => $screen_name));

//$user_id = $_SESSION['access_token']['user_id'];

if (is_array($content) || is_object($content)) {
	$content = object_to_array($content);
}
$_SESSION['name'] = $content['name'];

?>
<? 
if(strtotime($_SESSION['expireDate']) <= strtotime("now") && $_SESSION['planName'] =='' ) {
	header("Location: /oops.php?err=expired");
	exit();
}
?>	
<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>TweetJukebox</title>
  <meta http-Equiv="Cache-Control" Content="no-cache">
  <meta http-Equiv="Pragma" Content="no-cache">
  <meta http-Equiv="Expires" Content="0">
  <meta name="HandheldFriendly" content="True" />
  <meta name="MobileOptimized" content="320" />
  <meta content="minimum-scale=1.0, width=device-width, maximum-scale=1.0, user-scalable=no" name="viewport" />
  <meta name="description" content="">
  <link  href="http://fonts.googleapis.com/css?family=Oswald:regular" rel="stylesheet" type="text/css" >
  <link href='http://fonts.googleapis.com/css?family=Junge' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="../assets/css/style.css">
  <link rel="stylesheet" href="../assets/fonts/raphaelicons.css">
  <link rel="stylesheet" href="../assets/css/main.css">
  <link rel="shortcut icon" href="/favicon.ico?v=2" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
<link rel="stylesheet" href="/highslide-4.1.13/highslide/highslide.css" type="text/css" />
<style>
    fieldset { padding:0; border:0; margin-top:25px; }
    h1 { font-size: 1.2em; margin: .6em 0; }
    .ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
  </style>
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="/highslide-4.1.13/highslide/highslide-ie6.css" />
<![endif]-->
<!-- 9d83e18a764256552baf6956b777f190 -->
<script type="text/javascript" src="/highslide-4.1.13/highslide/highslide-full.packed.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/highslide-4.1.13/highslide/graphics/';
	hs.showCredits = false;
    hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = .45;
	hs.zIndexCounter = 2000;
//	hs.objectLoadTime = 'after';
//	hs.forceAjaxReload = 'true';
</script>
<script src="//code.jquery.com/jquery-1.11.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<!--link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" /-->
<link rel="stylesheet" href="../assets/css/jquery-ui.css">
<script>window.jQuery || document.write('<script src="../assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
<script src="../assets/js/script.js"></script>
<script src="../assets/js/libs/modernizr-2.5.2.min.js"></script>
<script src="../include/js_boot.js"></script>
<script type="text/javascript" src="jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="jquery.timepicker.css" />
<link href="/members/chart.css" rel="stylesheet" type="text/css">
</head>
<!--[if lt IE 7]> <body class="ie6 oldies"> <![endif]-->
<!--[if IE 7]>    <body class="ie7 oldies"> <![endif]-->
<!--[if IE 8]>    <body class="ie8 oldies"> <![endif]-->
<!--[if gt IE 8]><!--><body style="overflow:visible"><!--<![endif]-->
<!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]--> 
  <header class="clearfix">
      <div class="container">
        <a id="logo" href="/">TweetJukebox</a>
        <ul class="social-icons">
          <li><a href="http://www.facebook.com/TweetJukebox" class="icon flip">^</a></li>
          <li><a href="" class="icon">T</a></li>
          <li><a href="http://www.twitter.com/tweet_jukebox" class="icon">^</a></li>
        </ul>
        <nav class="clearfix">
          <ul role="navigation">
            <li>
              <a href="/"><span class="icon">S</span>Home</a>            </li>
            <li>
              <a href="../about.php"><span class="icon">E</span>About</a>            </li>
            <!--li><a href="../plans.php"><span class="icon">u</span>Plans </a></li-->
			<li>
              <a href="../contact.php"><span class="icon">M</span>Contact us</a>            </li>
          </ul>
        </nav>		
      </div>
  </header>
   <section role="banner">
	<hr style="border-bottom: 4px dotted #666;">
     <article role="main" class="clearfix contact">
         <div class="post">
 	     <h3 align="center">What do you want to do?</h3>
		 	 
			<table width="100%">
			<tr>
				<td>				 
				 <ul class="jbMenu">																		
					<li><a onClick="scheduledList();"> Scheduled Tweets</a></li>															
				 </ul>
				 <br>				 					
				 	<ul class="jbMenu">
						<li>My Jukeboxes
							<ul>
								<? 
								$SQL = "select list_name, list_id, schedule_status from user_schedule where user_id = '$user_id' order by list_id ";
								$result = mysqli_query($conn, $SQL) or die(mysqli_error());
								$row = mysqli_fetch_assoc($result);				
								?>
								<?	
									do {
										$list_name = stripslashes($row['list_name']);
										$list_id = stripslashes($row['list_id']);
										/*
										if($_SERVER['REMOTE_ADDR'] == '173.65.117.98' ) {
										?>
											<a href="#" onClick="masterListDEBUG(<? echo $list_id; ?>);">Debug <? echo $list_name;?></a>
										<?		
										}
										*/
								?>
								<?
								?>
									<li>
									<a href="#" onClick="masterList(<? echo $list_id; ?>);">
								<? 										
									echo $list_name; 
									if($row['schedule_status'] == '0') echo ' - off'; 	
								?>
									</a>
									</li>						
								<?
									} while($row = mysqli_fetch_assoc($result));										
								?>		
							</ul>
					  </li> 
					  </ul>
					  <br>
					  <ul class="jbMenu">
					  <li>
						<?  // allow more boxes?
						if( ( $_SESSION['planName'] == 'Silver' || $_SESSION['planName'] == 'Dummy' ) && $list_count < 3) {
						?>
							<a onClick="if(confirm('Add another Juke Box?') == true) { addJukeBox(); }">Add another Jukebox!</a>
						<?		
						}					
						?>
						<?  // allow more boxes?
						if($_SESSION['planName'] == 'Gold' && $list_count < 20) {
						?>
							<a onClick="if(confirm('Add another Juke Box?') == true) { addJukeBox(); }">Add another Jukebox!</a>
						<?		
						}					
						?>			
					  </li>					  						     
					</ul>	
					<br>												 				 				 				 
				</td>
				<td>&nbsp;&nbsp;&nbsp;</td>
				<td>				   				   
				 <ul class="jbMenu">					
					<!--li><a onClick="followersList();"> Followers</a></li><br /-->					
					<!--li><a onClick="tweet();"> Tweet Sumptin</a></li><br /-->
					<li><a onClick="recentMentions();"> Recent Mentions</a></li>
					<br />
				</ul>
				<ul class="jbMenu">	
					<li><a onClick="recentRetweets();"> Recent Retweets</a></li>
					<br />
				</ul>
				<? if ($_SESSION['isAdmin'] === '1' ) { ?>
				<ul class="jbMenu">	
					<li><a onClick="sayThanks();"> Thank You Tweets * </a></li>
					<br />
				 </ul>				 
				<? } ?> 
				<ul class="jbMenu">	
					<li><a onClick="thanksSetup();">  Thank You Tweets</a></li>
					<br />
			    </ul>				 
				</td>
			</tr>		
			<? if(strtotime($_SESSION['expireDate']) < strtotime("+ 10 day") && $_SESSION['planName'] =='' ) { ?>
					<tr><td colspan="3" class="ui-state-error" style="padding:5px;" align="center">Warning, Your Subscription Expires On <? echo $_SESSION['expireDate']; ?>
					<br><br><a href="/plans.php">Click here to renew now!</a>
					</td></tr>
			<? } ?>	
			<tr><td colspan="3" align="center">Your Jukebox is 
			<?
			if($all_tweet_status == 1) {
			?>	
					<div id="allTweetStatusOn"  style="display:block" onClick="$('#allTweetStatusOn').css('display', 'none'); $('#allTweetStatusOff').css('display', 'block'); switchAllTweets('off')"  ><a><img src="../images/button-power_basic_green.png" alt="on" /> Plugged In</a></div>
					<div id="allTweetStatusOff" style="display:none"  onclick="$('#allTweetStatusOn').css('display', 'block'); $('#allTweetStatusOff').css('display', 'none'); switchAllTweets('on')" ><a><img src="../images/button-power_basic_red.png" alt="off" /> Un Plugged</a></div>
			<?	
				} else { 
			?>
					<div id="allTweetStatusOn"  style="display:none"  onclick="$('#allTweetStatusOn').css('display', 'none'); $('#allTweetStatusOff').css('display', 'block'); switchAllTweets('off')" ><a><img src="../images/button-power_basic_green.png" alt="on" /> Plugged In</a></div>
					<div id="allTweetStatusOff" style="display:block" onClick="$('#allTweetStatusOn').css('display', 'block'); $('#allTweetStatusOff').css('display', 'none'); switchAllTweets('on')" ><a><img src="../images/button-power_basic_red.png" alt="off" /> Un Plugged</a></div>
			<?	
				} 
			?> * controls all Twitter&#8482; updates
		   </td></tr></table>
		 </div>		 
		 
		 <aside role="complementary" >
            <h2><img style="display:inline" src="<? echo $content['profile_image_url']; ?>" >&nbsp;<? echo $_SESSION['name']; ?> - <? echo $_SESSION['planName']; ?> Member</h2>            
			@<? echo $_SESSION['screen_name']; ?>			
			<!--h4><? echo $content['description']; ?>&nbsp;</h4-->
			<p><a href="logout.php"><span class="member-icon">W</span> Logout</a></p>
			<p><a href="#" onClick="myAccount();"><span class="member-icon">L</span> Manage My Account Information</a></p>	
			<? if($_SESSION['isAdmin'] == '1') { ?>
				<p><a href="/admin/">Admin Portal</a></p>
			<? } ?>										   
            <!--p><a href="http://www.twitter.com">Go To Twitter <span class="icon">:</span></a></p-->            
			<!--table width="100%">
				<tr>
				   	<td>
						Tweets
						<figure id="statuses_count">
						<? echo $content['statuses_count']; ?>						</figure>			   		</td>
					<td>
						Followers
						<figure id="followers_count">
						<? echo $content['followers_count']; ?>						</figure>			   		</td>
					<td>
						Following
						<figure id="friends_count">
						<? echo $content['friends_count']; ?>						</figure>			   		</td>
				</tr>
			</table-->			
    	 </aside>
	 </article>
   </section> <!-- // banner ends -->
   <section class="container" style="min-height:300px;">                      
		<article class="post content">		 			 			 			 			 			 
			 <div id="masterDiv" style="display:none; height:auto; float:left; padding-top:10px; width:900px">
			 <? 
			 if($_SESSION['new_user'] == '1') {
			 ?>
			 <script>masterList('1');</script>	
			 <?	
			 } 	
			 ?>
			 <? 
			 if($_SESSION['OFFER_SEEN'] == '0' ) {
			 ?>
			 <script>$("#masterDiv").css('display', 'block');</script>
			 <center>
			 <div align="center" >
			 <h1 align="center">Bonus Content Offer!</h1>			 
			 <p>We've added 100 more quotes and now have a 200 quote Bonus Jukebox for you.</p>
			 <p><input class="smallOnButton" style="height:80px;" type="button" value="Add this Jukebox to my Account!" onClick="bonusJB(1);">
			 </p>
			 <p><input class="smallOffButton" style="height:80px;" type="button" value="No Thanks, I'm Good" onClick="bonusJB(0);">
			 </p>
			 </div>
			 </center>
			 <?	
			 } 	
			 ?>				 
			 </div>			 			 
			 <div id="searchDiv" style="display:none; height:auto; float:left; padding-top:10px;" >			 			 
				 <form class="c-form" method="post" enctype="application/x-www-form-urlencoded" accept-charset="utf-8">
					 <h2>Search Twitter</h2>                
					 <label for="searchTerm">Enter Search Term</label>
					 <input type="text" id="searchTerm" name="searchTerm" value="" />
					 <input type="button" id="searchSubmit" name="submit" value="Go" class="button" onClick="searchTwitter()"  />
				 </form>
				 <div align="center" id="searchResultsDiv" style="padding-top:10px"></div>
			 </div>
			 			 			 			 
			 <div id="recentMentionsDiv" style="display:none; height:auto; float:left; padding-top:10px; width:960px"></div>
			 
			 <div id="scheduleDiv" style="display:none; height:auto; float:left; padding-top:10px; width:960px">			 				 								
			 	<h2 align="center" style="clear:both;">Schedule</h2><br><center>The Current Server Time Is: <span id="scheduleDivTime"><? echo date("H:i A"); ?></span></center><br />
				<form class="l-form" id="scheduleForm" name="scheduleForm" method="post" enctype="multipart/form-data" accept-charset="utf-8">				
				<center><h3>My JB Name</h3><span><input type="text" name="list_name" id="list_name" value="" ></span></center>
				<!--h3 align="center">You may set up to two time slots per day.</h3-->				
				<h3 align="center">Click on the names of the days you want to set or change your schedule for.</h3>
				<h3 align="center">Any changes will be applied to all the days you select.</h3>
				<h3 align="center">Clicking a selected day name again will lock in and save your settings for that day.</h3>				
				<table>
					<tr>						
						<td align="center"><span class="dayOfWeek" id="monDay" onClick="dayClicked('#monDay', '1')">Mon</span></td>
						<td align="center"><span class="dayOfWeek" id="tueDay" onClick="dayClicked('#tueDay', '2')">Tue</span></td>
						<td align="center"><span class="dayOfWeek" id="wedDay" onClick="dayClicked('#wedDay', '3')">Wed</span></td>
						<td align="center"><span class="dayOfWeek" id="thurDay" onClick="dayClicked('#thurDay', '4')">Thur</span></td>
						<td align="center"><span class="dayOfWeek" id="friDay" onClick="dayClicked('#friDay', '5')">Fri</span></td>
						<td align="center"><span class="dayOfWeek" id="satDay" onClick="dayClicked('#satDay', '6')">Sat</span></td>
						<td align="center"><span class="dayOfWeek" id="sunDay" onClick="dayClicked('#sunDay', '7')">Sun</span></td>
					</tr>								
					<!--tr><td colspan="8" align="center"><h2>Time Slots</h2></td></tr-->
					<tr id="selected_list_schedule">						
					</tr>																				
				</table>
				<br />
				<div onClick="location.reload();" align="center" style="font-size:12px; background-color:#FFFFFF; color:#000000; cursor:pointer; float:left;" class="button">exit without saving</div>
				<div onClick="disableAllDays();" align="center" style="font-size:18px; font-weight:bold; cursor:pointer; float:right;" class="button">save and exit</div>											
				<div style="clear:both">&nbsp;</div>
				</form>				
			 </div>
			
							
			 <div id="uploadDiv" style="display:none; height:auto; float:left; padding-top:10px; width:700px;  " align="center">
			 	<?
				$SQL = "select count(id) as totalTweets from tweets where user_id = '$user_id' ";
				$result = mysqli_query($conn, $SQL) or die(mysqli_error());
				$row = mysqli_fetch_assoc($result);
				$totalTweets = $row['totalTweets'];												
				?>
				<div onClick="hideDivs()" style="font-size:18px; font-weight:bold; float:right;">close</div>
				<form class="l-form" id="uploadForm" name="uploadForm" method="post" enctype="multipart/form-data" action="postData.php" accept-charset="utf-8">
				<h2 id='uploadH2' align="center">Upload Tweets</h2>
					<div style="text-align:left; width:700px;" align="center">
						<p>We will automatically post status updates (Tweets) to twitter on your behalf.  You can have as many Tweets as your plan allows.</p>
						<p>Once you have built your &quot;Jukebox&quot; of Tweets you can schedule how often they will be sent.&nbsp; We will randomly choose Tweets from your inventory that have not recently been tweeted, or have never been tweeted.&nbsp; We will NOT repeat any Tweet in the same day or those that have been Tweeted within a number of days that you select.&nbsp;</p>
						<p>Once all of your Tweets have been Tweeted at least once, we will continue to randomly select tweets to post, unless you put the process on &quot;hold&quot;.&nbsp; We post updates to Twitter every 5 minutes.</p>
						<p>Your file must be in a CSV format and must contain ONE column headings in the first row called: <br /><b>tweet_text</b></p>						
						<p>Click here to get a template -> <a href="download.php">Download Template</a></p>
						<p><input id="myFile" type="file" name="myFile" onChange="readURL(this);" /></p>
						<p class="ui-state-error">After your new Tweets are uploaded, we will list all your Tweets, newest / first - so you can review your latest additions for editing as you see fit.</p>
						<p id="messageArea" align="center">&nbsp;</p>
					</div>
					<input type="hidden" id="upload_list_number" value="" name="upload_list_number" />
				</form>				
				<script>
				var files;
				function readURL(input) {  	
					files = event.target.files;
					var filePathName = $('#myFile').val();
					var fileParts = filePathName.split(".");
					var ext = fileParts[fileParts.length -1];			
					ext = ext.toLowerCase();
					
					var fileNameParts = filePathName.split("\\");
					var fileName = fileNameParts[fileNameParts.length -1];											
					
					if(ext != 'csv') {
						$('#messageArea').css('color', 'red');
						$('#messageArea').html('Invalid file format! Your file must be a .csv file.');
						$('#myFile').val('');
					} else {
						$('#messageArea').html('');
						var reader = new FileReader();
						reader.onload = function (e) {									  
							csvAsArrayA = e.target.result.csvToArray();
							if( (csvAsArrayA[0][0]) != 'tweet_text' ) {
								$('#messageArea').css('color', 'red');
								$('#messageArea').html('Invalid file construction. <p>Your first row must contain this column heading:</p><p><b>tweet_text.</b></p><p class="content">Click here to get a template -> <a href="download.php">Download Template</a></p>');
								$('#myFile').val('');
							} else {
								
								if( confirm("Warning!!!!\n\nThis will ADD the records in this file to your Jukebox #" + $('#upload_list_number').val() + ".\n\nTHIS PROCESS CANNOT BE UNDONE.\n\nIf this is what you want to do, press OK to upload this file: " + fileName ) ) {																
									hideDivs();
									$('#recentMentionsDiv').css('display', 'block');
									$('#recentMentionsDiv').html('<div align="center">Please wait while we load your file....<p><img src="/images/wait.GIF" border="0"></p></div>');									
									$('#uploadForm').submit();																																																																	
								}	
							}
						};
						reader.readAsText(input.files[0]);
					}
				}	
				</script>	
				<script type="text/javascript">
				/* Copyright 2012-2013 Daniel Tillin
				 *
				 * Permission is hereby granted, free of charge, to any person obtaining
				 * a copy of this software and associated documentation files (the
				 * "Software"), to deal in the Software without restriction, including
				 * without limitation the rights to use, copy, modify, merge, publish,
				 * distribute, sublicense, and/or sell copies of the Software, and to
				 * permit persons to whom the Software is furnished to do so, subject to
				 * the following conditions:
				 *
				 * The above copyright notice and this permission notice shall be
				 * included in all copies or substantial portions of the Software.
				 * 
				 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
				 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
				 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
				 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
				 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
				 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
				 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
				 *
				 * csvToArray v2.1 (Unminifiled for development)
				 *
				 * For documentation visit:
				 * http://code.google.com/p/csv-to-array/
				 *
				 */
				String.prototype.csvToArray = function (o) {
					var od = {
						'fSep': ',',
						'rSep': '\r\n',
						'quot': '"',
						'head': false,
						'trim': false
					}
					if (o) {
						for (var i in od) {
							if (!o[i]) o[i] = od[i];
						}
					} else {
						o = od;
					}
					var a = [
						['']
					];
					for (var r = f = p = q = 0; p < this.length; p++) {
						switch (c = this.charAt(p)) {
						case o.quot:
							if (q && this.charAt(p + 1) == o.quot) {
								a[r][f] += o.quot;
								++p;
							} else {
								q ^= 1;
							}
							break;
						case o.fSep:
							if (!q) {
								if (o.trim) {
									a[r][f] = a[r][f].replace(/^\s\s*/, '').replace(/\s\s*$/, '');
								}
								a[r][++f] = '';
							} else {
								a[r][f] += c;
							}
							break;
						case o.rSep.charAt(0):
							if (!q && (!o.rSep.charAt(1) || (o.rSep.charAt(1) && o.rSep.charAt(1) == this.charAt(p + 1)))) {
								if (o.trim) {
									a[r][f] = a[r][f].replace(/^\s\s*/, '').replace(/\s\s*$/, '');
								}
								a[++r] = [''];
								a[r][f = 0] = '';
								if (o.rSep.charAt(1)) {
									++p;
								}
							} else {
								a[r][f] += c;
							}
							break;
						default:
							a[r][f] += c;
						}
					}
					if (o.head) {
						a.shift()
					}
					if (a[a.length - 1].length < a[0].length) {
						a.pop()
					}
					return a;
				}
				
				</script>			 
			 </div>			
			 
		</article>				 
			 
			 
			 
   </section>
  <footer role="contentinfo">
    <p> <span class="left">tj.local &copy; - <? echo date("Y"); ?> | <a href="#">Goto Top</a></span> <a href="/">HOME</a> | <a href="/about.php">ABOUT</a> | <!--a href="/plans.php">PLANS</a> | --><a href="/contact.php">CONTACT US</a> | <a href="/privacy.php">PRIVACY POLICY</a> | <a href="/terms.php">TERMS AND CONDITIONS</a> </p>
  </footer>


  
<script>
function readImgURL(input) {  	
	var imageExts = ["jpg","jpeg","png"];
	var filePathName = $('#myImg').val();
	var fileParts = filePathName.split(".");
	var ext = fileParts[fileParts.length -1];
	ext = ext.toLowerCase();
	
	
	if(imageExts.indexOf(ext) != -1) {   // only render if image is ok to display	
		if (input.files && input.files[0])  {
			var reader = new FileReader();
			reader.onload = function (e) {									  
				$('#blah')
				.prop('src',e.target.result)
				.width(100);
			};
			reader.readAsDataURL(input.files[0]);
	   }
	} else {
		alert("Invalid format. Must be .png or .jpg types only");
		$('#myImg').val('');
	}
}

</script>
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0029/6945.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
</body>
<? if($_SESSION['NEWJBUPLOAD']) { ?>	
	<script> 
	$('#masterDiv').css('display', 'block');
	$('#masterDiv').html('<div align="center">Please wait while we get your recent Jukebox additions for you....<p><img src="../images/wait.GIF" border="0"></p></div>');	
	setTimeout(function (){		
		$.ajax({
			type: "get",
			url: "/members/getMasterList.php?list_number=<? echo $_SESSION['LISTNUMBER'];?>&s=tweet_last_post_date, id DESC",
			//data: {'message': msg} ,
			dataType: "html"
		})
		.done(function( msg ) { 
			$('#masterDiv').html(msg);
		});	
	}, 2000);
	</script>
<? 
		unset($_SESSION['LISTNUMBER']);
		unset($_SESSION['NEWJBUPLOAD']);		
	}  	
?>
</html>
<? //phpinfo(); ?>