<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/twitteroauth/twitteroauth.php");
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/config.php");

if(!isset($_SESSION['access_token'])) die();
$access_token = $_SESSION['access_token'];
?>
<?	
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);		
$content = $connection->get('statuses/retweets_of_me', array('count' => 150));
//$content = $connection->get('search/tweets', array('q' => 'alphabetsuccess', 'count' => 200));				
if (is_array($content) || is_object($content)) {
		$thisContent = object_to_array($content);	
}

//print "<pre>";
//print_r($thisContent);

if( count($thisContent)  ) {	
	$mentions = 0;				
?><h2 align="center">What have I said lately that is being retweeted?</h2>
<table id="mentionsTable" border="" width="100%" >				  
  <tbody>
<?
	foreach($thisContent as $status) {
		//if($status['user']['screen_name'] != $_SESSION['screen_name'] ) {
?>
		  <tr>						
			<td nowrap="nowrap" align="center">
				Retweeted <? echo $status['retweet_count']; ?> times.<br />
				<a onclick="viewAll('<? echo $status['id_str'];?>')" style="cursor:pointer" >View All</a>
			</td>
			<td>				
				<? echo $status['text'] ; ?>
				<p><a href="http://twitter.com/<? echo $status['user']['screen_name'];?>/status/<? echo $status['id'];?>" target="_blank"><? echo $status['created_at'];?></a></p>
			</td>
		  </tr>	
<?
			$mentions ++;
		//}
	}
?>	
  </tbody>
  </table>	
<?
	if($mentions == 0 ) {
?>	
		<h2 align="center">Sorry, <? echo $_SESSION['screen_name'];?>, you don't seem to have any mentions</h2>
<? 
	} 
?>
<?					
} else {
?>
<h2 align="center">Sorry, <? echo $_SESSION['screen_name'];?>, you don't seem to have any mentions</h2>
<?	
}
?>	