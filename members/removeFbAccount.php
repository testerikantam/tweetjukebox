<?php
require_once($_SERVER['DOCUMENT_ROOT']."/include/config.php");
require_once($_SERVER['DOCUMENT_ROOT']."/ikantam/Ikantam/Models/SocialAccount.php");
    if (!empty($_GET['account_id'])) {
        $userId = $_SESSION['access_token']['user_id'];
        $socialAccount = new SocialAccount();
        $data = $socialAccount->getById($_GET['account_id']);
        if (!empty($data['user_id']) && $userId == $data['user_id']) {
            $socialAccount->delete($_GET['account_id']);
        }

    }
    header('Location: ./index.php');

?>