<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');
$user_id = $_SESSION['access_token']['user_id'];

$SQL = "select * from users where user_id  = '$user_id'  ";
$result1 = mysqli_query($conn, $SQL);
$row1 = mysqli_fetch_assoc($result1);
$schedule_filter = $row1['schedule_filter'];
//$userTimeZone = $row1['user_timezone'];		
//date_default_timezone_set($userTimeZone);	

?>
<?
if($_POST['delete'] ) {
	$rid = $_POST['delete'];
	$SQL = "delete from scheduled_tweets where id = '$rid' ";
	$result = mysqli_query($conn, $SQL) or die(mysqli_error());	
}
?>
<?
$maxRows = 25;
$pageNum = 0;
$currentPage = $_SERVER["PHP_SELF"];

$searchTerm = $_REQUEST['searchTerm'];
$searchField = $_REQUEST['searchField'];

$s = mysqli_real_escape_string($conn, $_REQUEST['s']);

if (isset($_GET['pageNum'])) {
  $pageNum = $_GET['pageNum'];
}
$startRow = $pageNum * $maxRows;




$SQL2 = "select count(id) as scheduledTweetTotal from scheduled_tweets where user_id = '$user_id' ";	
$result2 = mysqli_query($conn, $SQL2) or die(mysqli_error());
$row2 = mysqli_fetch_assoc($result2);
$scheduledTweetTotal = $row2['scheduledTweetTotal'];

$SQL2 = "select scheduled_tweets_status from users where user_id = '$user_id' ";
$result2 = mysqli_query($conn, $SQL2) or die(mysqli_error());
$row2 = mysqli_fetch_assoc($result2);
$scheduled_tweets_status = $row2['scheduled_tweets_status'];

if($schedule_filter == '0') {
	$SQL = "select scheduled_tweets.*, 
	DATE_ADD(tweet_last_post_date, INTERVAL frequency SECOND) as nextPostDate 
	from scheduled_tweets where user_id = '$user_id'  ";								
}
if($schedule_filter == '1') {
	$SQL = "select scheduled_tweets.*, 
	DATE_ADD(tweet_last_post_date, INTERVAL frequency SECOND) as nextPostDate 
	from scheduled_tweets where user_id = '$user_id' and tweet_status = 'active' ";								
}
if($schedule_filter == '2') {
	$SQL = "select scheduled_tweets.*, 
	DATE_ADD(tweet_last_post_date, INTERVAL frequency SECOND) as nextPostDate 
	from scheduled_tweets where user_id = '$user_id' and tweet_status = 'draft' ";								
}		
if($_REQUEST['search_tweet_text']) {
	$search_tweet_text = mysqli_real_escape_string($conn, $_REQUEST['search_tweet_text']);
	$SQL .= " and tweet_text like '%".$search_tweet_text."%' ";
}


$sortBy = "order by tweet_text";
if($_REQUEST['s']) $sortBy = "order by ".$_REQUEST['s'];

$SQL .= $sortBy;					
$SQL2 = getPagingQuery($SQL, $maxRows ); 

$pagingLink = getPagingLink($SQL, $maxRows, $_SERVER['QUERY_STRING'], true, $_REQUEST['s']."&search_tweet_text=$search_tweet_text", 'scheduled');

$result = mysqli_query($conn, $SQL2) or die(mysqli_error());
//$row = mysqli_fetch_assoc($result);

?>
<div class="row" align="center">
<div class="col-md-12">
<div id="scheduledTweetsHelp" style="display:none; padding:5px; border:solid 1px #666666; width:650px; text-align:left; margin:auto;">
	<h3>Scheduled Tweets Help<span style="cursor:pointer; float:right; padding-right:5px; color:#FF0000; font-weight:bold;" onclick="$('#scheduledTweetsHelp').css('display', 'none');$('#scheduledTweetsHelpButton').css('display', 'block');">X</span></h3>
	Scheduled Tweets are tweets you can set up today and schedule them to post in the future.  Scheduled tweets are ideal for brand reminders, birthdays, anniversaries, etc.  You enter what you want to say, select a date you want to post the tweet, and select a frequency.  
	<p>
	We have a wide choice of frequencies for you to choose from.
	</p>
	<p>
	Every 15 minutes we scan your scheduled tweets and post those that you have scheduled to go out. 
	</p>
</div>		
<button type="button" class="btn btn-primary btn-lg" id="scheduledTweetsHelpButton" onclick="$('#scheduledTweetsHelpButton').css('display', 'none');$('#scheduledTweetsHelp').css('display', 'block');"><i class="fa fa-life-ring"></i>Help With Scheduled Tweets</button>
</div>
</div>
<?
if(mysqli_num_rows($result)) {

	$row = mysqli_fetch_assoc($result); 
?>		
	<div class="row">
	<div class="col-md-12">
		<h2 align="center">Scheduled Tweets</h2>
		<div align="center" style="padding-bottom:10px;">			
		<? echo $pagingLink; ?>
		<? if($_SESSION['planName'] != 'Free' || $_SESSION['isAdmin']) { ?>								
				<br />Filter For: 
				<select class="form-control" style="width:150px; display:inline;" id="select_schedule_filter" name="select_schedule_filter" onchange="filterResults(this.value)" >
					<option <? if($schedule_filter == '0' ) echo 'selected="selected"'; ?> value="0">All Tweets</option>
					<option <? if($schedule_filter == '1' ) echo 'selected="selected"'; ?> value="1">Active Only</option>
					<option <? if($schedule_filter == '2' ) echo 'selected="selected"'; ?> value="2">Draft Only</option>
				</select>							    
		<? } else { ?>		
				<br />Filter For: 
				<select  class="form-control" style="width:150px; display:inline;" id="select_schedule_filter" name="select_schedule_filter" onchange="filterResults(this.value)" >
					<option  <? if($schedule_filter == '0' ) echo 'selected="selected"'; ?> value="0">All Tweets</option>
					<option disabled="disabled" <? if($schedule_filter == '1' ) echo 'selected="selected"'; ?> value="1">Active Only (Only With Paid Subscription)</option>
					<option disabled="disabled" <? if($schedule_filter == '2' ) echo 'selected="selected"'; ?> value="2">Draft Only (Only With Paid Subscription)</option>
				</select>	
		<? } ?>
		</div>
	</div>
	</div>		
	
	<div class="row" style="padding-bottom:20px;">
	<div class="col-md-12">
		<div align="center">	
			<ul class="nav navbar-nav">
				<li style="padding-right:30px;"><button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#searchSchDiv"><i class="fa fa-search"></i> Search</button></li>
				<?
				if($scheduledTweetTotal < $_SESSION['scheduledLimit'] || $_SESSION['isAdmin'] ) {	
				?>					
					<li style="padding-right:30px;"><a type="button" class="btn btn-primary btn-lg" href="/members/editScheduledTweet.php?rowID=0&currentMasterPage=<? echo $currentPage; ?>"  onclick="return hs.htmlExpand(this,  { objectType: 'iframe', width: '800' , height: '700', preserveContent: 'true', align: 'center' } ); " ><i class="fa fa-plus"></i> Schedule A New Tweet</a></li>		
				<? } ?>	
				<? if($_SESSION['planName'] != 'Free' || $_SESSION['isAdmin']) { ?>	
					<li style="padding-right:30px;"><a type="button" class="btn btn-primary btn-lg" href="downloadSCH.php"><i class="fa fa-download"></i> Download Tweets</a></li>
				<? } else { ?>	
					<li style="padding-right:30px;"><button type="button" class="btn btn-primary btn-lg" href="downloadSCH.php"><i class="fa fa-download"></i> Download Tweets <br />(Only With Paid Subscription)</button></li>	
				<? } ?>
		<?
			$myPlan = array_search($_SESSION['planName'], $_SESSION['plans']);
			$myPlanFit = array_search($_SESSION['planFit'], $_SESSION['plans']);
			if($myPlan >= $myPlanFit) {
				if($scheduled_tweets_status == 1) {
		?>	
				<li style="padding-right:30px;"><button type="button" class="btn btn-success btn-lg" id="schdTweetStatusOn"  style="display:block" onclick="$('#schdTweetStatusOn').css('display', 'none'); $('#schdTweetStatusOff').css('display', 'block'); switchScheduleTweets('off')"  ><i class="fa fa-thumbs-o-up"></i> Running</button></li>
				<li style="padding-right:30px;"><button type="button" class="btn btn-danger btn-lg" id="schdTweetStatusOff" style="display:none"  onclick="$('#schdTweetStatusOn').css('display', 'block'); $('#schdTweetStatusOff').css('display', 'none'); switchScheduleTweets('on')" ><i class="fa fa-thumbs-o-down"></i> Paused</button></li>
		<?	
				} else { 
		?>
				<li style="padding-right:30px;"><button type="button" class="btn btn-success btn-lg" id="schdTweetStatusOn"  style="display:none" onclick="$('#schdTweetStatusOn').css('display', 'none'); $('#schdTweetStatusOff').css('display', 'block'); switchScheduleTweets('off')"  ><i class="fa fa-thumbs-o-up"></i> Running</button></li>
				<li style="padding-right:30px;"><button type="button" class="btn btn-danger btn-lg" id="schdTweetStatusOff" style="display:block"  onclick="$('#schdTweetStatusOn').css('display', 'block'); $('#schdTweetStatusOff').css('display', 'none'); switchScheduleTweets('on')" ><i class="fa fa-thumbs-o-down"></i> Paused</button></li>
		<?	
				} 
			}
		?>
			</ul>					
		</div>
	</div>	
	</div>		
	<div class="row">
		<div class="col-md-5 col-sm-5 col-xs-5"> 
			<span style="cursor:pointer;" onclick="getNextScheduled('?s=<? if($sortBy == 'order by tweet_text') echo 'tweet_text DESC'; else echo 'tweet_text' ;?>')">Tweet Text</span>
			<? if ($sortBy == 'order by tweet_last_post_date' || $sortBy == 'order by tweet_last_post_date DESC' || $sortBy == 'order by tweet_next_post_date' || $sortBy == 'order by tweet_next_post_date DESC') echo ' <i class="fa fa-sort"></i>'; ?>
			<? if($sortBy == 'order by tweet_text')  echo ' <i class="fa fa-sort-asc"></i>'; ?>
			<? if($sortBy == 'order by tweet_text DESC') echo ' <i class="fa fa-sort-desc"></i>'; ?>			
		</div>
		<div class="col-md-1 col-sm-1 col-xs-1 text-center">Photo</div>
		<div class="col-md-2 col-sm-2 col-xs-2 text-center">Frequency</div>
		<div class="col-md-2 col-sm-2 col-xs-2 text-center">
			<span style="cursor:pointer;" onclick="getNextScheduled('?s=<? if($sortBy == 'order by tweet_last_post_date') echo 'tweet_last_post_date DESC'; else echo 'tweet_last_post_date' ;?>')">Last Posted</span>
			<? if($sortBy == 'order by tweet_text' || $sortBy == 'order by tweet_text DESC' || $sortBy == 'order by tweet_next_post_date' || $sortBy == 'order by tweet_next_post_date DESC') echo ' <i class="fa fa-sort"></i>'; ?>
			<? if($sortBy == 'order by tweet_last_post_date') echo ' <i class="fa fa-sort-asc fa"></i>'; ?>
			<? if($sortBy == 'order by tweet_last_post_date DESC') echo ' <i class="fa fa-sort-desc"></i>'; ?>
		</div>
		<div class="col-md-2 col-sm-2 col-xs-2 text-center">
			<span style="cursor:pointer;" onclick="getNextScheduled('?s=<? if($sortBy == 'order by nextPostDate') echo 'nextPostDate DESC'; else echo 'nextPostDate' ;?>')">Next</span>	
			<? if ($sortBy == 'order by tweet_text' || $sortBy == 'order by tweet_text DESC' || $sortBy == 'order by tweet_last_post_date' || $sortBy == 'order by tweet_last_post_date DESC' ) echo ' <i class="fa fa-sort"></i>'; ?>
			<? if($sortBy == 'order by nextPostDate') echo ' <i class="fa fa-sort-asc fa"></i>'; ?>
			<? if($sortBy == 'order by nextPostDate DESC') echo ' <i class="fa fa-sort-desc"></i>'; ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12"><hr /></div>
	</div>	
<?		
	do {
		$frequency = timetostr($row['frequency']);
?>  
    <div class="row" id="<? echo $row['id']; ?>" >
	<div class="col-md-5 col-sm-5 col-xs-5" id="text_<? echo $row['id'];?>">
		<a id="link0" href="/members/editScheduledTweet.php?rowID=<? echo $row['id'];?>&currentMasterPage=<? echo $currentPage; ?>"  onclick="return hs.htmlExpand(this,  { objectType: 'iframe', width: '800' , height: '700', preserveContent: 'true', align: 'center' } ); " ><i class="fa fa-pencil-square-o fa-2x"></i></a>
		<? echo stripslashes($row['tweet_text']);?>
		<span style="float:right; <? if($row['tweet_status'] == 'active') echo 'color:GREEN'; ?> " ><b><? echo $row['tweet_status']; ?></b>
			<a onclick="confirmScheduledTweetDelete( '<? echo $row['id'];?>' ); "><i class="fa fa-trash-o fa-2x"></i></a>
		</span>
		<? if($row['photo']) { ?>
			<span id="pop-up_<? echo $row['id']; ?>" style="position:absolute; left:0; display:none; z-index:99; "><img src="https://s3.amazonaws.com/jukebox-member-images/<? echo stripslashes($row['photo']) ; ?>" class="img-responsive"/></span>
		<? } ?>	
	</div>	
	<div class="col-md-1 col-sm-1 col-xs-1 text-center">        	
	  <? if($row['photo']) { ?>
		  <img id="currentTweetPhoto_<? echo $row['id']; ?>" onmouseout="$('#pop-up_<? echo $row['id']; ?>').hide();" onmouseover="$('#pop-up_<? echo $row['id']; ?>').show();"  src="https://s3.amazonaws.com/jukebox-member-images/<? echo stripslashes($row['photo']) ; ?>"  class="img-responsive"  />
	  <? } else { ?>
	  	  <img id="currentTweetPhoto_<? echo $row['id']; ?>" src="/images/spacer.gif"  class="img-responsive"  />
	  <? } ?>
    </div>	  
	<div class="col-md-2 col-sm-2 col-xs-2 text-center" id="frequency_<? echo $row['id'];?>">
		<? echo $frequency;?>	
	</div>
	<div class="col-md-2 col-sm-2 col-xs-2 text-center" id="tweetDate_<? echo $row['id'];?>">
		<? 		
		if($row['tweet_last_post_date'] == '0000-00-00 00:00:00' || $row['tweet_last_post_date'] == '1969-12-31 19:00:00') {
			echo "not posted yet"; 
		} else {
			echo date("m/d/Y h:i A", strtotime($row['tweet_last_post_date']));
			if($row['tweet_sent'] == '0') {
				echo '<br><span class="post-exception" data-content="'.$row['exceptions'].'" data-toggle="popover" data-placement="bottom" title="Reason">Tweet Was Not Posted <i class="fa fa-exclamation-triangle"></i></span>';						
			}	
		}			
		?>
		<br />
		<? if($_SESSION['planName'] != 'Free' || $_SESSION['isAdmin']) { ?>
			<a onClick="tweetScheduledTweetNow('<? echo urlencode(stripslashes($row['tweet_text']));?>', '<? echo $row['id']; ?>');"><i class="fa fa-twitter"></i>Tweet It Now!</a>
		<? } ?>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-2 text-center">
	    <? if($row['tweet_last_post_date'] == '0000-00-00 00:00:00')  {
					echo date("m/d/Y h:i A", strtotime($row['start_date_time']));
			} else {
				echo date("m/d/Y h:i A", strtotime($row['nextPostDate']));
			}
		?>					
	</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12"><hr /></div>
	</div>
    <?		
	} while($row = mysqli_fetch_assoc($result));
?>    
<? }  else { ?>			
		<? if($_REQUEST['search_tweet_text'] ) { ?>
			<h2 align="center">Your search for <? echo $_REQUEST['search_tweet_text']; ?> did not return any results!</h2>
			<div align="center">
			<button class="bth btn-primary btn-lg" onclick="scheduledList()">Show All</button>
			</div>
		<? } else { ?>		
			<h2 align="center">You don't seem to have any Scheduled Tweets!</h2>
			<ul class="nav navbar-nav">
				<li style="padding-right:30px;"><a type="button" class="btn btn-primary btn-lg" href="/members/editScheduledTweet.php?rowID=0&currentMasterPage=<? echo $currentPage; ?>"  onclick="return hs.htmlExpand(this,  { objectType: 'iframe', width: '800' , height: '700', preserveContent: 'true', align: 'center' } ); " ><i class="fa fa-plus"></i> Schedule A New Tweet</a></li>	
			</ul>
		<? } ?>	
		<? if($_SESSION['planName'] != 'Free' || $_SESSION['isAdmin']) { ?>								
				<br />Filter For: 
				<select class="form-control" style="width:150px; display:inline;" id="select_schedule_filter" name="select_schedule_filter" onchange="filterResults(this.value)" >
					<option <? if($schedule_filter == '0' ) echo 'selected="selected"'; ?> value="0">All Tweets</option>
					<option <? if($schedule_filter == '1' ) echo 'selected="selected"'; ?> value="1">Active Only</option>
					<option <? if($schedule_filter == '2' ) echo 'selected="selected"'; ?> value="2">Draft Only</option>
				</select>							    
		<? } else { ?>		
				<br />Filter For: 
				<select  class="form-control" style="width:150px; display:inline;" id="select_schedule_filter" name="select_schedule_filter" onchange="filterResults(this.value)" >
					<option  <? if($schedule_filter == '0' ) echo 'selected="selected"'; ?> value="0">All Tweets</option>
					<option disabled="disabled" <? if($schedule_filter == '1' ) echo 'selected="selected"'; ?> value="1">Active Only (Only With Paid Subscription)</option>
					<option disabled="disabled" <? if($schedule_filter == '2' ) echo 'selected="selected"'; ?> value="2">Draft Only (Only With Paid Subscription)</option>
				</select>	
		<? } ?>			
<? } ?>
<?
function timetostr($frequency) {  // frequency as seconds int
	if($frequency =="86") return "Once and Done";
	if($frequency =="3600") return "Once every hour";
	if($frequency =="7200") return "Once every two hours";
	if($frequency =="10800") return "Once every three hours";
	if($frequency =="14400") return "Once every four hours";
	if($frequency =="18000") return "Once every five hours";
	if($frequency =="21600") return "Once every six hours";
	if($frequency =="25200") return "Once every seven hours";
	if($frequency =="28800") return "Once every eight hours";
	if($frequency =="32400") return "Once every nine hours";
	if($frequency =="36000") return "Once every ten hours";
	if($frequency =="43200") return "Once every twelve hours";
	if($frequency =="54000") return "Once every fifteen hours";
	if($frequency =="64800") return "Once every eighteen hours";
	if($frequency =="75600") return "Once every twenty-one hours";
	if($frequency =="86400") return "Once every day";
    if($frequency =="108000") return "Once every thirty hours";
	if($frequency =="129600") return "Once every thirty-six hours";
	if($frequency =="151200") return "Once every fourty-two hours";
	if($frequency =="172800") return "Once every two days";
	if($frequency =="259200") return "Once every three days";
	if($frequency =="345600") return "Once every four days";
	if($frequency =="432000") return "Once every five days";
	if($frequency =="518400") return "Once every six days";
	if($frequency =="604800") return "Once every week";
	if($frequency =="691200") return "Once every eighth day";
	if($frequency =="777600") return "Once every nineth day";
	if($frequency =="864000") return "Once every tenth day";
	if($frequency =="950400") return "Once every eleventh day";
	if($frequency =="1036800") return "Once every twelfth day";
	if($frequency =="1123200") return "Once every thirteenth day";
	if($frequency =="1209600") return "Once every two weeks";
	if($frequency =="1814400") return "Once every three weeks";
	if($frequency =="2419200") return "Once every four weeks";
	if($frequency =="3024000") return "Once every five weeks";
	if($frequency =="3628800") return "Once every six weeks";
	if($frequency =="4838400") return "Once every eight weeks";
	if($frequency =="7257600") return "Once every 12 weeks";
	if($frequency =="14515200") return "Once every 24 weeks";
	if($frequency =="31536000") return "Once every year";	    	
}
?>
<script>
$(function () { 
	$('.post-exception').popover({trigger: "hover"});
});	
function filterResults(x) {
	$.ajax({
  		type: "post",
  		url: "/members/setScheduleFilter.php",
		data: {'filter': x} ,
		dataType: "html"
	})
  	.done(function( msg ) { 	
		scheduledList()		
  	});		
}
</script>