<?php  include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<? 
if($_SESSION['validated'] != 'validated') { 
	
	header("Location: /");
	exit();
}
$user_id = $_SESSION['access_token']['user_id'];

// aws code
require_once("/home/yauhen/dev/tweetjunkebox/tweetjunkebox/aws/aws-autoloader.php");

use Aws\Common\Aws;
// Create the AWS service builder, providing the path to the config file
$aws = Aws::factory('/home/yauhen/dev/tweetjunkebox/tweetjunkebox/aws/config.php');
$s3Client = $aws->get('s3');
?>
<?
if($_POST['tweet_text'] != '' ) {
	
	//print "<pre>";
	//print_r($_POST);
	
	$list_number = mysqli_real_escape_string($conn, $_POST['jbList']);
	$tweet_text = mysqli_real_escape_string($conn, $_POST['tweet_text'] );	
	$SQL = "insert into tweets set  user_id = '$user_id', tweet_text = '$tweet_text', list_id = '$list_number', photo='' ";									
	$result = mysqli_query($conn, $SQL) or die(mysqli_error());		
	$rowID = $conn->insert_id ;
	
	$_SESSION['EXTENSION_JB'] = $list_number;
	
	
	if($_POST['photoURL']) {
		
		$fileInfo = new SplFileInfo($_POST['photoURL']);
		$fileExt = $fileInfo->getExtension();		
		$fileName = $rowID . "_" . $user_id. "_" . rand(1,1000) . "." . $fileExt;
		
		
		copy($_POST['photoURL'], '/var/www/imageWorks/'.stripslashes($fileName) );
		
		
		
		$mime = image_type_to_mime_type( exif_imagetype ( '/var/www/imageWorks/'.stripslashes($fileName) ) );
		
						
		
		$result = $s3Client->putObject(array(
			'Bucket'     => 'jukebox-member-images',
			'Key'        => $fileName,
			'ContentType' => $mime,
			'SourceFile' => '/var/www/imageWorks/'.stripslashes($fileName) 			
		));
		
		
		$photo = stripslashes($fileName); 
		$SQL = "update  tweets set tweet_text = '$tweet_text', photo = '$photo', media_id = '' where user_id = '$user_id' and id = '$rowID' ";
		$result = mysqli_query($conn, $SQL);
		
		exec("rm /var/www/imageWorks/* -f ");
		
		
	}
?>
	<script type='text/javascript'>
		setTimeout(function () {
		   try {
			  window.close();
		   } catch (e) {}
		}, 200);      
	</script>	
<?	
}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=600, initial-scale=1.0" />
<title>Tweet Jukebox</title>
<meta name="description" content="Tweet Jukebox - Thinking Inside the Box">
<!-- BOOTSTRAP STYLES-->
<link href="../assets/css/bootstrap_v3.3.4.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="../assets/css/font-awesome.css" rel="stylesheet" />
<!-- MORRIS CHART STYLES-->
<link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="../assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="jquery.timepicker.css" />
<link href="/members/chart.css" rel="stylesheet" type="text/css">
<!-- JQUERY SCRIPTS -->
<script src="../assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../assets/js/bootstrap_v3.3.4.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="../assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../assets/js/custom.js"></script>
<script src="/include/js_boot.js"></script>
<script>
function showRemainingCharacters(msg, photo) {
	if(photo == '1') {	
		var c = 140 - 23 - twttr.txt.getTweetLength(msg); 	
	} else {
		var c = 140 - twttr.txt.getTweetLength(msg);
	}
	if(parseInt(c) >= 0) {
		$('#masterLengthError').html('');	
	} else {
		$('#masterLengthError').html('Tweet is too large');	
	}	
	$('#editMessageCount').val(c);
	

}
</script>
</head>

<body>
<?
if($_SESSION['extension_type'] == 'text') {
	$msg = $_SESSION['extension_title'] . '&#13;&#10;' . $_SESSION['extension_url'] . '&#13;&#10;' . $_SESSION['extension_content'];
} else {
	$msg = $_SESSION['extension_title'] . '&#13;&#10;' . $_SESSION['extension_url'];	
}
?>
<script src="/include/twitter-text.js" ></script>
<form accept-charset="utf-8" id="masterListForm" method="post" enctype="multipart/form-data" action="addContent.php" >
<input type="hidden" name="photoURL" id="photoUrl" value="<? if($_SESSION['extension_type'] == 'image') echo $_SESSION['extension_content']; ?>" />
<div class="container">
	<div class="row" style="padding-top:20px;">
	<div class="col-md-4 col-sm-4 col-xs-4">
		<img src="../images/tweetjukebox3.png" class="img-responsive">
		<div class="form-group">
			<label for="jbList">Add This To</label>
			<select class="form-control" name="jbList" id="jbList" size="1">
				<?
				$SQL = "select list_name, list_id, schedule_status from user_schedule where user_id = '$user_id' order by list_name ";
				$result = mysqli_query($conn, $SQL) or die(mysqli_error());
				$row = mysqli_fetch_assoc($result);	
				$x = 0;
				do {
					$list_name = stripslashes($row['list_name']);
					$list_id = stripslashes($row['list_id']);
				?>
					<option value="<? echo $list_id; ?>" <? if($x == 0  || $list_id == $_SESSION['EXTENSION_JB'] ) { echo 'selected="selected"'; $x=1; } ?>><? echo $list_name; ?></option>
				<?	
				} while($row = mysqli_fetch_assoc($result));
				?>										
		  	</select>
		</div>		
	</div>
	<div class="col-md-8 col-sm-8 col-xs-8">		
		<div class="form-group">
			<input class="form-control" id="editMessageCount" type="text" readonly="" value="" style="display:inline-block; width:50px;"><span id="masterLengthError" style="display:inline-block; color:#FF0000;"></span>
		</div>	
		<div class="form-group">	
			<textarea class="form-control" name="tweet_text" rows="3" id="tweet_text" onKeyUp="showRemainingCharacters(this.value, <? if($_SESSION['extension_type'] == 'image') echo "'1'" ; else echo "'0'" ?>);"><? echo $msg; ?></textarea>
		</div>
		<div class="form-group">	
			<img id="tweetPhoto" src="<? if($_SESSION['extension_type'] == 'image')  echo $_SESSION['extension_content']; else echo "/images/spacer.gif"; ?>" alt="" height="100" />
		</div>
		<div class="form-group">
			<input class="btn" type="button" name="submitButton" id="submitButton" value="Submit" onClick="checkMasterEdit();" />
		</div>	
	</div>	
	</div>	
</div>
</form>
<div class="container">
	<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12 text-center">
		<img id="wait_screen" src="/images/wait.GIF" style="visibility:hidden; " height="50"  />
	</div>
	</div>
</div>		
<script>
var remainingCharacters = 140 - twttr.txt.getTweetLength('<? echo mysqli_real_escape_string($conn, $msg); ?>'); 	
<? if($_SESSION['extension_type'] == 'image') { ?>	
	remainingCharacters = remainingCharacters - 23;
<? } ?>	
$('#editMessageCount').val(remainingCharacters);
</script>
<script>




function checkMasterEdit() {
	if( parseInt($('#editMessageCount').val()) >=0) {
		$('#wait_screen').css('visibility', 'visible'); 
		document.getElementById('masterListForm').submit();
	} else {
		$('#masterLengthError').html('Tweet is too large');		
	}	
}

function readImage(input) {  		
	var imageExts = ["jpg","jpeg","png"];
	var filePathName = $('#myPhoto').val();
	var fileParts = filePathName.split(".");
	var ext = fileParts[fileParts.length -1];
	ext = ext.toLowerCase();	
	
	
	if(imageExts.indexOf(ext) != -1) {   // only render if image is ok to display					
		if (input.files && input.files[0])  {
						
			var reader = new FileReader();
			reader.onload = function (e) {									  
				$('#tweetPhoto')
				.attr('src',e.target.result)
				.height(100);				
			};
			reader.readAsDataURL(input.files[0]);			
			var remains = 140 - 23 - twttr.txt.getTweetLength($('#tweet_text').val()); 
			$('#editMessageCount').val(remains);
	
	   }
	} else {
		alert("Invalid format. Must be .png or .jpg types only");
		$('#myPhoto').val('');
	}	
}
</script>
</body>
</html>
<?	
	/*
	print "<pre>";
	print_r($_SESSION);
	*/
	unset ($_SESSION['collection']);
	unset ($_SESSION['url-collection']);
	unset ($_SESSION['text-collection']);
	unset ($_SESSION['image-collection']);
?>
 