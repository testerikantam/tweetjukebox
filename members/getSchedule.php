<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');
$user_id = $_SESSION['access_token']['user_id'];
$list_number = $_REQUEST['list_number'];

$SQL = "select * from user_schedule where user_id = '$user_id' and list_id = '$list_number' " ;
$result = mysqli_query($conn, $SQL);
$schRow = mysqli_fetch_assoc($result);

$list_name = stripslashes($schRow['list_name']);

if($list_name == '') $list_name = 'Jukebox #'.$list_number;

$row = 1;
$days = array('mon', 'tue', 'wed', 'thur', 'fri', 'sat', 'sun');

echo '<input type="hidden" id="list_number" name="list_number" value="'.$list_number.'"  />';
echo '<input type="hidden" id="current_list_name" name="current_list_name" value="'.$list_name.'"  />';


do {
	$day = $days[$row - 1];
	
?>
<td align="center">
	<label for="start_1<? echo $row; ?>">Start:</label>	
	<select id="start_1<? echo $row; ?>" name="start_1<? echo $row; ?>" disabled="disabled" class="" onChange="$('.start_1_on').val(this.value);" style="width:100px;" >
		<option value="00:00" <? if($schRow[$day."_start_1"] == '00:00') echo 'selected="selected"'; ?> >00:00am</option>
		<option value="01:00" <? if($schRow[$day."_start_1"] == '01:00') echo 'selected="selected"'; ?>>01:00am</option>
		<option value="02:00" <? if($schRow[$day."_start_1"] == '02:00') echo 'selected="selected"'; ?>>02:00am</option>
		<option value="03:00" <? if($schRow[$day."_start_1"] == '03:00') echo 'selected="selected"'; ?>>03:00am</option>
		<option value="04:00" <? if($schRow[$day."_start_1"] == '04:00') echo 'selected="selected"'; ?>>04:00am</option>
		<option value="05:00" <? if($schRow[$day."_start_1"] == '05:00') echo 'selected="selected"'; ?>>05:00am</option>
		<option value="06:00" <? if($schRow[$day."_start_1"] == '06:00') echo 'selected="selected"'; ?>>06:00am</option>
		<option value="07:00" <? if($schRow[$day."_start_1"] == '07:00') echo 'selected="selected"'; ?>>07:00am</option>
		<option value="08:00" <? if($schRow[$day."_start_1"] == '08:00') echo 'selected="selected"'; ?>>08:00am</option>
		<option value="09:00" <? if($schRow[$day."_start_1"] == '09:00') echo 'selected="selected"'; ?>>09:00am</option>
		<option value="10:00" <? if($schRow[$day."_start_1"] == '10:00') echo 'selected="selected"'; ?>>10:00am</option>
		<option value="11:00" <? if($schRow[$day."_start_1"] == '11:00') echo 'selected="selected"'; ?>>11:00am</option>
		<option value="12:00" <? if($schRow[$day."_start_1"] == '12:00') echo 'selected="selected"'; ?>>12:00pm</option>
		<option value="13:00" <? if($schRow[$day."_start_1"] == '13:00') echo 'selected="selected"'; ?>>01:00pm</option>
		<option value="14:00" <? if($schRow[$day."_start_1"] == '14:00') echo 'selected="selected"'; ?>>02:00pm</option>
		<option value="15:00" <? if($schRow[$day."_start_1"] == '15:00') echo 'selected="selected"'; ?>>03:00pm</option>
		<option value="16:00" <? if($schRow[$day."_start_1"] == '16:00') echo 'selected="selected"'; ?>>04:00pm</option>
		<option value="17:00" <? if($schRow[$day."_start_1"] == '17:00') echo 'selected="selected"'; ?>>05:00pm</option>
		<option value="18:00" <? if($schRow[$day."_start_1"] == '18:00') echo 'selected="selected"'; ?>>06:00pm</option>
		<option value="19:00" <? if($schRow[$day."_start_1"] == '19:00') echo 'selected="selected"'; ?>>07:00pm</option>
		<option value="20:00" <? if($schRow[$day."_start_1"] == '20:00') echo 'selected="selected"'; ?>>08:00pm</option>
		<option value="21:00" <? if($schRow[$day."_start_1"] == '21:00') echo 'selected="selected"'; ?>>09:00pm</option>
		<option value="22:00" <? if($schRow[$day."_start_1"] == '22:00') echo 'selected="selected"'; ?>>10:00pm</option>
		<option value="23:00" <? if($schRow[$day."_start_1"] == '23:00') echo 'selected="selected"'; ?>>11:00pm</option>
		<option value="23:59" <? if($schRow[$day."_start_1"] == '23:59') echo 'selected="selected"'; ?>>11:59pm</option>
	</select>
	<label for="end_1<? echo $row; ?>">Stop:</label>
	<select id="end_1<? echo $row; ?>"  name="end_1<? echo $row; ?>" disabled="disabled" class="" onChange="$('.end_1_on').val(this.value);" style="width:100px;">
		<option value="01:00" <? if($schRow[$day."_end_1"] == '01:00') echo 'selected="selected"'; ?>>01:00am</option>
		<option value="02:00" <? if($schRow[$day."_end_1"] == '02:00') echo 'selected="selected"'; ?>>02:00am</option>
		<option value="03:00" <? if($schRow[$day."_end_1"] == '03:00') echo 'selected="selected"'; ?>>03:00am</option>
		<option value="04:00" <? if($schRow[$day."_end_1"] == '04:00') echo 'selected="selected"'; ?>>04:00am</option>
		<option value="05:00" <? if($schRow[$day."_end_1"] == '05:00') echo 'selected="selected"'; ?>>05:00am</option>
		<option value="06:00" <? if($schRow[$day."_end_1"] == '06:00') echo 'selected="selected"'; ?>>06:00am</option>
		<option value="07:00" <? if($schRow[$day."_end_1"] == '07:00') echo 'selected="selected"'; ?>>07:00am</option>
		<option value="08:00" <? if($schRow[$day."_end_1"] == '08:00') echo 'selected="selected"'; ?>>08:00am</option>
		<option value="09:00" <? if($schRow[$day."_end_1"] == '09:00') echo 'selected="selected"'; ?>>09:00am</option>
		<option value="10:00" <? if($schRow[$day."_end_1"] == '10:00') echo 'selected="selected"'; ?>>10:00am</option>
		<option value="11:00" <? if($schRow[$day."_end_1"] == '11:00') echo 'selected="selected"'; ?>>11:00am</option>
		<option value="12:00" <? if($schRow[$day."_end_1"] == '12:00') echo 'selected="selected"'; ?>>12:00pm</option>
		<option value="13:00" <? if($schRow[$day."_end_1"] == '13:00') echo 'selected="selected"'; ?>>01:00pm</option>
		<option value="14:00" <? if($schRow[$day."_end_1"] == '14:00') echo 'selected="selected"'; ?>>02:00pm</option>
		<option value="15:00" <? if($schRow[$day."_end_1"] == '15:00') echo 'selected="selected"'; ?>>03:00pm</option>
		<option value="16:00" <? if($schRow[$day."_end_1"] == '16:00') echo 'selected="selected"'; ?>>04:00pm</option>
		<option value="17:00" <? if($schRow[$day."_end_1"] == '17:00') echo 'selected="selected"'; ?>>05:00pm</option>
		<option value="18:00" <? if($schRow[$day."_end_1"] == '18:00') echo 'selected="selected"'; ?>>06:00pm</option>
		<option value="19:00" <? if($schRow[$day."_end_1"] == '19:00') echo 'selected="selected"'; ?>>07:00pm</option>
		<option value="20:00" <? if($schRow[$day."_end_1"] == '20:00') echo 'selected="selected"'; ?>>08:00pm</option>
		<option value="21:00" <? if($schRow[$day."_end_1"] == '21:00') echo 'selected="selected"'; ?>>09:00pm</option>
		<option value="22:00" <? if($schRow[$day."_end_1"] == '22:00') echo 'selected="selected"'; ?>>10:00pm</option>
		<option value="23:00" <? if($schRow[$day."_end_1"] == '23:00') echo 'selected="selected"'; ?>>11:00pm</option>
		<option value="23:59" <? if($schRow[$day."_end_1"] == '23:59') echo 'selected="selected"'; ?>>11:59pm</option>
	</select>		
	<input type="hidden" id="frequency_1<? echo $row; ?>" name="frequency_1<? echo $row; ?>" disabled="disabled" class=""  value="<? echo $schRow[$day."_frequency_1"];?>"  />
	<?
	if($schRow[$day."_frequency_1"] != 'off') {	
		$frequencyHours = floor($schRow[$day."_frequency_1"] / 60);
		$frequencyMinutes = $schRow[$day."_frequency_1"] % 60;
	} else {
		$frequencyHours = 0;
		$frequencyMinutes = 0;	
	}
	?>			
	<!--label><b>Frequency Demo - do not use! (<? echo $frequencyHours. " " . $frequencyMinutes; ?>)</b></label-->
	<table>	
	<tr><th colspan="2"><center>Tweet Every<br />Zero Hr/Min = off</center></th></tr>
	<tr align="center"><td>Hours</td><td>Minutes</td></tr>
	<tr>
	<td><input type="number" class="inputText" style="width:30px;" disabled="disabled"  id="frequency_hours_1<? echo $row; ?>" name="frequency_hours_1<? echo $row; ?>" min="0" max="24" value="<? echo $frequencyHours;?>" onchange="adjustFrequency(<? echo $row; ?>)"></td>
	<td><input type="number" class="inputText" style="width:30px;" disabled="disabled"  id="frequency_minutes_1<? echo $row; ?>" name="frequency_minutes_1<? echo $row; ?>" min="0" max="59" value="<? echo $frequencyMinutes;?>" onchange="adjustFrequency(<? echo $row; ?>)"></td>
	</tr>
	</table>	
</td>
<? 
   $row++;
 } while($row < 8);
?>