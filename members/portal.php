<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/twitteroauth/twitteroauth.php");
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/config.php");
?>
<?
//if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') {
	header('Location: /');
	exit();
}	
$screen_name = $_SESSION['screen_name'];
//$access_token = $_SESSION['access_token'];
//$user_id = $_SESSION['access_token']['user_id'];

$SQL = "select * from users where screen_name = '$screen_name'  ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$row = mysqli_fetch_assoc($result);	
$_SESSION['OFFER_SEEN'] = $row['offer_seen'];

if($row['status'] == 0 ) {
	header("Location: /oops.php?err=expired");
	exit();
}	





$_SESSION['planName'] = $row['planName'];
$_SESSION['isAdmin'] = $row['isAdmin'];
$list_count = $row['list_count'];
$all_tweet_status = $row['all_tweet_status'];
$user_id = $row['user_id'];
$send_thanks = $row['send_thanks'];



if($_SESSION['planName'] == 'Basic' || $row['planName'] == '') {
	$_SESSION['boxLimit'] = BASIC_PLAN_BOX_LIMIT;
}	
if($_SESSION['planName'] == 'Gold') {
	$_SESSION['boxLimit'] = GOLD_PLAN_BOX_LIMIT;
}	
if($_SESSION['planName'] == 'Silver') {
	$_SESSION['boxLimit'] = SILVER_PLAN_BOX_LIMIT;
}	

if($_SESSION['planName'] == 'Basic' || $row['planName'] == '') {
	$_SESSION['scheduleLimit'] = BASIC_PLAN_SCHEDULE_LIMIT;
}	
if($_SESSION['planName'] == 'Gold') {
	$_SESSION['scheduleLimit'] = GOLD_PLAN_SCHEDULE_LIMIT;
}	
if($_SESSION['planName'] == 'Silver') {
	$_SESSION['scheduleLimit'] = SILVER_PLAN_SCHEDULE_LIMIT;
}	

$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
$content = $connection->get('users/show', array('screen_name' => $screen_name));

//$user_id = $_SESSION['access_token']['user_id'];

//print $content->name; die();
$_SESSION['name'] = $content->name;

?>
<? 
if(strtotime($_SESSION['expireDate']) <= strtotime("now") && $_SESSION['planName'] =='' ) {
	header("Location: /oops.php?err=expired");
	exit();
}
?>	
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Tweet Jukebox</title>
<!-- BOOTSTRAP STYLES-->
<link href="../assets/css/bootstrap.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="../assets/css/font-awesome.css" rel="stylesheet" />
<!-- MORRIS CHART STYLES-->
<link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="../assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="jquery.timepicker.css" />
<link href="/members/chart.css" rel="stylesheet" type="text/css">
<!-- JQUERY SCRIPTS -->
<script src="../assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../assets/js/bootstrap.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="../assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../assets/js/custom.js"></script>
<script src="/include/js_boot.js"></script>
<script type="text/javascript" src="jquery.timepicker.js"></script>
<link rel="stylesheet" href="/highslide-4.1.13/highslide/highslide.css" type="text/css" />
<!-- 9d83e18a764256552baf6956b777f190 -->
<script type="text/javascript" src="/highslide-4.1.13/highslide/highslide-full.packed.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/highslide-4.1.13/highslide/graphics/';
	hs.showCredits = false;
    hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = .45;
	hs.zIndexCounter = 2000;
//	hs.objectLoadTime = 'after';
//	hs.forceAjaxReload = 'true';
</script>
</head>
<body>
<div id="wrapper">
  <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="/">Tweet Jukebox</a> 		
	</div>
    <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
		<ul class="nav navbar-nav navbar-cls-top" style="cursor:pointer;">
            <li><a href="/"><i class="fa fa-home"></i> Home </a></li>
            <li><a href="/about"> About </a></li>
        	<li><a href="/contact"> Contact us </a></li>
			<li><a href="/faq"><i class="fa fa-info-circle"></i> FAQ </a></li>					
		</ul>
		<span data-toggle="dropdown" aria-expanded="false"><a href="#"><img style="display:inline; border:solid 1px #FFFFFF; " src="<? echo $content->profile_image_url; ?>" ></a></span>
		<ul class="dropdown-menu dropdown-menu-right" role="menu" style="padding-top:10px;">
			<li style="padding-bottom:10px; padding-top:5px; border-bottom:solid 1px #cccccc;"><a href="#" onClick="thanksSetup();"><i class="fa fa-gift fa-2x"></i> Thank You Tweets</a></li>										
			<li style="padding-bottom:10px; padding-top:5px; border-bottom:solid 1px #cccccc;"><a href="#" onClick="settingsForm();"><i class="fa fa-cog fa-2x"></i> My Settings</a></li>
			<?
			if($all_tweet_status == 1) {
			?>	
					<li><a href="" id="schTweetStatusOn"  style="display:block; padding-bottom:10px; padding-top:5px; color:#009900;" onClick="if(confirm('Are you sure you want to stop all tweeting for your account?') == true) { $('#schTweetStatusOn').css('display', 'none'); $('#schTweetStatusOff').css('display', 'block'); switchAllTweets('off'); location.reload(); }"  ><i class="fa fa-thumbs-o-up fa-2x"></i> Circuit Breaker Is On<br />controls all tweets</a></li>
					<li><a href="" id="schTweetStatusOff" style="display:none; padding-bottom:10px; padding-top:5px; color:#FF0000;"  onclick="$('#schTweetStatusOn').css('display', 'block'); $('#schTweetStatusOff').css('display', 'none'); switchAllTweets('on'); location.reload();" ><i class="fa fa-thumbs-o-down fa-2x"></i> Circuit Breaker Is Off<br />controls all tweets</a></li>
			<?	
				} else { 
			?>
					<li><a href="" id="schTweetStatusOn"  style="display:none; padding-bottom:10px; padding-top:5px; color:#009900;" onClick="if(confirm('Are you sure you want to stop all tweeting for your account?') == true) { $('#schTweetStatusOn').css('display', 'none'); $('#schTweetStatusOff').css('display', 'block'); switchAllTweets('off');  location.reload(); }"  ><i class="fa fa-thumbs-o-up fa-2x"></i> Circuit Breaker Is On<br />controls all tweets</a></li>
					<li><a href="" id="schTweetStatusOff" style="display:block; padding-bottom:10px; padding-top:5px; color:#FF0000;" onClick="$('#schTweetStatusOn').css('display', 'block'); $('#schTweetStatusOff').css('display', 'none'); switchAllTweets('on');  location.reload();" ><i class="fa fa-thumbs-o-down fa-2x"></i> Circuit Breaker Is Off<br />controls all tweets</a></li>
			<?	
				} 
			?>			
			<li style="padding-bottom:10px; padding-top:5px; border-top:solid 1px #cccccc;"><a href="logout.php"><i class="fa fa-sign-out fa-2x"></i> Log Out</a></li>			
		</ul>
	</div>
  </nav>
  <!-- /. NAV TOP  -->  
  <nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
      <ul class="nav" id="main-menu">
        <li class="text-center"><img src="/images/tweetjukebox3.png" width="828" height="645" class="user-image img-responsive"/></li>        
		<li>
			<a id="myJBLI" href="#" class="jbList"><i class="fa fa-cloud fa-3x"></i>My Juke Boxes<span class="fa arrow"></span></a>
			<ul id="jukeBoxNavList" class="nav nav-second-level">
			<? 
			$SQL = "select list_name, list_id, schedule_status from user_schedule where user_id = '$user_id' order by list_name ";
			$result = mysqli_query($conn, $SQL) or die(mysqli_error());
			$row = mysqli_fetch_assoc($result);				
			?>
			<?	
				do {
					$list_name = stripslashes($row['list_name']);
					$list_id = stripslashes($row['list_id']);
			?>
			<?
			?>
				<li>
				<a href="#" id="jbLinkList<? echo $list_id;?>" onClick="
				if($('#tweetLI').length) {	
					if($('#scheduleLI').hasClass('jbc')) { setupSchedule(<? echo $list_id; ?>); }
					if($('#optionLI').hasClass('jbc')) { jbOptions(<? echo $list_id; ?>); }
					if($('#tweetLI').hasClass('jbc')) { masterList(<? echo $list_id; ?>); }
				} else {
					if($(this).hasClass('newList')) { 
						$(this).removeClass('newList');
						jbOptions(<? echo $list_id; ?>); 						
					} else {	
						masterList(<? echo $list_id; ?>);
					}	
				}	
				">
			<? 										
				echo $list_name; 
				if($row['schedule_status'] == '0') echo ' - off'; 	
			?>
				</a>
				</li>						
			<?
				} while($row = mysqli_fetch_assoc($result));										
			?>
			</ul>
		</li>
		<li><a href="#" onClick="if(confirm('Add another Juke Box?') == true) { addJukeBox(); }"><i class="fa fa-plus-square-o fa-3x"></i> Add A New Jukebox </a> </li>	
        <li><a href="#" class="schedule" onClick="scheduledList();"><i class="fa fa-calendar fa-3x"></i> Scheduled Tweets </a> </li>      
	  	<li><a  href="#" class="schedule" onClick="recentMentions();"><i class="fa fa-bullhorn fa-3x"></i> Recent Mentions</a></li>
		
		<? if ($_SESSION['isAdmin'] === '1' ) { ?>			
			<li><a  href="/admin" class="schedule" ><i class="fa fa-users fa-3x"></i> Admin Portal</a></li>
		<? } ?>	
	  </ul>
    </div>
  </nav>
  <!-- /. NAV SIDE  -->
  <div id="page-wrapper" >
    <div id="page-inner">      	  	  
	  <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div id="masterDiv" style="display:block; height:auto; padding-top:10px; ">
			 	<center><img src="/images/tweetjukebox3.png" class="img-responsive" style="padding-top:50px;" ></center>			
			 </div>
			 <div id="recentMentionsDiv" style="display:none;"></div>
			 <div id="uploadDiv" style="display:none; ">
			 	<?
				$SQL = "select count(id) as totalTweets from tweets where user_id = '$user_id' ";
				$result = mysqli_query($conn, $SQL) or die(mysqli_error());
				$row = mysqli_fetch_assoc($result);
				$totalTweets = $row['totalTweets'];
												
				if($totalTweets >= $_SESSION['boxLimit']) {	
				?>
					<h2 align="center">Sorry your current plan does not allow you to add any more new records to your Jukebox!</h2>			
				<? } else { ?>
				<div onClick="masterList(0);" style="font-size:18px; font-weight:bold; float:right;">close</div>
				<form class="l-form" id="uploadForm" name="uploadForm" method="post" enctype="multipart/form-data" action="postDataBoot.php" accept-charset="utf-8">
				<h2 id='uploadH2' align="center">Upload Tweets</h2>
					<div style="text-align:left;">
						<p>We will automatically post status updates (Tweets) to twitter on your behalf.  You can have as many Tweets as your plan allows.</p>
						<p>Once you have built your &quot;Jukebox&quot; of Tweets you can schedule how often they will be sent.&nbsp; We will randomly choose Tweets from your inventory that have not recently been tweeted, or have never been tweeted.&nbsp; We will NOT repeat any Tweet in the same day or those that have been Tweeted within a number of days that you select.&nbsp;</p>
						<p>Once all of your Tweets have been Tweeted at least once, we will continue to randomly select tweets to post, unless you put the process on &quot;hold&quot;.&nbsp; We post updates to Twitter every 5 minutes.</p>
						<p>Your file must be in a CSV format and must contain ONE column headings in the first row called: <br /><b>tweet_text</b></p>						
						<p>Click here to get a template -> <a href="download.php">Download Template</a></p>
						<p><input id="myFile" type="file" name="myFile" onChange="readURL(this);" /></p>
						<p class="ui-state-error">After your new Tweets are uploaded, we will list all your Tweets, newest / first - so you can review your latest additions for editing as you see fit.</p>
						<p id="messageArea" align="center">&nbsp;</p>
					</div>
					<input type="hidden" id="upload_list_number" value="" name="upload_list_number" />
				</form>				
				<script>
				var files;
				function readURL(input) {  	
					files = event.target.files;
					var filePathName = $('#myFile').val();
					var fileParts = filePathName.split(".");
					var ext = fileParts[fileParts.length -1];			
					ext = ext.toLowerCase();
					
					var fileNameParts = filePathName.split("\\");
					var fileName = fileNameParts[fileNameParts.length -1];											
					
					if(ext != 'csv') {
						$('#messageArea').css('color', 'red');
						$('#messageArea').html('Invalid file format! Your file must be a .csv file.');
						$('#myFile').val('');
					} else {
						$('#messageArea').html('');
						var reader = new FileReader();
						reader.onload = function (e) {									  
							csvAsArrayA = e.target.result.csvToArray();
							if( (csvAsArrayA[0][0]) != 'tweet_text' ) {
								$('#messageArea').css('color', 'red');
								$('#messageArea').html('Invalid file construction. <p>Your first row must contain this column heading:</p><p><b>tweet_text.</b></p><p class="content">Click here to get a template -> <a href="download.php">Download Template</a></p>');
								$('#myFile').val('');
							} else {
								
								if( confirm("Warning!!!!\n\nThis will ADD the records in this file to your Jukebox #" + $('#upload_list_number').val() + ".\n\nTHIS PROCESS CANNOT BE UNDONE.\n\nIf this is what you want to do, press OK to upload this file: " + fileName ) ) {																
									hideDivs();
									$('#recentMentionsDiv').css('display', 'block');
									$('#recentMentionsDiv').html('<div align="center">Please wait while we load your file....<p><img src="/images/wait.GIF" border="0"></p></div>');									
									$('#uploadForm').submit();																																																																	
								}	
							}
						};
						reader.readAsText(input.files[0]);
					}
				}	
				</script>	
				<script type="text/javascript">
				/* Copyright 2012-2013 Daniel Tillin
				 *
				 * Permission is hereby granted, free of charge, to any person obtaining
				 * a copy of this software and associated documentation files (the
				 * "Software"), to deal in the Software without restriction, including
				 * without limitation the rights to use, copy, modify, merge, publish,
				 * distribute, sublicense, and/or sell copies of the Software, and to
				 * permit persons to whom the Software is furnished to do so, subject to
				 * the following conditions:
				 *
				 * The above copyright notice and this permission notice shall be
				 * included in all copies or substantial portions of the Software.
				 * 
				 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
				 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
				 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
				 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
				 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
				 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
				 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
				 *
				 * csvToArray v2.1 (Unminifiled for development)
				 *
				 * For documentation visit:
				 * http://code.google.com/p/csv-to-array/
				 *
				 */
				String.prototype.csvToArray = function (o) {
					var od = {
						'fSep': ',',
						'rSep': '\r\n',
						'quot': '"',
						'head': false,
						'trim': false
					}
					if (o) {
						for (var i in od) {
							if (!o[i]) o[i] = od[i];
						}
					} else {
						o = od;
					}
					var a = [
						['']
					];
					for (var r = f = p = q = 0; p < this.length; p++) {
						switch (c = this.charAt(p)) {
						case o.quot:
							if (q && this.charAt(p + 1) == o.quot) {
								a[r][f] += o.quot;
								++p;
							} else {
								q ^= 1;
							}
							break;
						case o.fSep:
							if (!q) {
								if (o.trim) {
									a[r][f] = a[r][f].replace(/^\s\s*/, '').replace(/\s\s*$/, '');
								}
								a[r][++f] = '';
							} else {
								a[r][f] += c;
							}
							break;
						case o.rSep.charAt(0):
							if (!q && (!o.rSep.charAt(1) || (o.rSep.charAt(1) && o.rSep.charAt(1) == this.charAt(p + 1)))) {
								if (o.trim) {
									a[r][f] = a[r][f].replace(/^\s\s*/, '').replace(/\s\s*$/, '');
								}
								a[++r] = [''];
								a[r][f = 0] = '';
								if (o.rSep.charAt(1)) {
									++p;
								}
							} else {
								a[r][f] += c;
							}
							break;
						default:
							a[r][f] += c;
						}
					}
					if (o.head) {
						a.shift()
					}
					if (a[a.length - 1].length < a[0].length) {
						a.pop()
					}
					return a;
				}
				
				</script>			 
			 <? } ?>
			 </div>
			 
        </div>        
      </div>
  	  <div class="row" style="padding-top:50px;">
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<p align="center">
   			<span class="left">tj.local &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> | <!--a href="/plans.php">PLANS</a> | --><a href="/contact">CONTACT US</a> | <a href="/privacy">PRIVACY POLICY</a> | <a href="/terms">TERMS AND CONDITIONS</a>
			</p>
		</div>	
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
	  </div>
    </div>
    <!-- /. PAGE INNER  -->
  </div>
  <!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- MORRIS CHART SCRIPTS -->
<script src="/assets/js/morris/raphael-2.1.0.min.js"></script>
<script src="/assets/js/morris/morris.js"></script>
<script type="text/javascript">
$('.sidebar-collapse li a').on('click', function() { 		
	
	$('.sidebar-collapse li a').css( "background-color", "#4D4D4D" );	
	$('.sidebar-collapse li a').removeClass("active");
		
	$(this).addClass("active");	
			
	if($(this).hasClass('schedule')) {
		if($('#jukeBoxNavList').hasClass("in")) {
			$('#jukeBoxNavList').removeClass("in");			
		}
	}	
	if($(this).hasClass('jbList')) {
		$('#masterDiv').html('<center><img src="/images/tweetjukebox3.png" class="img-responsive" style="padding-top:50px;" ></center>');
	}			
});
$('.nav-second-level li').on('click', function() { 		


//	$( '.nav-second-level li' ).css( "background-color", "#4D4D4D" );	
	$( this ).children().css( "background-color", "red" );
						
});
</script>
</body>
<? if($_SESSION['NEWJBUPLOAD']) { ?>	
	<script> 
	$('#masterDiv').css('display', 'block');
	$('#masterDiv').html('<div align="center">Please wait while we get your recent Jukebox additions for you....<p><img src="../images/wait.GIF" border="0"></p></div>');	
	setTimeout(function (){		
		$.ajax({
			type: "get",
			url: "/members/getMasterListBoot.php?list_number=<? echo $_SESSION['LISTNUMBER'];?>&s=tweet_last_post_date, id DESC",
			//data: {'message': msg} ,
			dataType: "html"
		})
		.done(function( msg ) { 
			$('#masterDiv').html(msg);
		});	
	}, 2000);
	</script>
<? 
		unset($_SESSION['LISTNUMBER']);
		unset($_SESSION['NEWJBUPLOAD']);		
	}  	
?>
<? if($_SESSION['NEWJBADDED']) { ?>
<script>
	$('#myJBLI').click();
	$('#jukeBoxNavList').addClass("in");
	$('#jbLinkList<? echo $_SESSION['NEWJBADDED'];?>').addClass("newList");
	$('#jbLinkList<? echo $_SESSION['NEWJBADDED'];?>').click();
</script>
<?
	unset($_SESSION['NEWJBADDED']);
} 
?>
<? if($_SESSION['new_user'] == '1') { ?>
<script>
	$('#myJBLI').click();
	$('#scheduleLI').removeClass('jbc');
	$('#optionLI').removeClass('jbc');		
	$('#jukeBoxNavList').addClass("in");
	$('#jbLinkList1').click();
	masterList('1');
</script>
<?
} 
?>
</html>