<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/twitteroauth/twitteroauth.php");
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/config.php");
?>
<?
if(!isset($_SESSION['access_token'])) {
	header("Location: /");
	exit();
}

// get users last params
$user_id = $_SESSION['access_token']['user_id'];

$screen_name = $_SESSION['screen_name'];
//$access_token = $_SESSION['access_token'];
//$user_id = $_SESSION['access_token']['user_id'];

$SQL = "select * from users where user_id = '$user_id'  ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$row = mysqli_fetch_assoc($result);	
$send_thanks = $row['send_thanks'];
$thanks_limit = $row['thanks_limit'];
$thanks_days = $row['thanks_days'];
$blackList = stripslashes($row['blackList']);
$thanks_message = stripslashes($row['thanks_message']);
$thanks_prefix = stripslashes($row['thanks_prefix']);
if($thanks_prefix == '') $thanks_prefix = 'Thanks to my top interactors! ';
if($thanks_limit == 0 ) $thanks_limit = '1';
?>
<? if($_SESSION['planName'] == 'Gold' || $_SESSION['planName'] == 'Free' ) { ?>
<div class="row">
	<div class="col-md-1"></div>
	<div class="col-md-10 text-center">
	  <h3>As part of your free plan, you have an option to send out thank you tweets every Friday or to send out a daily random PROMO tweet for our service.</h3>	
	<p>You don't need to do anything except to check which option you would prefer.  We'll take care of the rest. </p>	
	</div>
	<div class="col-md-1"></div>
</div>

<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-6 text-center">	
		<form id="thanksSetupForm" name="thanksSetupForm" method="post" enctype="multipart/form-data" accept-charset="utf-8">	
    		<div class="form-group text-center">	
				<label class="inputLabel" for="send_thanks">Select Method
				<select name="send_thanks" id="send_thanks" class="form-control"  >				 
				  <option value="0" <? if($send_thanks == '0') echo 'selected="selected"' ; ?>>Do Not Send Any</option>
				  <option value="1" <? if($send_thanks == '1') echo 'selected="selected"' ; ?>>Send Friday Thank You Tweets</option>
				  <option value="2" <? if($send_thanks == '2' || $send_thanks == '') echo 'selected="selected"' ; ?>>Send Daily Random Promo Tweet</option>
				</select>
				</label>
			</div>	
		    <input type="hidden"  id="thanks_limit" name="thanks_limit"  value="50">							
				 <div class="form-group">					
					<b>EXCLUDE THESE MEMBERS:</b><br />If you chose to send Thank You Tweets on Friday's , enter the screen names of Twitter users you <u><strong>DO NOT</strong></u> want to include in your Thank You Tweets.  Use commas between each user name.
					<textarea class="form-control" id="blackList" name="blackList"><? echo $blackList; ?></textarea>
					Example:  johnDoe, marySmith, sallyFields
				 </div>			 
			<div class="form-group">
				<input type="button" value="Save Settings" id="thanks_button" name="thanks_button" onclick="submitThanksSetupForm()"  />					
			</div>						 						
			<span align="center" class="panel panel-success" id="saveThanksSettingMessage" style="visibility:hidden; font-weight:bold; color:#009900; width:200px;">&nbsp;</span>
		</form>	
	</div>
	<div class="col-md-3"></div>
</div>


	
<? } else { ?>
<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-6 text-center">
	  <h3>You have the power to  send out thank you tweets every Friday!</h3>	
	<p>Friday morning we will grab all unique users that mentioned you during the prior week and construct a thank you tweet for the number of users you want to thank.</p>
	<p>You don't need to do anything except to opt in and tell us how many unique users you want to thank, up to 50. </p>	
	</div>
	<div class="col-md-3"></div>
</div>	
<div class="row">
	<div class="col-md-3"></div>
	<div class="col-md-6 text-center">	
		<form id="thanksSetupForm" name="thanksSetupForm" method="post" enctype="multipart/form-data" accept-charset="utf-8">	
    		<div class="form-group text-center">	
				<label class="inputLabel" for="send_thanks">Send Thanks
				<select name="send_thanks" id="send_thanks" class="form-control" style="width:80px;" >
				  <option value="1" <? if($send_thanks == '1') echo 'selected="selected"' ; ?>>Yes</option>
				  <option value="0" <? if($send_thanks == '0' || $send_thanks == '') echo 'selected="selected"' ; ?>>No</option>
				</select>
				</label>
			</div>	
		    <div class="form-group text-center">	
				<label id="thanksLimitLabel" class="inputLabel" for="thanks_limit">Thank How Many Users (max <? echo $_SESSION['thankLimit'] ; ?>)
				<center><input type="number" class="form-control" style="width:80px;" id="thanks_limit" name="thanks_limit" min="1" max="<? echo $_SESSION['thankLimit'] ; ?>" value="<? echo $thanks_limit; ?>"></center>
				</label>
			</div>
			<div class="form-group text-center">
				<label class="inputLabel" for="thanks_prefix">Thanks Prefix (max 45 characters)</label>
				<input type="text" maxlength="45" style="width:220px;"  id="thanks_prefix" name="thanks_prefix" value="<? echo $thanks_prefix; ?>" >
			</div>
			<div class="form-group text-center">
				<label class="inputLabel" for="thanks_message">Thanks Message (max 45 characters)</label>
				<input type="text" maxlength="45" style="width:220px;" id="thanks_message" name="thanks_message" value="<? echo $thanks_message; ?>" >
			</div>
			<? if($_SESSION['planName'] == 'Pro' || $_SESSION['planName'] == 'Business' ) { 
				$thanksDays = explode(",", $thanks_days);
				$days=array("Mon","Tue","Wed","Thur","Fri","Sat","Sun");				
				echo "<p><b>Send Thanks On These Days<br>(Note: if you don't check ANY days, your thanks will be sent on Friday's.)</b></p>";
				foreach($days as $day)  {
					if(in_array($day, $thanksDays)) $dayStatus = 1; else $dayStatus = 0;
				?>	
					<? echo $day; ?>&nbsp;<input type="checkbox" id="<? echo $day; ?>" name="<? echo $day;?>" <? if($dayStatus == 1) echo 'checked="checked"'; ?> value="<? echo $day; ?>" >&nbsp;
				<?
				}  
			 } ?>		
				 <div class="form-group" style="padding-top:30px;">					
					<b>EXCLUDE THESE MEMBERS:</b><br />Enter the screen names of Twitter users you <u><strong>DO NOT</strong></u> want to include in your Thank You Tweets.  Use commas between each user name.
					<textarea class="form-control" id="blackList" name="blackList"><? echo $blackList; ?></textarea>
					Example:  johnDoe, marySmith, sallyFields
				 </div>			 
			<div class="form-group">
				<input type="button" value="Save Settings" id="thanks_button" name="thanks_button" onclick="submitThanksSetupForm()"  />					
			</div>						 						
			<span align="center" class="panel panel-success" id="saveThanksSettingMessage" style="visibility:hidden; font-weight:bold; color:#009900; width:200px;">&nbsp;</span>
		</form>	
	</div>
	<div class="col-md-3"></div>
</div>
<? } ?>	
<script>
function submitThanksSetupForm() {

	var send_thanks = $('#send_thanks').val();
	var thanks_limit = $('#thanks_limit').val(); 
	var blackList = $('#blackList').val(); 
	var thanks_message= $('#thanks_message').val(); 
	var thanks_prefix= $('#thanks_prefix').val(); 
	if( parseInt(thanks_limit) > <? echo number_format($_SESSION['thankLimit'], 0) ; ?> && parseInt(thanks_limit) != 50  ) {
		$('#thanksLimitLabel').css('color', '#FF0000');
		return;
	}				
	var tweetdays= new Array();	
	if($('#Mon').prop('checked')) {
		tweetdays.push('Mon');
	}
	if($('#Tue').prop('checked')) {
		tweetdays.push('Tue');
	}
	if($('#Wed').prop('checked')) {
		tweetdays.push('Wed');
	}
	if($('#Thur').prop('checked')) {
		tweetdays.push('Thur');
	}
	if($('#Fri').prop('checked')) {
		tweetdays.push('Fri');
	}
	if($('#Sat').prop('checked')) {
		tweetdays.push('Sat');
	}
	if($('#Sun').prop('checked')) {
		tweetdays.push('Sun');
	}

	$.ajax({
  		type: "post",
  		url: "/members/saveThanksSetup.php",
		data: {'thanks_limit': thanks_limit, 'send_thanks': send_thanks, 'blackList': blackList, 'thanks_message': thanks_message, 'thanks_prefix': thanks_prefix, 'thanks_days': tweetdays} ,
		dataType: "html"
	})
  	.done(function( msg ) {	
		$('#saveThanksSettingMessage').css('visibility', 'visible');
		$('#saveThanksSettingMessage').fadeTo(0, 3000);
		$('#saveThanksSettingMessage').html(msg);
		$('#saveThanksSettingMessage').fadeTo(3000, 0);		
  	});		
}
</script>		