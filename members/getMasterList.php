<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');
$user_id = $_SESSION['access_token']['user_id'];
$list_number = $_REQUEST['list_number'];
?>
<?
if($_POST['delete'] ) {
	$rid = $_POST['delete'];
	$SQL = "delete from tweets where id = '$rid' ";
	$result = mysqli_query($conn, $SQL) or die(mysqli_error());
	exit();
}
?>
<?


$SQL = "select schedule_status, retweet_days from user_schedule where user_id = '$user_id' and list_id = '".$list_number."'" ;
$result = mysqli_query($conn, $SQL);
if(mysqli_num_rows($result)) {
	$row = mysqli_fetch_assoc($result);
	$schedule_status = $row['schedule_status'];
	$retweet_days = $row['retweet_days'];
} else {
	$schedule_status = 'needed';
	$retweet_days = '';
}	

?>
<?
$maxRows = 25;
$pageNum = 0;
$currentPage = $_SERVER['QUERY_STRING'];

$searchTerm = $_REQUEST['searchTerm'];
$searchField = $_REQUEST['searchField'];

$s = mysqli_real_escape_string($conn, $_REQUEST['s']);

if (isset($_GET['pageNum'])) {
  $pageNum = $_GET['pageNum'];
}
$startRow = $pageNum * $maxRows;

$SQL2 = "select count(id) as tweetTotal from tweets where user_id = '$user_id' and list_id = '$list_number' ";	
$result2 = mysqli_query($conn, $SQL2) or die(mysqli_error());
$row2 = mysqli_fetch_assoc($result2);
$tweetTotal = $row2['tweetTotal'];


$SQL = "select * from tweets where user_id = '$user_id' and list_id = '".$list_number."'" ;					

if($_REQUEST['search_tweet_text']) {
	$search_tweet_text = mysqli_real_escape_string($conn, $_REQUEST['search_tweet_text']);
	$SQL .= " and tweet_text like '%".$search_tweet_text."%' ";
}
if($_REQUEST['search_tweet_category']) {
	$search_tweet_category = mysqli_real_escape_string($conn, $_REQUEST['search_tweet_category']);
	$SQL .= " and tweet_category like '%".$search_tweet_category."%' ";
}
if($_REQUEST['search_tweet_author']) {
	$search_tweet_author = mysqli_real_escape_string($conn, $_REQUEST['search_tweet_author']);
	$SQL .= " and tweet_author like '%".$search_tweet_author."%' ";
}
if($_REQUEST['search_tweet_tags']) {
	$search_tweet_tags = mysqli_real_escape_string($conn, $_REQUEST['search_tweet_tags']);
	$SQL .= " and tweet_tags like '%".$search_tweet_tags."%' ";
}

$sortBy = "order by tweet_text  ";
if($_REQUEST['s']) $sortBy = "order by ".$_REQUEST['s'];

$SQL .= $sortBy;					
$SQL2 = getPagingQuery($SQL, $maxRows ); 
$pagingLink = getPagingLink($SQL, $maxRows, $_SERVER['QUERY_STRING'], true, $_REQUEST['s']."&list_number=$list_number&search_tweet_text=$search_tweet_text&search_tweet_category=$search_tweet_category&search_tweet_author=$search_tweet_author&search_tweet_tags=$search_tweet_tags", 'master');

$result = mysqli_query($conn, $SQL2) or die(mysqli_error());
//$row = mysqli_fetch_assoc($result);
?>
<a name="NewUserHelp" id="NewUserHelp"></a>
<div id="jukeboxHelp" style="display:none; padding:5px; border:solid 1px #666666; width:650px; text-align:left; margin:auto;">
	<h3><center>Jukebox Help</center><span style="cursor:pointer; float:right; padding-right:5px; color:#FF0000; font-weight:bold;" onclick="$('#jukeboxHelp').css('display', 'none');$('#jukeboxHelpButton').css('display', 'block');">X</span></h3>
	<? 
	if($_SESSION['new_user'] == '1') {
	?>
	<script>location.hash = "#NewUserHelp";</script>
	<h3>Greetings <? echo $_SESSION['screen_name']; ?>:</h3>
	<p>As a new user, we've provided you with a sample Jukebox of 100 really cool quotes that you can start using right away!</p>
	<p>If you want to jump right in, just activate your schedule by clicking the <img src="../images/button-power_basic_red.png" alt="on" /> button below.  We took the liberty of setting up your scheule to send a tweet every hour of every day.  
		Too often? Not often enough?  That's OK, all you need to do is edit the schedule and set a schedule that meets your needs.</p>
	<?
	} 
	?>			
	Below you will see a tool bar that is there to aid you in building up your jukebox.  
		<ul>
			<li>Click the <span class="member-icon">z</span> icon if you want to find stuff using certain keywords.</li>
			<li>Add a new tweet to your jukebox by clicing on the <span class="member-icon">@</span> icon.</li>
			<li>Add a bunch of new tweets by clicking on the <span class="member-icon">b</span> icon.</li>
			<li>Change the name of your jukebox and or edit your schedule with the <span class="member-icon">></span> icon.</li>
			<li>Anytime you want to pause your jukebox, just click the <img src="../images/button-power_basic_green.png" alt="on" /> icon.</li>
			<li>If you hate our sample jukebox you can delete it (we'd suggest leaving it paused - once deleted - it's gone for good.) with the <span class="member-icon">J</span> button.</li>
			
		</ul>  	
	<p>
	You can sort the list by clicking on a column heading.  Indicators will show you how the list is currently being sorted.  
	<!--IMPORTANT: We only tweet what is in the Tweet Text box. The other coulumns are just there for you to orginize your list. -->
	</p>	
</div>
<? 
if($_SESSION['new_user'] == '1') {
?>
<script>$('#jukeboxHelpButton').css('display', 'none');$('#jukeboxHelp').css('display', 'block');</script>	
<?	
} 
$_SESSION['new_user'] = '0'	
?>	
<input type="hidden" id="list_number" name="list_number" value="<? echo $list_number; ?>"  />
	<div style="margin:auto; cursor:pointer;" id="jukeboxHelpButton" class="button" onclick="$('#jukeboxHelpButton').css('display', 'none');$('#jukeboxHelp').css('display', 'block');">Help With My Jukebox</div>
<? if(count($_SESSION['REJECTS']) > 0) { 		
?>
	<br />
	<div class="ui-state-error" style="padding:5px;">OOPS! These tweets did not make it - they were rejected because they are too large.<br />
	<p align="center"><b>WARNING ====>  These will NOT be shown again!!!</b></p>
		<ol>
<?	
		foreach($_SESSION['REJECTS'] as $reject) {
			print "<li>".$reject['reject']."</li>";
		}
?>	
		</ol>
		<p align="center"><b>If you want to include these, you need to edit them so they fit within the Twitter&#8482; length guidelines.</b></p>
	</div>
<? 	
	unset($_SESSION['REJECTS']);
} 
?>	
<?
if(mysqli_num_rows($result)) {
	$row = mysqli_fetch_assoc($result); 
?>	
	<h2 align="center">
	<? 
		$SQL2 = "select list_name from user_schedule where user_id = '$user_id' and list_id = '$list_number'  ";
		$result2 = mysqli_query($conn, $SQL2) or die(mysqli_error());
		$row2 = mysqli_fetch_assoc($result2);	
		$list_name = stripslashes($row2['list_name']);
		if($list_name) {
			echo $list_name; 
		} else {	
			echo 'Jukebox # '.$list_number; 
		}	
	?>
	&nbsp;<?  echo "(".$tweetTotal. " records)"; ?></h2>
	<p align="center"><? echo $pagingLink; ?></p>
	<p align="center"  class="ui-state-highlight more-link" >Do not repeat a tweet for at least 
	  <input type="number" id="retweet_days" name="retweet_days" min="0" style="text-align:center;" max="30" value="<? echo $retweet_days; ?>"  > days. <input type="button" value="OK" onclick="setRetweetDays( $('#retweet_days').val(), <? echo $list_number; ?> ); savedRepeatDays();"  />	  
	  <br><strong>WARNING</strong>, setting this value too high may stop your tweets from playing depending on your inventory and frequency of tweeting.
	  <br /><span style="color:red;" id="savedRepeatDaysMsg">&nbsp;</span>	
	</p>
	<ul class="member-icons">	
		<li><a onClick="$( '#search-form' ).dialog( 'open' );"><span class="member-icon">z</span> Search&nbsp;&nbsp;&nbsp;</a></li>
		<? if($tweetCombinedTotal < $_SESSION['boxLimit']) { ?>
			<li>
			<a id="link0" href="/members/editMasterRow.php?rowID=0&list_number=<? echo $list_number; ?>&currentMasterPage=<? echo $currentPage; ?>"  onclick="return hs.htmlExpand(this,  { objectType: 'iframe', width: '600' , height: '600', preserveContent: 'true', align: 'center' } ); " >
			<span class="member-icon">@</span> Add New Tweet</a></li>				
			<li><a onClick="uploadTweets(<? echo $list_number; ?>);" ><span class="member-icon">b</span> Upload Tweets</a></li>
		<? } ?>	
		<? 
		if($schedule_status == 'needed') {
		?>
			<li><a onClick="setupSchedule(<? echo $list_number; ?>)"><span class="member-icon">></span> Setup Schedule / Name</a></li>
		    <?
		} else { 
		?>
			<li><a onClick="setupSchedule(<? echo $list_number; ?>)"><span class="member-icon">></span> Edit Schedule / Name</a></li>				
		    <? 
		} 
		?>								
	<? 
	if($schedule_status != 'needed') {
	?>		
	<?
		if($schedule_status == 1) {
	?>	
			<li><div id="schStatusOn"  style="display:block" onclick="$('#schStatusOn').css('display', 'none'); $('#schStatusOff').css('display', 'block'); switchSchedule('off', <? echo $list_number; ?>)"  ><a><img src="../images/button-power_basic_green.png" alt="on" /> Running</a></div></li>
			<li><div id="schStatusOff" style="display:none"  onclick="$('#schStatusOn').css('display', 'block'); $('#schStatusOff').css('display', 'none'); switchSchedule('on', <? echo $list_number; ?>)" ><a><img src="../images/button-power_basic_red.png" alt="off" /> Paused</a></div></li>
	<?	
		} else { 
	?>
			<li><div id="schStatusOn"  style="display:none"  onclick="$('#schStatusOn').css('display', 'none'); $('#schStatusOff').css('display', 'block'); switchSchedule('off', <? echo $list_number; ?>)" ><a><img src="../images/button-power_basic_green.png" alt="on" /> Running</a></div></li>
			<li><div id="schStatusOff" style="display:block" onclick="$('#schStatusOn').css('display', 'block'); $('#schStatusOff').css('display', 'none'); switchSchedule('on', <? echo $list_number; ?>)" ><a><img src="../images/button-power_basic_red.png" alt="off" /> Paused</a></div></li>
	<?	
		} 
	?>		
	<?	
	}
	?>
	<li><a onClick="if(confirm('All tweets and the schedule for this JB will be removed.\n\nThis action is not reversible.\n\nAre you sure you want to delete this Juke Box?') == true) { deleteJukeBox(<? echo $list_number; ?>); }"><span class="member-icon">J</span> Delete This Jukebox!</a></li>
	</ul>	
	<table id="tweetTable" border="" width="100%" style="visibility:visible;" >
	  <thead>
		<tr>
		  <th width="300px">
		  	<span style="cursor:pointer;" onclick="getNextMaster('?list_number=<? echo $list_number;?>&s=<? if($sortBy == 'order by tweet_text') echo 'tweet_text DESC'; else echo 'tweet_text' ;?>')">Tweet Text</span>
		  	<? if($sortBy == 'order by tweet_text') echo ' &uArr;'; ?>
			<? if($sortBy == 'order by tweet_text DESC') echo ' &dArr;'; ?>			
		  </th>
		  <th>Photo</th>
		  <!--th>
		  	<span style="cursor:pointer;" onclick="getNextMaster('?list_number=<? echo $list_number;?>&s=<? if($sortBy == 'order by tweet_category') echo 'tweet_category DESC'; else echo 'tweet_category' ;?>')">Category</span>
			<? if($sortBy == 'order by tweet_category') echo ' &uArr;'; ?>
			<? if($sortBy == 'order by tweet_category DESC') echo ' &dArr;'; ?>
		  </th>
		  <th nowrap="nowrap">
		  	<span style="cursor:pointer;" onclick="getNextMaster('?list_number=<? echo $list_number;?>&s=<? if($sortBy == 'order by tweet_author') echo 'tweet_author DESC'; else echo 'tweet_author' ;?>')">Author / Artist </span>
			<? if($sortBy == 'order by tweet_author') echo ' &uArr;'; ?>
			<? if($sortBy == 'order by tweet_author DESC') echo ' &dArr;'; ?>		  </th>
		  <th>Tags</th-->
		  <th>
		  	<span style="cursor:pointer;" onclick="getNextMaster('?list_number=<? echo $list_number;?>&s=<? if($sortBy == 'order by tweet_last_post_date, id DESC') echo 'tweet_last_post_date DESC'; else echo 'tweet_last_post_date, id DESC' ;?>')">Last Posted</span>
			<? if($sortBy == 'order by tweet_last_post_date, id DESC') echo ' &uArr;'; ?>
			<? if($sortBy == 'order by tweet_last_post_date DESC') echo ' &dArr;'; ?>
		  </th>
		  <th>Action
		  <span style="cursor:pointer; float:right;" onclick="getNextMaster('?list_number=<? echo $list_number;?>&s=<? echo 'tweet_text' ;?>')">Show All</span>
		  </th>
		</tr>
	  </thead>
	<tbody>		
<?		
	do {
?>  
    <tr id="<? echo $row['id']; ?>" valign="middle">
      <td>
	  	<span id="text_<? echo $row['id'];?>"><? echo stripslashes($row['tweet_text']);?></span>
		<a style="text-align:right; vertical-align:text-top;" id="link<? echo $row['id'];?>" href="/members/editMasterRow.php?rowID=<? echo $row['id']; ?>&list_number=<? echo $list_number; ?>&currentMasterPage=<? echo $currentPage; ?>" onclick="return hs.htmlExpand(this,  { objectType: 'iframe', width: '600' , height: '600', preserveContent: 'false', align: 'center' } )" tabindex="0" ><span class="member-icon">></span></a>
	  </td>
	  <td id="photoExists_<? echo $row['id'];?>" onclick="$( '#photoDialog<? echo $row['id']; ?>' ).dialog( 'open' );">
	  	<? if($row['photo']) { ?>				
				<img src="../images/camera-128.png" width="50" />
		<? } ?>
		&nbsp;
	  </td>
	  <div class="tweetPhotoClass" id="photoDialog<? echo $row['id'];?>" title="Photo">
		  <img id="currentTweetPhoto_<? echo $row['id']; ?>" src="<? echo stripslashes($row['photo']) ; ?>" style="max-width:450px;"  />
	  </div>	  	
      <!--td id="category_<? echo $row['id'];?>"><? echo stripslashes($row['tweet_category']);?></td>
      <td nowrap="nowrap" id="author_<? echo $row['id'];?>"><? echo stripslashes($row['tweet_author']);?></td>
      <td id="tags_<? echo $row['id'];?>"><? echo stripslashes($row['tweet_tags']);?></td-->
      <td id="tweetDate_<? echo $row['id'];?>"><? 		
		if($row['tweet_last_post_date'] == '0000-00-00 00:00:00') {
			echo "never"; 
		} else {
			echo date("m/d/Y h:i A", strtotime($row['tweet_last_post_date']));
			if($row['tweet_sent'] == '0') echo '<br />Tweet Was Not Posted';
		}			
		?>
      </td>
	  <td> 	  
		<a onClick="tweetFromJukebox('<? echo urlencode(stripslashes($row['tweet_text']));?>', '<? echo $row['id']; ?>');"><span class="member-icon">^</span> Tweet It Now!</a>
		<span style="float:right;">
		<a onclick="confirmDelete( '<? echo $row['id'];?>' ); ">Delete</a></span>
	  </td>
    </tr>
    <?		
	
	} while($row = mysqli_fetch_assoc($result));
?>    
  </tbody>
</table>
<p align="center"><? echo $pagingLink; ?></p>
<? }  else { ?>	
<h2 align="center">You don't seem to have any Tweets!</h2>
<ul class="member-icons">		
		<li><a id="link0" href="/members/editMasterRow.php?rowID=0&list_number=<? echo $list_number; ?>&currentMasterPage=<? echo $currentPage; ?>"  onclick="return hs.htmlExpand(this,  { objectType: 'iframe', width: '600' , height: '600', preserveContent: 'true', align: 'center' } ); " ><span class="member-icon">@</span> Add New Tweet</a></li>		
		<li><a onClick="uploadTweets(<? echo $list_number; ?>);" ><span class="member-icon">b</span> Upload Tweets</a></li>
		<li><a onClick="if(confirm('All tweets and the schedule for this JB will be removed.\n\nThis action is not reversible.\n\nAre you sure you want to delete this Juke Box?') == true) { deleteJukeBox(<? echo $list_number; ?>); }"><span class="member-icon">J</span> Delete This Jukebox!</a></li>
</ul>	
<? } ?>
<div id="search-form" title="Search My Jukebox">
  <form class="c-form" accept-charset="utf-8">
  <fieldset>
    <label for="search_tweet_text">Tweet Text</label>
    <input type="text" name="search_tweet_text" id="search_tweet_text" value=""  />
    <!--
	<label for="search_tweet_category">Category</label>
    <input type="text" name="search_tweet_category" id="search_tweet_category" value=""  />
    <label for="search_tweet_author">Author</label>
    <input type="text" name="search_tweet_author" id="search_tweet_author" value=""  />
	<label for="search_tweet_tags">Tags</label>
    <input type="text" name="search_tweet_tags" id="search_tweet_tags" value=""  />
	-->
  </fieldset>    
  </form>	
</div>

<script>
$(function() {
    $( ".tweetPhotoClass" ).dialog({
		autoOpen: false,
		width: 500
	});
	
});
</script>
<script>
$( "#search-form" ).dialog({
	autoOpen: false,
	height: 575,
	width: 600,
	modal: true,
	buttons: {
	"Search": function() {
	  var bValid = true;
			
	
	 //bValid = bValid && checkLength( tweet_text, "Tweet Text", 3, 140 );
	 //bValid = bValid && checkLength( tweet_category, "Category", 3, 200 );
	 //bValid = bValid && checkLength( tweet_author, "Author", 3, 200 );
	 //bValid = bValid && checkLength( tweet_tags, "Tags", 3, 200 );
	 // bValid = bValid && checkRegexp( name, /^[a-z]([0-9a-z_])+$/i, "Username may consist of a-z, 0-9, underscores, begin with a letter." );
	 // bValid = bValid && checkRegexp( email, /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "eg. ui@jquery.com" );
	 // bValid = bValid && checkRegexp( password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );
	
	  if ( bValid ) {
											
		$.ajax({
			type: "get",
			url: "/members/getMasterList.php",
			data: {
				'search_tweet_text': $('#search_tweet_text').val(),
			//	'search_tweet_category': $(search_tweet_category).val(),
			//	'search_tweet_author': $(search_tweet_author).val(),
				'list_number': $('#list_number').val()
			//	'search_tweet_tags': $(search_tweet_tags).val()					
			} ,
			dataType: "html"
		})
		.done(function( msg ) { 
			$('#masterDiv').html(msg);
		});
			
		$( this ).dialog( "close" );
	  }
	},
	Cancel: function() {
	  $( this ).dialog( "close" );
	},
	"Show All": function() {
	  $.ajax({
			type: "get",
			url: "/members/getMasterList.php",
			data: {
				'search_tweet_text': '',
			//	'search_tweet_category': $(search_tweet_category).val(),
			//	'search_tweet_author': $(search_tweet_author).val(),
				'list_number': $('#list_number').val()
			//	'search_tweet_tags': $(search_tweet_tags).val()					
			} ,
			dataType: "html"
		})
		.done(function( msg ) { 
			$('#masterDiv').html(msg);
		});
		
		$( this ).dialog( "close" );
	}
	},      
});
</script>