<?php

include($_SERVER['DOCUMENT_ROOT']."/include/config.php");
require_once $_SERVER['DOCUMENT_ROOT'].'/braintree-php-3.1.0/lib/Braintree.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/ikantam/Ikantam.php';

$user_id = $_SESSION['access_token']['user_id'];
if (!isset($_SESSION['access_token'])) {
    echo json_encode(['success' => false]);
}
if ($_SESSION['validated'] != 'validated') {
    echo json_encode(['success' => false]);
}

$paymentMethod = new PaymentMethod();

if ($_POST['action'] == 'remove') {
    $result = $paymentMethod->removePaymentMethod($_POST['token']);
    echo json_encode($result);
}

if ($_POST['action'] == 'updateExpirationDate') {
    $result = $paymentMethod->updateExpirationDate($_POST['token'], $_POST['expirationMonth'], $_POST['expirationYear']);
    echo json_encode($result);
}

if ($_POST['action'] == 'create') {
    $result = $paymentMethod->checkCustomerExisting($_POST['customerId']);
    if (!$result['success']) {
        $result = $paymentMethod->createCustomer($_POST['customerId'], $_POST['firstName'], $_POST['lstName'], $_POST['email']);
        if (!$result['success']) {
            echo json_encode($result);
            exit();
        }
    }
    $result = $paymentMethod->createPaymentMethod($_POST['customerId'], $_POST['nonce'], $_POST['btPlanId']);
    echo json_encode($result);
}

if ($_POST['action'] == 'updateSubscription') {
    $result = $paymentMethod->updateSubscription($_POST['btPlanId'], $_POST['token']);
    echo json_encode($result);
}