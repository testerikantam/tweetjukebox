<?
ini_set("auto_detect_line_endings", true);
include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); 
?>
<?
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');
$rejects = array();
?>
<?

// aws code
require_once("/home/yauhen/dev/tweetjunkebox/tweetjunkebox/aws/aws-autoloader.php");

use Aws\Common\Aws;
// Create the AWS service builder, providing the path to the config file
$aws = Aws::factory('/home/yauhen/dev/tweetjunkebox/tweetjunkebox/aws/config.php');
$s3Client = $aws->get('s3');



$user_id = $_SESSION['access_token']['user_id'];
$upload_list_number = $_POST['upload_list_number'];
// give file a random name	
$fileName = $user_id . ".txt"; 

$result = $s3Client->putObject(array(
	'Bucket'     => 'jukebox-member-images',
	'Key'        => $fileName,
	'SourceFile' => $_FILES["myFile"]["tmp_name"]				
));


$rows = 0;
$firstID = 0;
$lastID = 0; 
	
if (($handle = fopen('https://s3.amazonaws.com/jukebox-member-images/' . $fileName, "r"))) {
	while (($data = fgetcsv($handle, 1000, "\t"))) {
		if($rows != 0 ) {
			$tweet_text = mysqli_real_escape_string($conn, $data[0]);
							
			$string = normalizer_normalize( $tweet_text, Normalizer::FORM_C );
				
			
			$SQL = "select count(id) as totalTweets from tweets where user_id = '$user_id' ";
			$result = mysqli_query($conn, $SQL) or die(mysqli_error());
			$row = mysqli_fetch_assoc($result);
			$totalTweets = $row['totalTweets'];
			
			if($totalTweets >= $_SESSION['tweetLimit']) {						
				header("Location: /oops.php?err=maxJ");
				exit();
			} else {						
				if($tweet_text) {
					$SQL = "insert into tweets set user_id = '$user_id', list_id = '$upload_list_number', tweet_text = '$tweet_text', tweet_category = '', tweet_author = '', tweet_tags = '' ";							
					$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn)); 
					if($firstID == 0 ) $firstID = mysqli_insert_id($conn);
					$lastID = mysqli_insert_id($conn);
				}	
			}	
		}									
		$rows++;									 
	}			
	fclose($handle);
	// now check what we did and remove fat tweets				
	$SQL = "select tweet_text, id from tweets where id >= $firstID and id <= $lastID and list_id = '$upload_list_number' and user_id = '$user_id' ";
	$result = mysqli_query($conn, $SQL);
	$row = mysqli_fetch_assoc($result);
	do {
		$tweet_text = stripslashes($row['tweet_text']);
		$string = normalizer_normalize( $tweet_text, Normalizer::FORM_C );
		//if(mb_strlen($tweet_text, 'utf-8') > 138 ) {
		if(strlen($tweet_text) > 138 ) {
		
			$string = str_replace("\r", "", $string);	
			$string = str_replace("\n", "", $string);	
		}
		
		// crude test for URL's
		$tweetParts = explode(' ', $string) ; // put all the parts in an array
		$tweetLength = 0;
		foreach($tweetParts as $tweetPart) {
			$thisLength = strlen($tweetPart);
			if(substr( strtoupper($tweetPart), 0, 4) == 'WWW.' && strlen($tweetPart) > 22 ) {
				$thisLength = 22;
			}
			if(substr( strtoupper($tweetPart), 0, 7) == 'HTTP://' && strlen($tweetPart) > 22 ) {
				$thisLength = 22;
			}
			if(substr( strtoupper($tweetPart), 0, 8) == 'HTTPS://' && strlen($tweetPart) > 22 ) {
				$thisLength = 22;
			}
								
			if(strpos( strtoupper($tweetPart), 'WWW.') > 0 ) { 
				$thisLength = strpos(strtoupper($tweetPart), 'WWW.' );
				$urlPart = substr($tweetPart, $thisLength); 
				if(strlen($urlPart) > 22) { 
					$thisLength = $thisLength + 22;
				} else {
					$thisLength = $thisLength + strlen($urlPart);
				}									
			}
			if( strpos( strtoupper($tweetPart) , 'HTTP://' ) > 0 ) { 
				$thisLength = strpos(strtoupper($tweetPart), 'HTTP://' );
				$urlPart = substr($tweetPart, $thisLength); 
				if(strlen($urlPart) > 22) { 
					$thisLength = $thisLength + 22;
				} else {
					$thisLength = $thisLength + strlen($urlPart);
				}									
			}
			if( strpos( strtoupper($tweetPart), 'HTTPS://' ) > 0 ) { 
				$thisLength = strpos(strtoupper($tweetPart), 'HTTPS://' );
				$urlPart = substr($tweetPart, $thisLength); 
				if(strlen($urlPart) > 22) { 
					$thisLength = $thisLength + 22;
				} else {
					$thisLength = $thisLength + strlen($urlPart);
				}									
			}
			
				
			$tweetLength = $tweetLength + $thisLength;			
		}
		$tweetLength = $tweetLength + count($tweetParts);
						
		if($tweetLength > 140) {
					
			$SQL2 = "delete from tweets  where id = '". $row['id'] ."'";
			$result2 = mysqli_query($conn, $SQL2);
			$newReject = array('reject' => $string);
			array_push($rejects, $newReject);  
			$_SESSION['REJECTS'] = $rejects;				
		
		}
		
																				
	?>		
	<?					
		$r++;	
	} while ($row = mysqli_fetch_assoc($result));		
		
		
		
	
	$rows--;
	
	$deleteResult = $s3Client->deleteObject(array(
		'Bucket' => 'jukebox-member-images',
		'Key'    => $fileName 
	));	
	
	
	$_SESSION['LISTNUMBER'] = $upload_list_number;
	$_SESSION['NEWJBUPLOAD'] = $firstID;
	//$_SESSION['REJECTS'] = array();
	//$_SESSION['REJECTS'] = array($rejects);
?>
		<script>
		window.location.assign("/members/")
		</script>
<?
		
	//	header("Location: /members/");	
	} else {	
?>
		<script>
		window.location.assign("/members/")
		</script>
<?	

//	header("Location: /members/");	
}		
exit();	
?>