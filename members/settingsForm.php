<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?php require_once(MODEL_PATH.'/User.php');?>
<?
$user_id = $_SESSION['access_token']['user_id'];
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');
$utc = new DateTimeZone('UTC');
$dt = new DateTime('now', $utc);
$countries = array();
$timeZones = array();
foreach(DateTimeZone::listIdentifiers() as $tz) {
	$country_city = explode("/", $tz);
	if(in_array($country_city[0], $countries) || $country_city[0] == 'UTC' || $country_city[0] =='' ) { 
	} else {
		array_push($countries, $country_city[0]);
	}
	array_push($timeZones, $tz);
}
?>
<?
if($_POST['email'] ) {	
	$user_first_name = mysqli_real_escape_string($conn, $_POST['user_first_name']);
	$user_last_name = mysqli_real_escape_string($conn, $_POST['user_last_name']);		
	$email = mysqli_real_escape_string($conn, $_POST['email']);
	$user_timezone = mysqli_real_escape_string($conn, $_POST['userTimeZoneSelection']);
	$twitterHashtagOption = $_POST['twitterHashtagOption'];
	$SQL = "update users set user_first_name = '$user_first_name', user_last_name = '$user_last_name', email = '$email', user_timezone = '$user_timezone', twitterHashtagOption = '$twitterHashtagOption' ";
	$SQL .= "where user_id = '$user_id' ";
		
	$result = mysqli_query($conn, $SQL) or die(mysqli_error());
	echo "Your changes were saved";	
	$_SESSION['email'] = $email;
	exit();		
}
?>
<?
$SQL = "select * from users where user_id = '$user_id' ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$row = mysqli_fetch_assoc($result);	
$user_timezone = stripslashes($row['user_timezone']);
$currentTimeZone = explode("/", $user_timezone);
?>
  <!--	
  <div class="text-center">
    <h3>Account Settings</h3>	   
  </div>
  -->
  	
  <div class="row">
  	<div class="col-lg-6">
		<h3 align="center">My Information</h3>
		<form id="accountInfoForm" method="post" enctype="multipart/form-data" accept-charset="utf-8">
        <div class="form-group">
          <label>First Name </label>
            <input class="form-control" type="text" id="user_first_name" name="user_first_name" value="<? echo stripslashes($row['user_first_name']); ?>" />
        </div>
		<div class="form-group">
           <label for="user_last_name">Last Name </label>
			<input class="form-control" type="text" id="user_last_name" name="user_last_name" value="<? echo stripslashes($row['user_last_name']); ?>" />        
        </div>
		<div class="form-group">
          <label for="email">Email <span id="emailMessages" style="color:#FF0000; display:inline;"></span></label>
			 <input class="form-control" type="text" id="email" name="email" onChange="validateEmail(this.value);" value="<? echo stripslashes($row['email']); ?>">
        </div>
		<div class="form-group">
		  <label for="email2">Re-type Email <span id="email2Messages" style="display:inline;"></span></label>
			 <input class="form-control" type="text" id="email2" name="email2" onKeyUp="checkEmails();"  value="<? echo stripslashes($row['email']); ?>">			 			 
		</div>
		
		<h4 align="center">Time Zone Setting (optional)</h4>
		<div class="form-group">
			<select name="userCountrySelection" onChange="getCountryTimeZones(this.value)">
				<option value=""> Select Your Region</option>
				<?	
					foreach($countries as $country) { ?>
						<option value="<? echo $country;?>" <? if ($currentTimeZone[0] == $country) echo 'selected="selected"'; ?> ><? echo $country; ?> </option>
				<?		}
				?>	
			</select>
		</div>	
		<div class="form-group" id="userTimeZoneDiv">

		<?		
		$country = $currentTimeZone[0];		
		$timeZones = array();
		foreach(DateTimeZone::listIdentifiers() as $tz) {
			$country_city = explode("/", $tz);
			if($country == $country_city[0]) { 
				if(in_array($tz, $timeZones)) {
					
				} else {
					array_push($timeZones, $tz);
				}
			}	
		}
		?>
		<select name="userTimeZoneSelection" id="userTimeZoneSelection">
		<?
		if(count($currentTimeZone > 2)) {
			array_shift($currentTimeZone);				
			$userCurrentTimeZone = implode("/",$currentTimeZone);
		} else {
			$userCurrentTimeZone = $currentTimeZone[1];  die();
		}
		foreach($timeZones as $timeZone) {
			$country_city = explode("/",$timeZone);
			array_shift($country_city);
			$timeZoneDisplay = implode("/",$country_city);										
		?>	
			<option value="<? echo $timeZone; ?>" <? if ($userCurrentTimeZone == $timeZoneDisplay) echo 'selected="selected"'; ?>><? echo $timeZoneDisplay; ?></option>
		<?	
		}
		?>
		</select> 
		
			<!--select name="userTimeZoneSelection" >
				<option value="">Select Region First</option>
			</select-->
		</div>
		<? if($_SESSION['isAdmin']) { ?>
		<div class="form-group" id="juboxSettings" style="padding-bottom:20px;">
			<h4 align="center">SCHEDULED TWEETS Hashtag Options</h4>
			<div><strong>Example: I'm going on #Vacation this month. #DE #Europe</strong></div>
			<div class="form-group">			
			<select name="twitterHashtagOption" id="twitterHashtagOption" >
				<option value="0" <? if($row['twitterHashtagOption'] == 0) echo 'selected="selected"';?> >No Action (I'm going on #Vacation this month. #DE #Europe)</option>
				<option value="1" <? if($row['twitterHashtagOption'] == 1) echo 'selected="selected"';?>>Remove all '#' (I'm going on Vacation this month. DE Europe)</option>
				<option value="2" <? if($row['twitterHashtagOption'] == 2) echo 'selected="selected"';?>>Remove all '#' and all ending Hashtags (I'm going on Vacation this month.)</option>
				<option value="3" <? if($row['twitterHashtagOption'] == 3) echo 'selected="selected"';?>>Remove all Hashtags (I'm going on this month.)</option>				
			</select>
			</div>					
		</div>			
		<? } ?>
		<div class="form-group">
			 <button type="button" id="submitAccount" onClick="validateAccountInfoForm();" >Save My Account Information</button>
			 <input type="hidden" id="action" name="action" value="" />
			 <div style="padding-top:20px;"><span align="center" class="panel panel-success" id="settingsMessage" style="visibility:hidden; font-weight:bold; color:#009900; width:200px;">&nbsp;</span></div>
      	</div>
		</form>
	</div>
    <div class="col-lg-6 payment-methods-list">
        <h3 align="center">Payment methods</h3>
        <?php
        require_once $_SERVER['DOCUMENT_ROOT'].'/braintree-php-3.1.0/lib/Braintree.php';
        //  sandbox
        Braintree_Configuration::environment('sandbox');
        Braintree_Configuration::merchantId('zsbs7swsdwdzvmwg');
        Braintree_Configuration::publicKey('yb7qkdry9qfvzbn9');
        Braintree_Configuration::privateKey('36ae489f8f321abd38f0971f5f1ff2cf');
        // prodcution
        //Braintree_Configuration::environment('production');
        //Braintree_Configuration::merchantId('dxpnw5b448vwd9rp');
        //Braintree_Configuration::publicKey('mg2pmbnsn3g4cqnq');
        //Braintree_Configuration::privateKey('cf12d53af0f632d8fb98c91af4b86741');
        try {
            /** @var \Braintree_Customer $customer */
            $customer = Braintree_Customer::find($user_id);
            $activePaymentMethodToken = null;
            foreach ($customer->paymentMethods as $paymentMethod) {
                if (count($paymentMethod->subscriptions)) {
                    foreach ($paymentMethod->subscriptions as $subscription) {
                        if ($subscription->id == $row['bt_plan_id']) {
                            $activePaymentMethodToken = $paymentMethod->token;
                        }
                    }
                }
            }
            ?>
            <?php foreach ($customer->paymentMethods as $paymentMethod) : ?>
                <?php if ($paymentMethod instanceof Braintree_CreditCard) : ?>
                    <div class="credit-card-row">
                        <input type="hidden" name="token" value="<?php echo $paymentMethod->token; ?>">
                        <div class="row form-group">
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div>Card number: <?php echo $paymentMethod->maskedNumber; ?></div>
                                        <div>Expiration date: <span class="expiration-date"><?php echo $paymentMethod->expirationDate; ?></span></div>
                                    </div>
                                    <div class="col-md-4">
                                        <img src="<?php echo $paymentMethod->imageUrl; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <span class="btn btn-info subscription-active-button <?php echo $activePaymentMethodToken == $paymentMethod->token ? 'display' : 'hide'; ?>">Active</span>
                                <button type="button" class="btn btn-success pay-by-saved-payment-method subscription-use-button <?php echo $activePaymentMethodToken == $paymentMethod->token ? 'hide' : ''; ?>" value="<?php echo $paymentMethod->token; ?>">Use this card</button>
                            </div>
                        </div>
                        <div class="row expiration-fields button form-group">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-default toggle-expiration-fields">Change Expiration Date</button>
                            </div>
                        </div>
                        <div class="row expiration-fields hide form-group">
                            <div class="col-lg-2">
                                <input type="text" name="expirationMonth" class="form-control" placeholder="MM" maxlength="2">
                            </div>
                            <div class="col-lg-2">
                                <input type="text" name="expirationYear"  class="form-control" placeholder="YY" maxlength="2">
                            </div>
                            <div class="col-lg-1">
                                <button type="button" class="btn btn-success update-expiration-month">Save</button>
                            </div>
                            <div class="col-lg-2">
                                <button type="button" class="btn btn-default toggle-expiration-fields">Cancel</button>
                            </div>
                        </div>
                        <div class="row remove-payment-method <?php echo $activePaymentMethodToken == $paymentMethod->token ? 'hide' : ''; ?>">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-danger btn-xs remove-button" data-user_id="<?php echo $user_id; ?>" value="">Remove</button>
                            </div>
                        </div>
                        <br/>
                    </div>
                <?php endif; ?>
                <?php if ($paymentMethod instanceof Braintree_PayPalAccount) : ?>
                    <div class="credit-card-row">
                        <input type="hidden" name="token" value="<?php echo $paymentMethod->token; ?>">
                        <div class="row  form-group">
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div>Email: <?php echo $paymentMethod->email; ?></div>
                                    </div>
                                    <div class="col-md-4">
                                        <img src="<?php echo $paymentMethod->imageUrl; ?>" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <span class="btn btn-info subscription-active-button <?php echo $activePaymentMethodToken == $paymentMethod->token ? 'display' : 'hide'; ?>">Active</span>
                                <button type="button" class="btn btn-success pay-by-saved-payment-method subscription-use-button <?php echo $activePaymentMethodToken == $paymentMethod->token ? 'hide' : ''; ?>" value="<?php echo $paymentMethod->token; ?>">Use this account</button>
                            </div>
                        </div>
                        <div class="row remove-payment-method <?php echo $activePaymentMethodToken == $paymentMethod->token ? 'hide' : ''; ?>">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-danger btn-xs remove-button" data-user_id="<?php echo $user_id; ?>" value="">Remove</button>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
            <div class="row">
                <div class="col-md-12">
                    <h3 class="text-center">Add new payment method</h3>
                    <form id="add-payment">
                        <div id="payment-form"></div>
                        <button type="submit" class="btn btn-info">Add Payment Method</button>
                    </form>
                </div>
            </div>
        <?php } catch (Exception $e) {} ?>
    </div>
  </div>
<?php
    $user = new User();
    $user->getByUserId($user_id);
?>
<?php if ($user->hasPlan()):?>
    <div class="row col-sm-6">
        <h3 align="center">Social accounts</h3>
        <?php if ($fbAccount = $user->hasSocialAccount('facebook')):?><?php //var_dump($fbAccount['id']);die;?>
            <div class="col-sm-2">
                <img class="user_account_image" src="<?php echo $fbAccount['image'] ?>" alt="">
            </div>
            <div class="col-sm-4">
                <p>
                <h4 class="user_account_name pull-md-left"><?php echo $fbAccount['name'] ?></h4>
                </p>
                <p>
                    <a href="/members/removeFbAccount.php?account_id=<?php echo $fbAccount['id'];?>">
                    <i class="fa fa-remove"></i> Remove
                    </a>
                </p>
            </div>
        <?php else:?>
            <div class="col-sm-6 sm-right">
                <form action="/members/addFbAccount.php" method="post">
                    <input type="hidden" name="connect_social_type" value="facebook">
                    <button type="submit"
                        class="btn btn-facebook btn-social-connect"
                        >
                        Facebook connect
                    </button>
                </form>
            </div>
        <?php endif;?>
        <?php if ($lnAccount = $user->hasSocialAccount('linkedin')):?>
            <div class="col-sm-2">
                <img class="user_account_image" src="<?php echo $lnAccount['image'] ?>" alt="">
            </div>
            <div class="col-sm-4">
                <p>
                <h4 class="user_account_name pull-md-left"><?php echo $lnAccount['name'] ?></h4>
                </p>
                <p>
                    <a href="/members/removeLnAccount.php?id=<?php echo $lnAccount['id'];?>">
                    <i class="fa fa-remove"></i> Remove
                    </a>
                </p>
            </div>
            <?php else:?>
            <div class="col-sm-6 sm-right">
                <form action="/members/addLnAccount.php" method="post">
                    <input type="hidden" name="connect_social_type" value="linkedin">
                    <button type="submit"
                            class="btn btn-linkedin btn-social-connect"
                        >
                        Linkedin connect
                    </button>
                </form>
            </div>
        <?php endif;?>
    </div>
<?php endif;?>

<?
function formatOffset($offset) {
        $hours = $offset / 3600;
        $remainder = $offset % 3600;
        $sign = $hours > 0 ? '+' : '-';
        $hour = (int) abs($hours);
        $minutes = (int) abs($remainder / 60);

        if ($hour == 0 AND $minutes == 0) {
            $sign = ' ';
        }
        return $sign . str_pad($hour, 2, '0', STR_PAD_LEFT) .':'. str_pad($minutes,2, '0');

}

?>  	      
<script>
function checkEmails() {
	var email_1 = $('#email').val();
	var email_2 = $('#email2').val();
			
	if(email_1.length == email_2.length) {
		if(email_1 == email_2) {
			if(validateEmail(email_1) == true ) {
				$('#email2Messages').css('color', '#00CC00');
				$('#email2Messages').html("Agree!");	
			} else {
				$('#email2Messages').css('color', '#FF0000');
				$('#email2Messages').html("Your email addresses appear to be invalid!");
			}
		} else {
			$('#email2Messages').css('color', '#FF0000');
			$('#email2Messages').html("Your email addresses do not agree!");
		}
	}
}
  
function checkPasswords() {
	var pwd_1 = $('#pwd').val();
	var pwd_2 = $('#pwd2').val();
		
	if(pwd_1.length == pwd_2.length) {
		if(pwd_1 != pwd_2) {
			$('#pwd2Messages').css('color', '#FF0000');
			$('#pwd2Messages').html("Your passwords do not agree!");
		} else {
			$('#pwd2Messages').css('color', '#00CC00');
			$('#pwd2Messages').html("Agree!");	
		}
	} else {
		$('#pwd2Messages').css('color', '#FF0000');
		$('#pwd2Messages').html("Your passwords do not agree!");
	}
}
  
function validateEmail(sEmail) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {
		$('#emailMessages').html("");
		return true;
	}
	else {
		$('#emailMessages').html("Your email appears to be invalid.");
		return false;
	}
}
 
function checkStrength(pwd) {
	//initial strength
	var strength = 0	  	;		  
	//if the password length is less than 6, return message.
	if (pwd.length < 6) {
		$('#result').css('color', '#FF0000');
		$('#result').html('Too Short');
		return true;
	}
	
	//length is ok, lets continue.
	
	//if length is 6 characters or more, increase strength value
	if (pwd.length > 5) strength += 1
	
	//if password contains both lower and uppercase characters, increase strength value
	if (pwd.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))  strength += 1;
	
	//if it has numbers and characters, increase strength value
	if (pwd.match(/([a-zA-Z])/) && pwd.match(/([0-9])/))  strength += 1;
	
	//if it has one special character, increase strength value
	if (pwd.match(/([!,%,&,@,#,$,^,*,?,_,~])/))  strength += 1;
	
	//if it has two special characters, increase strength value
	if (pwd.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,",%,&,@,#,$,^,*,?,_,~])/)) strength += 1;
	
	//now we have calculated strength value, we can return messages
	
	//if value is less than 2
	if (strength < 2 ) {			
		$('#result').css('color', '#FFCC00') ;
		$('#result').html('Weak');
		return true;
	} else if (strength == 2 ) {			
		$('#result').css('color', '#00CC00') ;
		$('#result').html('Good');
		return true;
	} else {			
		$('#result').css('color', '#2D98F3') ;
		$('#result').html('Strong');
		return true;
	}
}

	
function validateAccountInfoForm() {	
	var errors = '';
	var email_1 = $('#email').val();
	if(validateEmail(email_1) == false ) errors = errors + "\nYour email appears to be missing or invalid";
	
	var firstName = $('#user_first_name').val();
	var lastName = $('#user_last_name').val();
	if(firstName.length < 1 ) errors = errors + "\nPlease enter your first name";
	if(lastName.length < 2 ) errors = errors + "\nPlease enter your last name";
	
	if($('#email').val() != $('#email2').val() ) errors = errors + "\nYour emails do not agree";
			
	if(errors != '') {
		alert(errors);
	} else {
		$('#action').val("form posted");
		saveAccountData();
	}	
}	

function saveAccountData() {
		$.ajax({
			type: "post",
			url: "/members/settingsForm.php",
			data: { 
				'user_first_name': $('#user_first_name').val(),
				'user_last_name': $('#user_last_name').val(),
				'email': $('#email').val(),
				'twitterHashtagOption': $('#twitterHashtagOption').val(),
				'userTimeZoneSelection': $('#userTimeZoneSelection').val()
			},
			dataType: "html"
		})
		.done(function( msg ) { 										
			$('#settingsMessage').css('visibility', 'visible');
			$('#settingsMessage').fadeTo(0, 3000);
			$('#settingsMessage').html(msg);
			$('#settingsMessage').fadeTo(3000, 0);
			setTimeout(function () {
				$('#masterDiv').html('<center><img src="/images/tweetjukebox3.png" class="img-responsive" style="padding-top:50px;" ></center>');	   
			}, 2000);
	   });
}

$('.payment-methods-list').on('click', '.toggle-expiration-fields', function () {
    $(this).closest('.credit-card-row').find('.expiration-fields').toggleClass('hide');
});

$('.payment-methods-list').on('click', '.remove-button', function () {
    var confirmResult = confirm('Remove this payment method?');
    if (!confirmResult) {
        return false;
    }
    var $removeButton = $(this);
    var $creditCardRow = $removeButton.closest('.credit-card-row');
    var userId = $removeButton.data('user_id');
    var token = $creditCardRow.find('[name="token"]').val();
    removeCreditCard(userId, token, $creditCardRow)
});

$('.payment-methods-list').on('click', '.update-expiration-month', function () {
    var $creditCardRow = $(this).closest('.credit-card-row');
    var token = $creditCardRow.find('[name="token"]').val();
    var expirationMonth = $creditCardRow.find('[name="expirationMonth"]').val();
    var expirationYear = $creditCardRow.find('[name="expirationYear"]').val();
    updateExpirationDate(token, expirationMonth, expirationYear, $creditCardRow);
});

$('.payment-methods-list').on('click', '.pay-by-saved-payment-method', function () {
    var confirmResult = confirm('Make this your active payment method?');
    if (!confirmResult) {
        return false;
    }
    var $payButton = $(this);
    var token = $payButton.val();
    $.ajax({
        url: '/members/payments.php',
        method: 'POST',
        dataType: 'json',
        data: {
            action: 'updateSubscription',
            token: token,
            btPlanId: '<?php echo $row['bt_plan_id']; ?>'
        },
        success: function (response) {
            if (response.success) {
                var $creditCardRow = $payButton.closest('.credit-card-row');
                $('.payment-methods-list').find('.subscription-active-button.display').removeClass('display').addClass('hide');
                $('.payment-methods-list').find('.subscription-use-button.hide').removeClass('hide');
                $('.payment-methods-list').find('.remove-payment-method').removeClass('hide');

                $creditCardRow.find('.subscription-active-button.hide').removeClass('hide').addClass('display');
                $creditCardRow.find('.subscription-use-button').addClass('hide');
                $creditCardRow.find('.remove-payment-method').addClass('hide');
                alert(response.message);
            } else {
                alert(response.error);
            }
        },
        error: function (error) {
            console.log('Error: ' + error);
        }
    });
});

function removeCreditCard(userId, token, $creditCardRow) {
    $.ajax({
        url: '/members/payments.php',
        method: 'POST',
        dataType: 'json',
        data: {
            action: 'remove',
            userId: userId,
            token: token
        },
        success: function (response) {
            if (response.success) {
                $creditCardRow.remove();
                alert(response.message);
            } else {
                alert(response.error);
            }
        },
        error: function (error) {
            console.log('Error: ' + error);
        }
    });
}

function updateExpirationDate(token, expirationMonth, expirationYear, $creditCardRow) {
    $.ajax({
        url: '/members/payments.php',
        method: 'POST',
        dataType: 'json',
        data: {
            action: 'updateExpirationDate',
            token: token,
            expirationMonth: expirationMonth,
            expirationYear: expirationYear
        },
        success: function (response) {
            if (response.success) {
                $creditCardRow.find('span.expiration-date').html(response.expirationDate);
                $creditCardRow.find('.expiration-fields').toggleClass('hide');
                alert(response.message);
            } else {
                alert(response.error);
            }
        },
        error: function (error) {
            console.log('Error: ' + error);
        }
    });
}

</script>
<script>
function getCountryTimeZones(c) {
	$.ajax({
  		type: "get",
  		url: "/members/getCountryTimeZones.php?c="+c,
		//data: {'message': msg} ,
		dataType: "html"
	})
  	.done(function( msg ) { 
		$("#userTimeZoneDiv").html(msg);	
  	});
		
}
</script>

<script>
braintree.setup("<?php echo $clientToken = Braintree_ClientToken::generate([]); ?>", "dropin", {
    container: "payment-form",
    onPaymentMethodReceived: function (obj) {
        $.ajax({
            url: '/members/payments.php',
            method: 'POST',
            dataType: 'json',
            data: {
                action: 'create',
                customerId: <?php echo $user_id; ?>,
                firstName: '<?php echo $row['first_name']; ?>',
                lastName: '<?php echo $row['last_name']; ?>',
                email: '<?php echo $row['email']; ?>',
                nonce: obj.nonce,
                btPlanId: '<?php echo $row['bt_plan_id']; ?>'
            },
            success: function (response) {
                if (response.success) {
                    $('.payment-methods-list').html('<h3 align="center">Wait for update ...</h3>');
                    settingsForm();
                } else {
                    alert(response.error);
                    $('.payment-methods-list').html('<h3 align="center">Wait for update ...</h3>');
                    settingsForm();
                }
            },
            error: function (error) {
                console.log('Error: ' + error);
            }
        });
    }
});
/*$('.btn-social-connect').on('click', function() {
    var socialType = $(this).data('type');
    $.ajax({
        url:'/members/settings.php',
        method: 'POST',
        dataType: 'html',
        data: {social_type: socialType},
        success: function(data){

        },
        error: function (error) {
            console.log('Error: ' + error);
        }
    })
});*/
</script>