<?php 
ini_set('max_execution_time', 0);

include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); 
?>
<script src="//code.jquery.com/jquery-1.9.1.js"></script>
<script>window.jQuery || document.write('<script src="../assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
<style type="text/css">
	.chart-container {
		position: relative;
		height: 400px;
	}

	#placeholder {
		width: 750px;
	}

	</style>
	<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="excanvas.min.js"></script><![endif]-->
	<script language="javascript" type="text/javascript" src="jquery.flot.js"></script>
	<script language="javascript" type="text/javascript" src="jquery.flot.categories.js"></script>
<?

if(!isset($_SESSION['access_token'])) die();
?>
<?

$user_id = $_SESSION['access_token']['user_id'];

if($_REQUEST['range'] == 'Today') {
	$firstDay = date("Y-m-d");
	$today = date("Y-m-d", strtotime("tomorrow"));
	$days[date("Y-m-d")]['count'] = 0  ;
}

if($_REQUEST['range'] == 'Last 7 Days') {
	$today = date("Y-m-d");
	$firstDay = date("Y-m-d", strtotime("last Week"));	
	$days = array();	
	$days[date("Y-m-d", strtotime(" -7 days"))]['count'] = 0  ;
	$days[date("Y-m-d", strtotime(" -6 days"))]['count'] = 0  ;
	$days[date("Y-m-d", strtotime(" -5 days"))]['count'] = 0  ;
	$days[date("Y-m-d", strtotime(" -4 days"))]['count'] = 0  ; 
	$days[date("Y-m-d", strtotime(" -3 days"))]['count'] = 0  ;
	$days[date("Y-m-d", strtotime(" -2 days"))]['count'] = 0  ;
	$days[date("Y-m-d", strtotime(" -1 days"))]['count'] = 0  ;
}

if($_REQUEST['range'] == 'Last 30 Days') {
	$today = date("Y-m-d");	
	$days = array();	
	$firstDay = date("Y-m-d", strtotime("-30 days"));
	for($d = 30; $d >=1; $d--) {
		$days[date("Y-m-d", strtotime("-".$d." days"))]['count'] = 0  ;		
	}	
}

if($_REQUEST['range'] == 'Last Month') {
	$today = date("Y-m-01");
	$days = array();	
	$firstDay = date("Y-m-01", strtotime('last month'));	
	$daysInMonth = date("d", strtotime('last day of last month'));
			
	for ($d = $daysInMonth; $d >= 1 ; $d-- ) {						
		$day = date("Y-m-", strtotime('last month'));
		$day.= str_pad((int) $d,2,"0",STR_PAD_LEFT);
		$days[$day]['count'] = 0;
	}
}		


if($_REQUEST['range'] == 'This Month To Date') {
	$today = date("Y-m-d");
	$firstDay = date("Y-m") . "-01" ;
	$days = array();	
	for ($d = date("d"); $d >= 1 ; $d-- ) {
		
		if(date("m", strtotime("-".$d." days")) == date("m") ) {
			$days[date("Y-m-d", strtotime("-".$d." days"))]['count'] = 0  ;
		}
	}		
}			

if($_REQUEST['type'] == 'users') {
	$users_screen_name = array();
	$users_profile_image_url = array();
	$users_followers_count = array();
	$users_friends_count = array();
	$top_users = array();
	$top_users_by_reach = array();
}

/*
$SQL = "select mention_data, mention_date from user_mentions where user_id = '$user_id' and mention_date >= '$firstDay' and mention_date < '$today' order by mention_date ";
$result = mysqli_query($conn, $SQL, MYSQLI_USE_RESULT ) or die($SQL);
if($result) {
	$row = mysqli_fetch_assoc($result);
	do {
		//$mention_data = stripslashes($row['mention_data']);
		$str = $row['mention_data'];
		$str = utf8_encode($str); 
	 	$mention_data = json_decode($str, true); 

		//print "<pre>";
		//print_r($mention_data); 					
		//die();
		
		if($_REQUEST['type'] == 'users') {
			if(!in_array($mention_data['user']['screen_name'], $users_screen_name)) {
				array_push($users_screen_name, $mention_data['user']['screen_name']);
				array_push($users_profile_image_url, $mention_data['user']['profile_image_url']);
				array_push($users_followers_count, $mention_data['user']['followers_count']);
				array_push($users_friends_count, $mention_data['user']['friends_count']);
				$this_user = $mention_data['user']['screen_name'];
				$top_users[$this_user]['count'] ++;			
				$top_users_by_reach[$this_user]['reach'] = $mention_data['user']['followers_count'];	
			} else {
				$this_user = $mention_data['user']['screen_name'];
				$top_users[$this_user]['count'] ++;	
				$top_users_by_reach[$this_user]['reach'] = 	$mention_data['user']['followers_count'] * 	$top_users[$this_user]['count'];
			}
		} else {	
			$days[substr($row['mention_date'], 0, 10)]['count'] ++;
		}
		
		
	}while(	$row = mysqli_fetch_assoc($result));
	mysqli_free_result($result);
	mysqli_close($conn);	
*/
$SQL = "select * from user_mentions where user_id = '$user_id' and mention_date >= '$firstDay' and mention_date < '$today' order by mention_date ";
$result = mysqli_query($conn, $SQL, MYSQLI_USE_RESULT ) or die($SQL);
if($result) {
	$row = mysqli_fetch_assoc($result);
	// if(mysqli_num_rows($result)) do {	
	if($row['mention_screen_name']) do {
		$mention_screen_name = stripslashes($row['mention_screen_name']);
		$profile_image_url = stripslashes($row['profile_image_url']);
		$followers_count = stripslashes($row['followers_count']);
		$friends_count = stripslashes($row['friends_count']);
		
		if($_REQUEST['type'] == 'users') {
			if(!in_array($mention_screen_name, $users_screen_name)) {
				array_push($users_screen_name, $mention_screen_name);
				
				array_push($users_profile_image_url, $profile_image_url);
				array_push($users_followers_count, $followers_count);
				array_push($users_friends_count, $friends_count);
				$this_user = $mention_screen_name;
				$top_users[$this_user]['count'] ++;			
				$top_users_by_reach[$this_user]['reach'] = $followers_count;	
			} else {
				$this_user = $mention_screen_name;
				$top_users[$this_user]['count'] ++;	
				$top_users_by_reach[$this_user]['reach'] = 	$followers_count * 	$top_users[$this_user]['count'];
			}
		} else {	
			$days[substr($row['mention_date'], 0, 10)]['count'] ++;
		}
		
		
	}while(	$row = mysqli_fetch_assoc($result));
	mysqli_free_result($result);
	mysqli_close($conn);			
?>			
<? 
if($_REQUEST['type'] == 'users') { 
?>
	<div id="content">
		<span style="float:right; cursor:pointer;">
			<img src="../images/users.png" title="Unique Users"  />
			<select style="display:inline-table; cue:url(../images/bar-chart.png)" onchange="if(this.value != '0' ) recentMentionsUniqueUsers(this.value)">
				<option value="0">Select Time</option>
				<option value="Today">Today</option>
				<option value="Last 7 Days">Last 7 Days</option>
				<option value="Last 30 Days">Last 30 Days</option>
				<option value="This Month To Date">This Month</option>
				<option value="Last Month">Last Month</option>		
			</select>
			<img src="../images/spacer.gif" width="10px" />
			<img src="../images/bar-chart.png" title="Mentions By Day"  />
			<select style="display:inline-table; cue:url(../images/bar-chart.png)" onchange="if(this.value != '0' ) recentMentionsCharted(this.value)">
				<option value="0">Select Time</option>
				<option value="Today">Today</option>
				<option value="Last 7 Days">Last 7 Days</option>
				<option value="Last 30 Days">Last 30 Days</option>
				<option value="This Month To Date">This Month</option>
				<option value="Last Month">Last Month</option>		
			</select>
		</span>	
		<h2><? echo count($users_screen_name); ?> Unique Users - <? echo $_REQUEST['range']; ?></h2>
		<span id="clickdata"></span>
		<? 		
		$top_50_users = array();
		$top_50_users = array_msort($top_users, array('count'=>SORT_DESC));
		$top_50_users_by_reach = array();
		$top_50_users_by_reach = array_msort($top_users_by_reach, array('reach'=>SORT_DESC));
		
	//	print "<pre>";
	//	print_r($top_50_users_by_reach);
	//	die();
		$col_1 = array();
		$col_2 = array();
		$col_3 = array();
		$col_4 = array();
		
		?>
		<h3>Top 50</h3>
		<table width="200">
			<tr>
				<th>&nbsp;</th>
				<th align="left" >By Frequency</th>
				<th>&nbsp;</th>
				<th align="left" >By Reach</th>
			</tr>	
		<?
			$user_count = 0;
			foreach($top_50_users as $key=>$value) {
				array_push($col_1, $key);
				array_push($col_2, $top_50_users[$key]['count']);
				$user_count ++;
			 	if($user_count == 50) break;		
			}
			$user_count = 0;
			foreach($top_50_users_by_reach as $key=>$value) {
				array_push($col_3, $key);
				array_push($col_4, $top_50_users_by_reach[$key]['reach']);
				$user_count ++;
			 	if($user_count == 50) break;		
			}
			$user_count = 0;
			do {
		?>
			<tr>
				<td style="width:200px; padding-left:10px; text-align:left"><a href="http://twitter.com/<? echo $col_1[$user_count]; ?>" target="_blank" ><? echo $col_1[$user_count]; ?></a></td>
				<td style="text-align:left; padding-left:15px;">(<? echo $col_2[$user_count]; ?>)</td>
				<td style="width:200px; padding-left:10px; text-align:left"><a href="http://twitter.com/<? echo $col_3[$user_count]; ?>" target="_blank" ><? echo $col_3[$user_count]; ?></a></td>
				<td style="text-align:left; padding-left:15px;">(<? echo $col_4[$user_count]; ?>)</td>
			</tr>
		<?
			 	$user_count ++;					 
			} while($user_count < 50);
		?>
	  </table>
	</div>
	<!--table id="mentionsTable" border="" width="100%" >
	  <tbody>
	<?
		$userCount = 0;
		foreach($users_screen_name as $screenName) {
	?>
			  <tr>						
				<td nowrap="nowrap">								
					<img src="<? echo $users_profile_image_url[$userCount];?>" alt="image description" width="44" height="44" />										
					<a href="http://twitter.com/<? echo $screenName; ?>" target="_blank" ><? echo $screenName; ?></a>
				</td>
				<td>					
					<p>Has <? echo $users_followers_count[$userCount]. ' Followers'; ?>&nbsp;&nbsp;And Follows: <? echo $users_friends_count[$userCount]; ?> others.</p>
				</td>
				<td>???</td>
			  </tr>	
	<?
			$userCount++;
		}
	?>	
	  </tbody>
	  </table-->	

<?
	exit();
} else {
$dataVar = '';
$counter = 1;
foreach($days as $key => $val ) { 
	if(count($days) > 10 ) { 	
		//$dataVar .= '[ "'.$counter.'",'.$val['count'].' ],';
		$mon = date('M', strtotime($key));
		if($lastMon != $mon) {
			$dataVar .= '[ "'.date('M', strtotime($key)).'<br>'.date('d', strtotime($key)).'",'.$val['count'].' ],';
			$lastMon = $mon;
		} else {
			$dataVar .= '[ "'.'<br>'.date('d', strtotime($key)).'",'.$val['count'].' ],';			
		}	
	} else {
		$dataVar .= '[ "'.date('m/d/Y', strtotime($key)).'<br>'.$val['count'].'",'.$val['count'].' ],';
	}	
	$counter ++;
	//$dataVar .= '{ label: "'.date('m/d/Y', strtotime($key)).'", data: '.$val['count'].' },';
} 
$dataVar = substr($dataVar, 0, strlen($dataVar)-1);



?>	

<script type="text/javascript">

$(function() {
		var data = [<? echo $dataVar; ?>];
		$.plot("#placeholder", [ data ], {
			series: {
				bars: {
					show: true,
					barWidth: 0.6,
					align: "center"
				}
			},
			xaxis: {
				mode: "categories",
				tickLength: 0
			},
			grid: {
				clickable: true
			}
		});		
		
		$("#placeholder").bind("plotclick", function (event, pos, item) {
			if (item) {
				$("#clickdata").html(item.datapoint[1]);
				//alert(item.datapoint[1]);						
			} else {
				$("#clickdata").html('');
			}
		});
						
});

</script>	

<div id="content">
<span style="float:right; cursor:pointer;">
	<img src="../images/users.png" title="Unique Users"  />
	<select style="display:inline-table; cue:url(../images/bar-chart.png)" onchange="if(this.value != '0' ) recentMentionsUniqueUsers(this.value)">
		<option value="0">Select Time</option>
		<option value="Today">Today</option>
		<option value="Last 7 Days">Last 7 Days</option>
		<option value="Last 30 Days">Last 30 Days</option>
		<option value="This Month To Date">This Month</option>
		<option value="Last Month">Last Month</option>		
	</select>
	<img src="../images/spacer.gif" width="10px" />
	<img src="../images/bar-chart.png" title="Mentions By Day"  />
	<select style="display:inline-table; cue:url(../images/bar-chart.png)" onchange="if(this.value != '0' ) recentMentionsCharted(this.value)">
		<option value="0">Select Time</option>
		<option value="Today">Today</option>
		<option value="Last 7 Days">Last 7 Days</option>
		<option value="Last 30 Days">Last 30 Days</option>
		<option value="This Month To Date">This Month</option>
		<option value="Last Month">Last Month</option>		
	</select>
</span>	
<h2>Mentions - <? echo $_REQUEST['range']; ?></h2>
	<div class="chart-container">
		<span id="clickdata" style="float:right; margin-top:-20px;">&nbsp;</span>
		<div id="placeholder" class="chart-placeholder"></div>
	</div>
</div>
<?	
}
	exit();
} else {
?>
	<h2 align="center">Sorry, <? echo $_SESSION['screen_name'];?>, you don't seem to have any mentions</h2>
<?	
	exit();
}
?>
<?php
function array_msort($array, $cols)
{
    $colarr = array();
    foreach ($cols as $col => $order) {
        $colarr[$col] = array();
        foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
    }
    $params = array();
    foreach ($cols as $col => $order) {
        $params[] =& $colarr[$col];
        $params = array_merge($params, (array)$order);
    }
    call_user_func_array('array_multisort', $params);
    $ret = array();
    $keys = array();
    $first = true;
    foreach ($colarr as $col => $arr) {
        foreach ($arr as $k => $v) {
            if ($first) { $keys[$k] = substr($k,1); }
            $k = $keys[$k];
            if (!isset($ret[$k])) $ret[$k] = $array[$k];
            $ret[$k][$col] = $array[$k][$col];
        }
        $first = false;
    }
    return $ret;

}
?>