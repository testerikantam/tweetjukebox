﻿<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
ini_set("auto_detect_line_endings", true);
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/twitteroauth/twitteroauth.php");
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/config.php");
?>
<?
$_SESSION['plans'] = array('Free','Advanced','Pro','Business');

if(isset($_SESSION['text-collection'])) {
	header("Location: /members/addContent.php");
	exit();
}
if(isset($_SESSION['image-collection'])) {
	header("Location: /members/addContent.php");
	exit();
}
if(isset($_SESSION['url-collection'])) {
	header("Location: /members/addContent.php");
	exit();
}

if($_SESSION['validated'] != 'validated') {
	header('Location: /');
	exit();
}	

$access_token = $_SESSION['access_token'];
$screen_name = $_SESSION['access_token']['screen_name'];
$user_id = $_SESSION['access_token']['user_id'];

$SQL = "select * from users where user_id = '$user_id'  ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$row = mysqli_fetch_assoc($result);	

$now = strtotime(date("Y-m-d h:i:s"));
$signupDate = strtotime($row['signupDate']);
$duration = $now - $signupDate;

$_SESSION['MEMBERSHIPDAYS'] = floor($duration / 60 / 60 / 24) ;

if($_SESSION['MEMBERSHIPDAYS'] > 14 && $row['send_thanks'] == 0 && $row['planName'] == 'Free'  ) {
	$SQL = "update users set send_thanks = 1, thanks_limit = 50 where user_id = '$user_id'  ";
	//$result = mysqli_query($conn, $SQL) or die(mysqli_error());	
}


if($row['status'] == '2') {
	header("Location: /oops.php?err=cancelled");
	exit();

}

$_SESSION['OFFER_SEEN'] = $row['offer_seen'];
$_SESSION['thanksLimit'] = $row['thanks_limit'];

$_SESSION['master_account_id'] = $row['master_account_id'];

$_SESSION['user_first_name'] = stripslashes($row['user_first_name']);
$_SESSION['user_last_name'] = stripslashes($row['user_last_name']);
$_SESSION['email'] = stripslashes($row['email']);

if($row['status'] == 0 ) {
	header("Location: /oops.php?err=expired");
	exit();
}	

$_SESSION['planName'] = $row['planName'];
$_SESSION['isAdmin'] = $row['isAdmin'];
$_SESSION['isSpecial'] = $row['isSpecial'];


$SQL2 = "select isAdmin from users where user_id = '".$_SESSION['master_account_id']."' ";  
$result2 = mysqli_query($conn, $SQL2) or die(mysqli_error());
$row2 = mysqli_fetch_assoc($result2);
if($row2['isAdmin'] == 1) $_SESSION['isAdmin'] = 1;



$all_tweet_status = $row['all_tweet_status'];
$user_id = $row['user_id'];
$send_thanks = $row['send_thanks'];

if($row['planName'] == 'Gold') { 
	$_SESSION['isGrandfathered'] = '1'; 
} else {
	$_SESSION['isGrandfathered'] = '0';
	if($_SESSION['planName'] != 'Free') {
		$plan = explode("-", $_SESSION['planName']);
		$_SESSION['planName'] = $plan[0];
		$_SESSION['billingCycle'] = $plan[1];
	} 	
}

if( $_SESSION['planName'] == 'Free' || $row['planName'] == '' || $row['planName'] == 'Gold') {
	$_SESSION['planName'] = 'Free';
	$_SESSION['boxLimit'] = FREE_VERSION_JB_BOX_LIMIT;
	$_SESSION['tweetLimit'] = FREE_VERSION_JB_TWEET_LIMIT;
	$_SESSION['scheduledLimit'] = FREE_VERSION_SCHEDULED_TWEET_LIMIT;
	$_SESSION['accountsLimit'] = FREE_VERSION_ACCOUNTS_LIMIT;
	$_SESSION['thankLimit'] = FREE_VERSION_THANK_YOU_TWEETS;
}	
if($_SESSION['planName'] == 'Advanced') {
	$_SESSION['boxLimit'] = ADVANCED_VERSION_JB_BOX_LIMIT;
	$_SESSION['tweetLimit'] = ADVANCED_VERSION_JB_TWEET_LIMIT;
	$_SESSION['scheduledLimit'] = ADVANCED_VERSION_SCHEDULED_TWEET_LIMIT;
	$_SESSION['accountsLimit'] = ADVANCED_VERSION_ACCOUNTS_LIMIT;
	$_SESSION['thankLimit'] = ADVANCED_VERSION_THANK_YOU_TWEETS;
}	
if( $_SESSION['planName'] == 'Pro') {
	$_SESSION['boxLimit'] = PRO_VERSION_JB_BOX_LIMIT;
	$_SESSION['tweetLimit'] = PRO_VERSION_JB_TWEET_LIMIT;
	$_SESSION['scheduledLimit'] = PRO_VERSION_SCHEDULED_TWEET_LIMIT;
	$_SESSION['accountsLimit'] = PRO_VERSION_ACCOUNTS_LIMIT;
	$_SESSION['thankLimit'] = PRO_VERSION_THANK_YOU_TWEETS;
}	
if( $_SESSION['planName'] == 'Business') {
	$_SESSION['boxLimit'] = BUSINESS_VERSION_JB_BOX_LIMIT;
	$_SESSION['tweetLimit'] = BUSINESS_VERSION_JB_TWEET_LIMIT;
	$_SESSION['scheduledLimit'] = BUSINESS_VERSION_SCHEDULED_TWEET_LIMIT;
	$_SESSION['accountsLimit'] = BUSINESS_VERSION_ACCOUNTS_LIMIT;
	$_SESSION['thankLimit'] = BUSINESS_VERSION_THANK_YOU_TWEETS;
}	

if( $_SESSION['isAdmin']) {
	$_SESSION['boxLimit'] = 999999;
	$_SESSION['tweetLimit'] = 999999;
	$_SESSION['scheduledLimit'] = 999999;
	$_SESSION['accountsLimit'] = 999999;
	//$_SESSION['thankLimit'] = 999999;
}

/*
if($screen_name == 'booksfornooks') {
//	echo CONSUMER_KEY;
//	die();
}
*/

$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
$content = $connection->get('users/show', array('screen_name' => $screen_name));
$_SESSION['name'] = $content->name;

$SQL = "select id from user_schedule where user_id = '$user_id' ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$_SESSION['boxCount'] = mysqli_num_rows($result);


$SQL = "select id from tweets where user_id = '$user_id' ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$_SESSION['tweetCount'] = mysqli_num_rows($result);

$SQL = "select id from scheduled_tweets where user_id = '$user_id' ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$_SESSION['scheduledCount'] = mysqli_num_rows($result);


$SQL = "select id from users where master_account_id = '".$_SESSION['master_account_id']."' and master_account_id != ''"; 
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$_SESSION['linkedAccounts'] = mysqli_num_rows($result);

if($_SESSION['linkedAccounts'] == 0 ) $_SESSION['linkedAccounts'] = 1 ; 

//                           W H A T   P L A N   D O E S   T H E   U S E R   F I T ????	
$planFit = ''; // this is what plan the user should be in based on their useage
if(	
$_SESSION['boxCount'] <= FREE_VERSION_JB_BOX_LIMIT && 
$_SESSION['tweetCount'] <= FREE_VERSION_JB_TWEET_LIMIT && 
$_SESSION['scheduledCount'] <= FREE_VERSION_SCHEDULED_TWEET_LIMIT && 
$_SESSION['thanksLimit'] <= FREE_VERSION_THANK_YOU_TWEETS
// note need to check for linked accounts - somehow
) $planFit = 'Free';
if(	$planFit == '' && 
$_SESSION['boxCount'] <= ADVANCED_VERSION_JB_BOX_LIMIT && 
$_SESSION['tweetCount'] <= ADVANCED_VERSION_JB_TWEET_LIMIT && 
$_SESSION['scheduledCount'] <= ADVANCED_VERSION_SCHEDULED_TWEET_LIMIT && 
$_SESSION['thanksLimit'] <= ADVANCED_VERSION_THANK_YOU_TWEETS
// note need to check for linked accounts - somehow
) $planFit = 'Advanced';


if(	$planFit == '' && 
$_SESSION['boxCount'] <= PRO_VERSION_JB_BOX_LIMIT && 
$_SESSION['tweetCount'] <= PRO_VERSION_JB_TWEET_LIMIT && 
$_SESSION['scheduledCount'] <= PRO_VERSION_SCHEDULED_TWEET_LIMIT && 
$_SESSION['thanksLimit'] <= PRO_VERSION_THANK_YOU_TWEETS
// note need to check for linked accounts - somehow
) $planFit = 'Pro';
if($planFit == '') $planFit = 'Business'; 
$_SESSION['planFit'] = $planFit;

if($planFit != $row['planFit'] ) {
	$setPlan = $planFit."_Monthly"; // set default for use on the expiration of time to sign up
	$SQL = "update users set planFit = '$setPlan' where user_id = '$user_id'  ";
	$result = mysqli_query($conn, $SQL) or die(mysqli_error());	
}

// but wait if this account is a sub - we need the master plan fit here
if($user_id != $_SESSION['master_account_id'] && $_SESSION['master_account_id'] != '' ) {
	
	$SQL = "select id from user_schedule where user_id = '". $_SESSION['master_account_id']  ."' ";
	$result = mysqli_query($conn, $SQL) or die(mysqli_error());
	$_SESSION['boxCount'] = mysqli_num_rows($result);
	
	
	$SQL = "select id from tweets where user_id = '". $_SESSION['master_account_id']  ."' ";
	$result = mysqli_query($conn, $SQL) or die(mysqli_error());
	$_SESSION['tweetCount'] = mysqli_num_rows($result);
	
	$SQL = "select id from scheduled_tweets where user_id = '". $_SESSION['master_account_id']  ."' ";
	$result = mysqli_query($conn, $SQL) or die(mysqli_error());
	$_SESSION['scheduledCount'] = mysqli_num_rows($result);
		
	$SQL = "select id from users where master_account_id = '".$_SESSION['master_account_id']."' and master_account_id != ''"; 
	$result = mysqli_query($conn, $SQL) or die(mysqli_error());
	$_SESSION['linkedAccounts'] = mysqli_num_rows($result);
			
	if($_SESSION['linkedAccounts'] == 0 ) $_SESSION['linkedAccounts'] = 1 ; 

	$planFit = ''; // this is what plan the user should be in based on their useage
	if(	
	$_SESSION['boxCount'] <= FREE_VERSION_JB_BOX_LIMIT && 
	$_SESSION['tweetCount'] <= FREE_VERSION_JB_TWEET_LIMIT && 
	$_SESSION['scheduledCount'] <= FREE_VERSION_SCHEDULED_TWEET_LIMIT && 
	$_SESSION['thanksLimit'] <= FREE_VERSION_THANK_YOU_TWEETS
	// note need to check for linked accounts - somehow
	) $planFit = 'Free';
	if(	$planFit == '' && 
	$_SESSION['boxCount'] <= ADVANCED_VERSION_JB_BOX_LIMIT && 
	$_SESSION['tweetCount'] <= ADVANCED_VERSION_JB_TWEET_LIMIT && 
	$_SESSION['scheduledCount'] <= ADVANCED_VERSION_SCHEDULED_TWEET_LIMIT && 
	$_SESSION['thanksLimit'] <= ADVANCED_VERSION_THANK_YOU_TWEETS
	// note need to check for linked accounts - somehow
	) $planFit = 'Advanced';
	if(	$planFit == '' && 
	$_SESSION['boxCount'] <= PRO_VERSION_JB_BOX_LIMIT && 
	$_SESSION['tweetCount'] <= PRO_VERSION_JB_TWEET_LIMIT && 
	$_SESSION['scheduledCount'] <= PRO_VERSION_SCHEDULED_TWEET_LIMIT && 
	$_SESSION['thanksLimit'] <= PRO_VERSION_THANK_YOU_TWEETS
	// note need to check for linked accounts - somehow
	) $planFit = 'Pro';
	if($planFit == '') $planFit = 'Business'; 
	$_SESSION['planFit'] = $planFit;
}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Tweet Jukebox</title>
<meta name="description" content="Tweet Jukebox - Thinking Inside the Box">
<link rel="shortcut icon" href="/favicon.ico?v=2">
<!-- BOOTSTRAP STYLES-->
<link href="../assets/css/bootstrap.css" rel="stylesheet" />
<!--link href="../assets/css/bootstrap_v3.3.4.css" rel="stylesheet" /-->
<!-- FONTAWESOME STYLES-->
<link href="../assets/css/font-awesome.css" rel="stylesheet" />
<!-- MORRIS CHART STYLES-->
<link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="../assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="/members/jquery.timepicker.css" />
<link href="/members/chart.css" rel="stylesheet" type="text/css">
<!-- JQUERY SCRIPTS -->
<script src="../assets/js/jquery-1.10.2.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="../assets/js/metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../assets/js/custom.js"></script>
<script src="/include/js_boot_source.js"></script>
<script type="text/javascript" src="/members/jquery.timepicker.js"></script>
<link rel="stylesheet" href="/highslide-4.1.13/highslide/highslide.css" type="text/css" />
<!-- 9d83e18a764256552baf6956b777f190 -->
<script type="text/javascript" src="/highslide-4.1.13/highslide/highslide-full.packed.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/highslide-4.1.13/highslide/graphics/';
	hs.showCredits = false;
    hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = .45;
	hs.zIndexCounter = 2000;
//	hs.objectLoadTime = 'after';
//	hs.forceAjaxReload = 'true';
</script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../assets/js/bootstrap_v3.3.4.min.js"></script>
<script src="lib/bootstrap-datepicker.js"></script>
<link href="lib/bootstrap-datepicker.css" rel="stylesheet" type="text/css">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64707958-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="wrapper">
  <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="/">Tweet Jukebox</a>
	</div>
    <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
		<ul class="nav navbar-nav navbar-cls-top" style="cursor:pointer;">
            <? require_once($_SERVER['DOCUMENT_ROOT']."/include/mainNav.php") ; ?>			
		</ul>
		<span data-toggle="dropdown" aria-expanded="false" style="cursor:pointer; " ><img style="display:inline; border:solid 1px #FFFFFF; " src="<? echo $content->profile_image_url_https; ?>" >&nbsp;<? echo $screen_name; ?>&nbsp;<i class="fa fa-cog"></i></span>
		<ul class="dropdown-menu dropdown-menu-right" role="menu" style="padding-top:10px;">			
			<li style="padding-bottom:10px; padding-top:5px; border-bottom:solid 1px #cccccc;"><a href="#" onClick="thanksSetup();"><i class="fa fa-gift fa-2x"></i> Thank You Tweets</a></li>										
			<li style="padding-bottom:10px; padding-top:5px; border-bottom:solid 1px #cccccc;"><a href="#" onClick="settingsForm();"><i class="fa fa-cog fa-2x"></i> My Settings</a></li>			
			<?
			// a bit of high stepping here - it is possible we have someone logged in that has useage that exceeds there plan
			$myPlan = array_search($_SESSION['planName'], $_SESSION['plans']);
			$myPlanFit = array_search($_SESSION['planFit'], $_SESSION['plans']);
			
			
			if($myPlan < $myPlanFit) { // disable all linked accounts etc.
				$SQL86 = "update users set all_tweet_status = '0' where master_account_id = '" . $_SESSION['master_account_id'] . "'  and master_account_id != '' ";
				$result86 = mysqli_query($conn, $SQL86) or die(mysqli_error());
			} else {
				if( $_SESSION['master_account_id'] == $_SESSION['access_token']['user_id'] ) { ?>
					<? if( $_SESSION['linkedAccounts'] < $_SESSION['accountsLimit']) { ?>
						<li style="padding-bottom:10px; padding-top:5px; border-bottom:solid 1px #cccccc;"><a href="/TOC/redirectAuthorize.php" ><i class="fa fa-link fa-2x"></i> Link Another Account</a></li>
					<? } else { ?>
						<li style="padding-bottom:10px; padding-top:5px; border-bottom:solid 1px #cccccc;"><a href="#"><i class="fa fa-link fa-2x"></i> Link Another Account (Limit Reached!)</a></li>
					<? } ?>
			<?
			 	} 
			}
			?>		
			<? if($_SESSION['master_account_id'] == $_SESSION['loggedInAs'] && $_SESSION['master_account_id'] != $_SESSION['access_token']['user_id']) { ?>
					<li style="padding-bottom:10px; padding-top:5px; border-bottom:solid 1px #cccccc;"><a href="#" onClick="confirmUnlink();"><i class="fa fa-chain-broken fa-2x"></i> Un-Link This Account</a></li>
			<? } ?>			
			<? if( $_SESSION['master_account_id'] == $_SESSION['access_token']['user_id'] || $_SESSION['planName'] == 'Free' ) { // were in as the master account or we have a free account  ?>
				<li style="padding-bottom:10px; padding-top:5px; border-bottom:solid 1px #cccccc;"><a href="/plans/"><i class="fa fa-key fa-2x"></i> Review Plan Options</a></li>
			<? } ?>									
			<?			
			// now if myPlan is > myPlanFit - we have not yet trimmed enough fat to fit so no option here to turn the master switch on
			if($myPlan >= $myPlanFit) { // this is fine allow them to fit in
				if($all_tweet_status == 1) {
			?>	
						<li><a href="" id="schTweetStatusOn"  style="display:block; padding-bottom:10px; padding-top:5px; color:#009900;" onClick="if(confirm('Are you sure you want to stop all tweeting for your account?') == true) { $('#schTweetStatusOn').css('display', 'none'); $('#schTweetStatusOff').css('display', 'block'); switchAllTweets('off'); location.reload(); }"  ><i class="fa fa-thumbs-o-up fa-2x"></i> Tweet Jukebox Is ON<br />controls all tweets</a></li>
						<li><a href="" id="schTweetStatusOff" style="display:none; padding-bottom:10px; padding-top:5px; color:#FF0000;"  onclick="$('#schTweetStatusOn').css('display', 'block'); $('#schTweetStatusOff').css('display', 'none'); switchAllTweets('on'); location.reload();" ><i class="fa fa-thumbs-o-down fa-2x"></i> Tweet Jukebox Is OFF<br />controls all tweets</a></li>
			<?	
					} else { 
			?>
						<li><a href="" id="schTweetStatusOn"  style="display:none; padding-bottom:10px; padding-top:5px; color:#009900;" onClick="if(confirm('Are you sure you want to stop all tweeting for your account?') == true) { $('#schTweetStatusOn').css('display', 'none'); $('#schTweetStatusOff').css('display', 'block'); switchAllTweets('off');  location.reload(); }"  ><i class="fa fa-thumbs-o-up fa-2x"></i> Tweet Jukebox Is OFF<br />controls all tweets</a></li>
						<li><a href="" id="schTweetStatusOff" style="display:block; padding-bottom:10px; padding-top:5px; color:#FF0000;" onClick="$('#schTweetStatusOn').css('display', 'block'); $('#schTweetStatusOff').css('display', 'none'); switchAllTweets('on');  location.reload();" ><i class="fa fa-thumbs-o-down fa-2x"></i> Tweet Jukebox Is OFF<br />controls all tweets</a></li>
			<?	
					} 
			}	
			?>			
			<? if( $_SESSION['access_token']['user_id'] == $_SESSION['loggedInAs'] )  { // if current access user is the same as the user that originally logged in ?>
				<li style="padding-bottom:10px; padding-top:5px; border-top:solid 1px #cccccc;"><a href="" onClick="confirmAccountCancel();"><i class="fa fa-ban fa-2x"></i> Cancel My Account</a></li>
			<? } ?>	
		</ul>
	</div>
  </nav>
  <!-- /. NAV TOP  -->  
  <nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
      <ul class="nav" id="main-menu">
        <li class="text-center">
			<img src="/images/tweetjukebox3.png" width="828" height="645" class="user-image img-responsive"/>			
		</li>        
		<li>
			<a id="myJBLI" href="#" class="jbList"><i class="fa fa-cloud fa-3x"></i>My Juke Boxes<span class="fa arrow"></span></a>
			<ul id="jukeBoxNavList" class="nav nav-second-level">
			<? 									
			$listCount = 0;
			$SQL = "select list_name, list_id, schedule_status from user_schedule where user_id = '$user_id' order by list_name ";
			$result = mysqli_query($conn, $SQL) or die(mysqli_error());
			$row = mysqli_fetch_assoc($result);				
			?>
			<?	
				do {
					$list_name = stripslashes($row['list_name']);
					$list_id = stripslashes($row['list_id']);
					$uploadName = str_replace("'", "\'", $list_name);
					$uploadName = htmlentities($list_name, ENT_QUOTES);
					$list_name = $uploadName; 
			?>
			<?
			?>  
				<li>
				<a href="#" id="jbLinkList<? echo $list_id;?>" onClick="
				$('#upload_list_name').val('<? echo htmlentities($uploadName);?>');
				$('#myFile').val('');
				if($('#tweetLI').length) {	
					if($('#scheduleLI').hasClass('jbc')) { setupSchedule(<? echo $list_id; ?>); }
					if($('#optionLI').hasClass('jbc')) { jbOptions(<? echo $list_id; ?>); }
					if($('#tweetLI').hasClass('jbc')) { masterList(<? echo $list_id; ?>); }
				} else {
					if($(this).hasClass('newList')) { 
						$(this).removeClass('newList');
						jbOptions(<? echo $list_id; ?>); 						
					} else {	
						masterList(<? echo $list_id; ?>);
					}	
				}	
				">
			<? 										
				echo $list_name; 
				if($row['schedule_status'] == '0') echo ' - off'; 	
			?>
				</a>
				</li>						
			<?
				$listCount ++;
				} while($row = mysqli_fetch_assoc($result));										
			?>
			</ul>
		</li>				
		<? if( ($listCount < $_SESSION['boxLimit'] ) || $_SESSION['isAdmin'] == '1' || $_SESSION['isSpecial'] == '1'  ) {  ?>
		<li><a href="#" onClick="if(confirm('Add another Juke Box?') == true) { addJukeBox(); }"><i class="fa fa-plus-square-o fa-3x"></i> Add A New Jukebox </a> </li>	
		<? } ?>
		
		<? 
		if($_SESSION['isAdmin'] == '1' || $_SESSION['isSpecial'] == '1') { 
			$SQL = "select * from store_items where user_id = '$user_id' ";
			$result = mysqli_query($conn, $SQL) or die(mysqli_error());
			if(mysqli_num_rows($result)) {
		?>		
				<li id="myStoreLI"><a class="schedule" href="#" onClick="hideDivs(); myStoreItems(0);"><i class="fa fa-gift fa-3x"></i> My Library Items </a> </li>	
		<? 	} 
		}
		?>		
		
        <li><a href="#" class="schedule" onClick="scheduledList();"><i class="fa fa-calendar fa-3x"></i> Scheduled Tweets </a> </li>      
	  	<li><a  href="#" class="schedule" onClick="recentMentions();"><i class="fa fa-bullhorn fa-3x"></i> Recent Mentions</a></li>
		<li><a  href="#" class="schedule" onClick="jbDailyContent();" ><i class="fa fa-calendar-o fa-3x"></i> Visual Schedule</a></li>						
		<? if($_SESSION['accountsLimit'] > 1 ) { ?>
			<li>
				<a href="#" class="jbList"><i class="fa fa-exchange fa-3x"></i>Switch Account<span class="fa arrow"></span></a>				
				<ul class="nav nav-second-level">
					<?
					$SQL = "select screen_name, user_id from users where master_account_id = '". $_SESSION['master_account_id'] ."' and master_account_id != '' order by screen_name ";
					$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
					$row = mysqli_fetch_assoc($result);				
					?>
					<?	
						do {
							$multiScreenName = stripslashes($row['screen_name']);
							if($row['user_id'] == $_SESSION['master_account_id'] ) {
								$multiScreenName .= " *";
							}
							if( $_SESSION['master_account_id'] == $_SESSION['loggedInAs'] || $_SESSION['isAdmin'] == 1 ) {
					?>	
								<li><a onClick="switchAccount('<? echo bin2hex($row['user_id']);?>','<? echo bin2hex($_SESSION['master_account_id']);?>' )" <? if($multiScreenName == $_SESSION['screen_name']) echo 'style="color:#00FF00;"'; ?> ><? echo $multiScreenName; ?></a></li>
					<?
							}
						} while($row = mysqli_fetch_assoc($result));	
					?>
				</ul>
			</li>
		<? } else { ?>	
			<li>
				<a href="#" class="jbList"><i class="fa fa-exchange fa-3x"></i>Switch Account<br>(available with upgrade)<span class="fa arrow"></span></a>
			</li>	
		<? } ?>
		<? if ($_SESSION['isAdmin'] == '1' ) { ?>						
			<li><a  href="/admin" class="schedule" ><i class="fa fa-users fa-3x"></i> Admin Portal</a></li>
		<? } ?>
	  </ul>
    </div>
  </nav>
  <!-- /. NAV SIDE  -->
  <div id="page-wrapper" >
    <div id="page-inner">      	  	  
	  <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          	 <div id="masterDiv" style="display:block; height:auto; padding-top:10px; ">
			 	<? 
					if( $_SESSION['isGrandfathered'] )  { // the user needs to upgrade 															
				?>	
					<center>
					<div class="bg-success" style="width:60%; padding:10px; border:solid thin #000000;">
						<p><b>NOTICE:</b> Tweetjukebox now has tiered membership plans.</p>
						<p>Based on a review of your account, you would fit nicely into our <strong><? echo $_SESSION['planFit']; ?></strong> plan!</p>
						<p>You can view our plans <a href="/plans/">here</a>.</p>
						<p>Once you've selected and enrolled into a plan you like, you'll be all set!</p>
						<p>If you do nothing, we'll enroll you into our <strong><? echo $planFit; ?></strong> plan automatically.</p>
						<p>Deadline for you to successfully enroll in one of the plans is November 8th!</p>
					</div>
					</center>
				<?		
					}		
			 	?>
				<? if( $_SESSION['planName'] == 'Free' && $_SESSION['isGrandfathered'] == 0  )  { // the user may want to upgrade 						?>
					<center>
					<div class="bg-success" style="width:60%; padding:10px; border:solid thin #000000;">
						<p><a href="/plans/"><b>Upgrade now!</b>  Check out our three affordable membership plans ----></a></p>
					</div>
					</center>
				<?		
					}		
			 	?>
				<? if($_SESSION['linkError']) {  ?>
					<center>
					<div class="bg-warning" style="width:60%; padding:10px; border:solid thin #000000;">
						<p><? echo $_SESSION['linkError'];?></p>
					</div>
					</center>				
				
				<? 
					unset($_SESSION['linkError']);
				} 
				?>														
			 	<? 
				 if($_SESSION['OFFER_SEEN'] == '0' ) {
				?>
					 <script>$("#masterDiv").css('display', 'block');</script>
					 <center>
					 <div align="center" >
					 <h1 align="center">Bonus Content Offer!</h1>			 
					 <p class="text-info">We've added a new PHOTO Jukebox with 100 tweets, free of course!</p>
					 <p><input class="btn-primary" style="height:80px;" type="button" value="Add this Jukebox to my Account!" onClick="bonusJB(1);">
					 </p>
					 <p><input class="smallOffButton" style="height:80px;" type="button" value="No Thanks, I'm Good" onClick="bonusJB(0);">
					 </p>
					 </div>
					 </center>
				 <?	
				 } else {	
				 ?>													
					<center><img src="/images/tweetjukebox3.png" class="img-responsive" style="padding-top:50px;" ></center>			
				<? } ?>
			 </div>
			 <div id="recentMentionsDiv" style="display:none;"></div>
			 <div id="uploadDiv" style="display:none; ">
			 	<?
				$SQL = "select count(id) as totalTweets from tweets where user_id = '$user_id' ";
				$result = mysqli_query($conn, $SQL) or die(mysqli_error());
				$row = mysqli_fetch_assoc($result);
				$totalTweets = $row['totalTweets'];
												
				if($totalTweets >= $_SESSION['tweetLimit']) {	
				?>
					<h2 align="center">Sorry your current plan does not allow you to add any more new records to your Jukebox!</h2>			
				<? } else { ?>
				<div onClick="masterList(0);" style="font-size:18px; font-weight:bold; float:right;">close</div>
				<form class="l-form" id="uploadForm" name="uploadForm" method="post" enctype="multipart/form-data" action="postDataBoot.php" accept-charset="utf-8">
				<h2 id='uploadH2' align="center">Upload Tweets</h2>
					<div style="text-align:left;">
						<!--p>We will automatically post status updates (Tweets) to twitter on your behalf.  You can have as many Tweets as your plan allows.</p>
						<p>Once you have built your &quot;Jukebox&quot; of Tweets you can schedule how often they will be sent.&nbsp; We will randomly choose Tweets from your inventory that have not recently been tweeted, or have never been tweeted.&nbsp; We will NOT repeat any Tweet in the same day or those that have been Tweeted within a number of days that you select.&nbsp;</p>
						<p>Once all of your Tweets have been Tweeted at least once, we will continue to randomly select tweets to post, unless you put the process on &quot;hold&quot;.&nbsp; We post updates to Twitter every 5 minutes.</p-->
						<p>Your file must be in either a <b>CSV</b> or <b>TXT</b> format and must contain ONE heading in the first row called:&nbsp;&nbsp;<b>tweet_text</b></p>						
						<p>All tweets must in rows (one line per tweet) under the tweet_text row for the upload to work.</p>
						<p>
						Mac Users, special note: If you are creating a CSV file please use Google Docs. There are
						characters generated in the creation of spreadsheets using Excel 2011 that have
						persistently caused upload errors.
						</p>
						
						<p>Click here to get a CSV template -> <a href="download.php">Download CSV Template</a></p>						
						<p>Click here to get a TXT template -> <a href="downloadTxt.php">Download TXT Template</a></p>
						<p><input id="myFile" type="file" name="myFile" value="" /></p>
						<p class="ui-state-error">After your new Tweets are uploaded, we will list all your Tweets, newest / first - so you can review your latest additions for editing as you see fit.</p>
						<p id="messageArea" align="center">&nbsp;</p>

					</div>
					<input type="hidden" id="upload_list_number" value="" name="upload_list_number" />
					<input type="hidden" id="upload_list_name" value="" name="upload_list_name" />
				</form>				
				<script>
				//var files;
				$('#myFile').change(function(e) {
				//function readURL(input) {  	
					var files = e.target.files[0];
					var filePathName = $('#myFile').val();
					var fileParts = filePathName.split(".");
					var ext = fileParts[fileParts.length -1];			
					ext = ext.toLowerCase();
					
					var fileNameParts = filePathName.split("\\");
					var fileName = fileNameParts[fileNameParts.length -1];											
					
					if(ext != 'csv' && ext != 'txt') {
						$('#messageArea').css('color', 'red');
						$('#messageArea').html('Invalid file format! Your file must be a .csv OR .txt file.');
						$('#myFile').val('');
					} else {
						$('#messageArea').html('');
						var reader = new FileReader();
						reader.onload = function (e) {									  
							//csvAsArrayA = e.target.result.csvToArray();
							//if( (csvAsArrayA[0][0]) != 'tweet_text' ) {
							var contents = e.target.result;
							
							var firstLine = contents.substr(0, contents.indexOf("\n")-1);
							
							//alert(firstLine);
							//alert(firstLine.length);
							
							if( firstLine != 'tweet_text' && firstLine != 'tweet_tex') {
								$('#messageArea').css('color', 'red');
								$('#messageArea').html('Invalid file construction. <p>Your first row must contain this column heading:</p><p><b>tweet_text</b></p><p>Your first row is: ' + firstLine + '</p><p class="content">Click here to get a template -> <a href="download.php">Download Template</a></p>');
								$('#myFile').val('');
							} else {
								
								if( confirm("Warning!!!!\n\nThis will ADD the records in this file to your Jukebox " + $('#upload_list_name').val() + ".\n\nTHIS PROCESS CANNOT BE UNDONE.\n\nIf this is what you want to do, press OK to upload this file: " + fileName ) ) {																
									//hideDivs();
									//$('#recentMentionsDiv').css('display', 'block');
									//$('#masterDiv').html('<div align="center">Please wait while we load your file....<p><img src="/images/wait.GIF" border="0"></p></div>');									
									$('#uploadForm').submit();																																																																										
								}	
							}
						};
						reader.readAsText(files);
					}
				} );	
				</script>	
				<script type="text/javascript">
				/* Copyright 2012-2013 Daniel Tillin
				 *
				 * Permission is hereby granted, free of charge, to any person obtaining
				 * a copy of this software and associated documentation files (the
				 * "Software"), to deal in the Software without restriction, including
				 * without limitation the rights to use, copy, modify, merge, publish,
				 * distribute, sublicense, and/or sell copies of the Software, and to
				 * permit persons to whom the Software is furnished to do so, subject to
				 * the following conditions:
				 *
				 * The above copyright notice and this permission notice shall be
				 * included in all copies or substantial portions of the Software.
				 * 
				 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
				 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
				 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
				 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
				 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
				 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
				 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
				 *
				 * csvToArray v2.1 (Unminifiled for development)
				 *
				 * For documentation visit:
				 * http://code.google.com/p/csv-to-array/
				 *
				 */
				String.prototype.csvToArray = function (o) {
					var od = {
						'fSep': ',',
						'rSep': '\r\n',
						'quot': '"',
						'head': false,
						'trim': false
					}
					if (o) {
						for (var i in od) {
							if (!o[i]) o[i] = od[i];
						}
					} else {
						o = od;
					}
					var a = [
						['']
					];
					for (var r = f = p = q = 0; p < this.length; p++) {
						switch (c = this.charAt(p)) {
						case o.quot:
							if (q && this.charAt(p + 1) == o.quot) {
								a[r][f] += o.quot;
								++p;
							} else {
								q ^= 1;
							}
							break;
						case o.fSep:
							if (!q) {
								if (o.trim) {
									a[r][f] = a[r][f].replace(/^\s\s*/, '').replace(/\s\s*$/, '');
								}
								a[r][++f] = '';
							} else {
								a[r][f] += c;
							}
							break;
						case o.rSep.charAt(0):
							if (!q && (!o.rSep.charAt(1) || (o.rSep.charAt(1) && o.rSep.charAt(1) == this.charAt(p + 1)))) {
								if (o.trim) {
									a[r][f] = a[r][f].replace(/^\s\s*/, '').replace(/\s\s*$/, '');
								}
								a[++r] = [''];
								a[r][f = 0] = '';
								if (o.rSep.charAt(1)) {
									++p;
								}
							} else {
								a[r][f] += c;
							}
							break;
						default:
							a[r][f] += c;
						}
					}
					if (o.head) {
						a.shift()
					}
					if (a[a.length - 1].length < a[0].length) {
						a.pop()
					}
					return a;
				}
				
				</script>			 
			 <? } ?>
			 </div>
			 
        </div>        
      </div>
  	  <div class="row" style="padding-top:50px;">
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<p align="center">
   			<span class="left">tj.local &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> | <!--a href="/plans.php">PLANS</a> | --><a href="/contact">CONTACT US</a> | <a href="/privacy">PRIVACY POLICY</a> | <a href="/terms">TERMS AND CONDITIONS</a>
			</p>
		</div>	
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
	  </div>
    </div>
    <!-- /. PAGE INNER  -->
  </div>
  <!-- /. PAGE WRAPPER  -->
</div>
<!-- /. WRAPPER  -->
<!-- MORRIS CHART SCRIPTS -->
<script src="/assets/js/morris/raphael-2.1.0.min.js"></script>
<script src="/assets/js/morris/morris.js"></script>
<script src="https://js.braintreegateway.com/v2/braintree.js"></script>
<script type="text/javascript">
$('.sidebar-collapse li a').on('click', function() { 		
	
	$('.sidebar-collapse li a').css( "background-color", "#4D4D4D" );	
	$('.sidebar-collapse li a').removeClass("active");
		
	$(this).addClass("active");	
			
	if($(this).hasClass('schedule')) {
		if($('#jukeBoxNavList').hasClass("in")) {
			$('#myJBLI').click();
			//$('#jukeBoxNavList').removeClass("in");			
		}
	}	
	
	if($(this).hasClass('jbList')) {
		$('#masterDiv').html('<center><img src="/images/tweetjukebox3.png" class="img-responsive" style="padding-top:50px;" ></center>');
	}			
});
$('.nav-second-level li').on('click', function() { 		


//	$( '.nav-second-level li' ).css( "background-color", "#4D4D4D" );	
	$( this ).children().css( "background-color", "red" );
						
});
</script>

<div class="modal fade" id="searchDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button id="sbtn1" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       	 		<h4 class="modal-title" id="myModalLabel">Search</h4>
      		</div>
			<div class="modal-body">	 			 
				<form class="c-form" accept-charset="utf-8">  
					<label for="search_tweet_text">Tweet Text</label>
					<input type="text" name="search_tweet_text" id="search_tweet_text" value=""  />
					<button type="button" onClick="searchJB()">Submit</button>
				</form>
			</div>
		</div>	
	</div>			
</div>
<script>
$('#searchDiv').on('shown.bs.modal', function () {
    $('#search_tweet_text').focus();
})
function searchJB() {												
	//$('#sbtn1').click();
	$('#searchDiv').modal('toggle');
	$.ajax({
		type: "get",
		url: "/members/getMasterListBoot.php",
		data: {
			'search_tweet_text': $('#search_tweet_text').val(),
			'list_number': $('#list_number').val()
		} ,
		dataType: "html"
	})
	.done(function( msg ) { 		
		$('#masterDiv').html(msg);		
	});			
}

function switchAccount(e,f) {
	$.ajax({
  		type: "post",
  		url: "/members/login.php",
		data: {'logMeInPlease': e, 'mac': f} ,
		dataType: "html"
	})
  	.done(function( msg ) { 
		//alert(msg);
		location.reload();
  	});	
}
</script>
<div class="modal fade" id="searchSchDiv" tabindex="-1" role="dialog" aria-labelledby="mySchModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       	 		<h4 class="modal-title" id="mySchModalLabel">Search</h4>
      		</div>
			<div class="modal-body">	 			 
				<form class="c-form" accept-charset="utf-8">  
					<label for="sch_search_tweet_text">Tweet Text</label>
					<input type="text" name="sch_search_tweet_text" id="sch_search_tweet_text" value=""  />
					<button type="button" onClick="searchSchJB()">Submit</button>
				</form>
			</div>
		</div>	
	</div>			
</div>
<script>
$('#searchSchDiv').on('shown.bs.modal', function () {
    $('#sch_search_tweet_text').focus();
})
function searchSchJB() {											
	$('#searchSchDiv').modal('hide');
	
	$.ajax({
		type: "get",
		url: "/members/getScheduledTweetsBoot.php",
		data: { 'search_tweet_text': $('#sch_search_tweet_text').val() 	} ,
		dataType: "html"
	})
	.done(function( msg ) { 
		$('#masterDiv').html(msg);
	});			
}
</script>
</body>
<? if($_SESSION['NEWJBUPLOAD']) { ?>	
	<script> 
	$('#masterDiv').css('display', 'block');
	$('#masterDiv').html('<div align="center">Please wait while we get your recent Jukebox additions for you....<p><img src="../images/wait.GIF" border="0"></p></div>');	
	setTimeout(function (){		
		$.ajax({
			type: "get",
			url: "/members/getMasterListBoot.php?list_number=<? echo $_SESSION['LISTNUMBER'];?>&s=tweet_last_post_date, id DESC&uploaded=uploaded",
			//data: {'message': msg} ,
			dataType: "html"
		})
		.done(function( msg ) { 
			$('#masterDiv').html(msg);
		});	
	}, 2000);
	</script>
<? 
		unset($_SESSION['LISTNUMBER']);
		unset($_SESSION['NEWJBUPLOAD']);		
	}  	
?>
<? if($_SESSION['NEWJBADDED']) { ?>
<script>
	$('#myJBLI').click();
	$('#jukeBoxNavList').addClass("in");
	$('#jbLinkList<? echo $_SESSION['NEWJBADDED'];?>').addClass("newList");
	$('#jbLinkList<? echo $_SESSION['NEWJBADDED'];?>').click();
</script>
<?
	unset($_SESSION['NEWJBADDED']);
} 
?>
<? if($_SESSION['new_user'] == '1') { ?>
<script>
	$('#myJBLI').click();
	$('#scheduleLI').removeClass('jbc');
	$('#optionLI').removeClass('jbc');		
	$('#jukeBoxNavList').addClass("in");
	$('#jbLinkList1').click();
	masterList('1');
</script>
<?
} 
?>
<script>
function myStoreItems(l) {
	hideDivs();
	$('#masterDiv').css('display', 'block');	
	$('#masterDiv').html('<div align="center">Please wait while we get that for you....<p><img src="../images/wait.GIF" border="0"></p></div>');	
	
	$.ajax({
  		type: "get",
  		url: "/members/getMyStoreItems.php",
		//data: {'message': msg} ,
		dataType: "html"
	})
  	.done(function( msg ) { 
		$('#masterDiv').html(msg);
		if(l) {
			$('#storeItem'+l).click();		
		}
  	});	
}

function confirmAccountCancel() {
	if ( confirm("Are you sure you want to cancel your account?") ) {
		if( confirm("This action will turn off all of your Jukeboxes and scheduled tweets.\n\nIf you have a paid subscription, it will be canceled and no further charges will be incurred.\n\nAre you sure you wish to proceed?" ) ) {		
			$.ajax({
				type: "post",
				url: "/members/cancelAccount.php"				
			})
			.done(function( msg ) { 
				location.assign("/goodbye.php");
			});	
		} else {
			return false;
		}
	} else {
		return false;
	}
}
function confirmUnlink() {
	if ( confirm("Are you sure you want to un-link this account?") ) {
		$.ajax({
			type: "post",
			url: "/members/unlinkAccount.php"				
		})
		.done(function( msg ) { 
			location.assign("/members/");
		});	
	}
}
</script>
<?  
/*
print "<pre>"; print_r($_SESSION); 
print $myPlan;
print "<br>";
print $myPlanFit;
*/
?>
</html>