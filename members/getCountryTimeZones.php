<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');

$country = $_REQUEST['c'];



$timeZones = array();
foreach(DateTimeZone::listIdentifiers() as $tz) {
	$country_city = explode("/", $tz);
	if($country == $country_city[0]) { 
		if(in_array($tz, $timeZones)) {
			
		} else {
			array_push($timeZones, $tz);
		}
	}	
}



echo '<select name="userTimeZoneSelection" id="userTimeZoneSelection">';
foreach($timeZones as $timeZone) {
	$country_city = explode("/",$timeZone);
	array_shift($country_city);
	$timeZoneDisplay = implode("/",$country_city);
	echo '<option value="' .$timeZone. '">' .$timeZoneDisplay. ' </option>';
}
echo '</select>';



exit();

?>