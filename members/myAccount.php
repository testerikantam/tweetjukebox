<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
$user_id = $_SESSION['access_token']['user_id'];
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');

require_once $_SERVER['DOCUMENT_ROOT'].'/braintree-php-2.33.0/lib/Braintree.php';

Braintree_Configuration::environment('sandbox');
Braintree_Configuration::merchantId('zsbs7swsdwdzvmwg');
Braintree_Configuration::publicKey('yb7qkdry9qfvzbn9');
Braintree_Configuration::privateKey('36ae489f8f321abd38f0971f5f1ff2cf');
?>
<?
if($_POST['data'] ) {	
	foreach($_POST['data'] as $data) {
		if($data['name'] == 'user_first_name') {
			$user_first_name = stripslashes($data['value']);		
		}
		if($data['name'] == 'user_last_name') {
			$user_last_name = stripslashes($data['value']);		
		}
		if($data['name'] == 'email') {
			$email = stripslashes($data['value']);		
		}
	}
	$SQL = "update users set user_first_name = '$user_first_name', user_last_name = '$user_last_name', email = '$email' ";
	$SQL .= "where user_id = '$user_id' ";
	$result = mysqli_query($conn, $SQL) or die(mysqli_error());
	echo "Your changes were saved";	
	$_SESSION['email'] = $email;
	exit();		
}
?>
<?					///////////////////////////////////////   C R E D I T    C A R D     S T U F F ////////////////////////////////
if($_POST['CC_data'] ) {
	
	foreach($_POST['CC_data'] as $data) {
		if($data['name'] == 'cardNumber') {
			$cardNumber = stripslashes($data['value']);		
		}
		if($data['name'] == 'cc_cvv') {
			$cc_cvv = stripslashes($data['value']);		
		}
		if($data['name'] == 'month') {
			$month = stripslashes($data['value']);		
		}
		if($data['name'] == 'year') {
			$year = stripslashes($data['value']);		
		}
		if($data['name'] == 'tokenID') {
			$tokenID = stripslashes($data['value']);		
		}			
	}
	
	
	$customer = Braintree_Customer::find($user_id);  // see if customer already exists or not
	if($customer->success) {
		$result = Braintree_Customer::update($user_id, 
			array(
			'email' => $_SESSION['email'],
			'creditCard' => array(
				'number' => $cardNumber,
				'expirationMonth' => $month,
				'expirationYear' => $year,
				'cvv' => $cc_cvv,
				'options' => array(
					'updateExistingToken' => $tokenID,
					'verifyCard' => true
				)
			)
		));
		
		if($result->success) {
			echo "Your CC information was saved";
		} else {
			echo "There was an issue saving your CC information.\n\nPlease check your information and try again";
		}
		exit();
	} else {   // we need to build the BT customer, payment info, and plan
		
		
		$result = Braintree_Customer::create(array(
			'id' => $user_id,
			'firstName' => $_SESSION['user_first_name'],
			'lastName' => $_SESSION['user_last_name'],
			'email' => $_SESSION['email'],
			'creditCard' => array(
				'number' => $cardNumber,
				'expirationMonth' => $month,
				'expirationYear' => $year,
				'cvv' => $cc_cvv,
				'options' => array(
					'verifyCard' => true
				)
			)
		));
		
		
		
		if($result->success) { // we created customer and should now have the customer's CC Token from BT	
			$ccTokenID = $result->customer->creditCards[0]->token;
			$result = Braintree_Subscription::create(array(
			  'paymentMethodToken' => $ccTokenID,
			  'planId' => $_SESSION['planName'],
			  'id' => $_SESSION['planName'] . "_" . $user_id  // plan is Name_userID
			));				
		} 

		if($result->success) {
			echo "Your CC information was saved";
		} else {
			echo "There was an issue saving your CC information.\n\nPlease check your information and try again";
		}
		exit();	
	}	
}	
?>
<?
$year = date("Y"); 
$currentPaymentMethod = "not set";
$currentExpirationMonth = '01';
$currentExpirationMonth = $year;
$cardImage = "<img src='/images/spacer.gif' >";


$planName = $_SESSION['planName'];

$subscriptionID = $planName . "_" . $user_id;

$subscription = Braintree_Subscription::find($subscriptionID);

if(is_object($subscription)) {
	$tokenID = $subscription ->paymentMethodToken;
	$paymentMethod = Braintree_PaymentMethod::find($tokenID);
	if(is_object($paymentMethod)) {
		$cardImage = "<img src='". $paymentMethod ->imageUrl."' >";
		$currentCardNumber = $paymentMethod ->maskedNumber;
		$currentPaymentMethod = $currentCardNumber;
		$currentExpirationMonth = $paymentMethod ->expirationMonth;
		$currentExpirationYear =  $paymentMethod ->expirationYear;
	} 	
}
?>
<?
$SQL = "select * from users where user_id = '$user_id' ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$row = mysqli_fetch_assoc($result);	
?>
<div style="width:50%; float:left;">
<h3>My Current Account Information</h3>
<form id="accountInfoForm" class="c-form" method="post" enctype="multipart/form-data" accept-charset="utf-8">
	 <label for="user_first_name">First Name </label>
	 <input type="text" id="user_first_name" name="user_first_name" value="<? echo stripslashes($row['user_first_name']); ?>">
	 <label for="user_last_name">Last Name </label>
	 <input type="text" id="user_last_name" name="user_last_name" value="<? echo stripslashes($row['user_last_name']); ?>">
	 <label for="email">Email <span id="emailMessages" style="color:#FF0000; display:inline;"></span></label>
	 <input type="text" id="email" name="email" onChange="validateEmail(this.value);" value="<? echo stripslashes($row['email']); ?>">	 
	 <label for="email2">Re-type Email <span id="email2Messages" style="display:inline;"></span></label>
	 <input type="text" id="email2" name="email2" onKeyUp="checkEmails();"  value="<? echo stripslashes($row['email']); ?>">
	 
	 
	 <input type="button" id="submitAccount" value="Save"  onClick="validateAccountInfoForm();" >
	 <input type="hidden" id="action" name="action" value="" />
 </form> 
</div>
<div style="width:50%; float:left;">
<h3>My Current Payment Method<? if($currentPaymentMethod == 'not set') echo ' - Not Set'; ?></h3>
<form class="c-form" method="POST" id="paymentForm" accept-charset="utf-8">        	

		<label>Card Number <img title="All digits - no spaces or dashes" src="/images/question_blue.png">&nbsp;<? echo $cardImage; ?></label>
        <input id="cardNumber" name="cardNumber" type="text" size="20" onKeyUp="numbersOnly(this)" value="<? echo $currentCardNumber; ?>" />
   
        <label>CVV <img title="Three or four digit security code for your card" src="/images/question_blue.png"></label>
        <input id="cc_cvv" name="cc_cvv" type="text" size="4" onKeyUp="numbersOnly(this)" />
        <label>Expiration Month</label>
	    <select name="month" id="cc_expires_month" >
		    <option value="01" <? if($currentExpirationMonth == '01') echo 'selected="selected"'; ?>>January</option>
			<option value="02" <? if($currentExpirationMonth == '02') echo 'selected="selected"'; ?>>February</option>
			<option value="03" <? if($currentExpirationMonth == '03') echo 'selected="selected"'; ?>>March</option>
			<option value="04" <? if($currentExpirationMonth == '04') echo 'selected="selected"'; ?>>April</option>
			<option value="05" <? if($currentExpirationMonth == '05') echo 'selected="selected"'; ?>>May</option>
			<option value="06" <? if($currentExpirationMonth == '06') echo 'selected="selected"'; ?>>June</option>
			<option value="07" <? if($currentExpirationMonth == '07') echo 'selected="selected"'; ?>>July</option>
			<option value="08" <? if($currentExpirationMonth == '08') echo 'selected="selected"'; ?>>August</option>
			<option value="09" <? if($currentExpirationMonth == '09') echo 'selected="selected"'; ?>>September</option>
			<option value="10" <? if($currentExpirationMonth == '10') echo 'selected="selected"'; ?>>October</option>
			<option value="11" <? if($currentExpirationMonth == '11') echo 'selected="selected"'; ?>>November</option>
			<option value="12" <? if($currentExpirationMonth == '12') echo 'selected="selected"'; ?>>December</option>
		</select>		  
		<label>Exipration Year</label>
		<select name="year" id="cc_expires_year">			
			  <? do { ?>
					<option value="<? echo $year; ?>" <? if($currentExpirationYear == $year) echo ' selected="selected"'; ?>><? echo $year; ?></option>
			  <? 
					$year ++;
				} while ($year < date("Y") + 9);
			  ?>	
		</select>
		<input type="hidden" name="tokenID" value="<? echo $tokenID; ?>"  />
		<input type="hidden" name="currDate" id="currDate" value="<? echo date("Ym"); ?>" />	
		<input type="button" id="submit2" value="Update CC" onClick="validatePaymentForm();" >
</form>	 
</div>
<script>
function checkEmails() {
	var email_1 = $('#email').val();
	var email_2 = $('#email2').val();
			
	if(email_1.length == email_2.length) {
		if(email_1 == email_2) {
			if(validateEmail(email_1) == true ) {
				$('#email2Messages').css('color', '#00CC00');
				$('#email2Messages').html("Agree!");	
			} else {
				$('#email2Messages').css('color', '#FF0000');
				$('#email2Messages').html("Your email addresses appear to be invalid!");
			}
		} else {
			$('#email2Messages').css('color', '#FF0000');
			$('#email2Messages').html("Your email addresses do not agree!");
		}
	}
}
  
function checkPasswords() {
	var pwd_1 = $('#pwd').val();
	var pwd_2 = $('#pwd2').val();
		
	if(pwd_1.length == pwd_2.length) {
		if(pwd_1 != pwd_2) {
			$('#pwd2Messages').css('color', '#FF0000');
			$('#pwd2Messages').html("Your passwords do not agree!");
		} else {
			$('#pwd2Messages').css('color', '#00CC00');
			$('#pwd2Messages').html("Agree!");	
		}
	} else {
		$('#pwd2Messages').css('color', '#FF0000');
		$('#pwd2Messages').html("Your passwords do not agree!");
	}
}
  
function validateEmail(sEmail) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {
		$('#emailMessages').html("");
		return true;
	}
	else {
		$('#emailMessages').html("Your email appears to be invalid.");
		return false;
	}
}
 
function checkStrength(pwd) {
	//initial strength
	var strength = 0	  	;		  
	//if the password length is less than 6, return message.
	if (pwd.length < 6) {
		$('#result').css('color', '#FF0000');
		$('#result').html('Too Short');
		return true;
	}
	
	//length is ok, lets continue.
	
	//if length is 6 characters or more, increase strength value
	if (pwd.length > 5) strength += 1
	
	//if password contains both lower and uppercase characters, increase strength value
	if (pwd.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))  strength += 1;
	
	//if it has numbers and characters, increase strength value
	if (pwd.match(/([a-zA-Z])/) && pwd.match(/([0-9])/))  strength += 1;
	
	//if it has one special character, increase strength value
	if (pwd.match(/([!,%,&,@,#,$,^,*,?,_,~])/))  strength += 1;
	
	//if it has two special characters, increase strength value
	if (pwd.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,",%,&,@,#,$,^,*,?,_,~])/)) strength += 1;
	
	//now we have calculated strength value, we can return messages
	
	//if value is less than 2
	if (strength < 2 ) {			
		$('#result').css('color', '#FFCC00') ;
		$('#result').html('Weak');
		return true;
	} else if (strength == 2 ) {			
		$('#result').css('color', '#00CC00') ;
		$('#result').html('Good');
		return true;
	} else {			
		$('#result').css('color', '#2D98F3') ;
		$('#result').html('Strong');
		return true;
	}
}

	
function validateAccountInfoForm() {	
	var errors = '';
	var email_1 = $('#email').val();
	if(validateEmail(email_1) == false ) errors = errors + "\nYour email appears to be missing or invalid";
	
	var firstName = $('#user_first_name').val();
	var lastName = $('#user_last_name').val();
	if(firstName.length < 1 ) errors = errors + "\nPlease enter your first name";
	if(lastName.length < 2 ) errors = errors + "\nPlease enter your last name";
	
	if($('#email').val() != $('#email2').val() ) errors = errors + "\nYour emails do not agree";
			
	if(errors != '') {
		alert(errors);
	} else {
		$('#action').val("form posted");
		saveAccountData();
	}	
}	

function saveAccountData() {

		formData = $( "#accountInfoForm" ).serializeArray();										
		$.ajax({
			type: "post",
			url: "/members/myAccount.php",
			data: {'data': formData },
			dataType: "html"
		})
		.done(function( msg ) { 				
			alert(msg);
			hideDivs();				
	   });	
}

function saveCCData() {

		CC_Data = $( "#paymentForm" ).serializeArray();										
		$.ajax({
			type: "post",
			url: "/members/myAccount.php",
			data: {'CC_data': CC_Data },
			dataType: "html"
		})
		.done(function( msg ) { 				
			alert(msg);
			if(msg.indexOf("saved") > 0 ) {
				hideDivs();				
			}	
	   });	
}

</script>
<script>
function numbersOnly(obj) {
    if (obj.value != obj.value.replace(/[^0-9\.]/g, '')) {
       obj.value = obj.value.replace(/[^0-9\.]/g, '');
    }
};

function validatePaymentForm() {
	var errors = '';
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	var x = '';
	x = document.getElementById("cardNumber").value;
	if($.isNumeric(x)) {
		if(x.length < 15 || x.length > 19 ) {
			errors+="CC Number length must be betwen 15 and 19 digits without any spaces or dashes.\n";
		}
	} else {
		errors+="CC Number length must be betwen 15 and 19 digits without any spaces or dashes.\n";
	}	
	x = document.getElementById("cc_cvv").value;
	if(x.length != 3 && x.length != 4 ) {
		errors+="Please enter the CVV Number without any spaces or dashes.\n";
	}		
	var year = document.getElementById("cc_expires_year").value	
	var month = document.getElementById("cc_expires_month").value 
	var currDate = document.getElementById("currDate").value 
	if(currDate > year+month) {
		errors+="Credit card is expired.\n";
	}
			
	if(errors) {
		alert(errors);
		return false;
	} else {
		saveCCData();
	}	
}
</script>