<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');
?>
<?
$user_id = $_SESSION['access_token']['user_id'];  
$id = mysqli_real_escape_string($conn, $_REQUEST['id'] );
$tweet_text = mysqli_real_escape_string($conn, $_REQUEST['scheduled_tweet_text'] );
$start_date = mysqli_real_escape_string($conn, $_REQUEST['tweet_start_date'] );
$start_time = mysqli_real_escape_string($conn, $_REQUEST['tweet_start_time'] );
$frequency = mysqli_real_escape_string($conn, $_REQUEST['tweet_frequency'] );

if($_REQUEST['tweet_status'] == 'true' ) {
	$tweet_status = 'draft'; 
} else {
	$tweet_status = 'active';
}

$start_date_time = date("Y-m-d H:i:00", strtotime($start_date . " " . $start_time)); 

//$tweet_text = mb_convert_encoding($tweet_text, "utf-8");


if($id > 0) {
	$SQL = "select * from scheduled_tweets where user_id = '$user_id' and id = '$id' " ;
	$result = mysqli_query($conn, $SQL) or die(mysqli_error());
	if (mysqli_num_rows($result) ) {
		$SQL = "update scheduled_tweets set  tweet_text = '$tweet_text', start_date_time = '$start_date_time', frequency = '$frequency', tweet_status = '$tweet_status' where id = '$id' ";		
		$result = mysqli_query($conn, $SQL) or die(mysqli_error());		
	}
} else {
	$SQL = "insert into scheduled_tweets set  user_id = '$user_id', tweet_text = '$tweet_text',  start_date_time = '$start_date_time', frequency = '$frequency', tweet_status = '$tweet_status', tweet_sent = '0' ";
	$result = mysqli_query($conn, $SQL) or die(mysqli_error());	
	
	echo "New record added";
	
}
?>