<? include ($_SERVER['DOCUMENT_ROOT']."/include/config.php") ; ?>
<?
if($_SESSION['validated'] == 'validated') { 
} else {	
		header("Location: /oops.php");
	exit();
} 

require_once($_SERVER['DOCUMENT_ROOT']."/TOC/twitteroauth/twitteroauth.php");
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/config.php");
$screen_name = $_SESSION['screen_name'];
$SQL = "select * from users where screen_name = '$screen_name'  ";
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$row = mysqli_fetch_assoc($result);	
$user_id = $row['user_id'];

$SQL = "select id from user_schedule where user_id = '$user_id'  "; 
$result = mysqli_query($conn, $SQL) or die(mysqli_error());
$canPurchase = '1';
if(mysqli_num_rows($result) > 4 ) {
	if($_SESSION['isAdmin'] == 1 || $_SESSION['isSpecial'] == '1'  ) {
		$canPurchase = '1';
	} else {
		$canPurchase = '0';
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Tweet Jukebox</title>
<meta name="description" content="Tweet Jukebox - Thinking Inside the Box">
<link rel="shortcut icon" href="/favicon.ico?v=2">
<!-- BOOTSTRAP STYLES-->
<link href="/assets/css/bootstrap.css" rel="stylesheet" />
<!--link href="../assets/css/bootstrap_v3.3.4.css" rel="stylesheet" /-->
<!-- FONTAWESOME STYLES-->
<link href="/assets/css/font-awesome.css" rel="stylesheet" />
<!-- MORRIS CHART STYLES-->
<link href="/assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="/assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="/members/jquery.timepicker.css" />
<link href="/members/chart.css" rel="stylesheet" type="text/css">
<!-- JQUERY SCRIPTS -->
<script src="/assets/js/jquery-1.10.2.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="/assets/js/metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="/assets/js/custom.js"></script>
<script src="/include/js_boot.js"></script>
<script type="text/javascript" src="/members/jquery.timepicker.js"></script>
<link rel="stylesheet" href="/highslide-4.1.13/highslide/highslide.css" type="text/css" />
<!-- 9d83e18a764256552baf6956b777f190 -->
<script type="text/javascript" src="/highslide-4.1.13/highslide/highslide-full.packed.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/highslide-4.1.13/highslide/graphics/';
	hs.showCredits = false;
    hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = .45;
	hs.zIndexCounter = 2000;
//	hs.objectLoadTime = 'after';
//	hs.forceAjaxReload = 'true';
</script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="/assets/js/bootstrap_v3.3.4.min.js"></script>
<script src="/members/lib/bootstrap-datepicker.js"></script>
<link href="/members/lib/bootstrap-datepicker.css" rel="stylesheet" type="text/css">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64707958-1', 'auto');
  ga('send', 'pageview');
</script>
</head>
<body>
<div id="wrapper">
	<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="/">Tweet Jukebox</a>	</div>
    <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
		<ul class="nav navbar-nav navbar-cls-top" style="cursor:pointer;">
            <? require_once($_SERVER['DOCUMENT_ROOT']."/include/storeNav.php") ; ?>			
		</ul>			
	</div>
  </nav>
  <!-- /. NAV TOP  -->  
  <nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
      <ul class="nav" id="main-menu">
        <li class="text-center">
			<img src="/images/tweetjukebox3.png" width="828" height="645" class="user-image img-responsive"/>		
		</li>
		<li>
			<a href="/members/store/"><i class="fa fa-refresh"></i> Show All Items</a>
		</li>
	  	<li>
			<a data-toggle="modal" data-target="#searchStoreDiv"><i class="fa fa-search"></i> Keyword Search</a>		
		</li>
		<li>
			<a data-toggle="modal" data-target="#searchStoreDiv2"><i class="fa fa-search"></i> Title Search</a>		
		</li>
		<li>
			<a href="/members/store/index.php?s=m"><i class="fa fa-download"></i> Most Downloads</a>	
		</li>
		
		<li>
			<a href="/members/store/index.php?s=r"><i class="fa fa-rocket"></i> Highest Rated</a>	
		</li>
		
		<hr>
		
		<li>
			<a href="/members/"><i class="fa fa-user"></i> Return To My Portal</a>
		</li>
		
		
		
		
	  </ul>	
    </div>
  </nav>
  <!-- /. NAV SIDE  --> 

<div id="page-wrapper" >
    <div id="page-inner"> 
 

<?
$maxRows = 12;
$pageNum = 0;
$currentPage = $_SERVER['QUERY_STRING'];


if (isset($_GET['pageNum'])) {
  $pageNum = $_GET['pageNum'];
}
$startRow = $pageNum * $maxRows;


$SQL = "select store_items.*, users.screen_name as from_name from store_items join users on users.user_id = store_items.user_id where store_items.status = 1 ";

if($_REQUEST['k']) {
	$searchFor = $_REQUEST['k'];
	$SQL .= " and (keyword_1 like '%".$searchFor."%' or keyword_2 like '%".$searchFor."%' or keyword_3 like '%".$searchFor."%'  or description like '%".$searchFor."%' )";
}

if($_REQUEST['t']) {
	$searchFor = $_REQUEST['t'];
	$SQL .= " and (item_name like '%".$searchFor."%' )";
}

if($_REQUEST['s'] == 'm') {
	$SQL .= "order by downloads DESC, item_name ";
} else {
	$SQL .= "order by item_name ";
}	

if($_REQUEST['s'] == 'r') {
	$SQL = "select store_items.*, users.screen_name as from_name,
	sum(oneStars * 1 + twoStars * 2 + threeStars * 3 + fourStars * 4 + fiveStars * 5) 
	/  
	sum(oneStars + twoStars + threeStars + fourStars + fiveStars) as rating
	from store_items
	join users on users.user_id = store_items.user_id
	where oneStars != 0 or twoStars != 0 or threeStars != 0 or fourStars != 0 or fiveStars != 0
	group by id
	union select store_items.*, users.screen_name as from_name,
	sum(0) as rating from store_items
	join users on users.user_id = store_items.user_id
	where oneStars = 0 and twoStars = 0 and threeStars = 0 and fourStars = 0 and fiveStars = 0
	order by rating DESC, item_name";
}

$SQL2 = getPagingQuery($SQL, $maxRows ); 

$pagingLink = getPagingLink($SQL, $maxRows, $_SERVER['QUERY_STRING']);

$result = mysqli_query($conn, $SQL2);



//$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn)) ;
if(mysqli_num_rows($result)) { 

	$row = mysqli_fetch_assoc($result);	
	?>
	  <h2 align="center">Jukebox Library </h2>
	  <center><span id="storeMsg">&nbsp;</span></center>
	  <div class="row">
	  <?				
		do {
			foreach( $row AS $key => $val ) {
				$$key = stripslashes( $val );					
			}
			
			$score = $oneStars * 1 + $twoStars * 2 + $threeStars * 3 + $fourStars * 4 + $fiveStars * 5 ;
			$count = $oneStars + $twoStars + $threeStars + $fourStars + $fiveStars ;
			if($count != 0) $rating = number_format($score / $count, 2) . " stars" ; else $rating = "Unrated";
								
			if($album_cover == '') $album_cover = 'tjbIcon_128.png';
	?>	
		<div class="col-lg-3 text-center clearfix" style="height:300px;" >
		  <? 
			$SQL2 = "select id from user_schedule where user_id = '".$_SESSION['access_token']['user_id']."' and storeItemID = '$id' ";						
			$result2 = mysqli_query($conn, $SQL2) or die(mysqli_error($conn)) ;
			if(mysqli_num_rows($result2) == 1) {
			?>
				<img src="https://s3.amazonaws.com/jukebox-store-images/<? echo $album_cover;?>" height="100px" data-toggle="tooltip" data-placement="top" title="<? echo $description;?>" onDblClick="gotItAlready('<? echo str_replace("'","\'",$item_name); ?>','<? echo $from_name; ?>')" >
				<br><b>Already Purchased!</b>
			  <?
			} else {
				if($canPurchase == '1') {
			?>	
					<img src="https://s3.amazonaws.com/jukebox-store-images/<? echo $album_cover;?>" height="100px" data-toggle="tooltip" data-placement="top" title="<? echo $description;?>" onDblClick="confirmAddStoreItem('<? echo $id; ?>','<? echo str_replace("'","\'",$item_name); ?>','<? echo $from_name; ?>')" >
					<br><button class="btn-xs btn" onClick="confirmAddStoreItem('<? echo $id; ?>','<? echo str_replace("'","\'",$item_name); ?>','<? echo $from_name; ?>')">Download</button>
			  <?
			  	} else {
			  ?>	
					<img src="https://s3.amazonaws.com/jukebox-store-images/<? echo $album_cover;?>" height="100px" data-toggle="tooltip" data-placement="top" title="<? echo $description;?>" onDblClick="overQuota('<? echo str_replace("'","\'",$item_name); ?>','<? echo $from_name; ?>')" >
					<br><b class="small">Juke Box Limit Met</b>
			  <?	
				}
			}
			?>
		    <br><? echo $item_name; ?>
			<br />By: <? echo $from_name; ?>
			<br />Downloaded: <? echo $downloads;?>
			<br />Rating: <? echo $rating; ?>
		</div>	
	  <? } while($row = mysqli_fetch_assoc($result));	?>	
	  </div>  

	<div class="container">
	<div class="row text-center">
		<div class="col-lg-2 col-md-2 col-sm-2"></div>
		<div class="col-lg-8 col-md-8 col-sm-8"><? echo $pagingLink?></div>
		<div class="col-lg-2 col-md-2 col-sm-2"></div>
	</div>
	</div>


<?
} else { 
?>
	<h2 align="center">Jukebox Library </h2>
	  <div class="row">
		<div class="col-sm-12 text-center" style="padding-bottom:50px; padding-top:100px;" >
			<h3 class="warning">No items found</h3>
		</div>
	  </div>	
<?
}
?>


<div class="container" >
	<div class="row" style="padding-top:50px;">
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<p align="center">
   			<span class="left">tj.local &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> | <!--a href="/plans.php">PLANS</a> | --><a href="/contact">CONTACT US</a> | <a href="/privacy">PRIVACY POLICY</a> | <a href="/terms">TERMS AND CONDITIONS</a></span>			</p>
		</div>	
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
	</div>
</div>
</div>	
</div>	 
</div>
<script src="/assets/js/morris/raphael-2.1.0.min.js"></script>
<script src="/assets/js/morris/morris.js"></script>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});

function getThisItem(i,n,a) {
	if(confirm("Are you sure you want "+n+" by "+a) ) {
	
	
	}
}

function gotItAlready(n,a) {

	if(confirm("You already have "+n+" by "+a) ) {
	}
}

function overQuota(n,a) {
	if(confirm("We're sorry, but you are over your Jukebox limit and can't get "+n+" by "+a+" at this time.") ) {
	}
}
</script>
<div class="modal fade" id="searchStoreDiv" tabindex="-1" role="dialog" aria-labelledby="mySchModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       	 		<h4 class="modal-title" id="mySchModalLabel">Search Store Items Keywords</h4>
      		</div>
			<div class="modal-body">	 			 
				<form class="c-form" accept-charset="utf-8">  
					<label for="searchStoreTerm">Search Term</label>
					<input type="text" name="searchStoreKeyword" id="searchStoreKeyword" value=""  />																				
					<p align="center"><button type="button" onClick="searchStore('k', $('#searchStoreKeyword').val())">Submit</button></p>
				</form>
			</div>
		</div>	
	</div>			
</div>

<div class="modal fade" id="searchStoreDiv2" tabindex="-1" role="dialog" aria-labelledby="mySchModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       	 		<h4 class="modal-title" id="mySchModalLabel">Search Store Items Titles</h4>
      		</div>
			<div class="modal-body">	 			 
				<form class="c-form" accept-charset="utf-8">  
					<label for="searchStoreTerm">Search Term</label>
					<input type="text" name="searchStoreKeyword" id="searchStoreTitle" value=""  />																				
					<p align="center"><button type="button" onClick="searchStore('t', $('#searchStoreTitle').val())">Submit</button></p>					
				</form>
			</div>
		</div>	
	</div>			
</div>

<script>
$(document).ready(function() {  
  
    $('textarea[maxlength]').keyup(function(){  
        //get the limit from maxlength attribute  
        var limit = parseInt($(this).attr('maxlength'));  
        //get the current text inside the textarea  
        var text = $(this).val();  
        //count the number of characters in the text  
        var chars = text.length;  
  
        //check if there are more characters then allowed  
        if(chars > limit){  
            //and if there are use substr to get the text before the limit  
            var new_text = text.substr(0, limit);  
  
            //and change the current text with the new text  
            $(this).val(new_text);  
        }  
    });  
  
});  


$('#searchStoreDiv').on('shown.bs.modal', function () {
    $('#searchStoreTerm').focus();
})
function searchStore(o, s) {											
	$('#searchStoreDiv').modal('hide');
	//location.assign( "http://tj.local/members/store/index.php?" + o + "=" + s );
	location.assign( "/members/store/index.php?" + o + "=" + s );
	/*
	
	$.ajax({
		type: "get",
		url: "/members/store/getStoreItems.php",
		data: { 'searchStoreTerm': $('#searchStoreTerm').val() 	} ,
		dataType: "html"
	})
	.done(function( msg ) { 
		$('#masterDiv').html(msg);
	});			
	*/
}

function confirmAddStoreItem(i, n, a) {												
	//$('#sbtn1').click();
	$('#store-item-name').html(n);
	$('#storePurchaseItemID').val(i);
	$('#storeModal').modal('toggle');
	$('#storeTweet').val("I just downloaded "+n+" by @" + a + " from Tweetjukebox.com - check it out! It's awesome. #tweetjukebox");		
}

function addStoreItem() {
	//alert("item is " + $('#storePurchaseItemID').val() ); 		
	
	if( $('#tweet-it').prop('checked') ) {
		tweetFromJukebox($('#storeTweet').val());
	}	
	
	
	$.ajax({
		type: "post",
		url: "/members/purchaseStoreItem.php",
		data: {
			'itemID': $('#storePurchaseItemID').val()
		} ,
		dataType: "html"
	})
	.done(function( msg ) { 								
		jbOptions(msg);
		setTimeout(function () {
		   $('#storeMsg').fadeTo(0, 2000);
		   $('#storeMsg').html('Item Added!');
		   $('#storeMsg').fadeTo(2000, 0);		   
		}, 200); 
	        location.reload();	
	});
		
	
}


</script>
<!--Store Modal-->
<div class="modal fade" id="storeModal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
        <h2>Add <span id="store-item-name"></span></h2>
      </div>
      <div class="modal-body">	  
        <div class="row">
		<form name="store-purchase">
           <div class="col-lg-12">
		   	<textarea name="storeTweet" id="storeTweet" class="form-control" maxlength="140"></textarea>
			<input type="checkbox" checked="checked" name="tweet-it" id="tweet-it"  > Tweet my followers about my new Jukebox!
		   </div>		   
		   <input type="hidden" id="storePurchaseItemID" name="storePurchaseItemID" value="" />	 
        </form>		
		</div>
		<div class="row text-center">
			<div class="col-lg-12" style="padding-top:20px;">
				<button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onClick="addStoreItem();">Continue</button>
			</div>
		</div>	
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
</body>
</html>
