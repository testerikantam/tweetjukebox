<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<script src="/assets/js/jquery-1.10.2.js"></script>
<script src="/include/js_boot.js"></script>
<?
if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');
$user_id = $_SESSION['access_token']['user_id'];
$storeItemID = mysqli_real_escape_string($conn, $_REQUEST['storeItemID'] ); 
$currentStorePage = mysqli_real_escape_string($conn, $_REQUEST['currentStorePage'] ); 
$rowID = $_REQUEST['rowID'];

// aws code
require_once("/home/yauhen/dev/tweetjunkebox/tweetjunkebox/aws/aws-autoloader.php");

use Aws\Common\Aws;
// Create the AWS service builder, providing the path to the config file
$aws = Aws::factory('/home/yauhen/dev/tweetjunkebox/tweetjunkebox/aws/config.php');
$s3Client = $aws->get('s3');
?>
<?
if($_POST['editStoreRowID'] != '' ) {
	
	$key = 'AIzaSyDWTkJNT9grybmoRDpvy73yNcM3xkfcHM8';
	$googer = new GoogleURLAPI($key);

	
	$rowID = $_POST['editStoreRowID'];
	$tweet = mysqli_real_escape_string($conn, $_POST['tweet'] );	
	/*
	if($_SESSION['isAdmin'])  {
		$tweet_text_parts = explode(" ", $tweet_text);
		$newText = '';
		foreach($tweet_text_parts as $word) {		
			if(substr( strtoupper($word), 0, 4) == 'WWW.' ) {
				$response = $googer->shorten($word);
				if($response) $word = $response;
			}
			if(substr( strtoupper($word), 0, 7) == 'HTTP://' ) {
				$response = $googer->shorten($word);
				if($response) $word = $response;
			}
			if(substr( strtoupper($word), 0, 7) == 'HTTPS://' ) {
				$response = $googer->shorten($word);
				if($response) $word = $response;
			}
			$newText .= $word. " ";
		}
		$tweet_text = rtrim($newText);
	}
	*/
	
	$storeItemID = mysqli_real_escape_string($conn, $_POST['storeItemID'] ); 
	if($rowID == 0 ) {
		$SQL = "insert into store_items_tweets set  tweet = '$tweet', store_item_id = '$storeItemID', photo='' ";						
		$result = mysqli_query($conn, $SQL) or die(mysqli_error());	
		
		$rowID = $conn->insert_id ;
		
	
	}
	
	if($_FILES['myPhoto']['name']>'') {  // we are trying to upload an image<strong></strong>
	
		$fileName = $_FILES['myPhoto']['name'];
		$tmpName  = $_FILES['myPhoto']['tmp_name'];
		$fileSize = $_FILES['myPhoto']['size'];	
		$fileType = $_FILES['myPhoto']['type'];
	
		if($fileSize > 3145728) {
?>
			<script type='text/javascript'>
				
				alert("Your file is too large.  Please reduce the size to under 3Mb and try again");
				//var object = parent.jQuery("#containment_wrapper");
				//var currentDesign = parent.jQuery("#containment_wrapper").html();
				//parent.jQuery("#containment_wrapper").html(currentDesign);		
				setTimeout(function () {
				   try {
					  parent.window.hs.getExpander().close();
					  parent.window.viewMasterList(<? echo $storeItemID; ?>) 
				   } catch (e) {}
				}, 200);      
			</script>
	
<?	
			die();
		} else {
			
			
			if($rowID != '') {								
				
				$objects = $s3Client->getIterator('ListObjects', array(
					'Bucket' => 'jukebox-member-images',
					'Prefix' => $rowID . "_JB_" . $user_id
				));
				
				
				foreach ($objects as $object) {															
					$deleteResult = $s3Client->deleteObject(array(
						'Bucket' => 'jukebox-member-images',
						'Key'    => $object['Key'] 
					));	
				}								
			}	
			
			
			$xFileParts = explode(".", $fileName);
			$numberOfParts = count ($xFileParts) - 1;
			
			$mime = image_type_to_mime_type(exif_imagetype($_FILES['myPhoto']['tmp_name']));
			
			$fileName = $rowID . "_" . $user_id. "_JB_" . rand(1,1000) . "." . $xFileParts[$numberOfParts];									
			// Upload an object by streaming the contents of a file
			// $pathToFile should be absolute path to a file on disk
			$result = $s3Client->putObject(array(
				'Bucket'     => 'jukebox-member-images',
				'Key'        => $fileName,
				'ContentType' => $mime,
				'SourceFile' => $_FILES['myPhoto']['tmp_name']				
			));			
			$SQL = "update  store_items_tweets set tweet = '$tweet', photo = '$fileName' where  id = '$rowID' ";				
		}
	} else {		
		if($_POST['removePhoto']) {
			$SQL = "update  store_items_tweets set tweet = '$tweet', photo = '' where id = '$rowID' ";
		} else {
			$SQL = "update  store_items_tweets set tweet = '$tweet' where id = '$rowID' ";
		}	
	}
	$result = mysqli_query($conn, $SQL);
	
	$SQL = "select photo from store_items_tweets where id = '$rowID' ";
	$result = mysqli_query($conn, $SQL);
	if(mysqli_num_rows($result)) {
		$row = mysqli_fetch_assoc($result);
		$photo = $row['photo'];
	}
?>	
	<script type='text/javascript'>
		
		//$('#masterListForm')[0].reset();
		
		//document.getElementById('tweet_text').value = '';
		
		parent.jQuery('#text_<? echo $rowID;?>').html('<? echo htmlentities($tweet);?>');	
		
		//parent.jQuery('#photoDialog<? echo $rowID;?>').html('<img id="currentTweetPhoto_<? echo $row['id']; ?>" src="<? echo $photo; ?>" style="max-width:450px;"  />');

		<? if($photo) { ?>		
			parent.jQuery('#currentTweetPhoto_<? echo $rowID; ?>').attr('src', 'https://s3.amazonaws.com/jukebox-member-images/<? echo $photo; ?>');
		<? } else { ?>
			parent.jQuery('#currentTweetPhoto_<? echo $rowID; ?>').attr('src', '/images/spacer.gif');
		<? }?>	
		<? if($_POST['tweet_now'] == '1' ) { ?>
			tweetFromJukebox('<? echo urlencode(stripslashes($tweet));?>', '<? echo $rowID; ?>');

			var tweetDate = '#tweetDate_' + String(<? echo $rowID; ?>);
			parent.jQuery(tweetDate).text('Just Tweeted') ;


	     <? } ?>
		//	parent.masterList(<? echo $storeItemID; ?>);
		//masterList(<? echo $storeItemID; ?>);
		//var currentDesign = parent.jQuery("#containment_wrapper").html();										
		//parent.jQuery("#containment_wrapper").html(currentDesign);		
		setTimeout(function () {
		   try {
			  parent.window.hs.getExpander().close();
			  <? if($_POST['editStoreRowID'] == 0 ) { ?>
			  parent.window.viewStoreList(<? echo $storeItemID; ?>); // only refresh if adding a new row
			  <? } ?>
		   } catch (e) {}
		}, 200);      
	</script>
	
<?	
}
?>
<?
$tweet = '';
if($rowID > 0 ) {
	$SQL = "select * from store_items_tweets where id = '$rowID' ";
	$result = mysqli_query($conn, $SQL);
	if(mysqli_num_rows($result)) {
		$row = mysqli_fetch_assoc($result);
		foreach( $row AS $key => $val ) {
			$$key = stripslashes( $val );					
		}
	} else {
		header('Location: /oops.php');
		exit();
	}
} 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-Equiv="Cache-Control" Content="no-cache">
<meta http-Equiv="Pragma" Content="no-cache">
<meta http-Equiv="Expires" Content="0">
<title>Untitled Document</title>
<link rel="stylesheet" href="/assets/css/style.css">
<link rel="stylesheet" href="/assets/fonts/raphaelicons.css">
<link rel="stylesheet" href="/assets/css/main.css">

<script src="//code.jquery.com/jquery-1.11.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<!--link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" /-->
<link rel="stylesheet" href="/assets/css/jquery-ui.css">
<script>window.jQuery || document.write('<script src="/assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
<script src="/include/js_boot.js"></script>
<script>
function showRemainingCharacters(msg, photo) {
	if(photo.length > 0 ) {	
		var c = 140 - 25 - twttr.txt.getTweetLength(msg); 	
	} else {
		var c = 140 - twttr.txt.getTweetLength(msg);
	}
	<? if ($photo) { ?>
		c = c - 25;		
	<? } ?>		
	if(parseInt(c) >= 0) {
		$('#masterLengthError').html('');	
	} else {
		$('#masterLengthError').html('Tweet is too large');	
	}	
	$('#editMessageCount').val(c);
	

}
</script>
</head>

<body>
<script src="/include/twitter-text.js" ></script>
<div title="Jukebox">  
  <form class="c-form" accept-charset="utf-8" id="masterListForm" method="post" enctype="multipart/form-data" action="editStoreRow.php" >
    <input type="hidden" name="editStoreRowID" id="editStoreRowID" value="<? echo $rowID; ?>" />
	<input type="hidden" name="currentStorePage" id="currentStorePage" value="<? echo $currentStorePage;?>"  />
  <fieldset>
    <input id="editMessageCount" type="text" readonly="" value="" style="display:inline-block; width:60px;"><span id="masterLengthError" style="display:inline-block; color:#FF0000;"></span>	
    <textarea name="tweet" cols="30" rows="10" id="tweet" onKeyUp="showRemainingCharacters(this.value, $('#myPhoto').val() );"><? echo $tweet; ?></textarea>

	<!--p align="center"><input type="button" name="submitButton" id="submitButton" value="Submit & Tweet It Now" onClick="document.getElementById('tweet_now').value = '1'; checkMasterEdit();" /></p-->

	<p align="center"><input type="button" name="submitButton" id="submitButton" value="Submit" onClick="checkMasterEdit();" /></p>
	<img id="wait_screen" src="/images/wait.GIF" style="float:right; visibility:hidden; "  />
	<label for="myPhoto">Add Photo (optional)</label>
	<input type="hidden" name="tweet_now" id="tweet_now" value="0" />
	<input type="hidden" name="MAX_FILE_SIZE" value="0" />
	<input type="hidden" id="storeItemID" name="storeItemID" value="<? echo $storeItemID; ?>"  />
	<input type="file" name="myPhoto" id="myPhoto"  onChange="readImage(this); " />
	<input type="hidden" name="hasPhoto" id="hasPhoto" value="<? if($photo) echo '1'; else echo '0' ; ?>"  />
  </fieldset>      
  <div style="min-height:150px; vertical-align:text-top;">
  <? if($photo) { ?>Remove this photo <input name="removePhoto" id="removePhoto" type="checkbox" value="1"  />&nbsp;<? } ?>
  <img id="tweetPhoto" src="https://s3.amazonaws.com/jukebox-member-images/<? if($photo) echo $photo; else echo "spacer.gif"; ?>" alt="" height="100" />
  </div>
  <div>    
  </div>
  </form>
</div>
<script>

$( document ).ready(function() {
	
	
	showRemainingCharacters( $('#tweet').val(), $('#myPhoto').val() );			
	
	
});
</script>
<script>
function checkMasterEdit() {
	
	if( parseInt($('#editMessageCount').val()) >=0 && parseInt($('#editMessageCount').val()) <= 138  ) {
		$('#wait_screen').css('visibility', 'visible'); 
		document.getElementById('masterListForm').submit();
	} else {
		if( parseInt($('#editMessageCount').val()) < 0 ) {
			$('#masterLengthError').html('Tweet text too long!');
		} else {
			$('#masterLengthError').html('Tweet text too short!');
		}	
	}	
}

function readImage(input) {  		
	var imageExts = ["jpg","jpeg","png", "gif"];
	var filePathName = $('#myPhoto').val();
	var fileParts = filePathName.split(".");
	var ext = fileParts[fileParts.length -1];
	ext = ext.toLowerCase();	
	
	
	if(imageExts.indexOf(ext) != -1) {   // only render if image is ok to display					
		if (input.files && input.files[0])  {
						
			var reader = new FileReader();
			reader.onload = function (e) {									  
				$('#tweetPhoto')
				.attr('src',e.target.result)
				.height(100);				
			};
			reader.readAsDataURL(input.files[0]);			
			var remains = 140 - 25 - twttr.txt.getTweetLength($('#tweet').val()); 
			$('#editMessageCount').val(remains);
	
	   }
	} else {
		alert("Invalid format. Must be .png, .gif, or .jpg types only");
		$('#myPhoto').val('');
	}	
}
</script>
<?
// Declare the class
class GoogleUrlApi {
	
	// Constructor
	function GoogleURLAPI($key,$apiURL = 'https://www.googleapis.com/urlshortener/v1/url') {
		// Keep the API Url
		$this->apiURL = $apiURL.'?key='.$key;
	}
	
	// Shorten a URL
	function shorten($url) {
		// Send information along
		$response = $this->send($url); 
		// Return the result
		return isset($response['id']) ? $response['id'] : false;
	}
	
	// Expand a URL
	function expand($url) {
		// Send information along
		$response = $this->send($url,false);
		// Return the result
		return isset($response['longUrl']) ? $response['longUrl'] : false;
	}
	
	// Send information to Google
	function send($url,$shorten = true) {
		// Create cURL
		$ch = curl_init();
		// If we're shortening a URL...
		if($shorten) {
			curl_setopt($ch,CURLOPT_URL,$this->apiURL);
			curl_setopt($ch,CURLOPT_POST,1);
			curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode(array("longUrl"=>$url)));
			curl_setopt($ch,CURLOPT_HTTPHEADER,array("Content-Type: application/json"));
		}
		else {
			curl_setopt($ch,CURLOPT_URL,$this->apiURL.'&shortUrl='.$url);
		}
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		// Execute the post
		$result = curl_exec($ch);
		// Close the connection
		curl_close($ch);
		// Return the result
		return json_decode($result,true);
	}		
}

?>
</body>
</html>