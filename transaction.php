<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<? 
require_once $_SERVER['DOCUMENT_ROOT'].'/braintree-php-3.1.0/lib/Braintree.php';
//  sandbox 
Braintree_Configuration::environment('sandbox');
Braintree_Configuration::merchantId('zsbs7swsdwdzvmwg');
Braintree_Configuration::publicKey('yb7qkdry9qfvzbn9');
Braintree_Configuration::privateKey('36ae489f8f321abd38f0971f5f1ff2cf');

// prodcution
//Braintree_Configuration::environment('production');
//Braintree_Configuration::merchantId('ry95gz2bg2g4xd9s');
//Braintree_Configuration::publicKey('3xmty3qwmphwjfs9');
//Braintree_Configuration::privateKey('06502b4f7b1d9893d8bd88e7e6f02a2c');


?>
<?

$userID = $_SESSION['access_token']['user_id'];

// this either succeeds or fails if cc is invalid, or if the customer already was created


/*
print "<pre>";
print_r($_POST);
die();
*/


$result = Braintree_Customer::create(array(
	'id' => $_SESSION['access_token']['user_id'],
	'firstName' => $_POST['user_first_name'],
    'lastName' => $_POST['user_last_name'],
    'email' => $_POST['email'],
    'creditCard' => array(
        'number' => $_POST["number"],
        'expirationMonth' => $_POST["month"],
		'expirationYear' => $_POST["year"],
        'cvv' => $_POST["cvv"],
		'options' => array(
            'verifyCard' => true
        )
    )
));

$planID = $_POST['pid'];

if($result->success  /*&&  !$result->_attributes->errors*/) { // we created customer and should now have the customer's CC Token from BT	
	$ccTokenID = $result->customer->creditCards[0]->token;
	$result = Braintree_Subscription::create(array(
	  'paymentMethodToken' => $ccTokenID,
	  'planId' => $planID,
	  'id' => $planID . "_" . $_SESSION['access_token']['user_id']  // plan is Name_userID
	));
	if ($result->success) {
		$SQL = "update users set planName = '$planID' where user_id = '$userID'  ";
		$result = mysqli_query($conn, $SQL) or die(mysqli_error());
		$_SESSION['planName'] = $planID;
		//print_r("success!: " . $result->transaction->id);
	} else {  // something went wrong creating the subscription - this shouldn't happen 	
		foreach($result->errors->deepAll() AS $error) {
			echo($error->code . ": " . $error->message . "\n");
		}
	}	
} else {	// something went wrong creating customer / validating credit card  - we can't assume what it was but error 91609 means the customer already exists	
	foreach($result->errors->deepAll() AS $error) {
		//echo($error->code . ": " . $error->message . "\n");
		if($error->code == '91609') { // duplicate customer number
			$customer = Braintree_Customer::find($userID);  // get the user information 
			$token = $customer->creditCards[0]->token;
			// update customer with new payment info 
			$result = Braintree_Customer::update(
			  $userID,
			  array(
				'firstName' => $_SESSION['user_first_name'],
				'lastName' => $_SESSION['user_last_name'],
				'email' => $_SESSION['email'],
				'creditCard' => array(
					'number' => $_POST["number"],
					'expirationMonth' => $_POST["month"],
					'expirationYear' => $_POST["year"],
					'cvv' => $_POST["cvv"],
					'options' => array(
						'verifyCard' => true,
						'updateExistingToken' => $token
					)					
				 )
			  )
			);
			

			

			if($result->success) {			
				$subscriptionID = $customer->creditCards[0]->subscriptions[0]->id;			
				$newSubscriptionID = $planID."_".$userID;				
				$result = Braintree_Subscription::update($subscriptionID, array(
					'id' => $newSubscriptionID,
					'planId' => $planID,
					'price' => $amount
				));
				if($result->success) { // we be good 
					$SQL = "update users set planName = '$planID' where user_id = '$userID'  ";
					$result = mysqli_query($conn, $SQL) or die(mysqli_error());
					$_SESSION['planName'] = $planID;
					//print_r("success!: " . $result->transaction->id);
				} else {  // something went wrong creating the subscription - this shouldn't happen 	
					foreach($result->errors->deepAll() AS $error) {
						echo($error->code . ": " . $error->message . "\n");
					}
					header("Location: subscribe.php?err=1&pid=".$_REQUEST['pid']);																
				}
			} else {
				header("Location: subscribe.php?err=1&pid=".$_REQUEST['pid']);
			}	
		} else {
			header("Location: subscribe.php?err=1&pid=".$_REQUEST['pid']);
		}
	}						
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Transaction Receipt</title>
<link  href="http://fonts.googleapis.com/css?family=Oswald:regular" rel="stylesheet" type="text/css" >
<link href='http://fonts.googleapis.com/css?family=Junge' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="../tweetjukebox/assets/css/style.css">
<link rel="stylesheet" href="../tweetjukebox/assets/fonts/raphaelicons.css">
<link rel="stylesheet" href="../tweetjukebox/assets/css/main.css">
<link rel="shortcut icon" href="/../tweetjukebox/favicon.ico?v=2" type="image/x-icon">
<link rel="icon" href="/../tweetjukebox/favicon.ico" type="image/x-icon">
</head>
<body>
<span class="member-icon" style="font-size:24px; color:#00FF00; float:right;">W</span>
<img src="/../tweetjukebox/images/tweetJukebox_75.png" border="0" style="float:left" />	
<h2 align="center"> Payment Receipt </h2>
<p>&nbsp;</p>
<p align="center">Thank you for subscribing in the <? echo $planID; ?> plan!</p>
<p>We're glad you joined us and we're excited to help you make the most of your Twitter&#8482; account.  </p>
<p>Your payment has been successfully processed and you are now enrolled in a monthly subscription plan that includes all the benefits as described on our website.</p>
<p>&nbsp; </p>
<p>&nbsp;</p>
</body>
</html>
