<?php  include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Tweet Jukebox</title>
<meta name="description" content="Tweet Jukebox - Thinking Inside the Box">
<!-- BOOTSTRAP STYLES-->
<link href="../assets/css/bootstrap_v3.3.4.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="../assets/css/font-awesome.css" rel="stylesheet" />
<!-- MORRIS CHART STYLES-->
<link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="../assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="jquery.timepicker.css" />
<link href="/members/chart.css" rel="stylesheet" type="text/css">
<!-- JQUERY SCRIPTS -->
<script src="../assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../assets/js/bootstrap_v3.3.4.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="../assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../assets/js/custom.js"></script>
<script src="/include/js_boot.js"></script>
<script type="text/javascript" src="jquery.timepicker.js"></script>
<link rel="stylesheet" href="/highslide-4.1.13/highslide/highslide.css" type="text/css" />
<!-- 9d83e18a764256552baf6956b777f190 -->
<script type="text/javascript" src="/highslide-4.1.13/highslide/highslide-full.packed.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/highslide-4.1.13/highslide/graphics/';
	hs.showCredits = false;
    hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = .45;
	hs.zIndexCounter = 2000;
//	hs.objectLoadTime = 'after';
//	hs.forceAjaxReload = 'true';
</script>
<script src="../assets/js/dropdown.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64707958-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="wrapper">	    
	<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="/">Tweet Jukebox</a> 		
	</div>
    <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
		<ul class="nav navbar-nav navbar-cls-top" style="cursor:pointer;">
            <? require_once($_SERVER['DOCUMENT_ROOT']."/include/mainNav.php") ; ?>	
		</ul>
	</div>
</div>	
<div class="container" >
	<div class="row">
	<div class="col-md-1 col-sm-1 col-xs-1"></div>
	<div class="col-md-10 col-sm-10 col-xs-10">

   <section role="banner">
       <hgroup>
         <h1>Terms Of Use</h1>
       </hgroup>
	   
       <article role="main" class="clearfix contact">
<div class="post" style="width:auto;">
<P>TweetJukebox.com  (&quot;Tweet Jukebox&quot;) is the owner and operator of&nbsp;a Twitter&#8482; automated account management &nbsp;web service ("the Service") provided via a website interface located at <A href="http://tj.local">tj.local</A> ("the Site"). Your use of the Service and the Site are governed by the terms and conditions set out below and as amended from time to time ("Terms of Use"). </P>
<P>TweetJukebox.com reserves the right to amend the Terms of Use at any time by posting the amended terms to the Site without further notice to you. Your use of the Service and/or the Site shall be deemed to constitute your knowledge and acceptance of the Terms of Use. If you do not agree to be bound by the Terms of Use, you should immediately cease all use of the Service and/or the Site.</P>
<P><STRONG>1. USE OF THE SITE AND SERVICE </STRONG></P>
<P>1.1 Tweet Jukebox hereby grants you a non-exclusive, non-transferable, limited right and license to access the Site and use the Service for your personal use only and otherwise in accordance with these Terms of Use. </P>
<P>1.2 You may make copies or "cache" pages of the Site, but only to the extent automatically done by your internet browser software as a part of process of accessing the Site or using the Service. Any other copying or use of the Site shall be an infringement of our copyright and shall be prosecuted to the full extent permitted by law. </P>
<P>1.3 You may not copy, modify, adapt, transmit, publicly perform or display, sell, distribute, publish, customize, add to, delete from, or create derivative works of any part of the Site. Any other use or exploitation the Site or the Services, other than as expressly authorised by the Terms of Use is strictly prohibited.</P>
<P>1.4 You agree that you will not use any script, software or mechanical device to access, monitor or copy the Service or interfere with the normal functioning of the Service, unless specifically authorised by Tweet Jukebox .</P>
<P>1.5 You will not engage in any conduct that in the discretion of Tweet Jukebox restricts or inhibits any other person from using or enjoying the Service. You agree to use the Service only for lawful purposes. You warrant and promise that you are at least eighteen (18) years of age or have specific permission from a parent or legal guardian to use the Service. You are prohibited from posting on or transmitting through the Service any unlawful, harmful, threatening, abusive, harassing, defamatory, vulgar, obscene, profane, hateful, racially, ethnically or otherwise objectionable material of any kind, including, but not limited to, any material which encourages conduct that would constitute a criminal offence, give rise to civil liability or otherwise violate any applicable local, state, national or international law. </P>
<P><STRONG>2. ALTERATION / DISCONTINUANCE OF THE SITE OR SERVICE </STRONG></P>
<P>2.1 Tweet Jukebox may discontinue or alter any aspect of the Site or Service, including, but not limited to: (a) Restricting the availability and/or scope of the Service for certain platforms and operating systems; (b) Restricting the times at which the Site and/or the Service is available; (c) Restricting the amount of use of the Service permitted by a particular user; and (d) Restricting or terminating a user's right to use the Site and the Service, at Tweet Jukebox's sole discretion and without prior notice.&nbsp; </P>
<P>2.2 In the event you have subscribed to one of our Paid Subscriptions, you may unsubscribe at any time, and your account will be deactivated.&nbsp; Charges applied up to the time you cancel will not be refunded and  your credit card will no longer be charged for the subscription.</P>
<P>2.3 In the event your paid subscription service is canceled (either by us or by you), your account will be deactivated and your credit card will no longer be charged. </P>
<P><STRONG>3. MONITORING OF THE SERVICE </STRONG></P>
<P>3.1&nbsp;Tweet Jukebox may electronically monitor the Service and the users of the Service in order to ensure compliance with these Terms of Use and may disclose any information, record or electronic communication of a user of the Service: (a) In compliance with any law, regulation or authorized governmental request; (b) If such disclosure is necessary for the continued operation of the Service; or (c) To protect the rights or property of Tweet Jukebox or its partners.</P>
<P><STRONG>4. INTERNET ACCESS CHARGES </STRONG></P>
<P>4.1 You shall be solely responsible and liable for all charges (including internet access fees and associated charges) incurred by you in order to connect to the Site and/or use the Service. </P>
<P><STRONG>5. DISCLAIMER OF WARRANTY AND LIMITATION OF LIABILITY </STRONG></P>
<P>5.1 In relation to the Site and the Service, Tweet Jukebox disclaims any and all warranties (either express or implied) to the full extent permitted by law, including without limitation: (a) Any warranties regarding the availability or accuracy; (b) Any warranties of title, merchantability or fitness for a particular purpose.</P>
<P>5.2 Neither&nbsp;Tweet Jukebox nor any of its partners, agents, affiliates or content providers shall be liable for any direct, indirect, incidental, special or consequential damages arising out of or incidental to the use of the Site or the Service or inability to gain access to the Site or use the Service. </P>
<P>5.3 THIS DISCLAIMER OF LIABILITY APPLIES TO ANY DAMAGES OR INJURY CAUSED BY ANY FAILURE OF PERFORMANCE, ERROR, OMISSION, INTERRUPTION, DELETION, DEFECT, DELAY IN OPERATION OR TRANSMISSION, COMPUTER VIRUS, COMMUNICATION LINE FAILURE, THEFT OR DESTRUCTION OR UNAUTHORIZED ACCESS TO, ALTERATION OF, OR USE OF, THE SITE OR THE SERVICE, WHETHER ARISING OUT OF BREACH OF WARRANTY, BREACH OF CONTRACT, TORTUOUS BEHAVIOR, NEGLIGENCE, OR UNDER ANY OTHER CAUSE OF ACTION. YOU SPECIFICALLY ACKNOWLEDGE THAT Tweet Jukebox IS NOT LIABLE FOR THE DEFAMATORY, OFFENSIVE OR ILLEGAL CONDUCT OF OTHER USERS OR THIRD PARTIES AND THAT THE RISK OF INJURY FROM THE FOREGOING RESTS ENTIRELY WITH YOU. </P>
<P><STRONG>6. INDEMNITY </STRONG></P>
<P>6.1 You agree to indemnify, keep indemnified and forever hold harmless, Tweet Jukebox , its partners, agents, affiliates and content partners from any costs (including legal costs), loss, damage, claims or disputes, which may arise out of or incidental to your use of the Site or the Service or from a breach of these Terms of Use.</P>
<P><STRONG>7. DISCLAIMER FOR THIRD PARTY WEBSITES </STRONG></P>
<P>7.1 The Site may contain links to websites of our advertisers or other third parties ("Third Party Websites"). Tweet Jukebox has no control over and shall not be responsible or liable for: (a) The price, quality, safety or legality of the goods or services available on or through Third Party Websites; (b) The truth or accuracy or legality of the content on the Third Party Websites or for the actions you might take in reliance on that content; or (c) The availability or technical capabilities of the Third Party Websites or the links provided to those Third Party Websites. </P>
<P><STRONG>8. INTELLECTUAL PROPERTY RIGHTS </STRONG></P>
<P>8.1 Copyright. The Site contains information, software, photos, video, graphics, music, sounds or other material ("Site Content"). With the exception of member submitted property, the Site Content was created and or is owned by Tweet Jukebox&nbsp; and is protected by applicable domestic and international copyright laws. Unless expressly permitted by these Terms of Use or elsewhere in the Site, you shall not copy, distribute, publish, perform, modify, download, transmit, transfer, sell, or license, reproduce, create derivative works from or based on, distribute, post, publicly display, frame, link, or in any other way exploit any part of the Site Content, in whole or in part. Links to the Site are only permitted upon express permission from and by arrangement with&nbsp;Tweet Jukebox . Any rights not expressly granted to you herein are reserved. All copyright infringements will be prosecuted to the full extent permitted by law.</P>
<P>8.2 Trademarks. "Tweet Jukebox" and the "Tweet Jukebox" logo are trademarks and service marks of TweetJukebox.com ("Tweet Jukebox Marks"). Any unauthorized use of the Tweet Jukebox Marks is strictly prohibited. Any product, service, or trade name other than those owned by&nbsp;Tweet Jukebox that identify a third party as the source thereof may, even if not so indicated, be the service mark or trademark of that respective entity or individual.</P>
<P>8.3 Claims of Copyright Infringement by third parties Tweet Jukebox supports the Digital Millennium Copyright Act ("DMCA") and encourages users to contact us via our agent at the address below, with any claims of alleged copyright infringement by Third Party Websites: Customer Care 5002 Garrick Court Tampa, FL 33624 Email: <A href="mailto:info@TweetJukebox.com">info@TweetJukebox.com</A> Please include "DMCA" in the subject line if sending us notice by email. All notices to us in regard to potential copyright infringement should include all of the following details: (a) A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed. (b) Identification of the copyrighted work claimed to have been infringed, or if a single covers multiple copyrighted works at a single Web site claim, a representative list of such works at that Web site. (c) Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit Tweet Jukebox to locate the material. (d) Information reasonably sufficient to permit&nbsp;Tweet Jukebox to contact the complaining party, such as an address, telephone number, and if available, an electronic mail address at which the complaining party may be contacted. (e) A statement that the complaining party believes in good faith, that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law. (f) A statement that the information in the notice is accurate, and under penalty of perjury, that the complaining party is authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.</P>
<P>8.4 Dealing with Copyright Infringers In the case of Third Party Websites who are the subject of a claim of copyright infringement, Tweet Jukebox reserves the right to remove and/or to disable any link or other access to such Third Party Websites and/or terminate the accounts of the owners of the said websites.</P>
<P><STRONG>9. OBJECTIONABLE MATERIAL </STRONG></P>
<P>9.1 Tweet Jukebox supports responsible parenting in respect of the use of the Site and the Service by children. We encourage parents to implement commercially available hardware and software filtering devices that will help protect your children from exposure to material that is offensive, objectionable, harmful, deceptive or otherwise inappropriate for children.</P>
<P>9.2 If you come across any Third Party Websites that you believe contains child pornography or other illegal material, please contact us by email at: <A href="mailto:info@TweetJukebox.com">info@TweetJukebox.com</A> and we will pass the information on to the relevant authorities.</P>
<P><STRONG>10. DATA COLLECTION AND YOUR PRIVACY </STRONG></P>
<P>10.1 Due to the nature of the Service, Tweet Jukebox does not ordinarily collect, store, use or disclose any personally identifying information of a user of the Site or the Service, unless you are an advertiser or otherwise make direct contact with us. In the event that you do provide us with such personal information, it will be dealt with in accordance with our Privacy Policy, which may be accessed by clicking on the relevant link on the homepage of this Site. </P>
<P>10.2 The Internet is a global computer network. By submitting your personal information to us electronically over the Internet, you agree to our collecting and processing your personal data in this manner. Tweet Jukebox shall not be responsible or liable for any loss or damage sustained as a result of interception of your personal data during transmission and/or the unauthorised use of this data by third parties. </P>
<P><STRONG>11. GENERAL </STRONG></P>
<P>11.1 Governing Law This Agreement shall be construed and controlled by the laws of the State of Florida. Further, the laws of the State of Florida will govern any dispute arising from the terms of this agreement or a breach of this Agreement. Customer agrees to personal jurisdiction by the State and Federal courts sitting in the State of Florida. </P>
<P>11.2 Entire Agreement This Agreement constitutes the entire agreement between the parties with respect to the subject matter contained herein and supersedes all previous and contemporaneous agreements, proposals and communications, written or oral between TweetJukebox.com representatives and you. TweetJukebox.com may amend or modify this Agreement or impose new conditions at any time upon notice from TweetJukebox.com to Customer as published through the Service. Any use of the Service by you after such notice shall be deemed to constitute acceptance by you of such amendments, modifications or new conditions. </P>
<P>11.3 Notices All notices given to you by Tweet Jukebox shall be sent to your nominated e-mail address. You may give notice to Tweet Jukebox by sending an e-mail addressed to <A href="mailto:info@TweetJukebox.com">info@TweetJukebox.com</A>.</P>
<P>11.4 Severability In the event that any provision of these Terms of Use are found by a court of competent jurisdiction to be invalid, illegal or unenforceable, such provision shall be severed from the Terms of Use and the remaining provisions shall remain in full force and effect. The parties further agree that the court should endeavour to give effect to the parties' intentions as reflected in the severed provision these Terms of Use should be interpreted to affect the intent of the parties, and the remaining provisions will remain in effect.</P>
<P>11.5 Section Headings The section headings contained herein are for reference purposes only and shall not in any way affect the meaning or interpretation of this agreement.</P>
<P>11.6 Waiver Tweet Jukebox's failure to exercise or enforce any right or provision of the Agreement shall not be deemed to be a waiver of such right or provision.</P>
<P>11.7 Arbitration Any dispute or claim arising out of or relating to the use of Site or the Services or these Terms of Use shall be settled by binding arbitration conducted by an independent arbitrator appointed by the Florida Courts. </P>   
</div>
     </article>
	   </section>
	   </div>
	   <div class="col-md-1 col-sm-1 col-xs-1"></div>
	</div>
</div>   
<div class="container" >
	<div class="row" style="padding-top:50px;">
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<p align="center">
   			<span class="left">tj.local &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> | <!--a href="/plans.php">PLANS</a> | --><a href="/contact">CONTACT US</a> | <a href="/privacy">PRIVACY POLICY</a> | <a href="/terms">TERMS AND CONDITIONS</a>
			</p>
		</div>	
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
	</div>
</div>	   
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
  <script src="assets/js/script.js"></script>
</body>
</html>