<?php  include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Tweet Jukebox</title>
<meta name="description" content="Tweet Jukebox - Thinking Inside the Box">
<!-- BOOTSTRAP STYLES-->
<link href="../assets/css/bootstrap_v3.3.4.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="../assets/css/font-awesome.css" rel="stylesheet" />
<!-- MORRIS CHART STYLES-->
<link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="../assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="jquery.timepicker.css" />
<link href="/members/chart.css" rel="stylesheet" type="text/css">
<!-- JQUERY SCRIPTS -->
<script src="../assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../assets/js/bootstrap_v3.3.4.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="../assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../assets/js/custom.js"></script>
<script src="/include/js_boot.js"></script>
<script type="text/javascript" src="jquery.timepicker.js"></script>
<link rel="stylesheet" href="/highslide-4.1.13/highslide/highslide.css" type="text/css" />
<!-- 9d83e18a764256552baf6956b777f190 -->
<script type="text/javascript" src="/highslide-4.1.13/highslide/highslide-full.packed.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/highslide-4.1.13/highslide/graphics/';
	hs.showCredits = false;
    hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = .45;
	hs.zIndexCounter = 2000;
//	hs.objectLoadTime = 'after';
//	hs.forceAjaxReload = 'true';
</script>
<script src="../assets/js/dropdown.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64707958-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="wrapper">	    
	<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="/">Tweet Jukebox</a> 		
	</div>
    <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
		<ul class="nav navbar-nav navbar-cls-top" style="cursor:pointer;">
            <? require_once($_SERVER['DOCUMENT_ROOT']."/include/mainNav.php") ; ?>
		</ul>
	</div>
</div>	
<div class="container" >
	<div class="row">
	<div class="col-md-1 col-sm-1 col-xs-1"></div>
	<div class="col-md-10 col-sm-10 col-xs-10">

   <section role="banner">
       <hgroup>
           <h1>About TweetJukebox</h1>
       </hgroup>
	   
       <article role="main" class="clearfix contact">
         <div style="float:left; text-align:center"><img src="/images/tf.JPG" width="100" height="108"><br>Tim Fargo</div>
		 <div class="clearfix"></div>
         <p align="left"><strong>OUR HISTORY:</strong></p>
         <p align="left">Back  in July of 2013. I started using Twitter to try to help sell my book, Alphabet  Success. (A story about success built around my last business). In an effort to  keep things interesting, I scheduled a fair number of tweets. One thing became  clear very quickly: scheduling tweets took an incredible amount of time.</p>
         <p>Much  of what I was tweeting was “evergreen” and wouldn't change much over time, so I  ended up scheduling things repeatedly over the course of a month. However, it  had to be done, and it was better than sitting in front of the computer doing  it “live”. But the thought stuck in my mind: “There's got to be a better way”.</p>
         <p>Finally,  on November 7, 2013, I asked my friend and long-time colleague Len Sixt, if he  wanted to develop a Twitter™ app for me. At this point there was no thought of  marketing it as a product. I just wanted a tool to save me the time of  scheduling tweets. Even though the app was pretty expensive as a private toy, I  figured it saved me hiring someone to schedule things on my behalf.</p>
         <p>By  January 2014, a “baby” Tweet Jukebox was born, I had some basic functions to  use, and it automatically saved me a ton of time. So we just kept plowing away,  and improving the program. Over the next few months I would occasionally  mention it to people. They seemed intrigued.</p>
         <p>Then  at the end of January, 2015, we finally said, why not? So we pushed the app out  into the market. Quietly, but “out” nonetheless. The response has been  wonderful. People like it. We've got some more work to do, as with most  projects, but the overall consensus has been quite positive.</p>
         <p>We're  not the most likely app development duo. With a combined age of just under 120  years, we're prehistoric life forms in the software development world. But  we've got some history in this field. In fact, Len is the first person I worked  for when I graduated college. We worked together in technology back when  personal computers were still something new, and I was selling the software that  he wrote.</p>
         <p>But  the internet was a very dark place back in the 80s. Social media was nowhere in  sight.</p>
         <p>That's  our very brief history. We hope you'll become part of it. Let us help you make  your Twitter™ life easier and more enjoyable. In either event, thanks for  stopping by.</p>
         <p>Tim  Fargo and Len Sixt – The Ancient Ones. :-)</p>
         <p><strong>ABOUT TWEET JUKEBOX:</strong></p>
		 <p>You're  probably busy. Most of us are. Scheduling tweets and live tweeting are very  time consuming. Plus you have to remember when you last sent a particular piece  of content. It's tiring just thinking about it.</p>
		 <p>Tweet  Jukebox will eliminate the need to continually schedule your tweets, and manage  your content. It's all right at your fingertips. Once you turn on your jukebox,  it tweets for you. Automatically. No more wasted time. How's that for good  news?</p>
		 <p><strong>JUKE BOXES:</strong>&nbsp;You're going to love your Jukeboxes. You load one with  content , figure out how often you'd like it to tweet, and then turn it on.  With each Jukebox, you can store thousands of Tweets and set them up with our scheduling  software. You can send tweets from one to over a 100 times per day. Plus your  scheduling is flexible, allowing you to set different schedules for each day of  the week. When it runs out of tweets, your Jukebox will turn over like an  hourglass and keep tweeting. This is also completely automatic. You don't have  to touch a thing. (PS - to make it super easy to start out, we even give you a  FREE Jukebox with 200 quotes for you to try out.)&nbsp; To get started you can use a free account which allows you two jukeboxes and up to 300 stored jukebox tweets as well as 5 stored scheduled tweets. Plus you can thank up to 50 of your top interactors. All for free. If you like Tweet Jukebox, you can easily upgrade to one of our turbo charged plans <a href="/pricing/">here</a>.</p>
		 <p><strong>SCHEDULED TWEETS:</strong>&nbsp;Have a message you want to repeat a few times to ensure  your followers see it? Want to have a birthday message that tweets once a year?  This is where it happens. Scheduled tweets are those that you want to send out  on a specific date and, or frequency. By scheduling your tweet, you can get  your information out to your followers daily, weekly, or several times each  day. You'll find our scheduled tweet engine to be very flexible to fit  virtually any requirement.</p>
		 <p><strong>THANK YOU TWEETS:</strong>&nbsp;When a user mentions you, where we come from it is common  to &quot;thank&quot; them. Using our Thank You Tweet tool, you automatically  thank up to 50 people for their interaction every Friday. Your jukebox system  will thank the top Twitter™ users that mentioned you during the week. This  feature is automatic, you don't have to do anything. Yes, that's right, more  free time. Woohoo!</p>
		 <p><strong>MENTIONS DATABASE:</strong>&nbsp;Your Tweet Jukebox will collect all &quot;mentions&quot;  of your Twitter™ 24/7 automatically . We capture every mention and store it in  your own database. You'll be able to see mentions over time in a graph format,  see who your top &quot;promoters&quot; are using two different metrics (volume  and reach). Did we mention it's automatic?</p>
		 <p><strong>MORE TO COME:</strong>&nbsp;Tweet Jukebox might be new, but we're sure that our  initial &quot;tool kit&quot; is well worth investing your time in. Tweet Jukebox  is a &quot;living&quot; application and we promise to continually enhance and  improve it to make your life easier. We encourage your suggestions on how we  can make Tweet Jukebox the best it can be.</p>
		 <div class="post" style="width:auto;">
		   <p>Have a question or suggestion? Click <a href="/contact">here</a> to send it to us. We'll get back to you within a few hours.               </p>
             <p>To get started, just sign up using Twitter&#8482; and we'll walk you through the process. </p>
          </div>
       </article>
	   </section>
	   </div>
	   <div class="col-md-1 col-sm-1 col-xs-1"></div>
	</div>
</div>   
<div class="container" >
	<div class="row" style="padding-top:50px;">
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<p align="center">
   			<span class="left">tj.local &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> | <!--a href="/plans.php">PLANS</a> | --><a href="/contact">CONTACT US</a> | <a href="/privacy">PRIVACY POLICY</a> | <a href="/terms">TERMS AND CONDITIONS</a>
			</p>
		</div>	
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
	</div>
</div>	   
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
  <script src="assets/js/script.js"></script>
</body>
</html>