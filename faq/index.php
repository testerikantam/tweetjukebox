<?php  include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Tweet Jukebox</title>
<meta name="description" content="Tweet Jukebox - Thinking Inside the Box">
<!-- BOOTSTRAP STYLES-->
<link href="../assets/css/bootstrap_v3.3.4.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="../assets/css/font-awesome.css" rel="stylesheet" />
<!-- MORRIS CHART STYLES-->
<link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="../assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="jquery.timepicker.css" />
<link href="/members/chart.css" rel="stylesheet" type="text/css">
<!-- JQUERY SCRIPTS -->
<script src="../assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../assets/js/bootstrap_v3.3.4.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="../assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../assets/js/custom.js"></script>
<script src="/include/js_boot.js"></script>
<script type="text/javascript" src="jquery.timepicker.js"></script>
<link rel="stylesheet" href="/highslide-4.1.13/highslide/highslide.css" type="text/css" />
<!-- 9d83e18a764256552baf6956b777f190 -->
<script type="text/javascript" src="/highslide-4.1.13/highslide/highslide-full.packed.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/highslide-4.1.13/highslide/graphics/';
	hs.showCredits = false;
    hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = .45;
	hs.zIndexCounter = 2000;
//	hs.objectLoadTime = 'after';
//	hs.forceAjaxReload = 'true';
</script>
<script src="../assets/js/dropdown.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64707958-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<div id="wrapper">	    
	<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="/"> Tweet Jukebox </a> 		
	</div>
    <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
		<ul class="nav navbar-nav navbar-cls-top" style="cursor:pointer;">
            <? require_once($_SERVER['DOCUMENT_ROOT']."/include/mainNav.php") ; ?>	
		</ul>
	</div>
</div>	
<div class="container">
  <h1 align="center">Frequently asked questions:</h1>
  <ul>
    <li><strong>How much does Tweet Jukebox cost?</strong>
      <ul>
        <li>It's free for many users. But we also have three more powerful versions starting at $9.99 per month. If you'd like more information about our services, <a href="/pricing/">click here</a>. </li>
      </ul>
    </li>
	<br />  
    <li><strong>Why does it require access to my timeline and  profile information?</strong>
      <ul>
        <li> This is not up to us. In order for the system to work,  this is the permission level required by Twitter. We won't be messing with  your profile or anything other than the things you instruct us to do on your  behalf.</li>
      </ul>
    </li>
    <br />
    <li><strong>Why do you give me a Jukebox of quotes?</strong>
      <ul>
        <li>We do this as an easy means for new users to  see how tweets look in a Jukebox. Some people love it, others not so much. But  you can always delete the jukebox once you're comfortable with the system.</li>
      </ul>
    </li>
    <br />
	<li><strong>Have you had any major problems with the app?  What's it's track record?</strong>
      <ul>
        <li> This app was used for my account @alphabetsuccess  for a year before it was ever released. I continue to use it for over 95% of my  tweets.</li>
      </ul>
	</li>
    <br />
    <li><strong>How do I add additional accounts to Tweet  Jukebox?</strong>
      <ul>
        <li>Our free service is for a single account. If you'd like a multi-account version please <a href="/pricing/">click her</a> for more information..</li>
      </ul>
    </li>
    <br />
	<li><strong>Will we have the ability to download our Jukeboxes too?</strong>
      <ul>
        <li>Yes, our paid versions all allow you to download your tweets.</li>
      </ul>
	</li>
    <br />
    <li><strong>Why can't I see what will post?</strong>
      <ul>
        <li>Because  each Jukebox selection is random and “unknown” until it's made. The ease of the  system is predicated on not scheduling (and thus “knowing”).</li>
      </ul>
    </li>
  </ul>
  <p>Have a  question that isn't answered here? Please e-mail me at <a href="mailto:tim@tweetjukebox.com">tim@tweetjukebox.com</a> and I'll be happy to give you a hand. Even two if required. </p>
  <p>Thanks for  taking the time to consider us. We appreciate it!</p>
  <p>Tim Fargo</p>
  <p>Chief  Tweetologist</p>   
</div>



<div class="container" >
	<div class="row" style="padding-top:50px;">
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<p align="center">
   			<span class="left">tj.local &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> | <!--a href="/plans.php">PLANS</a> | --><a href="/contact">CONTACT US</a> | <a href="/privacy">PRIVACY POLICY</a> | <a href="/terms">TERMS AND CONDITIONS</a>
			</p>
		</div>	
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
	</div>
</div>	   
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
  <script src="assets/js/script.js"></script>
</body>
</html>