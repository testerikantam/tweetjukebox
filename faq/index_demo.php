<?php  include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Tweet Jukebox</title>
<meta name="description" content="Tweet Jukebox - Thinking Inside the Box">
<!-- BOOTSTRAP STYLES-->
<link href="../assets/css/bootstrap_v3.3.4.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="../assets/css/font-awesome.css" rel="stylesheet" />
<!-- MORRIS CHART STYLES-->
<link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="../assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="jquery.timepicker.css" />
<link href="/members/chart.css" rel="stylesheet" type="text/css">
<!-- JQUERY SCRIPTS -->
<script src="../assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../assets/js/bootstrap_v3.3.4.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="../assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../assets/js/custom.js"></script>
<script src="/include/js_boot.js"></script>
<script type="text/javascript" src="jquery.timepicker.js"></script>
<link rel="stylesheet" href="/highslide-4.1.13/highslide/highslide.css" type="text/css" />
<!-- 9d83e18a764256552baf6956b777f190 -->
<script type="text/javascript" src="/highslide-4.1.13/highslide/highslide-full.packed.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/highslide-4.1.13/highslide/graphics/';
	hs.showCredits = false;
    hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = .45;
	hs.zIndexCounter = 2000;
//	hs.objectLoadTime = 'after';
//	hs.forceAjaxReload = 'true';
</script>
<style>
.btn{
    white-space:normal !important;
    word-wrap:break-word;
}
</style>
<script src="../assets/js/dropdown.js"></script>
</head>
<body>
<div id="wrapper">	    
	<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="/"> Tweet Jukebox </a> 		
	</div>
    <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
		<ul class="nav navbar-nav navbar-cls-top" style="cursor:pointer;">
            <li><a href="/"><i class="fa fa-home"></i> Home </a></li>
            <li><a href="/about"> About </a></li>
        	<li><a href="/contact"> Contact us </a></li>
			<li><a href="/faq"><i class="fa fa-info-circle"></i> FAQ </a></li>						
			<li><a href="logout.php"><i class="fa fa-sign-out"></i> Log Out</a></li>
		</ul>
	</div>
</div>	
<div class="container">
  <h1 align="center">Tutorial Videos:</h1>
  <div class="row">
	<div class="col-md-3 col-sm-3 col-xs-3">
		<ul class="btn-group nav nav-stacked">
			<li><button id="firstVideo" class="btn btn-primary btn-group" onClick="$('#videoContainer').html('<iframe width=854 height=510 src=https://www.youtube.com/embed/vGfA6KwyNzU?autoplay=1 frameborder=0 allowfullscreen></iframe>')" >Tweet Jukebox Best Practices</button></li>
			<li><button class="btn btn-primary btn-group" onClick="$('#videoContainer').html('<iframe width=854 height=510 src=https://www.youtube.com/embed/-bJouAMD7Lo?autoplay=1 frameborder=0 allowfullscreen></iframe>')" >Some ideas on how to use your jukeboxes in Tweet Jukebox</button></li>
			<li><button class="btn btn-primary btn-group" onClick="$('#videoContainer').html('<iframe width=854 height=510 src=https://www.youtube.com/embed/wN3yGP0ztlY?autoplay=1 frameborder=0 allowfullscreen></iframe>')" >Creating a new jukebox in Tweet Jukebox</button></li>
			<li><button class="btn btn-primary btn-group" onClick="$('#videoContainer').html('<iframe width=854 height=510 src=https://www.youtube.com/embed/KoMaTmXFrMg?autoplay=1 frameborder=0 allowfullscreen></iframe>')" >Changing a Schedule in Tweet Jukebox</button></li>
			<li><button class="btn btn-primary btn-group" onClick="$('#videoContainer').html('<iframe width=854 height=510 src=https://www.youtube.com/embed/ggu5Jkc-TnM?autoplay=1 frameborder=0 allowfullscreen></iframe>')" >Turning a jukebox on and off in Tweet Jukebox</button></li>
			<li><button class="btn btn-primary btn-group" onClick="$('#videoContainer').html('<iframe width=854 height=510 src=https://www.youtube.com/embed/wN3yGP0ztlY?autoplay=1 frameborder=0 allowfullscreen></iframe>')" >Creating a new jukebox in Tweet Jukebox</button></li>
			<li><button class="btn btn-primary btn-group" onClick="$('#videoContainer').html('<iframe width=854 height=510 src=https://www.youtube.com/embed/zGGXVs94jM0?autoplay=1 frameborder=0 allowfullscreen></iframe>')" >How to Delete a Jukebox in Tweet Jukebox</button></li>
			<li><button class="btn btn-primary btn-group" onClick="$('#videoContainer').html('<iframe width=854 height=510 src=https://www.youtube.com/embed/7nX6atw37hc?autoplay=1 frameborder=0 allowfullscreen></iframe>')" >How to upload a csv file to a jukebox in Tweet Jukebox</button></li>
			<li><button class="btn btn-primary btn-group" onClick="$('#videoContainer').html('<iframe width=854 height=510 src=https://www.youtube.com/embed/yx8Aa35Kvks?autoplay=1 frameborder=0 allowfullscreen></iframe>')" >Turning on and off your thank you tweets.</button></li>
			<li><button class="btn btn-primary btn-group" onClick="$('#videoContainer').html('<iframe width=854 height=510 src=https://www.youtube.com/embed/yXqUE81S2-E?autoplay=1 frameborder=0 allowfullscreen></iframe>')" >Adjusting your Tweet Jukebox time zone settings</button></li>
		</ul>	
	</div>
	<div class="col-md-9 col-sm-9 col-xs-9">
		<div id="videoContainer" class="embed-responsive embed-responsive-16by9">
			<img src="../images/videoFrame2.png" class="img-responsive">
			<!--iframe src="https://www.youtube.com/embed/vGfA6KwyNzU"></iframe-->
		</div>	
	</div>
  </div>		
</div>
<div class="container" >
	<div class="row" style="padding-top:50px;">
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<p align="center">
   			<span class="left">tj.local &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> | <!--a href="/plans.php">PLANS</a> | --><a href="/contact">CONTACT US</a> | <a href="/privacy">PRIVACY POLICY</a> | <a href="/terms">TERMS AND CONDITIONS</a>
			</p>
		</div>	
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
	</div>
</div>	 

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
<script src="assets/js/script.js"></script>
<script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0029/6945.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
</body>
</html>