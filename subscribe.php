<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?


if($_SESSION['access_token']['user_id'] != '105556115') die("not implemented");

if(!isset($_SESSION['access_token'])) header('Location: /');
if($_SESSION['validated'] != 'validated') header('Location: /');

$pid = $_REQUEST['pid'];

$amount = '12.99';

?>
<link rel="stylesheet" href="/../tweetjukebox/assets/fonts/raphaelicons.css">
<link rel="stylesheet" href="/../tweetjukebox/assets/css/main.css">
<script>
function numbersOnly(obj) {
    if (obj.value != obj.value.replace(/[^0-9\.]/g, '')) {
       obj.value = obj.value.replace(/[^0-9\.]/g, '');
    }
};

function validateForm() {
	var errors = '';
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	var x = '';
	x = document.getElementById("cardNumber").value;
	if(x.length < 15 || x.length > 19 ) {
		errors+="CC Number length must be betwen 15 and 19 digits without any spaces or dashes.\n";
	}
	x = document.getElementById("cc_cvv").value;
	if(x.length != 3 && x.length != 4 ) {
		errors+="Please enter the CVV Number without any spaces or dashes.\n";
	}		
	var year = document.getElementById("cc_expires_year").value	
	var month = document.getElementById("cc_expires_month").value 
	var currDate = document.getElementById("currDate").value 
	if(currDate > year+month) {
		errors+="Credit card is expired.\n";
	}
	if(document.getElementById("agreedTo").checked == false) {
		errors+="You must acknowledge the subscription terms.\n";
	}		
	if(errors) {
		alert(errors);
		return false;
	} else {
		return true;
	}	
}
</script>
<html>
<body>

<span class="member-icon" style="font-size:24px; color:#00FF00; float:right;">W</span>
<img src="/../tweetjukebox/images/tweetJukebox_75.png" border="0" style="float:left" />	
<h2 align="center">Secure Payment Form</h2>
<p>Once you submit your payment, your access to HeardLocally.com will remain valid on an ongoing basis until your membership is canceled.</p>
<p>If you wish to cancel your membership you may do so by clicking on the "Cancel My Membersip" button on your control panel, or by emailing us with a notice to cancel. Your membership will remain active until it is canceled.</p>
<p align="center" class="fullLink"><? echo $pid. " membership  $". $amount; ?> / month</p>
<form action="transaction.php" method="POST" id="braintree-payment-form" onSubmit="return validateForm();" accept-charset="utf-8">        	
		<table>
	   <tr>
          <td valign="middle">First Name </td>
          <td valign="middle"><input  id="user_first_name," type="text" size="20" autocomplete="off" data-encrypted-name="user_first_name" /></td>
        </tr>
        <tr>
          <td valign="middle">Last Name </td>
          <td valign="middle"><input id="user_last_name," type="text" size="20" autocomplete="off" data-encrypted-name="user_last_name" /></td>
        </tr>
         <tr>
          <td valign="middle">Email </td>
          <td valign="middle"><input id="email" type="text" size="20" autocomplete="off" data-encrypted-name="email" value="<? echo $_SESSION['email']; ?>" /></td>
        </tr>

		<tr>
          <td valign="middle">Card Number <img title="All digits - no spaces or dashes" src="../tweetjukebox/images/question_blue.png"></td>
          <td valign="middle"><input id="cardNumber" type="text" size="20" autocomplete="off" data-encrypted-name="number" onKeyUp="numbersOnly(this)" /></td>
        </tr>
        <tr>
          <td valign="middle">CVV <img title="Three or four digit security code for your card" src="../tweetjukebox/images/question_blue.png"></td>
          <td valign="middle"><input id="cc_cvv" type="text" size="4" autocomplete="off" data-encrypted-name="cvv"  onKeyUp="numbersOnly(this)" /></td>
        </tr>
        <tr>
          <td valign="middle">Expiration Month</td>
          <td valign="middle">
		  <select name="month" id="cc_expires_month" >
		    <option value="01" selected="selected">January</option>
			<option value="02">February</option>
			<option value="03">March</option>
			<option value="04">April</option>
			<option value="05">May</option>
			<option value="06">June</option>
			<option value="07">July</option>
			<option value="08">August</option>
			<option value="09">September</option>
			<option value="10">October</option>
			<option value="11">November</option>
			<option value="12">December</option>
		  </select>		  
		  </td>
		</tr>
		<tr>
		  <td valign="middle">Exipration Year</td>
		  <td valign="middle">
		  <select name="year" id="cc_expires_year">
			<? $year = date("Y"); ?>
			  <? do { ?>
					<option value="<? echo $year; ?>" <? if($year == date("Y")) echo ' selected="selected"'; ?>><? echo $year; ?></option>
			  <? 
					$year ++;
				} while ($year < date("Y") + 9);
			  ?>	
		  </select>
		  
		  </td>
        </tr>
  </table>
		<p>You are subscribing to a monthly recurring payment agreement. By checking this box you agree to these terms. <input type="checkbox" id="agreedTo" name="agreedTo" value="1" /></p></td>
		<input type="hidden" value="Basic-Monthly" name="pid" id="pid"  />
		<input type="hidden" name="currDate" id="currDate" value="<? echo date("Ym"); ?>" />	
		<input type="submit" id="submit" value="Submit Payment" />

</form>	  
<div id="errorDiv" style="visibility:hidden; background-color:#FFE8E8; color:#FF0000; font-weight:bold; text-align:center; border:thin #750000 1px;" align="center">OOPS! An error occured.<br>Check your information and try again.</div>
<script src="https://js.braintreegateway.com/v1/braintree.js"></script>
<script>
	// sandbox
	var braintree = Braintree.create("MIIBCgKCAQEAnww8b1E5KYGIsDYHbyW06G7CsAf+gkUp3/x8go+EaqBiw9ZLjTqCwgze7Is+m1Wau8wn5QjA8NouOW9tik9IskoCYDF8IgHsp1IlGbKRo0gWSnBYcUoNU/PLA6O7Mqjqtep+i5wcrAxoyt89av65ZytYMeLwZCVG+5rxpUFLov6lXbLiJgtS+YdF5xkS+UYKcM/iD40Oefjo/U0SuUzxipTI1ryrqds3p8hYRPJ1IvP97taGIQf3mAnm3yB7m4HtlJS61tC3xYJFTge9DkNtRS9Pr4ta96A04g/x4n1dSG86qfd2TJOJpfj40vcYHBfgVtE1QdCYbl7fTZ+K7fKxKQIDAQAB");
    
	// production 
	//var braintree = Braintree.create('MIIBCgKCAQEArO6mDKhEP6AD4IvYCbD3dXP4Bu0EzGjODyYhVvORCQRP139imr3pFhRUWhHrky6ba7/jR0Oa4LYp5TnifAhPz7bzMrK+QWr3/YhoNYhkzR2vYY+kB7bKYUQyaGCCbJNRFIJ2eiiLsH5tguQFGv9DoU1/M//5uP92IzedHocZtaOQqVi0X47SK+4xAPQCJilcLfArvd5FCxBTG1oJdNROTKmVzPZitJOqYhr9ObSDTE8ruL1CGDvxaG85IE+eRu8ZyTjR/WYDMBxDTMkM7CsWL2sGZEMEgnl+1e4IAdQdR5/ZDwCqBLQn/goHB9ZF12d4vxrTQvUktYVXy25fvQvrGQIDAQAB');
	
	
	braintree.onSubmitEncryptForm('braintree-payment-form');
</script>
<?	
if($_REQUEST['err'] ) {
?>
<script>
document.getElementById('errorDiv').style.visibility = 'visible';
</script>
<?
}
?>
</body>	  
</html>