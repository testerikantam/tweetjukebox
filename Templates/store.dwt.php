<? include ($_SERVER['DOCUMENT_ROOT']."/include/config.php") ; ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<!-- TemplateBeginEditable name="doctitle" -->
<title>Tweet Jukebox</title>
<!-- TemplateEndEditable -->
<meta name="description" content="Tweet Jukebox - Thinking Inside the Box">
<link rel="shortcut icon" href="/favicon.ico?v=2">
<!-- BOOTSTRAP STYLES-->
<link href="/assets/css/bootstrap.css" rel="stylesheet" />
<!--link href="../assets/css/bootstrap_v3.3.4.css" rel="stylesheet" /-->
<!-- FONTAWESOME STYLES-->
<link href="/assets/css/font-awesome.css" rel="stylesheet" />
<!-- MORRIS CHART STYLES-->
<link href="/assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="/assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="/members/jquery.timepicker.css" />
<link href="/members/chart.css" rel="stylesheet" type="text/css">
<!-- JQUERY SCRIPTS -->
<script src="/assets/js/jquery-1.10.2.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="/assets/js/metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="/assets/js/custom.js"></script>
<script src="/include/js_boot.js"></script>
<script type="text/javascript" src="/members/jquery.timepicker.js"></script>
<link rel="stylesheet" href="/highslide-4.1.13/highslide/highslide.css" type="text/css" />
<!-- 9d83e18a764256552baf6956b777f190 -->
<script type="text/javascript" src="/highslide-4.1.13/highslide/highslide-full.packed.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/highslide-4.1.13/highslide/graphics/';
	hs.showCredits = false;
    hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = .45;
	hs.zIndexCounter = 2000;
//	hs.objectLoadTime = 'after';
//	hs.forceAjaxReload = 'true';
</script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="/assets/js/bootstrap_v3.3.4.min.js"></script>
<script src="/members/lib/bootstrap-datepicker.js"></script>
<link href="/members/lib/bootstrap-datepicker.css" rel="stylesheet" type="text/css">
<!-- TemplateBeginEditable name="head" -->
<!-- TemplateEndEditable -->
</head>
<body>
<div id="wrapper">
	<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="/">Tweet Jukebox</a>
	</div>
    <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
		<ul class="nav navbar-nav navbar-cls-top" style="cursor:pointer;">
            <? require_once($_SERVER['DOCUMENT_ROOT']."/include/mainNav.php") ; ?>			
		</ul>
		<span data-toggle="dropdown" aria-expanded="false" style="cursor:pointer; " ><img style="display:inline; border:solid 1px #FFFFFF; " src="<? echo $content->profile_image_url; ?>" >&nbsp;<? echo $screen_name; ?></span>
		
	</div>
  </nav>
  <!-- /. NAV TOP  -->  
  <nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
      <ul class="nav" id="main-menu">
        <li class="text-center">
			<img src="/images/tweetjukebox3.png" width="828" height="645" class="user-image img-responsive"/>			
		</li>		        		
	  </ul>
	  	<form class="form-group" accept-charset="utf-8">  
			<div>Search</div>
			<input class="form-control" type="text" name="search_store" id="search_store" value=""  />
			<div>
				<button type="button" onClick="searchStore()">Submit</button>
			</div>	
		</form>
	  
	  
    </div>
  </nav>
  <!-- /. NAV SIDE  --> 

<div id="page-wrapper" >
    <div id="page-inner"> 
 

<!-- TemplateBeginEditable name="pageHeaderArea" -->
<!-- TemplateEndEditable -->
<!-- TemplateBeginEditable name="pageContentArea" -->
<!-- TemplateEndEditable -->  




<div class="container" >
	<div class="row" style="padding-top:50px;">
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<p align="center">
   			<span class="left">tj.local &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> | <!--a href="/plans.php">PLANS</a> | --><a href="/contact">CONTACT US</a> | <a href="/privacy">PRIVACY POLICY</a> | <a href="/terms">TERMS AND CONDITIONS</a></span>
			</p>
		</div>	
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
	</div>

</div>
</div>	
	
</div>	 
</div>
<script src="/assets/js/morris/raphael-2.1.0.min.js"></script>
<script src="/assets/js/morris/morris.js"></script>
<!-- TemplateBeginEditable name="pageModalArea" -->
<!-- TemplateEndEditable -->
</body>
</html>
