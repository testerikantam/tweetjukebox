<? include ($_SERVER['DOCUMENT_ROOT']."/include/config.php") ; ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<!-- TemplateBeginEditable name="doctitle" -->
<title>Tweet Jukebox</title>
<!-- TemplateEndEditable -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/css/bootstrap.min.css">
<link href="/css/font-awesome.min.css" rel="stylesheet">
  <link  href="http://fonts.googleapis.com/css?family=Oswald:regular" rel="stylesheet" type="text/css" >
  <link href='http://fonts.googleapis.com/css?family=Junge' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="/assets/fonts/raphaelicons.css">
  <link rel="stylesheet" href="/assets/css/main.css">
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
      <script src="/js/respond.min.js"></script>
    <![endif]-->
<!-- TemplateBeginEditable name="head" -->
<!-- TemplateEndEditable -->
</head>
<body>
<div class="container-fluid" style="padding-bottom:50px;">
<div class="navbar navbar-default navbar-fixed-top ">
  <div class="container">
    <div class="navbar-header"> <a href="/" class="navbar-brand TJBlogo">Tweet Jukebox</a>
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
    </div>
    <div class="navbar-collapse collapse" id="navbar-main">
      <ul class="nav navbar-nav">
        <li> <a href="jbAssignments.php">Jukebox Assignments</a></li>
      </ul>      
    </div>
  </div>
</div>
</div>
<!-- TemplateBeginEditable name="pageHeaderArea" -->
<!-- TemplateEndEditable -->
<!-- TemplateBeginEditable name="pageContentArea" -->
<!-- TemplateEndEditable -->
<div class="container" style="padding-top:50px;">
  <footer>
    <div class="row well">
      <div class="col-lg-4"> tj.local &copy; - <? echo date("Y"); ?> </div>
      <div class="col-lg-8 text-right"> <a href="/">HOME</a> | <a href="/about.php">ABOUT</a> | <a href="/contact.php">CONTACT US</a> | <a href="/privacy.php">PRIVACY POLICY</a> | <a href="/terms.php">TERMS AND CONDITIONS</a> | <a href="#">Goto Top</a> </div>
    </div>
  </footer>
</div>
<!-- TemplateBeginEditable name="pageModalArea" -->
<!-- TemplateEndEditable -->
<script src="/js/jquery-1.10.2.min.js"></script>
<script src="../assets/js/bootstrap_v3.3.4.min.js"></script>
<script src="/include/js_min.js"></script>
</body>
</html>
