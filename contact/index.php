<?php  include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
if($_POST['submit']) {
	include($_SERVER['DOCUMENT_ROOT']."/include/mail/class.phpmailer.php");
	
	if($_POST['phone'] == '') {
		$email = mysqli_real_escape_string($conn, $_POST['email']);
		$name = mysqli_real_escape_string($conn, $_POST['name']);
		$msg = $_POST['msg'];
		
		$mail = new PHPMailer();
		$mail->IsSendmail();									// use sendmail
		$mail->From = SITE_EMAIL;
		$mail->FromName = SITE_EMAIL;
		$mail->AddBCC(SITE_EMAIL, SITE_EMAIL);
		$mail->AddAddress("tim@tweetjukebox.com", "tim@tweetjukebox.com");
		//$mail->AddAddress("ellen@example.com");                  // name is optional
		$mail->AddReplyTo($email, $email);
		$mail->WordWrap = 50;                                 // set word wrap to 50 characters
		//$mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
		//$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
		$mail->IsHTML(true);                                  // set email format to HTML
		$mail->Subject = SITE_NAME." Message From Website";
		$mail->Body = "Name: ".$name."<br>Email: ".$email."<br>Message:<br><br>".nl2br($msg);
		if(!$mail->Send())
		{
		   echo "Message could not be sent. <p>";
		   echo "Mailer Error: " . $mail->ErrorInfo;
		   exit;
		}
	}	
	$thanks = "Thank you for contacting us!";
}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Tweet Jukebox</title>
<meta name="description" content="Tweet Jukebox - Thinking Inside the Box">
<!-- BOOTSTRAP STYLES-->
<link href="../assets/css/bootstrap_v3.3.4.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="../assets/css/font-awesome.css" rel="stylesheet" />
<!-- MORRIS CHART STYLES-->
<link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="../assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<link rel="stylesheet" type="text/css" href="jquery.timepicker.css" />
<link href="/members/chart.css" rel="stylesheet" type="text/css">
<!-- JQUERY SCRIPTS -->
<script src="../assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../assets/js/bootstrap_v3.3.4.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="../assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../assets/js/custom.js"></script>
<script src="/include/js_boot.js"></script>
<script type="text/javascript" src="jquery.timepicker.js"></script>
<link rel="stylesheet" href="/highslide-4.1.13/highslide/highslide.css" type="text/css" />
<!-- 9d83e18a764256552baf6956b777f190 -->
<script type="text/javascript" src="/highslide-4.1.13/highslide/highslide-full.packed.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/highslide-4.1.13/highslide/graphics/';
	hs.showCredits = false;
    hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = .45;
	hs.zIndexCounter = 2000;
//	hs.objectLoadTime = 'after';
//	hs.forceAjaxReload = 'true';
</script>
<script>
function validateEmail(sEmail) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {
		$('#submit').prop( "disabled", false );
		return true;
	}
	else {
		$('#submit').prop( "disabled", true ); 
		return false;
	}
}
</script>
<script src="../assets/js/dropdown.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64707958-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="wrapper">	    
	<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="/">Tweet Jukebox</a> 		
	</div>
    <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
		<ul class="nav navbar-nav navbar-cls-top" style="cursor:pointer;">
            <? require_once($_SERVER['DOCUMENT_ROOT']."/include/mainNav.php") ; ?>	
		</ul>
	</div>
</div>	
<div class="container" >
	<div class="row">
	<div class="col-md-1 col-sm-1 col-xs-1"></div>
	<div class="col-md-10 col-sm-10 col-xs-10">
	       <h1 align="center">Contact us</h1>
           <h3 align="center">Please tell us how we can improve your experience.</h3>    
             <h3 align="center">Your Opinion Counts</h3>
             <p align="center">We appreciate your feedback and want to hear from you.</p>
             <p align="center"><a href="mailto:tim@tweetjukebox.com">tim[at]TweetJukebox[dot]com</a></p>
   	<div align="center">
       <? if($thanks) { ?>
	   <footer class="foo-slogan">
             <h2><? echo $thanks; ?></h2>
       </footer>
	   <? } else { ?>	   
             <form method="post" enctype="application/x-www-form-urlencoded" accept-charset="utf-8" >
                 <h3>Drop us a line ...</h3>
                 <div class="form-group">
				 <label for="name">Name</label>
                 <input type="text" id="name" name="name" value="" class="form-control" style="width:200px;" />
				 </div>
				 <div class="form-group">
                 <label for="email">Email</label>
                 <input type="email" id="email" name="email" onChange="validateEmail(this.value)" value="" class="form-control" style="width:200px;"/>
				 </div>
				 <div class="form-group">
                 <label for="msg">Message</label>
                 <textarea name="msg"  rows="5" id="msg" class="form-control" style="width:500px;"></textarea>
				 </div>
				 
				 <input type="hidden" id="phone" name="phone" value=""  style="width:200px;" />
				 
				 <div class="form-group">
                 <input type="submit" id="submit" name="submit" value="Send" disabled="disabled" class="button"/>
				 </div>
             </form>
       </footer>
	   <? } ?>
   </div>
   
	</div>
	   <div class="col-md-1 col-sm-1 col-xs-1"></div>
	</div>
</div>   
<div class="container" >
	<div class="row" style="padding-top:50px;">
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<p align="center">
   			<span class="left">tj.local &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> | <!--a href="/plans.php">PLANS</a> | --><a href="/contact">CONTACT US</a> | <a href="/privacy">PRIVACY POLICY</a> | <a href="/terms">TERMS AND CONDITIONS</a>
			</p>
		</div>	
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
	</div>
</div>	   
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
  <script src="assets/js/script.js"></script>
</body>
</html>