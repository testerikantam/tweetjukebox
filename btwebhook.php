<?php  include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/twitteroauth/twitteroauth.php");
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/config.php");
?>
<?
//$message = "Start Of Exceptions";

//file_put_contents("webhook.log", $message, FILE_APPEND);

include($_SERVER['DOCUMENT_ROOT']."/include/mail/class.phpmailer.php");


require_once $_SERVER['DOCUMENT_ROOT'].'/braintree-php-3.1.0/lib/Braintree.php';


// sandbox 
/*
Braintree_Configuration::environment('sandbox');
Braintree_Configuration::merchantId('zsbs7swsdwdzvmwg');
Braintree_Configuration::publicKey('yb7qkdry9qfvzbn9');
Braintree_Configuration::privateKey('36ae489f8f321abd38f0971f5f1ff2cf');
*/

Braintree_Configuration::environment('production');
Braintree_Configuration::merchantId('dxpnw5b448vwd9rp');
Braintree_Configuration::publicKey('mg2pmbnsn3g4cqnq');
Braintree_Configuration::privateKey('cf12d53af0f632d8fb98c91af4b86741');


if(isset($_GET["bt_challenge"])) {
    echo(Braintree_WebhookNotification::verify($_GET["bt_challenge"]));
}

if(
    isset($_POST["bt_signature"]) &&
    isset($_POST["bt_payload"])
) {
    $webhookNotification = Braintree_WebhookNotification::parse(
        $_POST["bt_signature"], $_POST["bt_payload"]
    );

    $message =
        "[Webhook Received " . $webhookNotification->timestamp->format('Y-m-d H:i:s') . "] "
        . "Kind: " . $webhookNotification->kind . " | "
        . "Subscription: " . $webhookNotification->subscription->id . "\r\n";

    //file_put_contents("webhook.log", $message, FILE_APPEND);
	
	if(strpos($webhookNotification->kind, 'unsuccessful')) {
		$bt_plan_id = $webhookNotification->subscription->id;	
		$SQL = "select * from users where bt_plan_id = '$bt_plan_id'  ";
		$result = mysqli_query($conn, $SQL) or die(mysqli_error());
		
		if(mysqli_num_rows($result) == 0 ) exit();
		
		$row = mysqli_fetch_assoc($result);
		$email = stripslashes($row['email']);
		$user_id = $row['user_id'];
		$plan = $row['planName'];
		$oauth_token = $row['oauth_token'];
		$oauth_token_secret = $row['oauth_token_secret'];	
		//////////////////////////////                   M A I L   
		$message = "Valued member:";
		$message .='<br /><br>Your recent payment for your '.$plan. ' plan was rejected by our payment processor.<br /><br />';
		$message .= 'If you need to update your payment information, you may do so by logging into our site and visiting your account information.';
		$message .= '<br><br>If your payment continues to be rejected, your subscription will automatically be canceled and you will receive a cancellation notice from us at that time.<br /><br />';
		$message .= "Thanks!<br /><br />TweetJukebox.com";
	
		$mail = new PHPMailer();
		//$mail->IsSendmail();								// use sendmail
		$mail->IsMail();
		//$mail->IsSMTP();									// use SMTP
		$mail->From = "no-reply@TweetJukebox.com";
		$mail->FromName = "no-reply@TweetJukebox.com";
		$mail->AddAddress($email);
		$mail->AddReplyTo($email);
		$mail->WordWrap = 50;                                 // set word wrap to 50 characters
		//$mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
		//$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
		//echo stripslashes($message); die("dead");
		//$mail->Body = stripslashes($_POST['message']);
		$mail->IsHTML(true);                                  // set email format to HTML
		$mail->Subject = "Payment failure - from TweetJukebox.com ";
		$mail->Body = $message;
		
		
		if(!$mail->Send())
		{
		   echo "Message could not be sent. <p>";
		   echo "Mailer Error: " . $mail->ErrorInfo;
		   exit;
		}
		
		sendDM('Sadly, your recent payment for your TweetJukebox plan failed.', $user_id, $oauth_token, $oauth_token_secret );
		
													
	}
	if(strpos($webhookNotification->kind, 'canceled')) {
		$bt_plan_id = $webhookNotification->subscription->id;	
		$SQL = "select * from users where bt_plan_id = '$bt_plan_id'  ";						
		$result = mysqli_query($conn, $SQL) or die(mysqli_error());
		
		if(mysqli_num_rows($result) == 0 ) exit();
		
		$row = mysqli_fetch_assoc($result);
		$email = stripslashes($row['email']);
		$user_id = $row['user_id'];
		$plan = $row['planName'];
		$oauth_token = $row['oauth_token'];
		$oauth_token_secret = $row['oauth_token_secret'];	
		//////////////////////////////                   M A I L   
		
		$message =  "Hi there,<br><br>Your account has been deactivated.<br><br>";
		$message .=	"May I ask why you are departing?<br><br>We're sorry to see you go, and would appreciate your feedback.<br><br>";
		$message .= "Thanks for trying TweetJukebox.<br><br>Sincerely,<br><br>Tim Fargo<br>President<br>Tweet Jukebox";
		
		
		
		$SQL = "update users set planName = 'Cancelled', status = '2', all_tweet_status = '0', bt_plan_id = '', master_account_id = '', scheduled_tweets_status = '0', send_thanks = '0'  where user_id = '$user_id'  " ;
		$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
		$SQL = "update user_schedule set schedule_status = '0' where user_id = '$user_id' ";
		$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
		$SQL = "update scheduled_tweets set tweet_status = 'draft' where user_id = '$user_id' ";
		$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));	
		
		if($user_id != '') {
			$SQL = "update users set planName = 'Cancelled', status = '2', all_tweet_status = '0', bt_plan_id = '', master_account_id = '', scheduled_tweets_status = '0', send_thanks = '0'  where master_account_id = '$user_id'  " ;
			$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));	
		}
	
		$mail = new PHPMailer();
		//$mail->IsSendmail();								// use sendmail
		$mail->IsMail();
		//$mail->IsSMTP();									// use SMTP
		$mail->From = "tim@tweetjukebox.com";
		$mail->FromName = "tim@tweetjukebox.com";
		$mail->AddAddress($email);
		$mail->AddReplyTo("tim@tweetjukebox.com");
		$mail->WordWrap = 50;                                 // set word wrap to 50 characters
		//$mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
		//$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
		//echo stripslashes($message); die("dead");
		//$mail->Body = stripslashes($_POST['message']);
		$mail->IsHTML(true);                                  // set email format to HTML
		$mail->Subject = "Subscription cancelled - from TweetJukebox.com ";
		$mail->Body = $message;
		
		
		if(!$mail->Send())
		{
		   echo "Message could not be sent. <p>";
		   echo "Mailer Error: " . $mail->ErrorInfo;
		   exit;
		}			
	}
}

function sendDM($message, $user_id, $oauth_token, $oauth_token_secret) {
	$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $oauth_token, $oauth_token_secret);
	$content = $connection->post('direct_messages/new', array('text' => $message, 'user_id' => $user_id));					
}

?>