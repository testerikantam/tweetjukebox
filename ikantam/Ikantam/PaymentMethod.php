<?php

class PaymentMethod
{
    const BRAINTREE_TYPE_SANDBOX = 'sandbox';
    const BRAINTREE_TYPE_PRODUCTION = 'production';

    public function __construct()
    {
        $this->braintreeConnect(self::BRAINTREE_TYPE_SANDBOX);
    }

    /**
     * @param string $connectionType
     */
    public function braintreeConnect($connectionType)
    {
        $braintreeConfig = [
            self::BRAINTREE_TYPE_SANDBOX => [
                'environment' => 'sandbox',
                'merchantId' => 'zsbs7swsdwdzvmwg',
                'publicKey' => 'yb7qkdry9qfvzbn9',
                'privateKey' => '36ae489f8f321abd38f0971f5f1ff2cf',
            ],
            self::BRAINTREE_TYPE_PRODUCTION => [
                'environment' => 'production',
                'merchantId' => 'dxpnw5b448vwd9rp',
                'publicKey' => 'mg2pmbnsn3g4cqnq',
                'privateKey' => 'cf12d53af0f632d8fb98c91af4b86741',
            ],
        ];

        Braintree_Configuration::environment($braintreeConfig[$connectionType]['environment']);
        Braintree_Configuration::merchantId($braintreeConfig[$connectionType]['merchantId']);
        Braintree_Configuration::publicKey($braintreeConfig[$connectionType]['publicKey']);
        Braintree_Configuration::privateKey($braintreeConfig[$connectionType]['privateKey']);
    }

    /**
     * @param string $token
     * @param string $expirationMonth
     * @param string $expirationYear
     * @return array
     */
    public function updateExpirationDate($token, $expirationMonth, $expirationYear)
    {
        try {
            $result = Braintree_PaymentMethod::update(
                $token,
                ['expirationDate' => $expirationMonth . '/' . $expirationYear]
            );
            if ($result->success) {
                return [
                    'success' => true,
                    'expirationDate' => $result->paymentMethod->expirationDate,
                    'message' => 'New date has been saved'
                ];
            } else {
                return [
                    'success' => false,
                    'error' => $result->_attributes['message'],
                ];
            }

        } catch (Exception $e) {
            return [
                'success' => false,
            ];
        }
    }

    /**
     * @param string $customerId
     * @param string $nonce
     * @param string $btPlanId
     * @param string | null $planId
     * @return array
     */
    public function createPaymentMethod($customerId, $nonce, $btPlanId, $planId = null)
    {
        try {
            $result = Braintree_PaymentMethod::create([
                'customerId' => $customerId,
                'paymentMethodNonce' => $nonce,
            ]);
            if ($result->success) {
                //check if new payment method is duplicate
                $customer = Braintree_Customer::find($customerId);
                foreach ($customer->paymentMethods as $paymentMethod) {
                    if (((
                            $paymentMethod instanceof Braintree_CreditCard
                            && $result->paymentMethod instanceof Braintree_CreditCard
                            && $paymentMethod->uniqueNumberIdentifier == $result->paymentMethod->uniqueNumberIdentifier
                        ) ||(
                            $paymentMethod instanceof Braintree_PayPalAccount
                            && $result->paymentMethod instanceof Braintree_PayPalAccount
                            && $paymentMethod->email == $result->paymentMethod->email)
                        )
                        && $paymentMethod->token != $result->paymentMethod->token
                    ) {
                        $deleteResult = Braintree_PaymentMethod::delete($result->paymentMethod->token);
                        return [
                            'success' => false,
                            'error' => 'You already have this card attached to your account.',
                        ];
                    }
                }
                //if payment method created - need to update user subscription
                if ($btPlanId) {
                    $subscriptionResult = $this->updateSubscription($btPlanId, $result->paymentMethod->token, $planId);
                }
                return [
                    'success' => true,
                    'message' => 'New payment method has been saved',
                ];
            } else {
                return [
                    'success' => false,
                    'error' => $result->_attributes['message'],
                ];
            }
        } catch (Exception $e) {
            return [
                'success' => false,
            ];
        }
    }

    /**
     * @param string $btPlanId
     * @param string $token
     * @param string | null $planId
     * @return array
     */
    public function updateSubscription($btPlanId, $token, $planId = null)
    {
        try {
            $attributes = [
                'paymentMethodToken' => $token,
            ];
            if ($planId) {
                $attributes['planId'] = $planId;
            }
            $result = Braintree_Subscription::update($btPlanId, $attributes);
            if ($result->success) {
                return [
                    'success' => true,
                    'message' => 'Subscription has been updated',
                ];
            } else {
                return [
                    'success' => false,
                    'error' => $result->_attributes['message'],
                ];
            }

        } catch (Exception $e) {
            return [
                'success' => false,
            ];
        }
    }

    /**
     * @param string $token
     * @return array
     */
    public function removePaymentMethod($token)
    {
        try {
            $paymentMethod = Braintree_PaymentMethod::find($token);
            $hasActiveSubscription = false;
            foreach ($paymentMethod->subscriptions as $subscription) {
                if ($subscription->status == Braintree_Subscription::ACTIVE) {
                    $hasActiveSubscription = true;
                }
            }
            if ($hasActiveSubscription) {
                return [
                    'success' => false,
                    'error' => 'Payment method have active subscriptions'
                ];
            }
            $result = Braintree_PaymentMethod::delete($token);
            return [
                'success' => true,
                'message' => 'Payment method has been removed',
            ];
        } catch (Exception $e) {
            return [
                'success' => false,
                'error' => $e->getMessage(),
            ];
        }
    }

    public function checkCustomerExisting($userId)
    {
        try {
            $customer = Braintree_Customer::find($userId);
            return [
                'success' => true,
            ];
        } catch (Exception $e) {
            return [
                'success' => false,
            ];
        }
    }

    public function createCustomer($userId, $firstName, $lastName, $email)
    {
        try {
            $result = Braintree_Customer::create([
                'id' => $userId,
                'firstName' => $firstName,
                'lastName' => $lastName,
                'email' => $email,
                ]
            );
            return [
                'success' => true,
            ];
        } catch (Exception $e) {
            return [
                'success' => false,
            ];
        }
    }
}