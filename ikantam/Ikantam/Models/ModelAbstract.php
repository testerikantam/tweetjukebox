<?php

require_once($_SERVER['DOCUMENT_ROOT']."/include/config.php");
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 3/31/16
 * Time: 11:34 AM
 */

class ModelAbstract
{
    protected $connection;

    protected $data;

    protected $table;

    public function __construct()
    {
        global $conn;

        if (!$this->connection) {
            $this->connection = $conn;
        }
    }

    public function getData($name = null)
    {
        $data = $this->data;

        return ($name && !empty($data[$name])) ? $data[$name] : $data;

    }

    public function getById($id)
    {
        $id = mysqli_real_escape_string($this->connection, $id);
        $sql = "SELECT * FROM ".$this->table." WHERE id='".$id."'";
        $result = mysqli_query($this->connection, $sql);

        return mysqli_fetch_assoc($result);
    }

    public function save($data)
    {
        if (empty($data['id'])) {
            $cols = '';
            $vals = '';
            foreach ($data as $k=>$v) {
                $sep = ($cols == '') ? '' : ', ';
                $cols .= $sep.$k;
                $v = (empty($v)) ? "'"."'" : "'".mysqli_real_escape_string($this->connection, $v)."'";
                $vals .= $sep.$v;
            }
            $sql = "INSERT INTO ".$this->table." (".$cols.") VALUES (".$vals.")";
        } else {
            $set = '';
            foreach ($data as $k=>$v) {
                if ($k == 'id') continue;
                $sep = ($set == '') ? '' : ', ';
                $v = (empty($v)) ? "'"."'" : "'".mysqli_real_escape_string($this->connection, $v)."'";
                $set .= $sep.$k.'='.$v;
            }
            $sql = "UPDATE ".$this->table." SET ".$set." WHERE id='".$data['id']."'";
        }

        $result = mysqli_query($this->connection, $sql);

        return (!empty($data['id'])) ? $data['id'] : mysqli_insert_id($this->connection);
    }

    public function delete($id)
    {
        $id = mysqli_real_escape_string($this->connection, $id);
        $sql = "DELETE FROM ".$this->table." WHERE id='$id'";
        $result = mysqli_query($this->connection, $sql);

        return $result;
    }
}