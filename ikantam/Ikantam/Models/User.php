<?php
require_once(__DIR__.'/ModelAbstract.php');
require_once(__DIR__.'/SocialAccount.php');
require_once(__DIR__.'/../PaymentMethod.php');
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 3/31/16
 * Time: 11:34 AM
 */
class User extends ModelAbstract
{
    protected $userId;

    protected $table = 'users';

    public function __construct($userId = null)
    {
        parent::__construct();
        if ($userId) {
            //$this->setUserId();
            $this->getByUserId($userId);
        }
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    public function getByUserId($userId = null)
    {
        if (!$userId) {
            $userId = $this->userId;
        }
        $userId = mysqli_real_escape_string($this->connection, $userId);
        $sql = "SELECT * FROM users WHERE user_id = '$userId' ";
        $result = mysqli_query($this->connection, $sql);
        if ($result) {
            $this->data = mysqli_fetch_assoc($result);
        }

        return $this;

    }

    public function hasPlan()
    {
        if (!$this->data) {
            $this->getByUserId();
        }
        //$paymentMethod = new PaymentMethod();
        //var_dump($paymentMethod->checkCustomerExisting($this->data['user_id']));die;

        return !empty($this->data['bt_plan_id']);

    }

    public function hasSocialAccount($type)
    {
        $socialAccount = new SocialAccount();

        return $socialAccount->getByUserIdAndType($this->getData('user_id'), $type);
    }
}