<?php
require_once(__DIR__ . '/ModelAbstract.php');

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 3/31/16
 * Time: 11:34 AM
 */
class SocialAccount extends ModelAbstract
{
    protected $userId;

    protected $table = 'social_accounts';

    /*public function __construct($socialAccountId = null)
    {
        parent::__construct();
        if ($socialAccountId) {
            $this->setId($socialAccountId);
            $this->data = $this->getById();
        }
    }*/

    public function getByUserIdAndType($userId, $type)
    {
        $userId = mysqli_real_escape_string($this->connection, $userId);
        $type = mysqli_real_escape_string($this->connection, $type);
        $sql = "SELECT * FROM " . $this->table . " WHERE user_id = '" . $userId . "' AND type = '" . $type . "'";
        $result = mysqli_query($this->connection, $sql);

        return mysqli_fetch_assoc($result);
    }

    public function add($data)
    {
        $this->save($data);
    }
}