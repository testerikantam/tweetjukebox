<?php
require_once(__DIR__ . '/ModelAbstract.php');

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 3/31/16
 * Time: 11:34 AM
 */
class SocialSchedule extends ModelAbstract
{
    protected $userId;

    protected $table = 'social_schedule';

    /*public function __construct($socialAccountId = null)
    {
        parent::__construct();
        if ($socialAccountId) {
            $this->setId($socialAccountId);
            $this->data = $this->getById();
        }
    }*/

    public function getByUserIdAndType($userId, $socialType)
    {
        $userId = mysqli_real_escape_string($this->connection, $userId);
        $socialType = mysqli_real_escape_string($this->connection, $socialType);
        $sql = "SELECT * FROM " . $this->table . " WHERE user_id = '$userId' AND social_type = '$socialType'";
        $result = mysqli_query($this->connection, $sql);

        return mysqli_fetch_assoc($result);
    }

    public function getSocialScheduleByJukeboxIdAndSocialType($jukeboxId, $socialType)
    {
        $jukeboxId = mysqli_real_escape_string($this->connection, $jukeboxId);
        $socialType = mysqli_real_escape_string($this->connection, $socialType);
        $sql = "SELECT * FROM " . $this->table . " WHERE jukebox_id = '$jukeboxId' AND social_type = '$socialType'";
        $result = mysqli_query($this->connection, $sql);
        if ($result) {
            $this->data = mysqli_fetch_assoc($result);
        }

        return $this;
    }

    public function getSocialSchedulesArrayByJukeboxId($jukeboxId)
    {
        $listId = mysqli_real_escape_string($this->connection, $jukeboxId);
        $sql = "SELECT * FROM " . $this->table . " WHERE jukebox_id = '$jukeboxId'";
        $result = mysqli_query($this->connection, $sql);
        $resultArray = array();
        if ($result->num_rows) {
            $rows = mysqli_fetch_array($result);
            foreach ($rows as $k=>$row){
                $resultArray[$row['social_type']] = $row;
            }
        }

        return $resultArray;
    }

    public function addScheduleByListAndSocialType($data)
    {
        $this->getSocialScheduleByJukeboxIdAndSocialType($data['jukebox_id'], $data['social_type']);
        $data['id'] = $this->getData('id');
        if (empty($data['id'])) {
            $days = array('mon', 'tue', 'wed', 'thur', 'fri', 'sat', 'sun');
            foreach($days as $day) {
                $data[$day.'_start_1'] = '00:00';
                $data[$day.'_end_1'] = '23:59';
                $data[$day.'_frequency_1'] = '60';
            }
        }
        $this->save($data);
    }

}