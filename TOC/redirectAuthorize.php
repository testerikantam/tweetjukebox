<?php // include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?php
/* Start session and load library. */
session_start();

if($_SESSION['linkedAccounts'] >= $_SESSION['accountsLimit'] ) { 
	$_SESSION['linkError'] = 'Sorry, you have reached your account limits and can not link any more accounts under your current plan.';
	header('Location: /members/');
	exit();
}

//session_destroy();
//session_start();
require_once('twitteroauth/twitteroauth.php');
require_once('config.php');
/* Build TwitterOAuth object with client credentials.  APP tokens */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET);

/* Get temporary credentials. */
if($_SESSION['ENTRY_DOMAIN'] == 'sjb') {
	$request_token = $connection->getRequestToken('https://www.socialjukebox.com/TOC/callbackAuthorize.php');  
} else {
	$request_token = $connection->getRequestToken('https://tj.local/TOC/callbackAuthorize.php');
}	
/* Save temporary credentials to session. */
$_SESSION['oauth_token'] = $token = $request_token['oauth_token'];
$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];


//print "consumer key: ". CONSUMER_KEY."<br>";
//print "consumer secret: ". CONSUMER_SECRET."<br>";


//print "<pre>";
//print_r($_SESSION);

//print "<hr>";



/* If last connection failed don't display authorization link. */
switch ($connection->http_code) {
  case 200:
    /* Build authorize URL and redirect user to Twitter. */
    $url = $connection->getAuthorizeURL($token, false);
	
//	print "request token: "; print_r($request_token);
//	print "token: ". $token."<br>"; 
//	print "url: ".$url;
//	die();	
    header('Location: ' . $url); 
    break;
  default:
    /* Show notification if something went wrong. */
    echo 'Could not connect to Twitter. Refresh the page or try again later.';
}
?>