<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?php
if($_REQUEST['test'] != 'TEST' ) {
	header("Location: /");
	exit();
}
/**
 * @file
 * User has successfully authenticated with Twitter. Access tokens saved to session and DB.
 */

require_once('twitteroauth/twitteroauth.php');
require_once('config.php');

/* If access tokens are not available redirect to connect page. */
if (empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) {
    header('Location: ./clearsessions.php');
}
/* Get user access tokens out of the session. */
$access_token = $_SESSION['access_token'];

/* Create a TwitterOauth object with consumer/user tokens. */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);

/* If method is set change API call made. Test is called by default. */
//$content = $connection->get('account/verify_credentials');

$screen_name = $access_token['screen_name'];

//$connection->post('direct_messages/new', array('screen_name' => 'TweetJukebox', 'text' => 'Thanks!'));

/* Some example calls */
//$content = $connection->get('users/show', array('screen_name' => $screen_name));
//$connection->post('statuses/update', array('status' => 'Hello World!'));
//$connection->post('statuses/destroy', array('id' => 5437877770));
//$connection->post('friendships/create', array('id' => 9436992));
//$connection->post('friendships/destroy', array('id' => 9436992));

//$content = $connection->get('statuses/user_timeline', array('screen_name' => $screen_name));

//$content = $connection->get('search/tweets', array('q' => '@alphabetsuccess',  'count' => 100));

$content = $connection->get('followers/ids', array('screen_name' => $screen_name) );


$cursor = -1;
$resultSet = array(); 
//do {
  //  $content = $connection->get('followers/list', array('screen_name' => $screen_name, 'count' => 1) );
	$content = $connection->get('application/rate_limit_status' );
    if (is_array($content) || is_object($content)) {
		$thisContent = object_to_array($content);	
	}
	
	$cursor = $thisContent['next_cursor'];
/*
	print "<pre>";
	print_r($thisContent);
	
	print "<hr>";
	print $cursor;
	die();
*/	
	array_push($resultSet, $thisContent) ;
	
//} while ( cursor != 0 ) ;

$content = $resultSet;
/*
if (is_array($content) || is_object($content)) {
		$thisContent = object_to_array($content);	
}
$max_id = $thisContent['statuses'][1]['id'] - 1;

$newContent = $connection->get('search/tweets', array('q' => '@alphabetsuccess', 'max_id' => $max_id, 'count' => 2, 'include_entities' => 1));

$content = object_to_array($content);
$newContent = object_to_array($newContent);
array_push($content, $newContent);

*/

/* Include HTML to display on the page */
include('html.php');

?>
