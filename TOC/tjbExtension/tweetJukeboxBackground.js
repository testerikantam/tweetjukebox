// Set up context menu at install time.
chrome.runtime.onInstalled.addListener(function() {  
  var contexts = ["selection","image"];
  for (var i = 0; i < contexts.length; i++) {
    var context = contexts[i];
    var title = "Tweet Jukebox This " + context ;
    var id = chrome.contextMenus.create({"title": title, "contexts":[context],
                                         "id": context});   
  }
});

chrome.tabs.onUpdated.addListener(function(a) {
    chrome.pageAction.show(a);
});

chrome.pageAction.onClicked.addListener(function(tab){
	var u=tab.url;
	var url = "http://tj.local/tjbCollector.php?u=" + encodeURIComponent(u);
  	window.open(url, "_blank", "status=no, titlebar=no, menubar=no, location=no, top=200, left=200, width=700, height=400");
});

// add click event
chrome.contextMenus.onClicked.addListener(onClickHandler);

// The onClicked callback function.
function onClickHandler(info, tab) {	
  
  if (info.menuItemId == "selection") {      
 	  var sText = info.selectionText;
	  var url = "http://tj.local/tjbCollector.php?t=" + encodeURIComponent(sText);
	  window.open(url, "_blank", "status=no, titlebar=no, menubar=no, location=no, top=200, left=200, width=700, height=400");
	  return;
  } 

  if (info.menuItemId == "image") {      
 	  var p = info.srcUrl;
	  var url = "http://tj.local/tjbCollector.php?p=" + encodeURIComponent(p) + "&t=Found%20at%20" + encodeURIComponent(tab.url);
	  window.open(url, "_blank", "status=no, titlebar=no, menubar=no, location=no, top=200, left=200, width=700, height=400");
	  return;
  } 	      
};