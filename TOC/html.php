<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>TOC</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
    <style type="text/css">
      img {border-width: 0}
      * {font-family:'Lucida Grande', sans-serif;}
    </style>
  </head>
  <body>
  <div>
      <?php if (isset($menu)) {    echo $menu; } ?>
  </div>
	<? 
		if (is_array($content) || is_object($content)) {
			$content = object_to_array($content);			
			print $content['location'];
		}	
		
	?>
	
	<?php if (isset($status_text)) { echo '<h3>'.$status_text.'</h3>';  } ?>
    <p>
  	<pre>
    <?php 
		if($content) print_r($content); 
	?>
      </pre>
    </p>
	<p>
  <pre>
        Sessions: <?php print_r($_SESSION); ?>
      </pre>
    </p>

  </body>
</html>
