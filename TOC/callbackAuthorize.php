<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?php
/**
 * @file
 * Take the user when they return from Twitter. Get access tokens.
 * Verify credentials and redirect to based on response from Twitter.
 */
require_once('twitteroauth/twitteroauth.php');
require_once('config.php');

if(isset($_REQUEST['denied'])) {
	session_start();
	session_destroy();
	header('Location: /');
	exit();
}

/* If the oauth_token is old redirect to the connect page. */
if (isset($_REQUEST['oauth_token']) && $_SESSION['oauth_token'] !== $_REQUEST['oauth_token']) {
  $_SESSION['oauth_status'] = 'oldtoken';
  header('Location: ./clearsessions.php');
}


/* Create TwitteroAuth object with app key/secret and token key/secret from default phase */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);

/* Request access tokens from twitter */
$access_token = $connection->getAccessToken($_REQUEST['oauth_verifier']);


/* this is the authorized user's tokens [oauth_token] [oauth_token_secret] [user_id] [screen_name] and [x_auth_expires]   */
// $_SESSION['access_token'] = $access_token;  normally we'd set the sessions for the account - but we are already logged in 



/* Remove no longer needed request tokens */
unset($_SESSION['oauth_token']);
unset($_SESSION['oauth_token_secret']);

/* If HTTP response is 200 continue otherwise send to connect page to retry */
if (200 == $connection->http_code) {
	/* The user has been verified and the access tokens can be saved for future use */
	// so now we check to see if this user ID is already linked up
	// let's use the user_id's to do this  we need the master ID and the new ID so let's get the master ID first
	$loggedInUser = $_SESSION['access_token']['user_id'];
	$SQL = "select master_account_id, send_thanks, thanks_limit, planName, scheduled_tweets_status  from users where user_id = '$loggedInUser' ";
	$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn)) ;
	if( mysqli_num_rows($result) == 0 ) header('Location: /oops.php');   //<=========================   master must exist get defaults here
	$row = mysqli_fetch_assoc($result); 
	$master_account_id = $row['master_account_id'];
	
	$send_thanks = $row['send_thanks'];
	$thanks_limit = $row['thanks_limit'];
	$planName = $row['planName'];
	$scheduled_tweets_status = $row['scheduled_tweets_status'];
	
	$new_id = $access_token['user_id'];
 
  	$_SESSION['linkError'] = '';
	
	
	if($master_account_id == $new_id) { // what?  you can't link yourself to yourself stop that
		$_SESSION['linkError'] = 'You can not link <b>'.$access_token['screen_name']."</b> to your master account.  It IS your master account!";
		header('Location: /members/');
		exit();
	}	

	// now let's see if the new user is "already linked" and active 
	$SQL = "select master_account_id, status from users where user_id = '$new_id' ";
	$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn)) ;
	if( mysqli_num_rows($result) == 0 ) { 
		// well the new user_id is a valid twitter user and we don't already have an account for the .... so create one!
		$password = rand(1234567, 9999999);	
		$user_id = $access_token['user_id'];
		$oauth_token = $access_token['oauth_token'];
		$oauth_token_secret = $access_token['oauth_token_secret'];
		$screen_name = mysqli_real_escape_string($conn, $access_token['screen_name']);
		$signupDate = date('Y-m-d H:i:s');
		$last_login = date('Y-m-d H:i:s');
		$grace = "+".TRIAL_PERIOD." days";	
		$expireDate = date('Y-m-d', strtotime($grace));		
		$ip_address = $_SERVER['REMOTE_ADDR'];
		
		if($_SESSION['ENTRY_DOMAIN'] == 'sjb') $isSJB = 1; else $isSJB = 0;
		
		$SQL = "insert into users set isSJB = '$isSJB', master_account_id = '$master_account_id', planName = '$planName', offer_seen = '1', last_login = '$last_login', all_tweet_status = '1', status = '1', user_id = '$user_id', screen_name = '$screen_name', email = '$email', user_last_name = '$user_last_name', user_first_name = '$user_first_name', oauth_token = '$oauth_token', oauth_token_secret = '$oauth_token_secret', password = '$password', signupDate = '$signupDate', expireDate = '$expireDate', list_count = 1, isAdmin = 0, send_thanks = '".$send_thanks."', thanks_limit = '".$thanks_limit."', last_mention_id = '1', user_timezone = 'America/New_York', ip_address = '$ip_address', scheduled_tweets_status = '$scheduled_tweets_status' ";
		$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
		$_SESSION['linkError'] = 'You have linked '.$access_token['screen_name'].' to your master account';
		header('Location: /members/');
		exit();
	} else {	
		$row = mysqli_fetch_assoc($result);
		$new_user_master_account_id = $row['master_account_id']; 
		$status = $row['status'];
		if( $new_user_master_account_id !='' && $new_user_master_account_id != $loggedInUser  && $new_user_master_account_id != $_SESSION['master_account_id'] && $status == 1 ) { // ok you can't link the same new account to two different masters
			$_SESSION['linkError'] = '<b>'.$access_token['screen_name']." </b>is linked to another user account!";
			header('Location: /members/');
			exit();
		}
		if($new_user_master_account_id != '' && ( $new_user_master_account_id == $loggedInUser || $new_user_master_account_id == $_SESSION['master_account_id']) ) { // already linked
			$_SESSION['linkError'] = 'You have already linked <b>'.$access_token['screen_name']." </b>to your master account.";
			header('Location: /members/');
			exit();
		} 
		// if we're this far - we link the new user to the master
		////                            make sure the new account fits into the "plan"
	
		$SQL = "select id from user_schedule where user_id = '$new_id' ";
		$result = mysqli_query($conn, $SQL) or die(mysqli_error());
		$boxCount = mysqli_num_rows($result);
		
		
		$SQL = "select id from tweets where user_id = '$new_id' ";
		$result = mysqli_query($conn, $SQL) or die(mysqli_error());
		$tweetCount = mysqli_num_rows($result);
		
		$SQL = "select id from scheduled_tweets where user_id = '$new_id' ";
		$result = mysqli_query($conn, $SQL) or die(mysqli_error());
		$scheduledCount = mysqli_num_rows($result);
		
		if($boxCount > $_SESSION['boxLimit'] || $tweetCount > $_SESSION['tweetLimit'] || $scheduledCount > $_SESSION['scheduledLimit'] ) {
			$_SESSION['linkError'] = 'The account <b> '.$access_token['screen_name'].'  </b>exceeds your plan limits and can not be linked at this time!';
			header('Location: /members/');
			exit();
		}
		
		if($_SESSION['ENTRY_DOMAIN'] == 'sjb') $isSJB = 1; else $isSJB = 0;
		$oauth_token = $access_token['oauth_token'];
		$oauth_token_secret = $access_token['oauth_token_secret'];
		$screen_name = mysqli_real_escape_string($conn, $access_token['screen_name']);
		
		
		$SQL = "update users set screen_name = '$screen_name',  oauth_token = '$oauth_token', oauth_token_secret = '$oauth_token_secret', isSJB = '$isSJB', status = '1', master_account_id = '".$_SESSION['master_account_id']."', planName = '". $_SESSION['planName']."-".$_SESSION['billingCycle']."' where user_id = '$new_id' ";
		//		die($SQL);
		$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn)) ;		
		$_SESSION['linkError'] = 'You have linked <b>'.$access_token['screen_name'].'</b> to your master account';
		header('Location: /members/');
		exit();
	}
} else {
  /* Save HTTP status for error dialog on connnect page.*/
  header('Location: ./clearsessions.php');
}
?>