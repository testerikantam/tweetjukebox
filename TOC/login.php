<? include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
	$oauth_token = $_SESSION['access_token']['oauth_token'];
	$oauth_token_secret = $_SESSION['access_token']['oauth_token_secret'];
	$user_id = $_SESSION['access_token']['user_id'];
	$screen_name = $_SESSION['access_token']['screen_name'];	
/*
The SESSION variable 'access_token'  looks like this:
Array
(
    [oauth_token] => 105556115-U79IKFh7IDoyq8iZRMWCEOMNW3YXnVXzP0afRQ0h
    [oauth_token_secret] => 8wroskAju2YfuSeReYwzrzIqhoV1M7GU7S4qF4mKpcZ4J
    [user_id] => 105556115
    [screen_name] => sandy6t
)
*/
	$SQL = "select * from users where user_id = '$user_id' ";
	$result = mysqli_query($conn, $SQL) or die(mysqli_error());
	if(mysqli_num_rows($result) == 1 ) { // user has already registered	
		$row=mysqli_fetch_assoc($result);
		
		if($row['isSJB'] == '1' && $_SESSION['ENTRY_DOMAIN'] != 'sjb') {
			header("Location: https://www.socialjukebox.com/oops.php?err=sjb");
			exit();			
		}
		
		if($row['status'] == '86') {
			header("Location: /oops.php?err=pending");
			exit();		
		}
		if($row['status'] == '2') {
			header("Location: /oops.php?err=cancelled");
			exit();		
		}		
		$ip_address = $_SERVER['REMOTE_ADDR'];
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip_address = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}		
		$last_login = date('Y-m-d H:i:s');
		$_SESSION['loggedInAs'] = $user_id;	
		$_SESSION['validated'] = 'validated';
		$_SESSION['screen_name'] = $screen_name;	
		$_SESSION['expireDate'] = date('m/d/Y', strtotime($row['expireDate']));
								
		if($_SESSION['ENTRY_DOMAIN'] == 'sjb') {
			$isSJB = 1;
		} else {
			$isSJB = 0;
		}
		
		$SQL = "update users set isSJB = '$isSJB', screen_name = '$screen_name',  oauth_token = '$oauth_token', oauth_token_secret = '$oauth_token_secret', status = '1', last_login = '$last_login', ip_address = '$ip_address'  where user_id = '$user_id' ";		
		$result = mysqli_query($conn, $SQL);	

		if($_SESSION['ENTRY_DOMAIN'] == 'sjb') {
			
			$SQL = "update users set master_account_id = '', status = '2', all_tweet_status = '0'  where master_account_id = '$user_id' and user_id != master_account_id and isSJB = 0 " ;
			$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
			
			
			header("Location: https://www.socialjukebox.com/members/");
			exit();
		} else {		
			header("Location: http://tj.local/members/");
			exit();
		}	
		
	} else {
		if($_SESSION['ENTRY_DOMAIN'] == 'sjb') {
			header("Location: https://www.socialjukebox.com/register.php");
			exit();
		} else {
			header("Location: https://tj.local/register.php");
			exit();
		}	
	}	
?>