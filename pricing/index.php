<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<!doctype html>
<html class="no-js" lang="en">
<head>
<link rel="shortcut icon" href="/favicon.ico?v=2">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Tweet Jukebox</title>
<meta name="description" content="Tweet Jukebox - Thinking Inside the Box">
<!-- BOOTSTRAP STYLES-->
<link href="/assets/css/bootstrap_v3.3.4.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="/assets/css/font-awesome.css" rel="stylesheet" />
<!-- MORRIS CHART STYLES-->
<link href="/assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="/assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
<link href="/members/chart.css" rel="stylesheet" type="text/css">
<!-- JQUERY SCRIPTS -->
<script src="/assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="/assets/js/bootstrap_v3.3.4.min.js"></script>
<!-- METISMENU SCRIPTS -->
<script src="/assets/js/jquery.metisMenu.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="/assets/js/custom.js"></script>
<script src="/include/js_boot.js"></script>
<link rel="stylesheet" href="/highslide-4.1.13/highslide/highslide.css" type="text/css" />
<!-- 9d83e18a764256552baf6956b777f190 -->
<script type="text/javascript" src="/highslide-4.1.13/highslide/highslide-full.packed.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/highslide-4.1.13/highslide/graphics/';
	hs.showCredits = false;
    hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = .45;
	hs.zIndexCounter = 2000;
//	hs.objectLoadTime = 'after';
//	hs.forceAjaxReload = 'true';
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64707958-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="wrapper">	    
	<nav class="navbar navbar-default navbar-cls-top navbar-collapse" role="navigation" style="margin-bottom: 0">
    <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
		<ul class="nav navbar-nav navbar-cls-top" style="cursor:pointer;">
            <? require_once($_SERVER['DOCUMENT_ROOT']."/include/mainNav.php") ; ?>
		</ul>
	</div>
</div>

<div class="container">
	<div class="text-center row">
   		<div class="col-md-12"><h1><img class="img-responsive" style="display:inline;" src="/images/tjbIcon_64.png"> Membership Plans </h1></div>
	</div>	
	<div class="row" style="margin-top:5px;">		   
		<div class="col-md-3">
			<? 
			if($_SESSION['planFit'] == 'Free' ) { 	
				echo '<div class="jumbotron" style="font-size:small; background-color:#FFFFCC; border:solid #000000 2px;">';
			} else { 
				echo '<div class="jumbotron" style="font-size:small; background-color:#F5F5F5; border:solid #000000 2px;">';
			}
			?> 
			<h3 style="margin-top:-20px; color:#0099FF;">Free</h3>
			<ul class="list-group">
				<li class="list-group-item"><strong><? echo FREE_VERSION_JB_BOX_LIMIT; ?></strong> Jukeboxes</li>
				<li class="list-group-item"><strong><? echo FREE_VERSION_JB_TWEET_LIMIT; ?></strong> Tweets</li>
				<li class="list-group-item"><strong><? echo FREE_VERSION_SCHEDULED_TWEET_LIMIT; ?></strong> Scheduled Tweets </li>
				<li class="list-group-item"><strong><? echo FREE_VERSION_ACCOUNTS_LIMIT; ?></strong> Linked Twitter&#8482; Accounts </li>
				<li class="list-group-item"><strong><? echo FREE_VERSION_THANK_YOU_TWEETS; ?></strong> Thankyou Tweets </li>
			</ul>			
			<h4><? // echo FREE_VERSION_MONTHLY_FEE; ?></h4>
			<? 			 
			if($_SESSION['planName'] == 'Free'  && $userStatus == '1'  ) { 
				echo '<div class="text-primary text-center"><b>Your Current Plan</b></div>';
			} 	
			echo '</div>';
			?>
		</div>
		
		<div class="col-md-3">
			<? 
			if($_SESSION['planFit'] == 'Advanced' ) { 	
				echo '<div class="jumbotron" style="font-size:small; background-color:#FFFFCC; border:solid #000000 2px;">';
			} else { 
				echo '<div class="jumbotron" style="font-size:small; background-color:#F5F5F5; border:solid #000000 2px;">';
			} ?>	
			<h3 style="margin-top:-20px; color:#0099FF;">Advanced</h3>
			<ul class="list-group">
				<li class="list-group-item"><strong><? echo ADVANCED_VERSION_JB_BOX_LIMIT; ?></strong> Jukeboxes</li>
				<li class="list-group-item"><strong><? echo ADVANCED_VERSION_JB_TWEET_LIMIT; ?></strong> Tweets</li>
				<li class="list-group-item"><strong><? echo ADVANCED_VERSION_SCHEDULED_TWEET_LIMIT; ?></strong> Scheduled Tweets </li>
				<li class="list-group-item"><strong><? echo ADVANCED_VERSION_ACCOUNTS_LIMIT; ?></strong> Linked Twitter&#8482; Accounts </li>
				<li class="list-group-item"><strong><? echo ADVANCED_VERSION_THANK_YOU_TWEETS; ?></strong> Thankyou Tweets </li>
			</ul>
			<h4 align="center">
				<? if($_SESSION['billingCycle'] == 'Monthly' && $userStatus == '1') { ?>
					$<? echo ADVANCED_VERSION_MONTHLY_FEE; ?> / month
				<? } elseif ($_SESSION['billingCycle'] == 'Annual' && $userStatus == '1')	{ ?>
					$<? echo ADVANCED_VERSION_ANNUAL_FEE; ?> / year
				<? } else { ?>
					$<? echo ADVANCED_VERSION_MONTHLY_FEE; ?> / month
					<br>or<br>$<? echo ADVANCED_VERSION_ANNUAL_FEE; ?> / year*<br><br>*save $<? $fullYear = (float)ADVANCED_VERSION_MONTHLY_FEE *12 ; $annual = (float)ADVANCED_VERSION_ANNUAL_FEE; echo number_format($fullYear - $annual, 2);   ?>
				<? } ?>	
			</h4>
			<? if( $_SESSION['planFit'] == 'Advanced' ) { ?>	
					<div class="text-primary text-center"><b>Perfect Match!</b></div>
			<? } ?>						
			<? if( $_SESSION['planName'] == 'Advanced'  && $userStatus == '1'  ) { ?>	
					<div class="text-primary text-center"><b>Your Current Plan</b></div>
			<? } ?>	
			</div>
		</div> 

		<div class="col-md-3">
			<? 
			if($_SESSION['planFit'] == 'Pro' ) { 
				echo '<div class="jumbotron" style="font-size:small; background-color:#FFFFCC; border:solid #000000 2px;">';
			} else { 
				echo '<div class="jumbotron" style="font-size:small; background-color:#F5F5F5; border:solid #000000 2px;">';
			} ?>	
			<h3 style="margin-top:-20px; color:#0099FF;">Pro</h3>
			<ul class="list-group">
				<li class="list-group-item"><strong><? echo PRO_VERSION_JB_BOX_LIMIT; ?></strong> Jukeboxes</li>
				<li class="list-group-item"><strong><? echo PRO_VERSION_JB_TWEET_LIMIT; ?></strong> Tweets</li>
				<li class="list-group-item"><strong><? echo PRO_VERSION_SCHEDULED_TWEET_LIMIT; ?></strong> Scheduled Tweets </li>
				<li class="list-group-item"><strong><? echo PRO_VERSION_ACCOUNTS_LIMIT; ?></strong> Linked Twitter&#8482; Accounts </li>
				<li class="list-group-item"><strong><? echo PRO_VERSION_THANK_YOU_TWEETS; ?></strong> Thankyou Tweets </li>
			</ul>
			<h4 align="center">
				<? if($_SESSION['billingCycle'] == 'Monthly' && $userStatus == '1') { ?>
					$<? echo PRO_VERSION_MONTHLY_FEE; ?> / month
				<? } elseif ($_SESSION['billingCycle'] == 'Annual' && $userStatus == '1')	{ ?>
					$<? echo PRO_VERSION_ANNUAL_FEE; ?> / year
				<? } else { ?>
					$<? echo PRO_VERSION_MONTHLY_FEE; ?> / month
					<br>or<br>$<? echo PRO_VERSION_ANNUAL_FEE; ?> / year*<br><br>*save $<? $fullYear = (float)PRO_VERSION_MONTHLY_FEE *12 ; $annual = (float)PRO_VERSION_ANNUAL_FEE; echo number_format($fullYear - $annual, 2);   ?>
				<? } ?>	
			</h4>
			<? if( $_SESSION['planFit'] == 'Pro' ) { ?>	
					<div class="text-primary text-center"><b>Perfect Match!</b></div>
			<? } ?>			
			<? if($_SESSION['planName'] == 'Pro' && $userStatus == '1'  ) { ?>	
					<div class="text-primary text-center"><b>Your Current Plan</b></div>
			<? } ?>	
			</div>
		</div> 
		
		<div class="col-md-3">
			<? if($_SESSION['planFit'] == 'Business' ) { 
				echo '<div class="jumbotron" style="font-size:small; background-color:#FFFFCC; border:solid #000000 2px;">';
			} else { 
				echo '<div class="jumbotron" style="font-size:small; background-color:#F5F5F5; border:solid #000000 2px;">';
			} ?>	
			<h3 style="margin-top:-20px; color:#0099FF;">Business</h3>
			<ul class="list-group">
				<li class="list-group-item"><strong><? echo BUSINESS_VERSION_JB_BOX_LIMIT; ?></strong> Jukeboxes</li>
				<li class="list-group-item"><strong><? echo BUSINESS_VERSION_JB_TWEET_LIMIT; ?></strong> Tweets</li>
				<li class="list-group-item"><strong><? echo BUSINESS_VERSION_SCHEDULED_TWEET_LIMIT; ?></strong> Scheduled Tweets </li>
				<li class="list-group-item"><strong><? echo BUSINESS_VERSION_ACCOUNTS_LIMIT; ?></strong> Linked Twitter&#8482; Accounts </li>
				<li class="list-group-item"><strong><? echo BUSINESS_VERSION_THANK_YOU_TWEETS; ?></strong> Thankyou Tweets </li>
			</ul>
			<h4 align="center">
				<? if($_SESSION['billingCycle'] == 'Monthly' && $userStatus == '1') { ?>
					$<? echo BUSINESS_VERSION_MONTHLY_FEE; ?> / month
				<? } elseif ($_SESSION['billingCycle'] == 'Annual' && $userStatus == '1')	{ ?>
					$<? echo BUSINESS_VERSION_ANNUAL_FEE; ?> / year
				<? } else { ?>
					$<? echo BUSINESS_VERSION_MONTHLY_FEE; ?> / month
					<br>or<br>$<? echo BUSINESS_VERSION_ANNUAL_FEE; ?> / year*<br><br>*save $<? $fullYear = (float)BUSINESS_VERSION_MONTHLY_FEE *12 ; $annual = (float)BUSINESS_VERSION_ANNUAL_FEE; echo number_format($fullYear - $annual, 2);   ?>
				<? } ?>	
			</h4>
			<? if( $_SESSION['planFit'] == 'Business' ) { ?>	
					<div class="text-primary text-center"><b>Perfect Match!</b></div>
			<? } ?>
			<? if( $_SESSION['planName'] == 'Business' && $userStatus == '1' ) { ?>	
					<div class="text-primary text-center"><b>Your Current Plan</b></div>
			<? } ?>
			</div>
		</div>
	</div>	
</div> 

<div class="container" >
	<div class="row" style="padding-top:50px;">
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<p align="center">
   			<span class="left">tj.local &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> | <!--a href="/plans.php">PLANS</a> | --><a href="/contact">CONTACT US</a> | <a href="/privacy">PRIVACY POLICY</a> | <a href="/terms">TERMS AND CONDITIONS</a>
			</p>
		</div>	
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
	</div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
<script src="/assets/js/script.js"></script>
<script>
function confirmSubscription(pid) {
	location.assign("subscribe.php?pid="+pid);
	//return hs.htmlExpand(null, {src:'subscribe.php?pid='+pid, objectType: 'iframe', width: 600 , height: 700, preserveContent: false, align: 'center' } )
}
</script>
<pre>
<? //print_r ($_SESSION); ?>
</body>
</html>