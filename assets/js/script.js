/* Author: $hekh@r d-Ziner */

(function($){
    $(document).ready(function(){

	    
	//fixing ie/older browsers
	$('.oldies .columns article:nth-child(4n)').css('border-right', '0');
	$('.oldies .thumb-list li:nth-child(4n)').css('margin-right','0');
	
	$('.jbMenu > li').bind('mouseover', openSubMenu);
		$('.jbMenu > li').bind('mouseout', closeSubMenu);
		
	function openSubMenu() {
		$(this).find('ul').css('visibility', 'visible');	
	};
	
	function closeSubMenu() {
		$(this).find('ul').css('visibility', 'hidden');	
	};

	
	}); //ready
})(jQuery);




