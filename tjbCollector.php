<?php  include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<? 

$_SESSION['extension_title'] = $_REQUEST['t'];
$_SESSION['extension_url'] = $_REQUEST['u'];
$_SESSION['extension_content'] = $_REQUEST['c'];
$_SESSION['extension_type'] = $_REQUEST['tp'];

$key = 'AIzaSyDWTkJNT9grybmoRDpvy73yNcM3xkfcHM8';
$googer = new GoogleURLAPI($key);

$response = $googer->shorten($_REQUEST['u']);
if($response) {	
	$_SESSION['extension_url'] = $response	;
}		


/*
if($_REQUEST['t'] ) {
	$_SESSION['text-collection'] = $_REQUEST['t'];
} 

if($_REQUEST['u']) {
	$key = 'AIzaSyDWTkJNT9grybmoRDpvy73yNcM3xkfcHM8';
	$googer = new GoogleURLAPI($key);

	$response = $googer->shorten($_REQUEST['u']);
	if($response) {	
		$_SESSION['text-collection'] = $response	;
	}		
}	

if($_REQUEST['p']) {
	$_SESSION['image-collection'] = $_REQUEST['p'];	
	//print '<img src="'.$_REQUEST['p'].'" />';	
}

*/


if($_SESSION['validated'] == 'validated') { 
	
	header("Location: /members/addContent.php");
	exit();
}	
?>  
<div align="center">
	<? if($_SESSION['ENTRY_DOMAIN'] == 'sjb') { ?>
		<h2>SocialJukebox</h2>
	<? } else { ?>
		<h2>TweetJukebox</h2>
	<? } ?>	
	<p align="center">
	<a href="/TOC/redirect2.php"><img src="images/sign-in-with-twitter-gray.png" alt="Sign in with Twitter" class="img-responsive" /></a>
	</p>
</div>

<?
// Declare the class
class GoogleUrlApi {
	
	// Constructor
	function GoogleURLAPI($key,$apiURL = 'https://www.googleapis.com/urlshortener/v1/url') {
		// Keep the API Url
		$this->apiURL = $apiURL.'?key='.$key;
	}
	
	// Shorten a URL
	function shorten($url) {
		// Send information along
		$response = $this->send($url); 
		// Return the result
		return isset($response['id']) ? $response['id'] : false;
	}
	
	// Expand a URL
	function expand($url) {
		// Send information along
		$response = $this->send($url,false);
		// Return the result
		return isset($response['longUrl']) ? $response['longUrl'] : false;
	}
	
	// Send information to Google
	function send($url,$shorten = true) {
		// Create cURL
		$ch = curl_init();
		// If we're shortening a URL...
		if($shorten) {
			curl_setopt($ch,CURLOPT_URL,$this->apiURL);
			curl_setopt($ch,CURLOPT_POST,1);
			curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode(array("longUrl"=>$url)));
			curl_setopt($ch,CURLOPT_HTTPHEADER,array("Content-Type: application/json"));
		}
		else {
			curl_setopt($ch,CURLOPT_URL,$this->apiURL.'&shortUrl='.$url);
		}
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		// Execute the post
		$result = curl_exec($ch);
		// Close the connection
		curl_close($ch);
		// Return the result
		return json_decode($result,true);
	}		
}

?>