<?php  include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?php 
$serverName = explode(".", $_SERVER['HTTP_HOST']);
if(strtolower($serverName[0]) != "www") header ("Location: http://tj.local/thankyou");
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Tweet Jukebox</title>
<meta name="description" content="Tweet Jukebox - Thinking Inside the Box">
<!-- BOOTSTRAP STYLES-->
<link href="../assets/css/bootstrap_v3.3.4.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="../assets/css/font-awesome.css" rel="stylesheet" />
<!-- MORRIS CHART STYLES-->
<link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="../assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

<!-- JQUERY SCRIPTS -->
<script src="../assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../assets/js/bootstrap_v3.3.4.min.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../assets/js/jquery.metisMenu.js"></script>
<script src="/include/js_boot.js"></script>
<link rel="stylesheet" href="/highslide-4.1.13/highslide/highslide.css" type="text/css" />
<!-- 9d83e18a764256552baf6956b777f190 -->
<script type="text/javascript" src="/highslide-4.1.13/highslide/highslide-full.packed.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/highslide-4.1.13/highslide/graphics/';
	hs.showCredits = false;
    hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = .45;
	hs.zIndexCounter = 2000;
//	hs.objectLoadTime = 'after';
//	hs.forceAjaxReload = 'true';
</script>
<script src="../assets/js/dropdown.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64707958-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="wrapper">	    
	<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="/">Tweet Jukebox</a> 		
	</div>
    <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
		<ul class="nav navbar-nav navbar-cls-top" style="cursor:pointer;">
            <? require_once($_SERVER['DOCUMENT_ROOT']."/include/mainNav.php") ; ?>	
		</ul>
	</div>
</div> 
<div align="center">
	<h2 style="color:#C62934;">Thank you for being an important member of my community!<br />I appreciate your engagement and conversation!</h2>
	<div class="row">
		<div class="col-md-4 col-sm-4 col-xs-4">
				<img src="/images/tjbIcon_128.png" class="img-responsive pull-right" >
		</div>
		<div class="col-md-4 col-sm-4 col-xs-4 text-center">			
			<center>
			<div id="videoContainer" class="embed-responsive embed-responsive-16by9">									
			<iframe width="560" height="315" src="https://www.youtube.com/embed/Ow5qWJzD7JA" frameborder="0" allowfullscreen>
			</iframe>
			</div>						
			<p>Btw, I use Tweet Jukebox to keep my Twitter life hands-free, and I love it! Click the link below to try it out - it's free!</p>		
<a href="/TOC/redirect.php"><img src="/images/sign-in-with-twitter-gray.png" alt="Sign in with Twitter" class="img-responsive" /></a>
			</center>								
			<!--img src="/images/tweetjukebox3.png" class="img-responsive" -->
		</div>
		<div class="col-md-4 col-sm-4 col-xs-4"></div>
	</div>
</div>
<div class="container" >
	<div class="row" style="padding-top:50px;">
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<p align="center">
   			<span class="left">tj.local &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> | <!--a href="/plans.php">PLANS</a> | --><a href="/contact">CONTACT US</a> | <a href="/privacy">PRIVACY POLICY</a> | <a href="/terms">TERMS AND CONDITIONS</a>
			</p>
		</div>	
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
	</div>
</div>	   
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
</body>
</html>