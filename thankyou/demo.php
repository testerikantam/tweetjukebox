<?php 
$serverName = explode(".", $_SERVER['HTTP_HOST']);
if(strtolower($serverName[0]) != "www") header ("Location: http://tj.local/thankyou");
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Tweet Jukebox</title>
<meta name="description" content="Tweet Jukebox - Thinking Inside the Box">
<!-- BOOTSTRAP STYLES-->
<link href="../assets/css/bootstrap_v3.3.4.css" rel="stylesheet" />
<!-- FONTAWESOME STYLES-->
<link href="../assets/css/font-awesome.css" rel="stylesheet" />
<!-- MORRIS CHART STYLES-->
<link href="../assets/js/morris/morris-0.4.3.min.css" rel="stylesheet" />
<!-- CUSTOM STYLES-->
<link href="../assets/css/custom.css" rel="stylesheet" />
<!-- GOOGLE FONTS-->
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

<!-- JQUERY SCRIPTS -->
<script src="../assets/js/jquery-1.10.2.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="../assets/js/bootstrap_v3.3.4.min.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="../assets/js/jquery.metisMenu.js"></script>
<script src="/include/js_boot.js"></script>
<link rel="stylesheet" href="/highslide-4.1.13/highslide/highslide.css" type="text/css" />
<!-- 9d83e18a764256552baf6956b777f190 -->
<script type="text/javascript" src="/highslide-4.1.13/highslide/highslide-full.packed.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '/highslide-4.1.13/highslide/graphics/';
	hs.showCredits = false;
    hs.outlineType = 'rounded-white';
	hs.fadeInOut = true;
	hs.dimmingOpacity = .45;
	hs.zIndexCounter = 2000;
//	hs.objectLoadTime = 'after';
//	hs.forceAjaxReload = 'true';
</script>
</head>
<body>
<div id="wrapper">	    
	<nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="/">Tweet Jukebox</a> 		
	</div>
    <div style="color: white; padding: 15px 50px 5px 50px; float: right; font-size: 16px;"> 
		<ul class="nav navbar-nav navbar-cls-top" style="cursor:pointer;">
            <li><a href="/"><i class="fa fa-home"></i> Home </a></li>
            <li><a href="/about"> About </a></li>
        	<li><a href="/contact"> Contact us </a></li>
			<li><a href="/faq"><i class="fa fa-info-circle"></i> FAQ </a></li>						
			<li><a href="logout.php"><i class="fa fa-sign-out"></i> Log Out</a></li>
		</ul>
	</div>
</div> 
<div class="container text-center">
	<div class="row" style="padding-top:50px;">		
        <div class="col-md-12 col-sm-12 col-xs-12">
		   <h2 style="margin-top:-10px; color:#C62934;">Thank you for being an important member of my community!<br />I appreciate your engagement and conversation!</h2>
		   <img src="../images/thanks.jpg" class="img-responsive">		   
       	   <p>Btw, I use Tweet Jukebox to keep my Twitter life hands-free, and I love it! Click the link below to try it out - it's free!</p>
		   <a href="/TOC/redirect.php"><img src="/images/sign-in-with-twitter-gray.png" alt="Sign in with Twitter" width="158" height="28" /></a></div>
	</div>	
</div> 
<div class="container" >
	<div class="row" style="padding-top:50px;">
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
		<div class="col-md-6 col-sm-6 col-xs-6">
			<p align="center">
   			<span class="left">tj.local &copy; - <? echo date("Y"); ?> | <a href="/">HOME</a> | <a href="/about">ABOUT</a> | <!--a href="/plans.php">PLANS</a> | --><a href="/contact">CONTACT US</a> | <a href="/privacy">PRIVACY POLICY</a> | <a href="/terms">TERMS AND CONDITIONS</a>
			</p>
		</div>	
		<div class="col-md-3 col-sm-3 col-xs-3"></div>
	</div>
</div>	   
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
  <script>window.jQuery || document.write('<script src="assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
  <script type="text/javascript">
setTimeout(function(){var a=document.createElement("script");
var b=document.getElementsByTagName("script")[0];
a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0029/6945.js?"+Math.floor(new Date().getTime()/3600000);
a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
</script>
</body>
</html>