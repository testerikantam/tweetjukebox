<?php include($_SERVER['DOCUMENT_ROOT']."/include/config.php"); ?>
<?
if($_POST['action']) {
		
	include($_SERVER['DOCUMENT_ROOT']."/include/mail/class.phpmailer.php");
	
	include ($_SERVER['DOCUMENT_ROOT']."/mc/Mailchimp.php"); 
	
	
	
	
	$connection = new Mailchimp();
	$list = new Mailchimp_Lists($connection);	
	$id = '769e5a2b56';	
	$mc_email = array('email'=>$_POST['email']);	
	$memberInfo =  array("NAME" => ' ');				
	$result = $list->memberInfo($id, array($mc_email));	
	
	if($result['success_count'] == '0') {
		$mc_email = array("email" => $_POST['email']);	
		$result = $list->subscribe($id, $mc_email, $memberInfo, 'html', 'false', 'false', 'true', 'false'); 
	}
	
	
	
	
	if(!isset($_SESSION['access_token'])) header('Location: /');
	$access_token = $_SESSION['access_token'];	
	$email = mysqli_real_escape_string($conn, $_POST['email']);
	
	$user_first_name = '';
	$user_last_name = '';
	
	$password = rand(1234567, 9999999);	
	$user_id = $access_token['user_id'];
	$oauth_token = $access_token['oauth_token'];
	$oauth_token_secret = $access_token['oauth_token_secret'];
	$screen_name = mysqli_real_escape_string($conn, $access_token['screen_name']);
	
	$_SESSION['email'] = $email;
	
	$signupDate = date('Y-m-d H:i:s');
	$last_login = date('Y-m-d H:i:s');
	
	
	
	$grace = "+".TRIAL_PERIOD." days";
	
	$expireDate = date('Y-m-d', strtotime($grace));
	
	if($_POST['TY_option'] == '1') { 
		$send_thanks = '1'; 
	} else { 
		$send_thanks = '0';
	}
	
	if($screen_name == '') {
		header('Location: /');
		exit();
	}
	
	
	
	$ip_address = $_SERVER['REMOTE_ADDR'];
 
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip_address = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }
	
	$newUserPlan = 'Gold';
	if( date("Y-m-d") >= '2015-11-01' ) $newUserPlan = 'Free';
	
	if($_SESSION['ENTRY_DOMAIN'] == 'sjb') $isSJB = 1; else $isSJB = 0;
	
	$SQL = "insert into users set isSJB = '$isSJB', planName = '$newUserPlan', offer_seen = '1', last_login = '$last_login', all_tweet_status = '1', status = '1', user_id = '$user_id', screen_name = '$screen_name', email = '$email', user_last_name = '$user_last_name', user_first_name = '$user_first_name', oauth_token = '$oauth_token', oauth_token_secret = '$oauth_token_secret', password = '$password', signupDate = '$signupDate', expireDate = '$expireDate', list_count = 1, isAdmin = 0, send_thanks = '".$send_thanks."', thanks_limit = 50, last_mention_id = '1', user_timezone = 'America/New_York', ip_address = '$ip_address', scheduled_tweets_status = '1' ";
	$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
	
	// create sample quote DB for new user
	
	$SQL = "insert into tweets (user_id, tweet_text, list_id) select '$user_id', quote, '1' from bonus_jukebox ";
	$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
	
	if($_POST['JB_option'] == '1') { 
		$schedule_status = '1'; 
	} else { 
		$schedule_status = '0';
	}
	
	
	$SQL = "INSERT INTO user_schedule (`user_id`, `list_id`, `schedule_status`, `mon_start_1`, `mon_end_1`, `mon_frequency_1`, `tue_start_1`, `tue_end_1`, `tue_frequency_1`, `wed_start_1`, `wed_end_1`, `wed_frequency_1`, `thur_start_1`, `thur_end_1`, `thur_frequency_1`, `fri_start_1`, `fri_end_1`, `fri_frequency_1`, `sat_start_1`, `sat_end_1`, `sat_frequency_1`, `sun_start_1`, `sun_end_1`, `sun_frequency_1`, `list_name`) 			
			VALUES ('$user_id', 1, '".$schedule_status."', '12:15', '16:00', '180', '12:15', '16:00', '180', '12:15', '16:00', '180', '12:15', '16:00', '180', '12:15', '16:00', '180', '12:15', '16:00', '180', '12:15', '16:00', '180', 'Free Quote Jukebox')";	
	$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
	
	
	
	$mail = new PHPMailer();
		$mail->IsSendmail();									// use sendmail
		$mail->From = SITE_EMAIL;
		$mail->FromName = SITE_EMAIL;
		$mail->AddAddress($email, $email);
		//$mail->AddBCC(SITE_EMAIL, SITE_EMAIL);
		//$mail->AddAddress("ellen@example.com");                  // name is optional
		$mail->AddReplyTo(SITE_EMAIL, SITE_EMAIL);
		$mail->WordWrap = 50;                                 // set word wrap to 50 characters
		//$mail->AddAttachment("/var/tmp/file.tar.gz");         // add attachments
		//$mail->AddAttachment("/tmp/image.jpg", "new.jpg");    // optional name
		$mail->IsHTML(true);                                  // set email format to HTML
		$mail->Subject = SITE_NAME." Registration Information";
		$mail->Body = "Hi there,
<p>We're delighted to welcome you to Tweet Jukebox. We're confident you'll be equally delighted by how much time you save in managing content for your Twitter account. Valuable time you can use to create and procure more awesome content, and engage with your followers.</p>
<p>To help you get started, we would encourage you to watch the following video. While it's seventeen minutes in length, it will give you a great overview of the features of the system. We're very confident you'll get a great return on time invested from watching it.</p>
<p>https://www.youtube.com/watch?v=yzeAc5CAsU4&feature=youtu.be</p>
<p>If you need us for anything please pop us a note anytime at tim@tweetjukebox.com It's our mission to make the management of your Twitter content smooth, simple and fun.</p>
<p>To ensure you get the most possible out of Tweet Jukebox, I'd encourage you to use your sample jukebox, fiddle with the schedule a bit, and add some of your own content to it. Since you won't have to schedule a tweet ever again (unless you want to) why not start building your content empire?</p>
<p>If you have any trouble getting set-up you'll find several tutorial videos on the Tweet Jukebox site to help you navigate the system. Have a question that's not there? Write us at tim@tweetjukebox.com and we'll get back to you with the answers you need.</p>
<p>Thanks again for putting your faith in Tweet Jukebox. We look forward to doing as many somersaults as needed to make you feel the decision was one of remarkable insight and wisdom. Really, that's just how we are. </p>
<p>Have an amazing day. 
<p>&nbsp;</p>
<p>Tim Fargo</p>
<p>Chief Tweetologist</p>";
		
		//$hiddenString = '<img src="http://tj.local/images/tweetJukebox_75.png" /><img src="http://tj.local/emailValidator.php?sfxr='.$user_id.'" height="0" width="0" />';
		//$mail->Body .= $hiddenString;
		

		if(!$mail->Send())
		{
		   echo "Message could not be sent. <p>";
		   echo "Mailer Error: " . $mail->ErrorInfo;
		   exit;
		}
		
		
		$SQL = "select * from users where user_id = '$user_id' and screen_name = '$screen_name'";
		$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
		if(mysqli_num_rows($result)) {
			$row = mysqli_fetch_assoc($result);		
			$last_login = date('Y-m-d H:i:s');
			$ip_address = $_SERVER['REMOTE_ADDR'];
			
			$ip_address = $_SERVER['REMOTE_ADDR'];
 
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				$ip_address = $_SERVER['HTTP_CLIENT_IP'];
			} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
			}
			
			$SQL = "update users set last_login = '$last_login', status = '1', ip_address = '$ip_address' where user_id = '$user_id' and screen_name = '$screen_name'";
			$result = mysqli_query($conn, $SQL) or die(mysqli_error($conn));
							
			$_SESSION['access_token']['oauth_token'] = $row['oauth_token'];
			$_SESSION['access_token']['oauth_token_secret'] = $row['oauth_token_secret'];
			$_SESSION['access_token']['user_id'] = $row['user_id'];
			$_SESSION['access_token']['screen_name'] = $row['screen_name'];
			$_SESSION['screen_name'] = $row['screen_name'];
			$_SESSION['validated'] = 'validated';
			$_SESSION['new_user'] = '1';
			
			header ("Location: /members/");
			exit();
		}							
}
?>
<?
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/twitteroauth/twitteroauth.php");
require_once($_SERVER['DOCUMENT_ROOT']."/TOC/config.php");
?>
<?
if(!isset($_SESSION['access_token'])) header('Location: /');

$screen_name = $_SESSION['access_token']['screen_name'];
$user_id = $_SESSION['access_token']['user_id'];
$access_token = $_SESSION['access_token'];
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);
$content = $connection->get('users/show', array('screen_name' => $screen_name));

if (is_array($content) || is_object($content)) {
	$content = object_to_array($content);
}

$_SESSION['profile_background_image_url'] = $content['profile_background_image_url'];
$_SESSION['profile_image_url'] = $content['profile_image_url'];
$_SESSION['description'] = $content['description'];
$_SESSION['statuses_count'] = $content['statuses_count'];
$_SESSION['followers_count'] = $content['followers_count'];
$_SESSION['friends_count'] = $content['friends_count'];

?>
<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8">
  <title>TweetJukebox</title>
  <meta name="HandheldFriendly" content="True" />
  <meta name="MobileOptimized" content="320" />
  <meta content="minimum-scale=1.0, width=device-width, maximum-scale=1.0, user-scalable=no" name="viewport" />
  <meta name="description" content="">
  <link  href="http://fonts.googleapis.com/css?family=Oswald:regular" rel="stylesheet" type="text/css" >
  <link href='http://fonts.googleapis.com/css?family=Junge' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="assets/fonts/raphaelicons.css">
  <link rel="stylesheet" href="assets/css/main.css">
  <link rel="shortcut icon" href="/favicon.ico?v=2" type="image/x-icon">
  <link rel="icon" href="/favicon.ico" type="image/x-icon">
  <script src="assets/js/libs/modernizr-2.5.2.min.js"></script>
<script>
function checkEmails() {
	var email_1 = $('#email').val();
	var email_2 = $('#email2').val();
			
	if(email_1.length == email_2.length) {
		if(email_1 == email_2) {
			if(validateEmail(email_1) == true ) {
				$('#email2Messages').css('color', '#00CC00');
				$('#email2Messages').html("Agree!");	
			} else {
				$('#email2Messages').css('color', '#FF0000');
				$('#email2Messages').html("Your email addresses appear to be invalid!");
			}
		} else {
			$('#email2Messages').css('color', '#FF0000');
			$('#email2Messages').html("Your email addresses do not agree!");
		}
	}
}
  
function checkPasswords() {
	var pwd_1 = $('#pwd').val();
	var pwd_2 = $('#pwd2').val();
		
	if(pwd_1.length == pwd_2.length) {
		if(pwd_1 != pwd_2) {
			$('#pwd2Messages').css('color', '#FF0000');
			$('#pwd2Messages').html("Your passwords do not agree!");
		} else {
			$('#pwd2Messages').css('color', '#00CC00');
			$('#pwd2Messages').html("Agree!");	
		}
	} else {
		$('#pwd2Messages').css('color', '#FF0000');
		$('#pwd2Messages').html("Your passwords do not agree!");
	}
}
  
function validateEmail(sEmail) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {
		$('#emailMessages').html("");
		return true;
	}
	else {
		$('#emailMessages').html("Your email appears to be invalid.");
		return false;
	}
}
 
function checkStrength(pwd) {
	//initial strength
	var strength = 0	  	;		  
	//if the password length is less than 6, return message.
	if (pwd.length < 6) {
		$('#result').css('color', '#FF0000');
		$('#result').html('Too Short');
		return true;
	}
	
	//length is ok, lets continue.
	
	//if length is 6 characters or more, increase strength value
	if (pwd.length > 5) strength += 1
	
	//if password contains both lower and uppercase characters, increase strength value
	if (pwd.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))  strength += 1;
	
	//if it has numbers and characters, increase strength value
	if (pwd.match(/([a-zA-Z])/) && pwd.match(/([0-9])/))  strength += 1;
	
	//if it has one special character, increase strength value
	if (pwd.match(/([!,%,&,@,#,$,^,*,?,_,~])/))  strength += 1;
	
	//if it has two special characters, increase strength value
	if (pwd.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,",%,&,@,#,$,^,*,?,_,~])/)) strength += 1;
	
	//now we have calculated strength value, we can return messages
	
	//if value is less than 2
	if (strength < 2 ) {			
		$('#result').css('color', '#FFCC00') ;
		$('#result').html('Weak');
		return true;
	} else if (strength == 2 ) {			
		$('#result').css('color', '#00CC00') ;
		$('#result').html('Good');
		return true;
	} else {			
		$('#result').css('color', '#2D98F3') ;
		$('#result').html('Strong');
		return true;
	}
}

	
function validateForm() {	
	var errors = '';
	var email_1 = $('#email').val();
	if(validateEmail(email_1) == false ) errors = errors + "\nYour email appears to be missing or invalid";			
	if(errors != '') {
		alert(errors);
	} else {
		ga('send', 'event', 'signup', 'click');
		$('#action').val("form posted");
		$('#register').submit();
	}	
}	
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-64707958-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<!--[if lt IE 7]> <body class="ie6 oldies"> <![endif]-->
<!--[if IE 7]>    <body class="ie7 oldies"> <![endif]-->
<!--[if IE 8]>    <body class="ie8 oldies"> <![endif]-->
<!--[if gt IE 8]><!--><body><!--<![endif]-->

<div align="center">
  <!--[if lt IE 7]><p class=chromeframe>Your browser is <em>ancient!</em> <a href="http://browsehappy.com/">Upgrade to a different browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience this site.</p><![endif]-->
</div>

<div align="center">
         
<h1 align="center"><img src="images/tweetjukebox3.png" width="600"></h1>
<h1 align="center">Welcome to TweetJukebox</h1>
<section class="container" style="padding-bottom:20px; padding-top:20px; vertical-align:text-top;">                   
 <form id="register" class="c-form" method="post" enctype="multipart/form-data" action="register.php" accept-charset="utf-8">
	 <div align="center"><span class="content"><strong>Add your email address below to get started:</strong></span><br><br>
	   <input type="text" id="email" name="email" onChange="validateEmail(this.value);"  style="display:inline; border:1px solid #000;"  >
	   <input type="hidden" id="action" name="action" value="" />
	   <!--input type="hidden" id="screen_name" name="screen_name" value="<? echo $screen_name; ?>" />
	   <input type="hidden" id="user_id" name="user_id" value="<? echo $user_id; ?>" /-->
	   <br>
	   <strong>Plug my Jukebox in</strong> 
	   <input name="JB_option" id="JB_option" type="checkbox" value="1" checked><br>
	   <strong>Say thanks to folks that mention me</strong> 
	   <input name="TY_option" id="TY_option" type="checkbox" value="1" checked><br>
	   <input name="button" type="button" class="button"  style="display:inline;" onClick="validateForm();" value="Continue" >
	 </div>
 </form>						  
</section>  
</div>
   <div align="center">
     <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
      <script>window.jQuery || document.write('<script src="assets/js/libs/jquery-1.7.1.min.js"><\/script>')</script>
      <script src="assets/js/script.js"></script> 
   </div>
</body>
</html>